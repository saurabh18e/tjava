package com.practice.ds.tree;

public class BSTTest {

    //public static void treeToListTest() {
    //    BST bst = testData();
    //    System.out.println(bst.preorder());
    //    BSTNode list1 = bst.copy().root;
    //    treeToList1(list1);
    //    ArrayList<Integer> output1 = new ArrayList<>();
    //    while (list1 != null) {
    //        assert (list1.left == null);
    //        output1.add(list1.value);
    //        list1 = list1.right;
    //    }
    //    System.out.println("tree to list with approach1: " + output1);
    //
    //    BSTNode list2 = bst.copy().root;
    //    treeToList2(list2);
    //    ArrayList<Integer> output2 = new ArrayList<>();
    //    while (list2 != null) {
    //        assert (list2.left == null);
    //        output2.add(list2.value);
    //        list2 = list2.right;
    //    }
    //    System.out.println("tree to list with approach2: " + output2);
    //
    //    BSTNode list3 = bst.copy().root;
    //    treeToList3(list3);
    //    ArrayList<Integer> output3 = new ArrayList<>();
    //    while (list3 != null) {
    //        assert (list3.left == null);
    //        output3.add(list3.value);
    //        list3 = list3.right;
    //    }
    //    System.out.println("tree to list with approach3: " + output3);
    //}
    //
    //public static void insertAndWalksTest() {
    //    BST bst = testData();
    //    System.out.println("inorder: " + bst.inorder());
    //    System.out.println("preorder: " + bst.preorder());
    //    System.out.println("postorder: " + bst.postorder());
    //    System.out.println("bfs: " + bst.bfs());
    //    System.out.println("dfs: " + bst.dfs());
    //    bst.printLevels();
    //}
    //public static void insertAndCompareTest() {
    //    BST bst1 = testData();
    //    List<Integer> values = bst1.inorder().stream().map(Integer::parseInt).collect(Collectors.toList());
    //    for (int i = 0; i <values.size(); i++) {
    //        int rand = (int)(Math.random()*values.size());
    //        Integer tmp = values.get(i);
    //        values.set(i, values.get(rand));
    //        values.set(rand, tmp);
    //    }
    //    BST bst2 = testData(values);
    //    values.remove(0);
    //    BST bst3 = testData(values);
    //    System.out.println("bst1: " + bst1.inorder());
    //    System.out.println("bst2: " + bst2.inorder());
    //    System.out.println("bst3: " + bst3.inorder());
    //    System.out.println("bst1 == bst1: " + areEqual(bst1, bst1));
    //    System.out.println("bst1 == bst1 copy: " + areEqual(bst1, bst1.copy()));
    //    System.out.println("bst1 == bst2: " + areEqual(bst1, bst2));
    //    System.out.println("bst1 == bst3: " + areEqual(bst1, bst3));
    //
    //    System.out.println("bst1 =S= bst1: " + areEqualByStructure(bst1, bst1));
    //    System.out.println("bst1 =S= bst1 copy: " + areEqualByStructure(bst1, bst1.copy()));
    //    System.out.println("bst1 =S= bst2: " + areEqualByStructure(bst1, bst2));
    //    System.out.println("bst1 =S= bst3: " + areEqualByStructure(bst1, bst3));
    //}
    //public static void findAndExistsTest() {
    //    BST bst = testData();
    //    System.out.println(bst.find(9));
    //    System.out.println(bst.find(15));
    //    System.out.println(bst.find(16));
    //
    //    System.out.println(bst.find(9).value == findRec(bst.root, 9).value);
    //    System.out.println(bst.find(15).value == findRec(bst.root, 15).value);
    //    System.out.println(findRec(bst.root, 16) == null);
    //
    //    System.out.println(bst.exists(15));
    //    System.out.println(bst.exists(16));
    //}
    //public static void sizeHeightDiameterTest() {
    //    BST bst = new BST();
    //    System.out.println("empty bst: size: " + bst.size() + ", height: " + bst.height() + ", diameter: " + bst.diameter());
    //    bst.insert(1);
    //    System.out.println("single node bst: size: " + bst.size() + ", height: " + bst.height() + ", diameter: " + bst.diameter());
    //    bst = testData();
    //    System.out.println(bst.inorder());
    //    System.out.println(bst.preorder());
    //    System.out.println(bst.postorder());
    //    bst.printLevels();
    //    System.out.println("testData bst: size: " + bst.size() + ", height: " + bst.height() + ", diameter: " + bst.diameter());
    //}
    //public static void printAsTreeTest() {
    //    BST bst = testData();
    //    bst.printLevels();
    //    bst.printAsTree();
    //}
    //
    //public static BST testData(int ... arr) {
    //    BST bst = new BST();
    //    for (int e : arr) {
    //        bst.insert(e);
    //    }
    //    return bst;
    //}
    //public static BST testData(List<Integer> list) {
    //    int[] arr = new int[list.size()];
    //    for (int i = 0; i < arr.length; i++) {
    //        arr[i] = list.get(i);
    //    }
    //    return testData(arr);
    //}
    //public static BST testData() {
    //    return testData(10, 5, 9, 2, 1, 7, 8, 11, 13, 15, 17, 12);
    //}
    //
    //public static void main(String[] args) {
    //    BST bst1 = testData();
    //    System.out.println(bst1.preorder());
    //
    //    BSTIterator.SerDe serde = new BSTIterator.SerDe();
    //
    //    String bstStr = serde.serialize(bst1.root);
    //    System.out.println(bstStr);
    //
    //    BST bst2 = new BST();
    //    bst2.root = serde.deserialize(bstStr);
    //    System.out.println(bst2.preorder());
    //}

}
