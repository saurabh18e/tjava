package com.practice.ds.graph.adjmatrix;

public class GraphTest {
    
    public static Graph testData() {
        Graph graph = new Graph(9);
        int[][] edges = {{1, 2}, {1, 4}, {2, 5}, {3, 5}, {3, 6}, {4, 2}, {5, 4}, {6, 6}, {7, 8}, {8, 7}};
        graph.addEdges(edges);
        return graph;
    }
    
    public static void dfsTest() {
        Graph graph = testData();
        // graph.print();
        State state = graph.dfs();
        graph.print(state);
        graph.printPathFromRoot(4, state);
    }
    
    public static void bfsTest() {
        Graph graph = testData();
        // graph.print();
        State state = graph.bfs();
        graph.print(state);
        graph.printPathFromRoot(5, state);
    }
    
    public static void main(String[] args) {
        bfsTest();
    }
    
}
