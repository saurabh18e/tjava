package upstart.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class DateUtil {
    
    public static Date toDate(String str)  {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = sdf.parse(str);
            return date;
        } catch (ParseException ex) {
            throw new RuntimeException("invalid date format: " + str);
        }
    }
    
    public static int dayCount(Date d1, Date d2) {
        long diffInMillies = Math.abs(d2.getTime() - d1.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return (int) diff;
    }
}
class Loan {
    public Date date;
    public float amount;
    public float roiYearly;
    public float roiDaily;
    
    public Loan(String str) {
        String[] tokens = str.split(" ");
        this.date = DateUtil.toDate(tokens[0]);
        this.amount = Float.parseFloat(tokens[1]);
        this.roiYearly = Float.parseFloat(tokens[2]);
        this.roiDaily = this.roiYearly/365;
    }
}

class Payment {
    public Date date;
    public float amount;
    
    public Payment(String payment) {
        String[] tokens = payment.split(" ");
        this.date = DateUtil.toDate(tokens[0]);
        this.amount = Float.parseFloat(tokens[1]);
    }
    
    public Payment(Date date, float amount) {
        this.date = date;
        this.amount = amount;
    }
}

class LoanCalculator {
    
    public Loan loan;
    public List<Payment> payments;
    
    public Date lastActionDate;
    public float remainingAmount;
    public float remainingInterest;
    
    public LoanCalculator(Loan loan, List<Payment> payments) {
        this.loan = loan;
        this.payments = payments;
        
        this.payments.add(new Payment(new Date(), 0.0f));
        this.lastActionDate = this.loan.date;
    }
    
    public void calculate() {
        for (Payment payment : payments) processPayment(payment);
    }
    
    public void processPayment(Payment p) {
        // take care of remainingInterest and remainingAmount;
        int days = DateUtil.dayCount(lastActionDate, p.date);
        float interestPenalty = days*(remainingAmount*loan.roiDaily/100);
        this.remainingInterest += interestPenalty;
        if (this.remainingInterest > p.amount) {
            this.remainingInterest -= p.amount;
        } else {
            float interestPaid = this.remainingInterest;
            this.remainingInterest = 0.0f;
            this.remainingAmount -= (p.amount - interestPaid);
        }
        
        // update last action date
        this.lastActionDate = p.date;
    }
}

public class Solution {
    
    public static List<String> readLines(String path) throws Exception {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            lines = br.lines().collect(Collectors.toList());
        }
        return lines;
    }
    
    public static void main(String[] args) throws Exception {
        // read input loan
        Loan loan = new Loan(readLines("loan.txt").get(0));
        
        // read payments
        List<String> paymentsStr = readLines("loan.txt");
        List<Payment> payments = new ArrayList<>();
        for (String payment : paymentsStr) {
            payments.add(new Payment(payment));
        }
        
        // call the calculator
        LoanCalculator calculator = new LoanCalculator(loan, payments);
        calculator.calculate();
        System.out.println("Pending Principal is: " + calculator.remainingAmount);
        System.out.println("Pending Interest is: " + calculator.remainingInterest);
    }
}
