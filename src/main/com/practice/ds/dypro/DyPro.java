package com.practice.ds.dypro;

public class DyPro { }

/*
  DyPro Theory
  -------------
  
  - thinking in dypro
    - approach1: iterate: add one element at a time i.e. compute for all i
    - approach2: divide and conquer: split arr[i,j] for all k, where
    - approach3: brute force: compute for all i,j
    -
  
  - how and when to apply dypro
    1. for a given optimization problem, you have to apply brute force
    2. can comeup with recursive solution, which also has optimal substructure and overlapping subproblems
    3. apply memoization to the recursive solution
       (also note that the memoization table can be collected recursively(memoization) or iteratively(tabulation).....top-down or bottom-up)
  
  - all dypro: https://www.geeksforgeeks.org/dynamic-programming/
  - video tutorial: https://people.cs.clemson.edu/~bcdean/dp_practice/
  
 */


/*
  Set1: LCS: Longest Common Subsequence
  --------------------------------------
  
  - find longest common subsequence in two given sequences
  - ref: https://www.geeksforgeeks.org/longest-common-subsequence/
  - method:
                      = 0,                              if i==0 or j==0
          lcs(i, j)   = max(lcs(i-1, j), lcs(i, j-1)),  if X[i] != Y[j]
                      = 1 + lcs(i-1, j-1)            ,  if X[i] == Y[j]
   
   - extensions:
    - space optimization
    - print the LCS
 */

/*
  Set2: LIS:Longest Increasing SubSequence
  -----------------------------------------
  
  - find logest increasing subsequence in a given sequence
  - ref: https://www.geeksforgeeks.org/longest-increasing-subsequence/
  - method:
      
      lis(i)  =  1 + max(lis(j)), for all j such that 0 < j < i and a[j] < a[i]
              =  1              , i=0
              
  - extension
    - print lis: https://www.geeksforgeeks.org/construction-of-longest-increasing-subsequence-using-dynamic-programming/
  
 */

/*
  Set3: Edit Distance
  --------------------
  
  - find min cost of converting one string to another by insert, delete or replacement of chars
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-5-edit-distance/
  - method:
                 = max(i, j), if i==0 or j==0
      ed(i, j)   = ed(i-1, j-1), if x[i] == y[j]
                 = 1 + min(ed(i-1, j), ed(i, j-1), ed(i-1, j-1))
  
 */

/*
  Set6: Min Cost Path In Matrix
  ------------------------------
  
  - find min cost to reach bottom-right cell of a matrix from upper-left cell
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-6-min-cost-path/
  - method:    mincost(i, j) = cost(i, j), if i==0 or j==0
                             = cost(m, n) + min(minCost(i-1, j), minCost(i, j-1), minCost(i-1, j-1))
  
 */


/*
  Set7: Coin Change
  -------------------
  
  - number of ways to make change of n using any of the m coins with infinite supply
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/
  - method1: dypro: change(n, m) = change(n-a[m], m) + change(n, m-1)
    method2: dypro: change(n, m) = sum(change(n-a[i], m)), for all i in [0, m-1], such that n >= a[i]
    
  - min coins to make change of n using any of the m coins with infinite supply
  - ref: https://www.geeksforgeeks.org/find-minimum-number-of-coins-that-make-a-change/
  - method1: dypro: minCoins(n, m) = min(minCoins(n-a[m], m)+1, minCoins(n, m-1))
             ref: https://www.youtube.com/watch?v=Y0ZqKpToTic
             T(mn), S(n)
    method2: dypro: minCoins(n, m) = 1 + min (minCoins(n-a[i], m)), for all i in [0, m-1], such that n >= a[i]
             T(nm), S(n)
 */

/*
  Set8: Matrix Chain Multiplication
  -----------------------------------
  
  - calculate min cost parenthesization for a given set of matrices
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-8-matrix-chain-multiplication/
  - method:
                        =  0, if i==j
      minCostMult(i, j) =  min( minCostMult(i, k) + minCostMult(k+1, j) + p(i-1)*p(k)*p(j) ), for all k in (1, j-1)
      
                        
 */

/*
  Set9: Binomial Coefficient
  ---------------------------
  
  - calculate nCk
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-9-binomial-coefficient/
         https://www.geeksforgeeks.org/space-and-time-efficient-binomial-coefficient/
  - method1: dypro
      - method1.1: nCk = (n-1)C(k-1) + (n-1)Ck, T(nk) S(nk)
      - method1.2: use pascal triangle: PT(i, j) = PT(i-1, j) + PT(i-1, j-1), T(nk) S(n)
    method2: use expanded factorial definition, T(k), S(1)
    
 */

/*
  Set10: 0-1 Knapsack
  --------------------
  
  - given n items, pick maximum value in weight W. item chosen must be picked in full.
  - ref: https://www.geeksforgeeks.org/knapsack-problem/
  - method:
      maxval(i, w)  = max(maxval(i-1, w-a[i]), maxval(i-1, w)), if w <= a[i]
                    = maxval(i-1, w)
                    = 0, i<0
 */

/*
  Set11: Egg Dropping Puzzle
  ---------------------------
  
  - general egg dropping puzzle for n eggs and k floors
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-11-egg-dropping-puzzle/
  - method:
  
 */

/*
  Set12: Longest Palindromic SubSequence
  ---------------------------------------
  
  - find longest palindromic subsequence in a given string
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-12-longest-palindromic-subsequence/
  - method1:
                  = 0, i>j
                  = 1, i==j
                  = 2 + lps(i+1, j-1), if a[i]==a[j]
      lps(i, j)   = max(lps(i+1, j), lps(i, j+1)), if a[i]!=a[j]
  
  - solve lps using lcs
  - method: lps(a) = lcs(a, rev(a))
  
  - print lps
  - ref: https://www.geeksforgeeks.org/print-longest-palindromic-subsequence/
  - method: follow the dypro table in reverse order
  
  - lps with space optimization: https://www.geeksforgeeks.org/longest-palindrome-subsequence-space/
  - method: note that we always refer only to current row or previous row
  
 */


/*
  Set13: Cutting A Rod
  ---------------------
  
  - cut rod into pieces to get maximum total value
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-13-cutting-a-rod/
  - method:
                = 0, n<=0
      maxCut(n) =  max(i)(cutval(i) + maxCut(n-i)), for all i in [1, n]
      
 */

/*
  Set14: Max Sum Increasing SubSequence
  --------------------------------------
  
  - find increasing subsequence of a given sequence whose sum is maximum
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-14-maximum-sum-increasing-subsequence/
  - method: this is similar to LIS
                    
                    =  0, i < 0
      maxSumLIS(i)  =  a[i] + max(maxSumLIS(j)) for all j in [0, i-1] such that a[j] < a[i]
  
 */

/*
  Set15: Longest Bitonic Subsequence
  -----------------------------------
  
  - find longest subsequence in an array that first increases and then decreases
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-15-longest-bitonic-subsequence/
  - method: longest bitonic subsequence = longest increasing subsequence + longest decreasing subsequence
  
 */

/*
  Set21: LIS Variations
  -----------------------
  
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-14-variations-of-lis/
  
  - building bridges across river bank
  - method: find lis of index of cities on the other bank in 0(n lg n) time
  
  - maximum sum increasing subsequence
  
  - longest chain of pair of numbers
  
  - box stacking problem
  
 */

/*
  Set22: Box Stacking Problem
  ----------------------------
  
  - given n boxes, find max height achievable by stacking them. box rotations are allowed.
    only a box of smaller length and width can be placed over another box(with higher dimensions)
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-21-box-stacking-problem/
  - method: make the size of the array 3 times and sort by base area(why?)
     
      maxHeight(i) = max(maxHeight(j) + height[i]), for all j in [0, i-1] such that l[j] < l[i] and b[j] < b[i]
                   = height[i], i==1
                   = 0, i=0
 */

/*
  Set29: Longest Common SubString
  --------------------------------
  
  - find longest common substring in two given strings
  - ref: https://www.geeksforgeeks.org/longest-common-substring/
  - method: LCSubStr(X, Y, m, n) = max(LCSuffix(X, Y, i, j)), for all i in [0, m] and j in [0, n]
            LCSuffix(X, Y, i, j) = 1 + LCSuffix(X, Y, i-1, j-1), if X[i] == Y[j]
                                 = 0, otherwise
            
 */

/*
  Set30: Dice Throw
  ------------------
  
  - calculate number of ways to get sum X using n dice numbered 1 to m
  - ref: https://www.geeksforgeeks.org/dice-throw-problem/
  - method:
      dicePerm(x, n, m) = sum(dicePerm(x-i, n-1, m)) for all i in [1, m] such that i <= x
                        = 1,   if n=1 and x<=m
                        = 0,   if n=1 and x>m
                        = 0,   if x <= 0
                        
 */

/*
  Set31: Optimal Strategy For A Game
  -----------------------------------
  
  - optimal strategy for a game
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-31-optimal-strategy-for-a-game/
  - method:
      
      opt(i, j) = max( a[i] + min(opt(i+2, j), opt(i+1, j-1)),
                       a[j] + min(opt(i, j-2), opt(i+1, j-1))), for all i,j in [0, n-1] such that i+2<=j
                = 0, i>j
                = a[i], i==j
                = max(a[i], a[j]), i+1=j
  
 */

/*
  Set32: Word Break
  -------------------
  
  - given a dictionary and a word, check if word can be broken into words from dictionary
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-32-word-break-problem/
  - method1: WB(i) = for each valid suffix, check if WB(i-lenght(valid suffix)) is true
    method2: tabulation: for each valid prefix, mark all adjacent words
    optimization: since in method2, we are goind to check all valid prefixes which are valid words, trie can be really helpful
  
  - extension: print all possible word break
  
 */

/*
  Set33: String Interleaving
  ---------------------------
  
  - print/count all possible interleaving of two given strings(assuming no common chars in the two strings)
  - ref: https://www.geeksforgeeks.org/?p=17743
  - method: count(m, n) = count(m-1, n) + count(m , n-1)
                          (string1 first char chosen) + (string2 first char chosen)
  
  - check if a given string is interleaving of two given strings or not(assuming no common chars in the two strings)
  - ref: https://www.geeksforgeeks.org/check-whether-a-given-string-is-an-interleaving-of-two-other-given-strings/
  - method: linear scan and check
  
  - check if a given string is interleaving of two given strings or not(string can have common chars)
  - ref: https://www.geeksforgeeks.org/check-whether-a-given-string-is-an-interleaving-of-two-other-given-strings-set-2/
  - method: IL(i, j) = C(i+j) is an interleaving of A(i) and B(j)
                     = IL(i-1, j) or IL(i, j-1), if C(i+j) is A(i) and C(i+j) is B(j), i and j > 0
                       IL(i-1, j), if C(i+j) is A(i), i and j>0
                       IL(i, j-1), if C(i+j) is B(j), i and j>0
                       IL(i-1, j) and C(i)=A(i), if i>0 and j=0
                       IL(i, j-1) and C(j)=A(j), if i=0 and j>0
                       true, i=0 and j=0
 */

/*
  Set35: Longest Arithmetic Progression
  --------------------------------------
  
  -
  - ref: https://www.geeksforgeeks.org/length-of-the-longest-arithmatic-progression-in-a-sorted-array/
  - method:
  
 */

/*
  Set36: Maximum Product Cut
  ---------------------------
  
  - cut n into partitions so that product of partitions is maximum
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-36-cut-a-rope-to-maximize-product/
  - method: maxProduct(n) = max(maxProduct(n-i)), for all i in [1, n]
            (use tabulation of array from left to right)
 */


/*
  Set37: Boolean Parenthesization
  --------------------------------
  
  - find total number of ways to parenthesize a given boolean expression of T and F using &, | and ^
  - ref: https://www.geeksforgeeks.org/dynamic-programming-set-37-boolean-parenthesization-problem/
  - method: find formulas for T(i, j), F(i, j) and Total(i, j) and use tabulation along diagonals i.e.g gaps
  
 */

/*
  
  Maximum Sum Subarray
  ----------------------
  
  - given an array of +ve and -ve integers, find subarray with maximum sum
  - ref: https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/
  - method1: brute force: for all i,j...find sum of subarray and keep track of max...T(n^3)
    method2: left scan + dypro: create cumulative sum array and use it to find subarray sum for all i,j...T(n^2)
    method3: super left scan: Kadane's Algorithm: current max and max so far
  
 */




/*
  
  Minimum Jumps To End
  ---------------------
  
  - minimum number of jumps to reach end of array
  - ref: https://www.geeksforgeeks.org/minimum-number-of-jumps-to-reach-end-of-a-given-array/
  - method: use dypro
  
  - minimum number of jumps to reach end of array, T(n)
  - ref: https://www.geeksforgeeks.org/minimum-number-jumps-reach-endset-2on-solution/
  
  
 */

/*
  Fibonacci Number
  -----------------
  
  - fibonacci number
  - ref: https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
  - method: dypro: f(n) = f(n-1) + f(n-2)
  
 */








/*
  REDO
  
  - single robot on an infinite line
  - ref: https://www.geeksforgeeks.org/minimum-steps-to-reach-a-destination/
  - method: sol(n, i) = i, if n+i = destination
                      = min(sol(n+i, i+1), sol(n-i, i+1)), otherwise
    (note: this is recursion, not dynamic programming, since there is no optimal substructure as i is always increasing)
  - follow: code and print the actual breakup as well
  
 */

/*
  REDO
  
  
  Paths with atmost k-turns
  --------------------------
  
  - count of paths with atmost k-turns
  - ref: https://www.geeksforgeeks.org/count-number-of-paths-with-k-turns/
  - method: paths(i, j, turns, direction)
  
 */

/*
  REDO
  
  
  Knight Walk problem
  --------------------
  
  - find probability that a knight will be inside of a chessboard after k steps from a given position
  - ref: https://www.geeksforgeeks.org/probability-knight-remain-chessboard/
  - method1: count all paths and then count valid paths
    method2: count all paths and valid paths in one go
    method3: use dypro for counting all and valid paths
    method4: use dypro for probability based calculation
  
  
  Matrix Walk Problem
  --------------------
  
  - find probability that a person will be inside of a matrix after k steps from a given position, when he can move left-right and up-down
  - ref: https://www.geeksforgeeks.org/a-matrix-probability-question/
  - method: similar to knight walk problem
  
 */

/*
  REDO
  
  Stock Buy and Sell
  -------------------
  
  - maximize profit when you can buy and sell share only once
  - ref: https://www.geeksforgeeks.org/maximum-difference-between-two-elements/
  - method: find elements with max diff, such that max element appears to right of the smaller element
            (use min-so-far or max-so-far approach)
  
  - maximize profit when you can buy and sell share twice
  - ref: https://www.geeksforgeeks.org/maximum-profit-by-buying-and-selling-a-share-at-most-twice/
  - method:
  
 */



/*
  Easy
  -----
  
  - gold mine problem
  - ref: https://www.geeksforgeeks.org/gold-mine-problem/
  - method: clear application of dypro
  
 */
