package com.practice.ds.dypro;

public class Greedy { }

/*
  
  Greedy Theory
  --------------
  
  - all greedy questions: https://www.geeksforgeeks.org/greedy-algorithms/
  
 */

/*
  Connect Ropes With Min Cost
  ----------------------------
  
  - find minimum cost of connecting ropes
  - ref: https://www.geeksforgeeks.org/connect-n-ropes-minimum-cost/
  - method: greedy choice: always connect ropes of current min lengths
  
 */