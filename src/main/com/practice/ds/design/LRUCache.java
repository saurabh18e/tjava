package com.practice.ds.design;

import com.practice.ds.list.dlist.DLL;
import com.practice.ds.list.dlist.DLLNode;
import com.practice.util.Tuple;

import java.util.HashMap;
// LRU cache: https://leetcode.com/explore/interview/card/facebook/56/design-3/311/
/*
    - approach1: use java's linked hashmap
    - approach2: implement your own linked hashmap
 */
public class LRUCache {
    
    public int capacity;
    public HashMap<Integer, Tuple<Integer, DLLNode>> map;
    public DLL list;
    
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>(capacity);
        this.list = new DLL();
    }
    
    public int get(int key) {
        Tuple<Integer, DLLNode> entry = this.map.getOrDefault(key, null);
        if (entry == null) return -1;
        this.list.moveToHead(entry.second);
        return entry.first;
    }
    
    public void put(int key, int value) {
        Tuple<Integer, DLLNode> entry = this.map.getOrDefault(key, null);
        if (entry != null) {
            map.put(key, new Tuple<>(value, entry.second));
            list.moveToHead(entry.second);
        } else {
            if (this.map.size() == capacity) {
                DLLNode nodeToRemove = this.list.tail.prev;
                this.list.remove(nodeToRemove);
                this.map.remove(nodeToRemove.val);
            }
            DLLNode node = new DLLNode(key);
            list.addToFront(node);
            map.put(key, new Tuple<>(value, node));
        }
    }
}
