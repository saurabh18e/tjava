package com.practice.ds.misc;

public class Misc {
}

/*
  Tail Call Optimization
  -----------------------
  
  - provide details of general tail call optimization
  - ref: https://www.geeksforgeeks.org/tail-call-elimination/
  - method1:
             while loop(!base condition) {
                handle non base case;
                update parameters
             }
             handlebase case;
             
    method2:
             while(true) {
                handle base case and break;
                handle non base case;
                update parameters
             }
  
  - is tail call optimization a useful thing to do in code
  - method: no, because modern day compilers take care of it
  
 */

/*
  Tower of Hanoi
  -----------------------
  
  - tower hanoi
  - ref: https://www.geeksforgeeks.org/c-program-for-tower-of-hanoi/
  - method: smart recursive thinking
  
  public static void toh(int n, char fromRod, char tmpRod, char toRod) {
    if (n==1) {
      print "move disk fromRod to toRod"
      return;
    }
    toh(n-1, fromRod, toRod, tmpRod)
    print "move nth disk from fromRod to toRod"
    toh(n-1, tmpRod, fromRod, toRod)
  }
  
  - calculate the number of disks moved in tower of hanoi(and hence it's complexity)
  - method: take few examples and prove using induction that number is (2^n - 1), hence complexity is exponential
  
 */

