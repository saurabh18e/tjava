package com.practice.ds.string;

public class Strings { }

/*
  
  String Pattern Matching Theory
  -------------------------------
  
  note:
    - m = length of pattern
    - n = length of text or number of words in text
  
  PreProcessing Pattern
  ----------------------
  
  note: best time complexity with such algorithms is 0(n), with 0(m) preprocessing
        so these algorithms are good for one time searching(single search query)
  
  - Brute force pattern matching - pre(0) T(nm)
  - Rabin Karp - pre(m) worst case: T(nm)...expected T(n)
  - Finite Automata based pattern matching - pre(m*(size of set of alphabets)) T(n)
  - KMP - pre(m) T(n)
  
  
  PreProcessing Text: String Data Structures
  --------------------------------------------
  
  note: best time complexity with such algorithms is 0(m), with 0(n) preprocessing
        so these algorithms are good when we have to search a given text again and again(multiple search queries)
  
  - Trie
    - ref: https://www.geeksforgeeks.org/trie-insert-and-search/
    - search time is T(m) and not T(n)...this is huge improvement since pattern length is generally << total text to be searched
    - specially useful when we need to find all occurrences of a pattern
    - shortcomings: huge storage cost and text should not change a lot
    - treat trie as hashmap for strings
    - complexity: insert: 0(m), search: 0(m), traversal: 0(A^M)(A=size of alphabets, M=max length of a string)
                  to improve on traversal time complexity, use linked hashmap
                    ref: http://www.techiedelight.com/memory-efficient-trie-implementation-using-map-insert-search-delete/
                  this reduces retreival complexity to 0(sum(length of all n strings)), n=number of strings in trie
  - Compressed Trie
    - ref: http://theoryofprogramming.com/2016/11/15/compressed-trie-tree/
    - trie uses exponential space, to solve space problem, compressed trie is used
  - Suffix tree(compressed trie of all suffixes of text)
    - ref: https://www.geeksforgeeks.org/pattern-searching-set-8-suffix-tree-introduction/
    - how does this work: every pattern(if present) must be a prefix of a suffix of the text
      (basically, we are doing prefix match in all suffixes at the same time, ignoring useless suffixes with each char match)
    - complexity of traversal
      a string of length n has n suffixes and each suffix leads to 1 leaf node in suffix tree, hence there will be n leaves in suffix tree
      each internal node joins atleast two internal nodes/leaf nodes
      this means there will be atmost n-1 internal nodes
      so even if we traverse all nodes of suffix tree, complexity is 0(n)
    - all occurrence complexity
      suppose a string occurs x times in another tree, that means it will have x leaves in suffix tree under that string,
      with x-1 internal nodes....leading to a complexity of 0(x) to find all occurrences + 0(m) to find root of suffix tree node for pattern
      hence overall complexity: 0(m+x) = 0(m+n) in worst case
  - Suffix Array
    - ref: https://www.geeksforgeeks.org/suffix-array-set-1-introduction/
    - any suffix tree algorithm can be implemented with suffix array with same time complexity
      preprocessing time gets reduced to linear along with saving of space
    - binary search is used to check pattern match of an input string
  
  Generalized Suffix Trees
  -------------------------
  
  note: used where multiple substrings are involved
  
  - ref: https://www.geeksforgeeks.org/generalized-suffix-tree-1/
  - to build generalized suffix tree for X and Y, build suffix tree for X#Y$ and remove extra path labels
    that has complete subtring Y$, then mark each node as XY, X, Y depending on whether they have nodes from both trees or not
  - complexity: both build and search are linear: 0(m+n)
  
 */

/*
  Find All Occurrences
  ---------------------
  
  - find all occurrences of a pattern in a string
  - method1: suffix tree....preprocess 0(n)....search (m) but uses a lot of memory
    method2: KMP....preprocess 0(m)....search (n)....S(n)
    verdict: method2 of KMP is clearly better as there is single query
  
  - find all occurrences of multiple patterns in a string
  - method1: suffix tree....preprocess 0(n)....search (m*k) but uses a lot of memory
    method2: KMP....preprocess 0(m*k)....search (n*k)....S(n)
    verdict: method1 of suffix tree is clearly better for multiple queries
    
 */

/*

  Suffix Tree Applications
  -------------------------
  
  - Substring Check
    - check if given string is a substring of another string or not
    - ref: https://www.geeksforgeeks.org/suffix-tree-application-1-substring-check/
           https://www.geeksforgeeks.org/pattern-searching-using-trie-suffixes/
    - method: traverse suffix tree, if we exhaust input string before reaching a leaf node, then the substring exist
              complexity: 0(m)
              
  - Find all Occurrences
    - find all occurrences of a given pattern in a string
    - ref: https://www.geeksforgeeks.org/suffix-tree-application-2-searching-all-patterns/
    - method: traverse suffix tree, if we exhaust input string before reaching a leaf node, then the substring exist for all paths to leaf
              (leaf node will actually give the index of the suffix in the input string)
              complexity: 0(m+z), z=number of occurrences
  
  - Longest Repeated SubString
    - find max length subtring in a string that is repeated
    - ref: https://www.geeksforgeeks.org/suffix-tree-application-3-longest-repeated-substring/
    - method: find deepest internal node in the suffix tree(with max sum of edge lengths)
  
  - Build Suffix Array in linear time
    - build suffix array in linear time
    - ref: https://www.geeksforgeeks.org/suffix-tree-application-4-build-linear-time-suffix-array/
    - method: build suffix tree and traverse all root to leaf paths in it in dfs form, in lexographic order of edges from root, T(n)
  
  
  Generalized Suffix Tree Applications
  -------------------------------------
  
  - Longest Common Substring
    - find longest commong substring in two given strings
    - ref: https://www.geeksforgeeks.org/suffix-tree-application-5-longest-common-substring-2/
    - method: build generalized suffix tree and find deepest XY internal node with max sum of edges
  
  - Longest Palindromic Substring
    - find longest subtring in a string which is also a palindrome
    - ref: https://www.geeksforgeeks.org/suffix-tree-application-6-longest-palindromic-substring/
    - method: build generalized suffix tree of S and reverse of S. find longest common substring
  
 */

/*
  
  Trie Applications
  ------------------
  
  - T9 dictionary for old mobile phones i.e. pressing digits should give corresponding matching words
  - ref: https://stackoverflow.com/questions/2574016/data-structure-behind-t9-type-of-dictionary
         https://www.careercup.com/question?id=5783522430156800
  - method: use trie, where edges are digits from 0-9 and nodes stores words ending with that edge char
  
  
  - phone directory for mobile phones
  - ref: https://www.geeksforgeeks.org/implement-a-phone-directory/
  - method: use trie
  
  
  
  Trie Based Interview Questions
  -------------------------------
  
  - print all strings present in a trie
  - ref: https://stackoverflow.com/questions/13685687/how-to-print-all-words-in-a-trie
  - method: top down recursion
  
  - print all the words which are not present in two given sentences
  - ref: https://practice.geeksforgeeks.org/problems/print-the-non-repeating-words
  - method: print (A - B) and (B - A) i.e. A - (A intersect B) and B - (A intersect B)
            1. find intersection I using hashmap/trie. put all words of A in hashset and output words of B which are in hashset.
            2. print all words of A and B which are not in hashset
  
  - print all the substrings of a given string present in given dictionary
  - method1: use hashset as dictionary and lookup for each possible substring of pattern, T(n^3)
    method2: build a trie out of dictionary and lookup in dictionary for all suffixes of the pattern string, T(n^2)
  
  - given 10 files, suggest data structure to efficiently search string in the files.
    if string appears more than once, then print line number and the filename in which it appears.
  - method: use trie
  
  - top 10 3-page sequence of website pages visited by users at a given time
    given a website having several web-pages and many user who are accessing the web-site.
    user 1 has access pattern : x->y->z->a->b->c->d->e->f     user 2 has access pattern : z->a->b->c->d
    user 3 has access pattern : y->z->a->b->c->d              user 4 has access pattern : a->b->c->d
    determine the top 10 most occurring k page sequence
    for the above example result will be : (k=3) a->b->c , b->c->d , z->a->b.
  - ref: https://www.geeksforgeeks.org/amazon-interview-experience-set-284-on-campus/
  - method: use trie of 3 page sequences along with a heap
  
  - given a file having many lines(one word per line) and a dictionary with an API function boolean isValid(String word),
    for each line, find all possible substring of words in the dictionary(from L to R and R to L),
    e.g. say first line in file is name, then possible valid words are NAME, AM(L to R), ME(L to R), MAN(R to L), AN(R to L).
  - method: own sol: since we need to check all subtrings, store dictionary as trie and
                     look for each suffix of the given word and it's reverse
  
 */

/*
  
  Longest Palindromic Substring
  ------------------------------
  
  note: this is a classic problem with many approaches, hence needs special attention
  
  - find longest subtring in a string which is also a palindrome
  - ref: https://www.geeksforgeeks.org/suffix-tree-application-6-longest-palindromic-substring/
  - method1: brute force, for each substring, check if it's a palindrome or not, T(n^3) S(1)
    method2: dypro: for each (i, j), check if it's a palindrome or not, T(n^2) S(n^2)
    method3: dypro: for each i, find even and odd length palindromes with i as centre
             - ref: https://www.geeksforgeeks.org/longest-palindromic-substring-set-2/, T(n^2), S(1)
    method3: build generalized suffix tree of S and reverse of S. find longest common substring, T(n) S(n)
    method5: manacher's algorithm T(n) S(1)
  
  - count of all palindromes in a string
  - ref: https://www.geeksforgeeks.org/count-palindrome-sub-strings-string/
         https://www.geeksforgeeks.org/count-palindrome-sub-strings-string-set-2/
  - method: all methods for finding longest palindromic substring are applicable here
  
 */

/*
  
  Anagrams
  ---------
  
  - group list of words by anagrams
    - list is fairly small
    - list if very huge e.g. words from huge file/dictionary
  - ref: https://www.geeksforgeeks.org/given-a-sequence-of-words-print-all-anagrams-together/
         https://www.geeksforgeeks.org/given-a-sequence-of-words-print-all-anagrams-together-set-2/
  - method:
    - list is fairly small
      - sort words by anagram as signature, T(MN lg MN)
      - put words in hashmap by anagram signature....but this could show bad complexity if hashing is not uniform
        worst case complexity of this solution is 0(m*n), n=number of words, m=average length of words
        but expected complexity is 0(1)
    - list is very huge: use trie of anagram signature. since set of words is huge, simple hashmap would be inefficient
    
  - find all anagram substring occurrences, constraint: T(n)
    (find all occurrences of all permutations of a pattern in a string, constraint: T(n))
  - ref: https://www.geeksforgeeks.org/anagram-substring-search-search-permutations/
  - method1: modified rabin karp to check for anagrams, but this will be 0(mn) in worst case
    method2: trie of anagrams
    method3: use two count arrays
  
  - given a word, find it's anagram in a list of words
  - method1: trie of anagrams...but this is useless since we are not going to run multiple queries on the trie
    method2: compare signature of word with signature of each word in the list
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  Handling URLs
  --------------
  
  - what's the best way to store and search billions of urls
  - ref: https://www.quora.com/What-is-the-best-way-to-store-search-through-1-million-URLs
  - method: use trie
  
  - given a stream of urls, find first non-repeating url at any point in stream
  - ref: https://www.careercup.com/question?id=11856466
  - method: use trie
  
  - efficient way to maintain url to ip address mapping
  - ref: https://www.geeksforgeeks.org/amazon-interview-experience-set-258-for-sde1/
  - method: use trie
  
 */

/*
  
  First/Kth Non-Repeated Char
  ----------------------------
  
  - find first non-repeated char in a string, constraint: T(single pass)
  - ref: https://www.geeksforgeeks.org/given-a-string-find-its-first-non-repeating-character/
  - method1: create count array for chars of string. in 2nd pass, find first char for which count is 1
    method2: create count array and a doubly linked list. on first occurrence, add in linked list while on 2nd, remove it
    method3: create count array in first pass. then iterate count array to look for least index with count=1
  
  - find kth non-repeating char in a string, constraint: T(single pass)
  - ref: https://www.geeksforgeeks.org/kth-non-repeating-character/
  - method: create count array with indexes, find kth order static in index array(or simply sort it and find kth element for simplicity)
  
  - find first non-repeated char in an infinite stream of chars, constraint T(1)
  - ref: https://www.geeksforgeeks.org/find-first-non-repeating-character-stream-characters/
  - method: this is similar to finding first non-repeating char in array
  
 */

/*
  
  Top K Words
  ------------
  
  - find top k most frequent words in a file
  - ref: https://www.geeksforgeeks.org/find-the-k-most-frequent-words-from-a-file/
  - method1: using hashmap
    method2: using hashmap and min heap...but:
              - hashmap performance is dependent on load factor
              - hashmap searching for strings in not efficient since we need to calculate hashcode again and again
              - also in case of collisions, there would be a lot of string matchings
                which is highly inefficient when we are adding words one by one
    method3: using trie inter-linked with min heap
    
 */

/*

  Rotation Check
  ---------------
  
  - given two words, check if second word is rotation of first word or not
  - ref: https://www.geeksforgeeks.org/a-program-to-check-if-strings-are-rotations-of-each-other/
  - method: (x1.length == x2.length) && x1 is substring of x2+x2
  
  Check if AB is C
  -----------------
  -
  
  Replace all occurrences of AB with C
  --------------------------------------
  
  - given a string, replace all occurrences of ab with c, constraints: T(single traversal)
  - ref: https://www.geeksforgeeks.org/replace-occurrences-string-ab-c-without-using-extra-space/
  - method: smart iterating through the array
  
 */

/*
  String Sorting
  ---------------
  
  - which sorting algorithm will you use for sorting chars of a string
  - method: counting sort
  
  - what sorting algorithm would you use for sorting a set of strings
  - ref: https://stackoverflow.com/questions/6972635/efficient-string-sorting-algorithm
  - method1: bucket sort, radix sort
    method2: build a trie
  
  - what sorting algorithm would you use for sorting a set of strings, given that memory is not enough to hold all strings
  - ref: https://www.careercup.com/question?id=20703665
  - method: external merge sort
  
 */

/*
  
  Endianness and UTF
  -------------------
  
  - find previous char in a stream of chars of 1-byte or 2-byte chars. 1-byte char start with 0 while 2 byte char starts with 1
  - ref: https://www.careercup.com/question?id=2145
  - method: look at previous bytes: only tricky case is series with pattern: (...1 1 1 0)
            we need too look back until we hit a 0 at start(0 1 1 ...1 1 1 0) and count the number of 1's in between the two zeros.
            if the number of ones is even it's single byte, else double byte.
  
  
  - write code that will convert big endian 4 byte int to little endian and vice-versa
  - ref: https://stackoverflow.com/questions/11799391/how-do-i-convert-a-signed-decimal-value-to-a-32-bit-little-endian-binary-string
  - method:
          public static int flipEndianess(int i) {
            return (i << 24)           | // shift byte 0 to byte 3
                   (i >>> 24)          | // shift byte 3 to byte 0
                   ((i & 0xFF00) << 8);  // shift byte 1 to byte 2
                   ((i >> 8) & 0xFF00) | // shift byte 2 to byte 1
          }
  
  
 */

/*
  
  Big Data String Problems
  -------------------------
  
  - given 2 files, find common words. both files are too large and neither of them can be loaded in memory.
  - ref: https://careercup.com/question?id=19802662
  - method1: use mapreduce if in a distributed system like hadoop
    method2: if on single machine, use mapreduce like algorithm
             divide each file into n(1000) buckets of lines from the file, based on md5 hashsum of the line.
             each bucket now has lines of pairs: <md5hash, line>. sort each bucket.
             do the same for file. not do iteratie traversal of corresponding buckets and find common lines
             (we can skip sorting each bucket and rather load corresponding buckets in memory and bucket them further,
              this will further reduce cost)
             caveat: if a bucket has too many lines, it will cause OOM error duing sorting.
                     so we need to use external merge sort to sort each bucket.
  
  
  
 */

/*
  
  Easy
  -----
  
  - reverse string without use of temp variable
  - method: use XOR
  
  
  - what data structure would you use to store huge dictionary of words
  - method: trie
  
  
  - fill row-wise, print column-wise
    given a string s and int r being number of columns in a matrix,
    fill matrix row wise with chars from string and then print matrix column wise.
    for e.g. String s = “abcdefgh” and r = 3
    so filling column wise would give : [[abc], [def], [gh]] and final output would be adgbehcf
    constraint: T(n) S(1)
  - method: just iterate over the string again and again with hops of size r
  
  
  - find min distance between 2 given chars in a string
  - method: this is same as finding min distance between 2 numbers in an array
  
  
  - count number of substrings with same first and last chars
  - ref: https://www.geeksforgeeks.org/count-substrings-with-same-first-and-last-characters/
  - method: create count array. total count = sum of ch in [a, z] ((count of ch) + 1)C2
  
  - reverse sentence given as a string
  - ref: https://www.geeksforgeeks.org/reverse-words-in-a-given-string/
  - method: reverse individual words, then reverse whole sentence
            code in java: https://stackoverflow.com/questions/2713655/reverse-a-given-sentence-in-java
  
  - find chars common in all given n strings
  - ref: https://www.geeksforgeeks.org/common-characters-n-strings/
  - method: use two boolean hash arrays of size 256: current intersection and new intersection
            for each string, output in new intersection array if present in current intersection. exchange new and current intersection arrays.
            (basically prepare boolean hash array for each string and do and of all the arrays)

  - given a string and a pattern state whether it follows the pattern generated from chars a, b and c,
    where a, b anc c can represent any of the strings in {red, green, blue}
    e.g. s = “redblueredgreen”, matches pattern abac but not aaab
    string s only contains words “red”, “green” & “blue” and pattern can have any character.
  - method: own sol: normalize the given pattern into abc.
                     also normalize input pattern so that it starts from a followed by b and then c. compare the two patterns.
  
  - given an array of alphabets and a dictionary of english words, find longest dictionary word consisting of given alphabets
  - ref: https://www.careercup.com/question?id=16148684
  - method: use count array for each word
  
 */
















