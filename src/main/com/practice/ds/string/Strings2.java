package com.practice.ds.string;

public class Strings2 {
}

/*
  String Problem Techniques
  --------------------------
  
  - brute force, for all i,j
  - left scan: for all i....find j in (i+1, n)
  - smart left scan: for all i...find j but use the information collected till i-1 (use some data structure like count array/hash etc.)
  - super left scan: where one suffix can be discared and continuation happens with next suffix
  - string matching
  - string based data structures
  - dypro
  
 */

/*
  Longest Substring Without Repeating Char
  -----------------------------------------
  
  - for a given string, find longest substring without any char repetition in it, constraint: T(n)
  - ref: https://www.geeksforgeeks.org/length-of-the-longest-substring-without-repeating-characters/
  - method1: brute force: for each substring, check repetition, T(n^3)
    method2: left to right scan: for each index, find longest substring starting from it which does have repeating chars, T(n^2)
    method3: smart single left to right scan: keep hash of seen chars and extend or move to next index, T(n)
  
 */

/*
  Longest Substring With K Unique Chars
  --------------------------------------
  
  - given a string, find longest substring in it that consists of k unique chars
  - ref: https://www.geeksforgeeks.org/find-the-longest-substring-with-k-unique-characters-in-a-given-string/
  - method: super left scan: use hashset of unique chars
  
 */

/*
  Smallest Window With All Chars
  -------------------------------
  
  - find the smallest window in a string containing all characters of another string(where 2nd string has only unique chars)
  - method: own sol: smart left scan: keep hashtable to record last seen position of each char and scan from lft to right
  
  - find smallest window in a string containing all distinct chars in the string
  - ref: https://www.geeksforgeeks.org/smallest-window-contains-characters-string/
  - method: find distict chars in the string and then find smallest window containing distinct chars
  
  
 */

/*
  Make All Consecutive Chars Different
  -------------------------------------
  
  - rearrange characters in a string so that no character repeats twice
  - ref: https://www.careercup.com/question?id=5693863291256832
  - method1: own sol: create count array. place highest occurring chars next to each other
                      create gap between these chars using next highest repeating char(s)
                      repeat above process
    method2: easy to implement: create count array. start filling even positions from chars with highest occurrence,
                                then 2nd highest, 3rd highest and so no. wrap around to fill odd positions
  
 */

/*
  FAQ: Word Break Problem
  ------------------------
  
  - given a dictionary of words, check if a given string can be made from the words in the dictionary.
    e.g. sungsamsungsam can be made from dictionary: (sam, samsung, sung, ...)
  - ref: https://www.geeksforgeeks.org/word-break-problem-trie-solution/
  - method1: use dypro: https://www.geeksforgeeks.org/dynamic-programming-set-32-word-break-problem/
    method2: use dypro. also use trie to store the dictionary: https://www.geeksforgeeks.org/word-break-problem-trie-solution/
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */

/*
  
  
  -
  - ref:
  - method:
  
 */
