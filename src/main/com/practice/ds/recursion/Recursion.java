package com.practice.ds.recursion;

/*
    - recursion: top down(preorder dfs)
        - use info from current node and divide the problem into smaller ones and solve as child problems(recursively)
    - recursion: bottom up(postorder dfs)
        - suitable when we want all child combinations(avoid recomputation) and then add combinations of current node
        - example: all phone number combinations of given length
    - recursion: top down backtracking: same as top down but we:
        - track solution constructed on each path
        - check and add to output on leaf node
        - go back one step and then check next child node
        - examples: sets, permutation, combination,
    - dynamic programming
        - bottom up recursion, where solution for current node depends on solution of child problem
        - where memoization can be used to avoid recalculation of same child problem
    - greedy
        - top down recursion, where we use current node and create limited child nodes
        - examples: huffman coding
 */
public class Recursion {

}
