package com.practice.ds.recursion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TopDown {

    // regex matcher: https://leetcode.com/explore/interview/card/facebook/53/recursion-3/307/
    public boolean isMatch(String s, String p) {
        s = (s == null) ? "" : s;
        p = (p == null) ? "" : p;
        return isMatch(s.toCharArray(), p.toCharArray(), 0, 0);
    }
    
    public boolean isMatch(char[] s, char[] p, int i, int j) {
        if (j == p.length) return (i == s.length);
        
        if (i==s.length) {
            if (j == p.length-1) return false;
            if (p[j+1] != '*') return false;
            return isMatch(s, p, i, j+2);
        } else {
            char s1 = s[i];
            char p1 = p[j];
            if (j < p.length-1 && p[j+1] == '*') {
                if (s1 == p1 || p1 == '.') {
                    if (isMatch(s, p, i, j+2)) return true;
                    if (isMatch(s, p, i+1, j+2)) return true;
                    return isMatch(s, p, i+1, j);
                } else {
                    return isMatch(s, p, i, j+2);
                }
            } else {
                return (s1 == p1 || p1 == '.') ? isMatch(s, p, i + 1, j + 1) : false;
            }
        }
    }
    
    // invalid parentheses: https://leetcode.com/explore/interview/card/facebook/53/recursion-3/324/
//    public List<String> removeInvalidParentheses(String s) {
//        // calculate max left and right deletions
//        // then delete recursively using: left, right and current
//        int balance = 0, l=0, r=0;
//        for (char ch : s.toCharArray()) {
//            if (ch == '(') {
//                balance++;
//            } else {
//                balance--;
//                if (balance < 0) {
//                    r++;
//                    balance = 0;
//                }
//            }
//        }
//        l = Math.max(balance, 0);
//
//        List<String> output = new ArrayList<String>();
//        return removeInvalidParentheses(s.toCharArray(), 0, l, r,
//                new char[s.length()], 0, 0, 0, output);
//        return output;
//    }
//    private List<String> removeInvalidParentheses(char[] str, int i, int lExtra, int rExtra,
//                                                  char[] curr, int j, int lSeen, int rSeen,
//                                                  List<String> output) {
//
//    }
}
