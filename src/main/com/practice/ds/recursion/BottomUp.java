package com.practice.ds.recursion;

import java.util.ArrayList;
import java.util.HashMap;

public class BottomUp {
    
    // letter combination of a phone number: https://leetcode.com/explore/interview/card/facebook/53/recursion-3/267/
    /*
        - approach1: bottom up
            - this will require 0(3^n) space....but this is coded below
        - approach2: backtracking
            - this approach is better and requires only 0(n) space
     */
    public static HashMap<String, String> map = new HashMap<String, String>() {{
        put("2", "abc");
        put("3", "def");
        put("4", "ghi");
        put("5", "jkl");
        put("6", "mno");
        put("7", "pqrs");
        put("8", "tuv");
        put("9", "wxyz");
    }};
    public static ArrayList<String> letterCombinations(String digits) {
        ArrayList<String> output = new ArrayList<String>();
        if (digits == null || digits.isEmpty()) return output;
        
        ArrayList<String> subCombinations = letterCombinations(digits.substring(1));
        if (subCombinations.isEmpty()) {
            subCombinations.add("");
        }
        String letters = map.get(digits.substring(0, 1));
        for (char ch : letters.toCharArray()) {
            for (String subCombination : subCombinations) {
                output.add(ch + subCombination);
            }
        }
        return output;
    }
    
}
