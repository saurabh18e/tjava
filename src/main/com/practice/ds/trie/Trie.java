package com.practice.ds.trie;

// implement t9 dictionary: https://leetcode.com/explore/interview/card/facebook/56/design-3/300/

import java.util.HashMap;
import java.util.Map;
public class Trie {
    public TrieNode root;
    
    public Trie() {
        this.root = new TrieNode();
    }
    
    public void add(String str) { this.root.add(str); }
    public boolean search(String word) {
        return this.root.search(word);
    }
}

class TrieNode {
    
    public Map<Character, TrieNode> children;
    boolean isWord;
    
    public TrieNode() {
        this.children = new HashMap<>();
    }
    
    public void add(String str) {
        TrieNode curr = this;
        for (char ch : str.toCharArray()) {
            if (!curr.children.containsKey(ch)) curr.children.put(ch, new TrieNode());
            curr = curr.children.get(ch);
        }
        curr.isWord = true;
    }
    
    public boolean search(String word) {
        return search(word, this);
    }
    public boolean search(String word, TrieNode node) {
        TrieNode curr = node;
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            if (ch == '.') {
                // search all children
                for (TrieNode child : curr.children.values()) {
                    if (search(word.substring(i+1), child)) return true;
                }
                return false;
            } else {
                curr = curr.children.getOrDefault(ch, null);
                if (curr == null) return false;
            }
        }
        return curr.isWord;
    }
}

class WordDictionary {
    
    public TrieNode root;
    public WordDictionary() {
        this.root = new TrieNode();
    }

    public void addWord(String word) {
        this.root.add(word);
    }

    public boolean search(String word) {
        return this.root.search(word);
    }
}
