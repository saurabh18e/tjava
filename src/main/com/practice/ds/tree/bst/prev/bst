
/*
    write code for
        - tree insert, search and delete (recursive and iterative)
        - walks: inorder, preorder, postorder
        - min, max, predessor and successor
 */
/*
  - implement basic tree or bst operations as dictionary data structure: insert, search, delete
  - ref: https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
         https://www.geeksforgeeks.org/binary-search-tree-set-2-delete/

  - write code to delete complete tree
 */

/*
  Tree Traversals
  ----------------

  - ref: https://www.geeksforgeeks.org/bfs-vs-dfs-binary-tree/
  - unlike linear data structures (Array, Linked List.java, Queues, Stacks, etc)
    which have only one logical way to traverse them, trees can be traversed in different ways


  DFS vs BFS
  -----------
  - time complexity: T(n)
  - space complexity
    - DFS: T(height of tree), expected = T(lg n), worst case = T(n) i.e. when tree is skewed
    - BFS: T(width of tree), expected = T(n), worst case = T(n) i.e. when tree is perfect binary tree
  - which one to use when
    - BFS: when searching for nodes near root
    - DFS: when searching for nodes near leaves



  DFS
  ----
  - implement various DFS traversals of a tree(recursive as well as iterative). also state their uses
  - ref: https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
  - method
    - inorder: ascending order sorted printing of bst
    - reverse indorder: descending order printing of bst
    - preorder: polish notation
    - postorder: reverse polish notation, tree deletion



  BFS(level order)
  ----------------
  - implement level order traversal of a tree
  - ref: https://www.geeksforgeeks.org/level-order-tree-traversal/

  - print level n of a given tree, with and without recursion
  - ref: https://www.geeksforgeeks.org/level-order-tree-traversal/
  - method:
    - iterative: use Queue
    - recursive: use DFS and keep track of given level

  BFS variations
  ---------------

  - tips:
    - 1. if we need to print in reverse order, we need to push elements on stack
    - 2. if we need to zig-zag, we need to use two stacks else we need a single queue(straight or reverse)

  - zig zag traversal
  - ref: https://www.geeksforgeeks.org/zigzag-tree-traversal/
  - method1: use two stacks
    method2: use single queue
      method2.1: use flag
      method2.2: process two levels at a time

  - filling next pointer in tree but in zig-zag order
  - method: this is same as doing zig-zag traversal and maintaining bfs queue using next pointers

  - reverse level order traversal
  - ref: https://www.geeksforgeeks.org/reverse-level-order-traversal/
  - method1: stack of queues: use bottom up recursion
    method2: stack of elements: create each level in reverse order as a queue so that it gets pushed to stack in right order

  - reverse zig-zag level order traversal
  - method: use two stacks and push elements on stack

  - reverse nodes at alternate levels of a tree
  - ref: https://www.geeksforgeeks.org/reverse-alternate-levels-binary-tree/
  - method1: use level order traversal
    method2: use two stack approach

  - reverse nodes at alternate levels of a perfect binary tree
  - ref: https://www.geeksforgeeks.org/reverse-alternate-levels-binary-tree/
  - method1: divide and use preorder traversal, keeping track of level of the tree

  Boundary traversal
  -------------------

  - boundary traversal of a COMPLETE tree ANTICLOCKWISE(assumption: view tree as a clean wide tree i.e. no criss-crossing of nodes)
  - ref: https://www.geeksforgeeks.org/boundary-traversal-of-binary-tree/
         https://articles.leetcode.com/print-edge-nodes-boundary-of-binary/
  - method1: print left boundary + print leaves + print right boundary
    method2: recursion with single traversal: https://stackoverflow.com/questions/30275735/to-print-the-boundary-of-binary-tree

  - boundary traversal of a COMPLETE tree(any order)
  - ref: https://www.careercup.com/question?id=17245689
  - method: recursion with single traversal: https://www.careercup.com/question?id=17245689

  - boundary traversal of a tree
  - method: use level order and find left and right nodes

  Vertical traversal
  -------------------

  - vertical printing of a tree
  - ref: https://www.geeksforgeeks.org/print-binary-tree-vertical-order/
         https://www.geeksforgeeks.org/print-binary-tree-vertical-order-set-2/
         https://www.geeksforgeeks.org/print-a-binary-tree-in-vertical-order-set-3-using-level-order-traversal/
  - method1: naive: find min-max levels, for each level i, print all nodes in level i
    method2: use hashmap + preorder
    method3: use hashmap + level order traversal

  - vertical sum of a tree
  - ref: https://www.geeksforgeeks.org/vertical-sum-in-a-given-binary-tree/
  - method: use hashmap + preorder to keep track of linked list

  - for ech node in a tree, set it's next pointer to vertically next node
  - ref: https://www.geeksforgeeks.org/wp-content/uploads/tree1.jpg
  - method: use hahsmap + level order traversal

  - top view of a tree(with criss-crossing of nodes): https://www.geeksforgeeks.org/print-nodes-top-view-binary-tree/

  Diagonal traversal
  -------------------
  - diagonal traversal of a tree
  - ref: https://www.geeksforgeeks.org/diagonal-traversal-of-binary-tree/
  - method1: use queue: level order traversal at diagonal level
    method2: use hashmap and preorder traversal

  - diagonal sums of a tree
  - ref: https://www.geeksforgeeks.org/diagonal-sum-binary-tree/
  - method: use diagonal level order traversal


 */

/*
  Iterative Tree Traversals
  --------------------------

  - iterative preorder
  - ref: https://www.geeksforgeeks.org/iterative-preorder-traversal/
  - method: own sol using explicit stack, which also matches with the market solution

  - iterative inorder
  - ref: https://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/
  - method1: own sol: use explicit stack to mimic recursive call with flag
    method2: market sol: use explicit stack and CURSOR with trick for optimization
                         http://web.cs.wpi.edu/~cs2005/common/iterative.inorder

  - iterative postorder
  - ref: https://www.geeksforgeeks.org/iterative-postorder-traversal/
  - method1: own sol: use explicit stack to mimic recursive call with flag
    (similar approach: iterative postorder using two stacks: https://www.geeksforgeeks.org/iterative-postorder-traversal/)
    method2: market sol: use explicit stack with trick for optimization
             least ratta code: (subTreeProcessed || leaf): https://stackoverflow.com/questions/1294701/post-order-traversal-of-binary-tree-without-recursion
 */

/*

 */

/*
  - connect nodes at same level, constraint: no extra queue
  - ref: https://www.geeksforgeeks.org/connect-nodes-level-level-order-traversal/
  - method: own solution: current level queue is maintained via extra pointer itself

  - given a complete tree, connect it's node at same level using recursion
  - ref: https://www.geeksforgeeks.org/connect-nodes-at-same-level/
  - method: use preorder traversal
 */

/*
  - algo to select k random nodes from bst
  - ref: https://www.careercup.com/question?id=13374666
  - method1: traverse the tree once and use reservoir sampling
             (note: here we need to keep track of count, so we need to traverse iteratively)
    method2: copy tree to array, follow array shuffle algorithm
 */

/*
  Middle level of a tree
  -----------------------

  - print mid level of a tree
  - method: find height of tree and do level order traversal to print (h/2)th level

  - print mid level of a complete tree
  - ref: https://www.geeksforgeeks.org/print-middle-level-perfect-binary-tree-without-finding-height/
  - method: use slow and fast pointers with recursion

 */

/*
  Tournament tree
  ----------------

  - 2n players compete with each other and each player gets a final rank, find the winner and runner up using minimum comparisons
  - ref: https://www.geeksforgeeks.org/second-minimum-element-using-minimum-comparisons/
  - method: tournament tree approach

 */

/*
  - check if two trees have same leaves in last level, constraint: T(linear) S(max(height of tree))
  - ref: https://www.geeksforgeeks.org/check-if-leaf-traversal-of-two-binary-trees-is-same/
  - method: combined iterative traversals of the two trees
            (ofcourse simpler solution would have been BFS to find last level of the tree but with S(max(width of tree)))
 */

/*
  - check if two trees have leaves in last level which are mirror image of each other, constraint: T(linear) S(max(height of tree))
  - method: iterative traversal of one tree in inorder and other tree in reverse inorder
 */

/*
  - given a tree stored in array in heap format, print all paths to leaf with a given sum k. # in a cell represents null
    Input : 10 11 13 # 15 28 6 # # 8 4 # 9 11 #
    Output: 10 11 15 4 and 10 13 6 11
  - method: use recursion and helper heap methods
 */

/*
  Tree to List and vice-versa
  ----------------------------

  - convert binary tree to list
  - ref: https://www.geeksforgeeks.org/in-place-convert-a-given-binary-tree-to-doubly-linked-list/
  - method1: use recursive inorder traversal
             - https://www.geeksforgeeks.org/in-place-convert-a-given-binary-tree-to-doubly-linked-list/
    method2: keep track of last nodes
             method2.1: iterative inorder traversal, https://www.geeksforgeeks.org/convert-given-binary-tree-doubly-linked-list-set-3/
             method2.2: own sol: inorder traversal, keep track of first and last node of the list when returning solution
             method2.3: use doubly circular linked list
    crap method3: fix left pointers, then fix right pointers
             - https://www.geeksforgeeks.org/convert-a-given-binary-tree-to-doubly-linked-list-set-2/
    crap method4: inorder traversal and add elements to end of list

  - convert tree to circular doubly linked list
  - ref: https://www.geeksforgeeks.org/convert-a-binary-tree-to-a-circular-doubly-link-list/
  - method: this is easier since circular doubly list means we can keep track of last node easily for concat

  - convert doubly linked list to tree, constraint: T(n), S(1)
  - ref: https://www.geeksforgeeks.org/in-place-conversion-of-sorted-dll-to-balanced-bst/
         https://www.geeksforgeeks.org/?p=17063
  - method1: divide and conquer: T(n lg n), S(lg n)
    method2: copy list to array and convert to tree: T(n), S(n)
    method3: smart divide and conquer: T(n), S(1)


  PreOrder List
  --------------

  - convert tree to preorder list using right pointers
  - ref: https://www.geeksforgeeks.org/modify-binary-tree-get-preorder-traversal-using-right-pointers/
  - method: do preorder traversal(recursive/iterative), keeping track of last pointer for easy joining

 */

/*

  Tree SerDe
  -----------

  - SerDe of binary trees
  - ref: https://www.geeksforgeeks.org/serialize-deserialize-binary-tree/
  - method
    - is tree a bst: then store just preorder or postorder traversal
    - is tree a complete tree: then store level order traversal i.e. store tree in heap format
    - is tree a full tree(0-2 child): store preorder with marker for leaf nodes
    - general case: store inorder and preorder traversals
                    (why: pre/post order helps determining the root node, inorder helps to find the split based on count)
    - general case where data of each node is huge: store preorder traversal
        - marker can be for NULLs(i.e. treat tree as )
        - marker for end of children
    - nary tree: preorder and end of children marker

  - construct bst from it's preorder traversal, constraint: T(n)
  - ref:
      - https://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversa/
      - https://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversal-set-2/
  - method1: break the input list and use recursion
    method2: use range to filter elements for a subtree
    method3: use stack based approach/ recursion without breaking the list...i.e. iterative preorder traversal

  - check if a given array can represent a preorder traversal of a valid bst or not
  - ref: https://www.geeksforgeeks.org/check-if-a-given-array-can-represent-preorder-traversal-of-binary-search-tree/
  - method: idea is to do inorder traversal of the tree using given array and ensure the node values travelled inorder is monotonically increasing

  - construct tree from inorder and preorder traversals
  - ref: https://www.geeksforgeeks.org/construct-tree-from-given-inorder-and-preorder-traversal/
  - method: top-down recursion

  - construct tree from inorder and preorder traversals
  - ref: https://www.geeksforgeeks.org/construct-a-binary-tree-from-postorder-and-inorder/
  - method: top-down recursion
 */

/*
  Diameter of a tree
  -------------------

  - diameter of a binary tree in single traversal
  - ref: https://www.geeksforgeeks.org/diameter-n-ary-tree/
  - method: calculate height and diameter of subtrees in single traversal

  - diameter of nary tree
  - ref: https://www.geeksforgeeks.org/diameter-n-ary-tree/
  - method: approach similar to that of a binary tree

  - find longest leaf to leaf path in a tree
  - method: this is same as finding diameter of a tree

  - diameter of a tree using dfs/bfs
  - ref: https://www.geeksforgeeks.org/diameter-tree-using-dfs/
  - method: imagine tree as undirected graph, find farthest node from any node, then find farthest node from the farthest node

  - diameter of an nary tree using dfs/bfs
  - ref: https://www.geeksforgeeks.org/diameter-n-ary-tree-using-bfs/
  - method: imagine tree as undirected graph, find farthest node from any node, then find farthest node from the farthest node

  - find longest leaf to leaf path in a tree with parent pointers
  - method: same as finding diameter in a tree using dfs/bfs, imagined as undirected graph

 */

/*
  - extract leaves of a tree
  - ref: https://www.geeksforgeeks.org/connect-leaves-doubly-linked-list/
  - method: do preorder traversal and add nodes to end of a linked list

  - find height of the tree with extracted leaves(i.e. leaves sameSet as a circular DLL)
  - ref: https://www.geeksforgeeks.org/find-height-of-a-special-binary-tree-whose-leaf-nodes-are-connected/
  - method: recursion with special check for leaf nodes

  - print inorder traversal of a tree with extracted leaves(i.e. leaves sameSet as a circular DLL)
    modification: instead of circular, the first and last nodes are pointing to themselves
  - method: recursion with special check for leaf nodes, specially for first and last nodes

 */

/*
  Sum Tree
  ----------
  - convert binary tree to binary sum tree, with increment of sum only
  - https://www.geeksforgeeks.org/convert-an-arbitrary-binary-tree-to-a-tree-that-holds-children-sum-property/
  - Replace the data of every node of a Binary Search Tree with the sum of all the nodes greater than it

  - convert a given tree to sum tree
  - ref: https://www.geeksforgeeks.org/convert-a-given-tree-to-sum-tree/

  - check child sum property
  - ref: https://www.geeksforgeeks.org/?p=4358

  - convert to tree with child sum property, but value can only be increased at each node
  - ref: https://www.geeksforgeeks.org/convert-an-arbitrary-binary-tree-to-a-tree-that-holds-children-sum-property/

 */

/*
  Tree Paths
  -----------

  - print all root to leaf paths
  - ref: https://www.geeksforgeeks.org/given-a-binary-tree-print-all-root-to-leaf-paths/
  - method: preorder traversal and backtracking

  - print all root to leaf paths without recursion
  - ref: https://www.geeksforgeeks.org/print-root-leaf-path-without-using-recursion/
  - method: iterative preorder traversal

  - print all root to leaf paths with given sum
  - ref: https://www.geeksforgeeks.org/print-paths-root-specified-sum-binary-tree/
  - method: preorder traversal and backtracking

  - check if tree has a root to leaf path with given sum(don't need to print the path)
  - ref: https://www.geeksforgeeks.org/root-to-leaf-path-sum-equal-to-a-given-number/
  - method: preorder traversal


  - print all top-down paths(i.e. from a node towards leaf) with given sum, path can start from any node and can end at any node
  - ref: https://www.geeksforgeeks.org/print-k-sum-paths-binary-tree/
  - method: own sol: for each node, check k-sum path in subtrees, also check (k-node) sum path in subtrees
                     https://stackoverflow.com/questions/11328358/algorithm-to-print-all-paths-with-a-given-sum-in-a-binary-tree
 */

/*
  Order Static in a BST
  ----------------------

  - find kth largest element in a bst
  - ref: https://www.geeksforgeeks.org/find-k-th-smallest-element-in-bst-order-statistics-in-bst/
  - method1: find kth element in inorder traversal....T(n)
    method2: order static tree....T(lg n)

  - find top 3 elements or third highest value in tree
  - ref: https://www.geeksforgeeks.org/top-three-elements-binary-tree/
  - method1: use min heap of size 3
    method2: use array of size 3 and keep track of top 3 after each iteration
             (benefits: simpler code and T(n) since 3 is constant in asymptotic terms)

  - find median of a bst, constraint: T(n), S(1)
  - ref: https://www.geeksforgeeks.org/find-median-bst-time-o1-space/
  - method1: inorder traversal....S(height)
    method2: morris traveral....S(1)

 */

/*
  Pair With Sum in BST
  ---------------------

  - find pairs with given sum in a bst
  - ref: https://www.geeksforgeeks.org/find-a-pair-with-given-sum-in-bst/
  - method1: flatten tree to list and use two pointer algorithm. if we cann't destroy the tree, then use auxiliary array
  - method2: use inorder traversal and hashmap
  - method3: use simultaneous iterative inorder and reverse inorder traversals

 */

/*
  Ideantical, Mirror and Isomorphic Trees
  ----------------------------------------

  Identical
  ----------
  - check if two tree are identical
  - https://www.geeksforgeeks.org/write-c-code-to-determine-if-two-trees-are-identical/
  - method: simultaneous travel and check


  Mirror Image
  -------------
  - check if a tree is symmetric i.e. mirror image of itself(both structure and values), constraint: S(1)
  - ref: https://www.geeksforgeeks.org/symmetric-tree-tree-which-is-mirror-image-of-itself/
  - method1: use recursion
    method2: use level order traversal and ensure that each level is palindrome
             (trick: in this case we can use smart insert in level order or we can use dequeue)

  - convert a tree into it's mirror tree
  - ref: https://www.geeksforgeeks.org/write-an-efficient-c-function-to-convert-a-tree-into-its-mirror-tree/
  - method: go to each node and swap left child with right child

  - convert a tree into it's mirror tree using bfs
  - ref: https://www.geeksforgeeks.org/write-an-efficient-c-function-to-convert-a-tree-into-its-mirror-tree/
  - method: swap child of each node and add them to queue

  Isomorphic
  -----------
  - check if two trees are isomorphic or not
  - ref: https://www.geeksforgeeks.org/tree-isomorphism-problem/
  - method: use preorder traversal

 */

/*
  BST Check
  ----------

  - check if a binary tree is bst or not
  - ref: https://www.geeksforgeeks.org/a-program-to-check-if-a-binary-tree-is-bst-or-not/
  - method1: use definition
    method2: min-max range check(will null pointers) and top down recursion
    method3: use iterative inorder traversal: inorder traversal should me monotonically increasing
 */

/*

  Trees in Arrays
  ----------------

  - given preorder of 2 bst in 2 arrays, find if they are identical without constructing the trees
  - method: this is same as constructing the trees from preorder....so use simultaneous iterative preorder traversal

  Parent Array Representation
  ----------------------------
  - construct binary tree from parent array representation
  - ref: https://www.geeksforgeeks.org/construct-a-binary-tree-from-parent-array-representation/
         http://www.techiedelight.com/build-binary-tree-given-parent-array/
  - method: own solution create extra array

  - find height of a binary tree from parent array representation
  - ref: https://www.geeksforgeeks.org/find-height-binary-tree-represented-parent-array/
  - method: for each node, find it's depth recursively
            since the recursion solves overlapping subproblems, therefore use memoization of dypro

  Ancestor Matrix
  ----------------

  - tree to ancestor matrix
  - ref: https://www.geeksforgeeks.org/construct-ancestor-matrix-from-a-given-binary-tree/
  - method: use preorder traversal, keeping track of the path from root node

  - ancestor matrix to tree
  - ref: https://www.geeksforgeeks.org/construct-tree-from-ancestor-matrix/
         http://www.ritambhara.in/build-binary-tree-from-ancestor-matrics/
  - method: own sol: create a summary data structure
    hash table from 0 to level, for each level, create a hashset of nodes at that level
    create tree bottom up: for each node, check for ancestor presence in previous level

 */

/*
  - merge two bst into a sorted list, constraint: T(n), S(height)
  - ref: https://www.geeksforgeeks.org/merge-two-bsts-with-limited-extra-space/
  - method1: flatten the two lists and merge
    method2: simultaneous inorder traversal of the two bst
 */

/*
  Siblings and Cousins
  ---------------------

  - check if two nodes are cousins or not
  - ref: https://www.geeksforgeeks.org/check-two-nodes-cousins-binary-tree/
  - method: do single traversal to find the two nodes and their parent

  - print cousins of a node
  - ref: http://www.techiedelight.com/print-cousins-of-given-node-binary-tree/
  - method: do level order traversal to find the node. print all nodes of the level except sibling node.

 */

/*
  Remove nodes
  --------------

  tips: nice way to handle these questions is:
    1. use bottom up postorder recursion
    2. set left and right pointers for each node to result of recursive calls

  - remove nodes outside given range
  - ref: https://www.geeksforgeeks.org/remove-bst-keys-outside-the-given-range/
  - method: use postorder recursion and set left and right pointers for each node to result of recursive calls.

  - remove all half nodes from a binary tree
  - ref: https://www.geeksforgeeks.org/given-a-binary-tree-how-do-you-remove-all-the-half-nodes/
  - method: use postorder recursion and set left and right pointers for each node to result of recursive calls.

 */

/*
  Colliding Nodes of a tree
  --------------------------

  - given a binary tree, count the number of colliding nodes of the tree
  - ref: http://yougeeks.blogspot.in/2014/10/amazongiven-binary-tree-count-number-of.html
  - method: own sol: do bfs and keep on passing vertial distance info to each child, count collisions at each level using hashmap

 */

/*
  Clone Tree With Random Pointers
  --------------------------------

  - clone tree with random pointers
  - ref: https://www.geeksforgeeks.org/clone-binary-tree-random-pointers/
  - method: idea is to use techniques of cloning linked list with random pointers
    - method1: use hashing
    - method2: create cloned tree inside given tree and then split

 */

/*

  Tree to BST
  ------------

  - convert a given binary tree to bst
  - ref: https://www.geeksforgeeks.org/binary-tree-to-binary-search-tree-conversion/
  - method
    - can change structure:
      - delete each node and insert in a new bst
      - flatten, sort, create bst
        https://stackoverflow.com/questions/2577098/how-to-convert-a-binary-tree-to-binary-search-tree-in-place-i-e-we-cannot-use
    - cannot change structure: copy to array, sort and copy to tree
 */

/*

  Subtree Check
  --------------

  - given two trees T1 and T2, check if one is subtree of another
  - ref: https://www.geeksforgeeks.org/check-if-a-binary-tree-is-subtree-of-another-binary-tree/
         https://www.geeksforgeeks.org/check-binary-tree-subtree-another-binary-tree-set-2/
  - method: step1: find the smaller tree, then check if smaller is subtree of the other tree
            step2: if T1 is smaller, then check if inorder and preorder of T1 is subtring of corresponding traversals of T2
            step3: for subtring match, use KMP string algorithm

  - largest BST in a binary tree
  - ref: https://www.geeksforgeeks.org/find-the-largest-subtree-in-a-tree-that-is-also-a-bst/
         https://www.geeksforgeeks.org/largest-bst-binary-tree-set-2/
  - method: call isBST(range check) in top-down fashion and return value will be yes/no and size of bst

 */

/*
  Swapping Nodes in a Tree
  -------------------------

  - find minimum swaps to convert a tree to bst
  - ref: https://www.geeksforgeeks.org/minimum-swap-required-convert-binary-tree-binary-search-tree/
  - method: this is same as min swaps to sort array of inorder traversal of a tree = sum(for each cycle: cycle length - 1)

  - fix swapped nodes of a bst
  - ref: https://www.geeksforgeeks.org/fix-two-swapped-nodes-of-bst/
  - method: do inorder traversal and find the swapped nodes and fix them. handle special case when swapped nodes are adjacent

 */

/*
  Nodes at Distance K
  --------------------

  - print all nodes at distance k from root
  - ref: https://www.geeksforgeeks.org/print-nodes-at-k-distance-from-root/
  - method: top down recursion

  - print all nodes at distance k from leaf nodes
  - ref: https://www.geeksforgeeks.org/print-nodes-distance-k-leaf-node/
  - method1: bottom up: for each leaf node, find path from root and print ancestor at distance k
    method2: top down: travel from root to each leaf, keeping track of path and print ancestor at distance k on reaching leaf node

  - print all nodes(ancestor/descendant) at distance k from a given node
  - ref: https://www.geeksforgeeks.org/print-nodes-distance-k-given-node-binary-tree/
  - method: 2 step algorithm
    - step1: print all nodes at distance k in subtree rooted at given node
    - step2: in ancestor chain, for each ancestor: find notes at right distance in the other subtree of the ancestor

 */

/*
  - given a boolean binary tree:
    - if parent node is 0, then left child is 0 and right child is 1
    - if parent node is 1, then left child is 1 and right child is 0
    root of the tree is 0. find the kth node value which is present at nth level
  - method: own solution:
    calculate node number from k and n, then treat it as heap traversal to root
    at each level, flip answer based on two things: parent node is 0 or 1 + current node is left or right child

  - given a heap as binary tree, find the nth node is it's bfs traversal without doing bfs
  - ref: https://stackoverflow.com/questions/13846585/k-th-element-in-a-heap-tree
  - method: path can be traced from bits of the number n

 */

/*
  LCA
  ----

  - find lca of two nodes in a bst
  - ref: https://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/
  - method: use top down preorder and find the first node which lies in the range of the two nodes

  - find lca of two given nodes in a tree with parent pointers
  - ref: https://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-tree-set-2-using-parent-pointer/
  - method1: find intersection of the lists of paths from the node to the root using parent pointers
    method2: use hashing to find the first already visited node

  - find lca of two nodes in a tree(not bst and without parent pointers), constraint: T(n), S(height)
  - ref: https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
  - method1: find paths and find last node after which paths diverge
    method2: bottom up postorder recursion, each subtree returns if it has either of the two nodes

  - PHD question:
  - find LCA of two nodes using Euler Travel, RMQ(Range Min Query) and Segment Trees
  - ref: https://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-tree-set-3-using-rmq/
         https://www.geeksforgeeks.org/find-lca-in-binary-tree-using-rmq/
  - method: EFL arrays: Euler Travel, First in Euler Travel and Level arrays

  - find shortest distance/path between two nodes in a tree
  - ref: https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/
  - method: find lca and then find distance of the two nodes from the lca

  - shortest distance between two nodes in a bst
  - ref: https://www.geeksforgeeks.org/shortest-distance-between-two-nodes-in-bst/
  - method: find lca and then find distance of the two nodes from the lca

  - distance between 2 repeated nodes of a tree
  - ref: https://stackoverflow.com/questions/20171221/least-distance-between-two-values-in-a-large-binary-tree-with-duplicate-values
  - method: basically we need to find lca for each possible pair of nodes
            so instead of finding multiple lca explicitly, we will use lca bottom up algorithm and cover all possibilites

 */

/*
  Nary Tree(or directed acyclic graph)
  --------------------------------------

  - nary tree bfs
  - ref: https://www.quora.com/How-do-I-iterate-through-an-n-ary-tree
  - method: similar to binary tree

  - nary tree dfs
  - method: similar to bianry tree, but there is only preorder(top-down) or postorder(bottom-up) only, no inorder

  - mirror an nary tree
  - https://www.geeksforgeeks.org/mirror-of-n-ary-tree/
  - method: similar to that of binary tree, special case: if there is only one child, then there is nothing to mirror


  - depth of an nary tree: https://www.geeksforgeeks.org/depth-n-ary-tree/
  - number of ways to traverse nary tree: https://www.geeksforgeeks.org/number-of-ways-to-traverse-an-n-ary-tree/

  - flatten nary tree
  - method: similar to binary tree

  - serde nary tree
  - ref: https://www.geeksforgeeks.org/serialize-deserialize-n-ary-tree/
  - method: end of children marker

  - diameter of an nary tree
  - ref: https://www.geeksforgeeks.org/diameter-n-ary-tree/
  - method: similar to binary tree

  - print nary tree in zig-zag manner
  - method: similar to binary tree

  - right boundary of nary tree
  - method: use level order and find left and right nodes

  - LCA of K given nodes in a nary tree
  - ref: https://www.geeksforgeeks.org/lca-for-general-or-n-ary-trees-sparse-matrix-dp-approach-onlogn-ologn/

 */

/*
  Inorder Successor/Predecessor
  ------------------------------

  - find inorder successor of a node in bst(with and without parent pointers)
  - ref: https://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
  - method1: parent pointer given: return either min value in right subtree or ancestor at first left turn
    method2: search from root, keeping track of nodes from which left turn is taken

  - find inorder successor and predecessor of a node in a bst
  - ref: https://www.geeksforgeeks.org/inorder-predecessor-successor-given-key-bst/
  - method: predecessor search approach is similar to successor. fuse the two searches into one

  - populate inorder successor of all nodes in a tree
  - ref: https://www.geeksforgeeks.org/populate-inorder-successor-for-all-nodes/
  - method: do iterative inorder traversal, keeping track of the previous element

  - write inorder iterator for a tree
  - method: iterator will internally do iterative inorder traversal

  - find inorder successor of a node in bst(with and without parent pointers)
  - ref: https://www.geeksforgeeks.org/inorder-succesor-node-binary-tree/
  - method: similar approach to that of finding inorder successor in a bst
    - with parent pointers: http://www.ritambhara.in/inorder-successor-of-node-in-a-binary-tree/
    - without parent pointers: https://www.geeksforgeeks.org/inorder-succesor-node-binary-tree/
                               (ows sol: do iterative preorder search and keep track of succ and pred)

 */

/*
  Max Sum Paths in Tree
  ----------------------

  - find max sum path between any two leaves of a tree
  - ref: https://www.geeksforgeeks.org/find-maximum-path-sum-two-leaves-binary-tree/
  - method: bottom up approach as the final solution has optimal substructure



 */

/*
  Identical Trees
  ----------------

  - check if two bst are same(value wise and structurally). the two tree are given in array format, elements in array
    represent the order in which keys will be inserted into the trees. constraint: check this without constructing the trees.
  - ref: https://www.geeksforgeeks.org/check-for-identical-bsts-without-building-the-trees/
  - method1: compare the root element and then partition the tree into left and right subtrees and recurse
             ref: https://www.ideserve.co.in/learn/check-if-identical-binary-search-trees-without-building-them-set-2
    method2: for simpler coding, use min-max approach of check bst algorithm
             ref: https://www.ideserve.co.in/learn/check-if-identical-binary-search-trees-without-building-them-set-1

  - given preorder and inorder traversals of a binary tree, output it's postorder traversal without constructing the tree
  - ref: https://www.geeksforgeeks.org/print-postorder-from-given-inorder-and-preorder-traversals/
  - method: find root from preorder and put it on stack, create subproblems for left and right subtress and solve recursively

  - given preorder and inorder traversals of a binary tree, print preoder and inorder traversals of it's sum tree
    i.e. after modifying it such that each node stores the sum of its left and right subtree(without building the tree)
  - method: recurse and travel the tree, find sum of left and right subtrees and replacing sum value for each node
            i.e. create inorder and preorder traversals of sum tree and then just print them

 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  -
  -
 */

/*
  Heap
  ----

  - implement heap operations, heap sort and priority queue

  - prove that constructing heap is a linear time operation, while heap sort is T(n lg n)
  - ref: https://www.geeksforgeeks.org/time-complexity-of-building-a-heap/

 */

/*
  Easy ones
  ----------

  - sum of left leaf nodes: https://www.geeksforgeeks.org/find-sum-left-leaves-given-binary-tree/
  - method: use recursion

  - ceil and floor of a number in bst in a single traversal
  - ref: https://www.geeksforgeeks.org/floor-and-ceil-from-a-bst/

  - find minimum height of a tree
  - method: this is same as saying find leaf node with minimum depth. this indicates bfs would be better than dfs

  - check if all nodes of a tree are at same level
  - ref: https://www.geeksforgeeks.org/check-leaves-level/
  - method: use bfs(and not dfs) and verify that all leaves are at same level

  - check if a binary tree is balanced or not
  - ref: https://www.geeksforgeeks.org/how-to-determine-if-a-binary-tree-is-balanced/
  - method: bottom up recursion to calculate height and compare balance of left and right subtrees

  - given the marks of a class in a bst, subtract 5 marks from even ranked student
  - method: iterative inorder and flag for checking even ranked elements

  - find sum of n elements after kth smallest element in a bst. tree is very large, you are not allowed to store tree traversal.
  - method: iterative inorder

  - find all tree nodes with k leaves
  - ref: https://www.geeksforgeeks.org/print-nodes-binary-tree-k-leaves/
  - method: bottom up postorder traversal

  - given a bst, change +/- signs of all nodes and still maintain the bst property
  - method1: flattern, change sign, reverse and create a bst
    method2: change sign and convert tree to it's mirror image

  - common nodes in two bst
  - ref: https://www.geeksforgeeks.org/print-common-nodes-in-two-binary-search-trees/
  - method: compare inorder traversals, either copy both tree to array but for optimization, do simultaneous inorder traversal of the trees

  - max diff between a node and it's ancestor
  - ref: https://www.geeksforgeeks.org/maximum-difference-between-node-and-its-ancestor-in-binary-tree/
  - method1: top down recursion, passing max-so-far
    method2: bottom up recursion, diff of each node with it's min child

  - efficient data structure for given operations: find/delete min and max, insert delete
  - ref: https://www.geeksforgeeks.org/a-data-structure-question/
  - method: sorted linked list, balanced bst, 2 heaps

  - given a binary tree to be drawn over x,y coordinate system with root node as (0,0).
    write code to display coordinate of all nodes(all nodes will expand along x-axis so that no node overlaps)
                       o(0,0)
               /           \
         o(-1,-1)           o(1,-1)
        /       \          /       \
    o(-2,-2)  o(-1,-2)   o(0,-2)  o(1,-2)
    ref: https://www.geeksforgeeks.org/amazon-interview-set-38/
  - method: draw tree level wise, in case of collision, shift nodes to left

  - count number of turns in path between two nodes in a tree
  - ref: https://www.geeksforgeeks.org/number-turns-reach-one-node-binary-tree/
  - method: own sol: find lca and hence the path in terms of LR turns, count the number of LR flips

  - deepest leaf node that is also a left child
  - ref: https://www.geeksforgeeks.org/deepest-left-leaf-node-in-a-binary-tree/
  - method: recursion

  - given a bst, each node has an additional attribute ‘score’ along with the value, you need to write two functions:
    a. Update(value, newScore) -> update the score of the node with value to the newScore
    b. SubTreeScore(value) -> get the sum of scores of all nodes in the subtree with node having the value as the root
      constraints: update should be 0(height), while subTreeScore should be 0(1)
    c. what if nodes can be deleted from the tree
  - method: maintain sum of score of subtree nodes for each node
            updating score would lead to updating score of all the ancestors
            deletion:
              - if node is leaf, update ancestor chain
              - if node has one child, update ancestor chain
              - if node has two children, swap with successor, update ancestor chain and ancestor chain of predecessor

  - given a binary tree where each node contains three pointers left,right,succ, where succ points to any successor node,
    mark succ pointer as null if it is pointing to a predecessor node
  - method: do preorder traversal and mark each as visited, if successor of any node is pointing to a visited node,
            then it is pointing to a predecessor node, so mark it as null

  - first pair of non-matching leaves
  - ref: https://www.geeksforgeeks.org/find-first-non-matching-leaves-two-binary-trees/
  - method: use iterator for iterative inorder traversal of leaf nodes and find first non matching pair

 */
