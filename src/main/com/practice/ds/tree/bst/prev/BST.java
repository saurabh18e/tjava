package com.practice.ds.tree.bst.prev;

/*
    Binary Search Trees
    --------------------
    - dynamic set operations supported: insert, search, delete, min, max, predecessor and successor
        - so dictionary can be used both as dictionary and priority queue
    - complexity analysis
        - worst case: 0(n), avg case: 0(lg n)
        - S(n)
    - info maintained: left, right, parent
    - BST property: for each node, all nodes in left are smaller and all nodes in right are greater
    - bst walks: inorder, preorder, postorder
    - randomly built bst

    Augmented BST
    --------------
    - Red Black Trees
        - extra info: 1 bit: color each node as red or black
        - worst case height: 2*(log n), hence insert, search and delete are always 0(log n)
        - advantage of AVL
            - only 1 bit extra info
            - lesser rotations for insert(2) and delete(3)
    - Order Static Tree
        - extra info: each nodes stores size of subtree under it
        - use: when we need search and order static
    - Interval trees
        - skip
 */

import com.practice.util.Tuple;

import java.util.*;

public class BST {

    /*
        TODO
        -----
        - diameter
        - morris traversal
        - delete (iter and rec)
        - parent pointer iterative
     */

    BSTNode root;

    public static BSTNode insertRec(BSTNode root, int value) {
        if (root == null) {
            return new BSTNode(value);
        } else {
            if (root.value < value) {
                root.right = insertRec(root.right, value);
            } else {
                root.left = insertRec(root.left, value);
            }
            return root;
        }
    }
    public static BSTNode insertIter(BSTNode root, int value) {
        BSTNode nnd = new BSTNode(value);
        if (root == null) {
            return nnd;
        } else {
            BSTNode curr = root;
            while (true) {
                if (curr.value < value) {
                    if (curr.right == null) {
                        curr.right = nnd;
                        break;
                    } else {
                        curr = curr.right;
                    }
                } else {
                    if (curr.left == null) {
                        curr.left = nnd;
                        break;
                    } else {
                        curr = curr.left;
                    }
                }
            }
            return root;
        }
    }
    public void insertIter(int value) {
        this.root = insertIter(this.root, value);
    }
    public void insert(int value) {
        this.root = insertIter(this.root, value);
    }

    public static List<String> preorder(BSTNode root) {
        if (root != null) {
            ArrayList<String> output = new ArrayList<String>();
            output.add(root.value + "");
            output.addAll(preorder(root.left));
            output.addAll(preorder(root.right));
            return output;
        } else {
            return new ArrayList<>();
        }
    }
    public List<String> preorder() { return preorder(this.root); }
    public static List<String> inorder(BSTNode root) {
        if (root != null) {
            ArrayList<String> output = new ArrayList<String>();
            output.addAll(inorder(root.left));
            output.add(root.value + "");
            output.addAll(inorder(root.right));
            return output;
        } else {
            return new ArrayList<>();
        }
    }
    public List<String> inorder() { return inorder(this.root); }
    public static List<String> postorder(BSTNode root) {
        if (root != null) {
            ArrayList<String> output = new ArrayList<String>();
            output.addAll(postorder(root.left));
            output.addAll(postorder(root.right));
            output.add(root.value + "");
            return output;
        } else {
            return new ArrayList<>();
        }
    }
    public List<String> postorder() { return postorder(this.root); }
    public static ArrayList<String> bfs(BSTNode root) {
        ArrayList<String> output = new ArrayList<String>();
        Queue<BSTNode> q = new LinkedList<BSTNode>();
        q.add(root);
        while (!q.isEmpty()) {
            BSTNode node = q.remove();
            if (node != null) {
                output.add(node.value + "");
                if (node.left != null) q.add(node.left);
                if (node.right != null) q.add(node.right);
            }
        }
        return output;
    }
    public List<String> bfs() { return bfs(this.root); }
    public static List<List<String>> levels(BSTNode root) {
        List<List<String>> output = new ArrayList<List<String>>();
        Queue<BSTNode> q = new LinkedList<BSTNode>();
        if (root != null) {q.add(root);q.add(null);}
        while (!q.isEmpty()) {
            ArrayList<String> level = new ArrayList<String>();
            while(q.peek() != null) {
                BSTNode node = q.remove();
                level.add(node.value + "");
                if (node.left != null) q.add(node.left);
                if (node.right != null) q.add(node.right);
            }
            output.add(level);
            q.remove();
            if (!q.isEmpty()) q.add(null);
        }
        return output;
    }
    public List<List<String>> levels() { return levels(this.root); }
    public static List<String> dfs(BSTNode root) {
        return postorder(root);
    }
    public List<String> dfs() { return dfs(this.root); }

    // compare trees
    public static boolean areEqual(BSTNode bst1, BSTNode bst2) {
        if (bst1 == null && bst2 == null) return true;
        if (bst1 == null || bst2 == null) return false;
        if (bst1.value != bst2.value) return false;
        return areEqual(bst1.left, bst2.left) && areEqual(bst1.right, bst2.right);
    }
    public static boolean areEqual(BST bst1, BST bst2) { return areEqual(bst1.root, bst2.root);}
    public static boolean areEqualByStructure(BSTNode bst1, BSTNode bst2) {
        if (bst1 == null && bst2 == null) return true;
        if (bst1 == null || bst2 == null) return false;
        return areEqualByStructure(bst1.left, bst2.left) && areEqualByStructure(bst1.right, bst2.right);
    }
    public static boolean areEqualByStructure(BST bst1, BST bst2) { return areEqualByStructure(bst1.root, bst2.root);}

    // search: rec, itr
    public static BSTNode findIter(BSTNode root, int value) {
        while (root != null) {
            if (root.value == value) break;
            root = (root.value < value) ? root.right : root.left;
        }
        return root;
    }
    public static BSTNode findRec(BSTNode root, int value) {
        if (root == null || root.value == value) return root;
        return (root.value < value) ? findRec(root.right, value) : findRec(root.left, value);
    }
    public static BSTNode find(BSTNode root, int value) { return findIter(root, value); }
    public BSTNode find(int value) { return findIter(this.root, value); }
    public boolean exists(int value) { return this.find(value) != null; }

    public BSTNode copy(BSTNode root) {
        if (root == null) return null;
        BSTNode nnd = new BSTNode(root.value, copy(root.left), copy(root.right));
        return nnd;
    }
    public BST copy() {
        BST bst = new BST();
        bst.root = copy(this.root);
        return bst;
    }

    // delete: bst and pbst
//    public static BSTNode delete(BSTNode root, int value) {
//        if (root == null) return root;
//        if (root.value == value) {
//            if (root.le == null) {
//                BSTNode newRoot = root.left;
//                return root.left;
//            } else {
//
//            }
//        }
//    }

    // size, height and diameter
    public static int size(BSTNode root) {
        if (root == null) return 0;
        return 1 + size(root.left) + size(root.right);
    }
    public int size() {return size(this.root);}
    public int length() {return size(this.root);}
    public static int height(BSTNode root) {
        if (root == null || root.isLeafNode()) return 0;
        return 1 + Math.max(height(root.left), height(root.right));
    }
    public int height() {return height(this.root);}
    public static int diameter(BSTNode root) {
        // ref1: https://www.geeksforgeeks.org/diameter-of-a-binary-tree/
        // ref2: https://www.geeksforgeeks.org/diameter-of-a-binary-tree-in-on-a-new-method/
        // ref3: https://leetcode.com/problems/diameter-of-binary-tree/solution/
        if (root == null || root.isLeafNode()) return 0;
        int leftDiameter = root.left == null ? 0 : 1 + diameter(root.left);
        int rightDiameter = root.right == null ? 0 : 1 + diameter(root.right);
        return leftDiameter + rightDiameter;
    }
    public int diameter() {return diameter(this.root);}

    public void printLevels() {
        List<List<String>> levels = this.levels();
        System.out.println("bst levels are: ");
        for (int i = 0; i <levels.size(); i++) {
            System.out.println("level[" + i + "]: " + levels.get(i));
        }
    }
    private static String spaces(int count) {
        StringBuffer buff = new StringBuffer();
        for(int i=0; i<count;i++) buff.append(" ");
        return buff.toString();
    }
    private static void printLevel(List<BSTNode> level, int currentHeight) {
        if (currentHeight < 0) return;

        String padding = spaces((int)Math.pow(2, currentHeight)-1);
        String space = spaces((int)Math.pow(2, currentHeight+1)-1);
        // System.out.println("printing level[" + currentHeight + "] with pad: " + padding.length() + ", space: " + space.length());
        // todo: print slashes

        System.out.print(padding);
        for (int i = 0; i < level.size(); i++) {
            String val = level.get(i) == null ? "N" : level.get(i) + "";
            System.out.print(val);
            if (i != level.size()-1) {
                System.out.print(space);
            }
        }
        System.out.println(padding);

        // print next level
        ArrayList<BSTNode> nextLevel = new ArrayList<BSTNode>();
        for (BSTNode n : level) {
            if (n != null) {
                nextLevel.add(n.left);
                nextLevel.add(n.right);
            } else {
                nextLevel.add(null);
                nextLevel.add(null);
            }
        }
        printLevel(nextLevel, currentHeight-1);
    }
    public static void print(BSTNode root) {
        int h = height(root);
        ArrayList<BSTNode> level = new ArrayList<>();
        level.add(root);
        printLevel(level, h);
    }
    public void print() {print(this.root);}
    public void printAsTree() {print();}

    /*
        tree to list
        -------------
        - ref: https://leetcode.com/problems/flatten-binary-tree-to-linked-list/solution/
        - approach 1: recursion (can make doubly since we need end....or return end since root pointer is already there)
        - approach 2: own stack
        - approach 3: morris traversal (0(n) and S(1))
     */
    public static BSTNode treeToList1(BSTNode root) {
        if (root == null) return root;
        BSTNode leftTail = treeToList1(root.left);
        BSTNode rightTail = treeToList1(root.right);
        if (leftTail != null) {
            leftTail.right = root.right;
            root.right = root.left;
            root.left = null;
            return rightTail == null ? leftTail : rightTail;
        } else {
            return rightTail == null ? root : rightTail;
        }
    }
    public static BSTNode treeToList2(BSTNode root) {
        if (root == null) return null;
        BSTNode tail = null;
        Stack<Tuple<BSTNode, Integer>> stack = new Stack<>();
        stack.push(new Tuple<>(root, 0));

        while(!stack.isEmpty()){
            Tuple<BSTNode, Integer> p = stack.pop();
            if (p.second == 0) {
                if (p.first.right != null) stack.push(new Tuple<>(p.first.right, 0));
                if (p.first.left != null) stack.push(new Tuple<>(p.first.left, 0));
                if (tail == null) {
                    tail = p.first;
                } else {
                    tail.right = p.first;
                    tail = tail.right;
                }
                tail.left = tail.right = null;
            }
        }

        return root;
    }
    public static BSTNode treeToList3(BSTNode root) {
        return treeToListUsingMorrisTraversal(root);
    }
    public static BSTNode treeToListUsingMorrisTraversal(BSTNode root) {
        BSTNode node = root;
        while (node != null) {
            if (node.left == null) {
                node = node.right;
            } else {
                BSTNode rightMost = node.left;
                while (rightMost.right != null) rightMost = rightMost.right;
                rightMost.right = node.right;
                node.right = node.left;
                node.left = null;
                node = node.right;
            }
        }
        return root;
    }

    /*
        - validate tree
            - approach 1: preorder top down: pass min-max
            - approach 2: inorder: get the inorder list and then check that it is sorted
            - approach 1: postorder: get min-max from both the child subtrees and check for required constraints and return result
     */

    /*
        Side View
        ----------
        - A1: BFS -> last node
            - A1.1: use two queues, one for current and one for next level
            - A1.2: one queue with sentinal
            - A1.3: one queue + size measurement
        - A2: using preorder (in preorder, right most nodes of each level are visited first)
     */

}

