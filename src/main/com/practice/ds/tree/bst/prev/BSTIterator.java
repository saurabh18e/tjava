package com.practice.ds.tree.bst.prev;

import com.practice.util.Tuple;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
// bst iterator: https://leetcode.com/problems/binary-search-tree-iterator/solution/
/*
    - approach1: convert bst to inorder list....not practical since this destroys the tree structure
    - approach2: custom stack and controlled recursion....
        - inorder dfs visit in graph with node coloring
        - shortcut: keep a stack of elements(being visited), whose left are visited
 */
public class BSTIterator {
    
    private Stack<Tuple<BSTNode, Integer>> stack;
    
    public BSTIterator(BSTNode root) {
        this.stack = new Stack<>();
        stack.push(new Tuple<>(root, 0));
    }
    
    public int next() {
        while (true) {
            Tuple<BSTNode, Integer> tpl = stack.pop();
            BSTNode node = tpl.first;
            Integer color = tpl.second;
            if (color == 1) return node.value;
            if (node.right != null) stack.push(new Tuple<>(node.right, 0));
            stack.push(new Tuple<>(node, 1));
            if (node.left != null) stack.push(new Tuple<>(node.left, 0));
        }
    }
    
    public boolean hasNext() {
        return !stack.isEmpty();
    }
    
    // tree serde: https://leetcode.com/explore/interview/card/facebook/56/design-3/271/
    /*
        - ref: https://www.geeksforgeeks.org/serialize-deserialize-binary-tree/
            - is tree bst: then only storing preorder or postorder is sufficient
            - is complete tree(as in heap): then storing bfs is sufficient
            - is full tree:
            - general tree
                - approach1: store preorder and inorder (requires 2n space)
                - approach2: store as full tree (store N for single or 0 child nodes)
     */
    public static class SerDe {
        
        // Encodes a tree to a single string.
        public String serialize(BSTNode root) {
            ArrayList<String> preorder = new ArrayList<>();
            serialize(root, preorder);
            String output = String.join(",", preorder);
            return output;
        }
        public void serialize(BSTNode root, List<String> preorder) {
            if (root == null) {
                preorder.add("null");
            } else {
                preorder.add(root.value + "");
                serialize(root.left, preorder);
                serialize(root.right, preorder);
            }
        }
        
        // Decodes your encoded data to tree.
        public BSTNode deserialize(String data) {
            String[] tokens = data.split(",");
            LinkedList<String> list = new LinkedList();
            for (String token : tokens) list.add(token);
            return deserialize(list);
        }
        public BSTNode deserialize(List<String> preorder) {
            String str = preorder.remove(0);
            if (str.equals("null")) {
                return null;
            } else {
                BSTNode node = new BSTNode(Integer.parseInt(str));
                node.left = deserialize(preorder);
                node.right = deserialize(preorder);
                return node;
            }
        }
        
    }
}
