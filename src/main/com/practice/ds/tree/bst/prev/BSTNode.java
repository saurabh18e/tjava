package com.practice.ds.tree.bst.prev;

class BSTNode {
    int value;
    BSTNode left;
    BSTNode right;

    public BSTNode() {
        this(0, null, null);
    }

    public BSTNode(int value) {
        this(value, null, null);
    }

    public BSTNode(int value, BSTNode left, BSTNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public boolean isLeafNode() {
        return this.left == null && this.right == null;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
