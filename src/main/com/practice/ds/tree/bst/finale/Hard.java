package com.practice.ds.tree.bst.finale;

import java.util.*;

import static com.practice.ds.tree.bst.finale.Basic.*;
import static com.practice.ds.tree.bst.finale.BinarySearchInBST.*;
import static com.practice.ds.tree.bst.finale.BstUtil.*;
import static com.practice.ds.tree.finale.TreeUtil.*;
import static com.practice.util.Util.*;

public class Hard {
}

class CRUD {
    
    // in case of crud operation in a tree,
    // we always follow philosophy of return root of new tree post crud
    
    public TreeNode insert(TreeNode root, int val) {
        TreeNode nnd = new TreeNode(val);
        if (root == null) return nnd;
        TreeNode iter = root, prev = null;
        while (iter != null) {
            prev = iter;
            iter = (iter.val >= val) ? iter.left : iter.right;
        }
        if (prev.val >= val) prev.left = nnd;
        else prev.right = nnd;
        return root;
    }
    
    /*
        step1: search, keep track of previous
        step2: find newRoot while updating the search subtree (algo, no height balancing)
        step3: update previous with newRoot
     */
    public TreeNode delete(TreeNode root, int val) {
        if (root == null) return null;
        TreeNode iter = root, prev = null;
        while (iter != null && iter.val != val) {
            prev = iter;
            iter = (iter.val > val) ? iter.left : iter.right;
        }
        
        // key not found
        if (iter == null) return root;
        // key found
        TreeNode left = iter.left;
        TreeNode right = iter.right;
        iter.left = iter.right = null;
        
        TreeNode newRoot = null;
        // right subtree exists
        if (right != null) {
            TreeNode minOfRight = min(right);
            minOfRight.left = left;
            newRoot = right;
        } else {
            newRoot = left;
        }
        
        // update previous and return root
        if (prev == null) {
            return newRoot;
        } else {
            if (iter == prev.left)
                prev.left = newRoot;
            else
                prev.right = newRoot;
            return root;
        }
    }
    
    /*
        step1: search, keep track of previous
        step2: find newRoot while updating the search subtree (algo, height balanced)
        step3: update previous with newRoot
     */
    public TreeNode deleteHeightBalanced(TreeNode root, int val) {
        if (root == null) return null;
        TreeNode iter = root, prev = null;
        while (iter != null && iter.val != val) {
            prev = iter;
            iter = (iter.val > val) ? iter.left : iter.right;
        }
        
        // key not found
        if (iter == null) return root;
        // key found
        TreeNode left = iter.left;
        TreeNode right = iter.right;
        
        // find and update newRoot
        TreeNode newRoot = null;
        // right subtree exists
        if (right != null) {
            TreeNode minIter = right, minPrev = iter;
            while (minIter.left != null) {
                minPrev = minIter;
                minIter = minIter.left;
            }
            
            iter.val = minIter.val;
            if (minIter == right) {
                minPrev.right = minIter.right;
                minIter.right = null;
            } else {
                minPrev.left = minIter.right;
            }
            newRoot = iter;
        } else {
            newRoot = left;
            iter.left = iter.right = null;
        }
        
        // update previous and return root
        if (prev == null) {
            return newRoot;
        } else {
            if (iter == prev.left)
                prev.left = newRoot;
            else
                prev.right = newRoot;
            return root;
        }
        
    }
    
}

class Codec {
    
    // simple recursive code but it has bad perf
    public static TreeNode deserializeV1(int[] preorder) {
        if (preorder == null || preorder.length == 0) return null;
        return deserializeHelperV1(preorder, 0, null, null);
    }
    private static TreeNode deserializeHelperV1(int[] preorder, int i, Integer min, Integer max) {
        if (i >= preorder.length) return null;
        if (min != null && preorder[i] < min)
            return deserializeHelperV1(preorder, i + 1, min, max);
        if (max != null && preorder[i] > max)
            return null;
        
        TreeNode root = new TreeNode(preorder[i]);
        root.left = deserializeHelperV1(preorder, i + 1, min, root.val);
        root.right = deserializeHelperV1(preorder, i + 1, root.val, max);
        return root;
    }
    
    public static TreeNode deserializeV2(int[] preorder) {
        if (preorder == null || preorder.length == 0) return null;
        return deserializeHelperV2(preorder, new int[]{0}, null, null);
    }
    private static TreeNode deserializeHelperV2(int[] preorder, int[] index, Integer min, Integer max) {
        if (index[0] >= preorder.length) return null;
        if (min != null && preorder[index[0]] < min) return null;
        if (max != null && preorder[index[0]] > max) return null;
        TreeNode root = new TreeNode(preorder[index[0]]);
        index[0]++; // global index
        root.left = deserializeHelperV2(preorder, index, min, root.val);
        root.right = deserializeHelperV2(preorder, index, root.val, max);
        return root;
    }
    
    // Decodes your encoded data to tree.
    public static TreeNode deserialize(String data) {
        if (data == null || data.trim().isEmpty()) return null;
        String[] tokens = data.split(",");
        int[] arr = new int[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            arr[i] = Integer.parseInt(tokens[i].trim());
        }
        return deserializeV2(arr);
    }
    
    // Encodes a tree to a single string.
    public static String serialize(TreeNode root) {
        if (root == null) return "";
        StringBuilder builder = new StringBuilder();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            if (curr == null) continue;
            
            builder.append(curr.val);
            builder.append(",");
            stack.push(curr.right);
            stack.push(curr.left);
        }
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }
    
    public static void main(String[] args) {
        Codec codec = new Codec();
        String str = codec.serialize(dummyTree());
        System.out.println(str);
        TreeNode root = codec.deserialize(str);
        printPreorder(root);
        System.out.println("\n" + codec.serialize(root));
    }
    
}

class TotalOrdering {
    
    private static boolean checkTotalOrdering(TreeNode root, int val1, int mid, int val2) {
        TreeNode node1 = search(root, val1);
        if (node1 == null) return false;
        TreeNode middle = search(root, mid);
        if (middle == null) return false;
        TreeNode node2 = search(root, val2);
        if (node2 == null) return false;
        return checkTotalOrdering(node1, middle, node2);
    }
    // simultaneous in-tandem search
    private static boolean checkTotalOrdering(TreeNode node1, TreeNode mid, TreeNode node2) {
        // move towards mid
        TreeNode iter1 = node1, iter2 = node2;
        while ((iter1 != null && iter1.val != mid.val && iter1 != node2)
            || (iter2 != null && iter2.val != mid.val && iter2 != node1)) {
            if (iter1 != null) iter1 = (mid.val > iter1.val) ? iter1.right : iter1.left;
            if (iter2 != null) iter2 = (mid.val > iter2.val) ? iter2.right : iter2.left;
        }
        
        if (iter1 != mid && iter2 != mid) return false;
        
        TreeNode start = (iter1 == mid) ? iter1 : iter2;
        TreeNode end = (iter1 == mid) ? iter2 : iter1;
        while (start != end && start != null) {
            start = (start.val < end.val) ? start.right : start.left;
        }
        return (start == end);
    }
    
}

class OrderStaticTree {
    
    // generally order static tree have size field, but we can also use external map
    private HashMap<TreeNode, Integer> sizeInfo = new HashMap<>();
    private TreeNode root;
    
    private OrderStaticTree(TreeNode root) {
        this.root = root;
        updateSizeInfo(root);
    }
    private void updateSizeInfo(TreeNode node) {
        if (node == null) return;
        updateSizeInfo(node.left);
        updateSizeInfo(node.right);
        int total = size(node.left) + size(node.right) + 1;
        sizeInfo.put(node, total);
    }
    
    private int size(TreeNode node) {
        if (node == null) return 0;
        return sizeInfo.getOrDefault(node, 0);
    }
    
    public TreeNode kthSmallest(int k) {
        return kthSmallest(this.root, k);
    }
    private TreeNode kthSmallest(TreeNode node, int k) {
        if (node == null || k <= 0) return null;
        int leftCount = size(node.left);
        if (leftCount + 1 == k) {
            return node;
        } else if (leftCount + 1 < k) {
            return kthSmallest(node.right, k - (leftCount + 1));
        } else {
            return kthSmallest(node.left, k);
        }
    }
    
    private int rank(int i) {
        int rank = 0;
        TreeNode iter = root;
        while (iter != null) {
            if (i == iter.val) {
                rank = rank + size(iter.left) + 1;
                break;
            } else if (i > iter.val) {
                rank = rank + size(iter.left) + 1;
                iter = iter.right;
            } else {
                iter = iter.left;
            }
        }
        return rank;
    }
    private int size(int low, int high) {
        return rank(high) - rank(low) + 1;
    }
    private int countOfElementsInRange(int low, int high) {
        return size(low, high);
    }
    
    private List<Integer> getElementsInRange(int low, int high) {
        List<Integer> output = new ArrayList<>();
        populateElementsInRange(this.root, low, high, output);
        return output;
    }
    private void populateElementsInRange(TreeNode node, int low, int high, List<Integer> output) {
        if (node == null) return;
        if (node.val >= low) populateElementsInRange(node.left, low, high, output);
        if (low <= node.val && node.val <= high) output.add(node.val);
        if (node.val <= high) populateElementsInRange(node.right, low, high, output);
        
    }
    
    public static int rangeSum(TreeNode node, int low, int high) {
        int sum = 0;
        if (node == null) return sum;
        if (node.val >= low && node.val <= high) sum += node.val;
        if (node.val >= low) sum += rangeSum(node.left, low, high);
        if (node.val <= high) sum += rangeSum(node.right, low, high);
        return sum;
    }
    
    private static void testKthSmallest() {
        TreeNode tree = dummyTree();
        System.out.println("tree inorder is: " + inorderString(tree));
        System.out.println("tree preorder is: " + preorderString(tree));
        OrderStaticTree osTree = new OrderStaticTree(tree);
        for (int i = 0; i < 10; i++) {
            System.out.println("node " + i + " is: " + osTree.kthSmallest(i));
        }
    }
    
    private static void printRangeCount(OrderStaticTree tree, int a, int b) {
        System.out.println("rank of " + a + " is: " + tree.rank(a));
        System.out.println("rank of " + b + " is: " + tree.rank(b));
        System.out.println(tree.countOfElementsInRange(a, b));
        int[] arr = toIntArray(tree.getElementsInRange(a, b));
        System.out.println("elements in range are: " + Arrays.toString(arr));
    }
    private static void testRangeCount() {
        TreeNode tree = dummyTree();
        System.out.println("tree inorder is: " + inorderString(tree));
        OrderStaticTree osTree = new OrderStaticTree(tree);
        printRangeCount(osTree, 45, 70);
        printRangeCount(osTree, 40, 45);
        printRangeCount(osTree, 60, 68);
    }
    public static void main(String[] args) {
        // testKthSmallest();
        testRangeCount();
    }
    
}

class PracticalProblems {
    
    /*
        ques: find nth number of the form a+sqrt(b), where a=1 and b=sqrt(2)
        algo0 (brute force):
            - create n^2 numbers by inc a or by inc b
            - sort and select nth
        algo1 (incremental):
            - maintain a sorted list of numbers and iterate till n
            - since we have to maintain a dynamic sorted list, it's better to use bst
        algo2 (incremental shrink):
            - maintain separate list and iterate till n
     */
    public static double aPlusSqrtB_algo1(int n) {
        double a = 1, b = Math.sqrt(2);
        List<Double> list = new ArrayList<>(2 * n);
        for (int num1 = 0; num1 < n; num1++) {
            for (int num2 = 0; num2 < n; num2++) {
                list.add(num1 * a + num2 * b);
            }
        }
        Collections.sort(list);
        return list.get(n - 1);
    }
    
    // same index in sorted list: add to smallest
    public static double aPlusSqrtB_algo2(int n) {
        double a = 1, b = Math.sqrt(2);
        // heap could have been used but heap cannot take care of dups, so we need sorted set
        // PriorityQueue<Double> minHeap = new PriorityQueue<>();
        TreeSet<Double> sortedSet = new TreeSet<>();
        sortedSet.add(0.0);
        int seen = 0;
        while (seen < n) {
            double num = sortedSet.pollFirst();
            seen++;
            if (seen == n) return num;
            sortedSet.add(num + a);
            sortedSet.add(num + b);
        }
        return -1;
    }
    
    // different indexes in sorted list
    public static double aPlusSqrtB_algo3(int n) {
        double a = 1, b = Math.sqrt(2);
        List<Double> sortedList = new ArrayList<>();
        sortedList.add(0.0);
        int i = 0, j = 0;
        while (sortedList.size() < n) {
            double num1 = sortedList.get(i) + a;
            double num2 = sortedList.get(j) + b;
            int cmp = compareDouble(num1, num2);
            sortedList.add(cmp <= 0 ? num1 : num2);
            if (cmp == 0) {
                i++;
                j++; // avoid duplicates
            } else if (cmp < 0) {
                i++;
            } else {
                j++;
            }
        }
        return sortedList.get(n - 1);
    }
    
    public static int compareDouble(double num1, double num2) {
        double epsilon = 0.00000001;
        if (Math.abs(num1 - num2) < epsilon) return 0;
        return Double.compare(num1, num2);
    }
    
    public static void testAPlusSqrtB(int n) {
        double epsilon = 0.0000001d;
        for (int i = 9; i < n; i++) {
            double num1 = aPlusSqrtB_algo1(i);
            double num2 = aPlusSqrtB_algo2(i);
            double num3 = aPlusSqrtB_algo3(i);
            if (compareDouble(num1, num2) != 0 || compareDouble(num2, num3) != 0) System.out.println("Algo results mismatch for index "
                + i + " : " + num1 + " != " + num2 + " != " + num3);
        }
    }
    
    public static void main(String[] args) {
        testAPlusSqrtB(10);
    }
    
    /////////// min distance/closest in k arrays //////
    
    /*
        algo: use greedy algo: pick the smallest and increment it
              use min-heap (though the book says that bst should be used)
     */
    public static int minDistanceInArrays(List<List<Integer>> lists) {
        return 0;
    }
    
}

class CheckBSTValidity {
    
    // check bst
    public boolean isValidBST(TreeNode root) {
        return isValidBSTIter(root);
    }
    
    public boolean isValidBSTRec(TreeNode root) {
        // null means true infinity
        return checkBSTRange(root, null, null);
    }
    
    private boolean checkBSTRange(TreeNode root, Integer minValue, Integer maxValue) {
        if (root == null) return true;
        if (!inRange(root, minValue, maxValue)) return false;
        return checkBSTRange(root.left, minValue, root.val)
            && checkBSTRange(root.right, root.val, maxValue);
    }
    
    // check bst - iterative
    class StackContext {
        public TreeNode node;
        public Integer min;
        public Integer max;
        public StackContext(TreeNode node, Integer min, Integer max) {
            this.node = node;
            this.min = min;
            this.max = max;
        }
    }
    public boolean isValidBSTIter(TreeNode root) {
        if (root == null) return true;
        Stack<StackContext> stack = new Stack<>();
        stack.push(new StackContext(root, null, null));
        while (!stack.isEmpty()) {
            StackContext ctxt = stack.pop();
            if (!inRange(ctxt.node, ctxt.min, ctxt.max)) return false;
            if (ctxt.node.right != null) stack.push(new StackContext(ctxt.node.right, ctxt.node.val, ctxt.max));
            if (ctxt.node.left != null) stack.push(new StackContext(ctxt.node.left, ctxt.min, ctxt.node.val));
        }
        return true;
    }
    private boolean inRange(TreeNode node, Integer min, Integer max) {
        if (min != null && node.val <= min) return false;
        if (max != null && node.val >= max) return false;
        return true;
    }
    
    // check bst inorder walk invariant check with global state
    public static boolean isValidBstInorderWalk(TreeNode root) {
        Integer[] prev = new Integer[]{null};
        return isValidBstInorderWalk(root, prev);
    }
    private static boolean isValidBstInorderWalk(TreeNode root, Integer[] prev) {
        if (root == null) return true;
        if (!isValidBstInorderWalk(root.left, prev)) return false;
        if (prev[0] != null && prev[0] >= root.val) return false;
        prev[0] = root.val;
        return isValidBstInorderWalk(root.right, prev);
    }
}


class ClosestInKArray {
    
    /*
        question:
            closest in k sorted arrays: pick entries from k sorted lists suck that range in minimum
        solution:
            use greedy algo and move the min
            since we need min and max for range calculation, use BST
     */
    
    private static class ArrayInfo implements Comparable<ArrayInfo> {
        public int listIndex;
        public int elementIndex;
        public int elementValue;
        
        public ArrayInfo(int listIndex, int elementIndex, int elementValue) {
            this.listIndex = listIndex;
            this.elementIndex = elementIndex;
            this.elementValue = elementValue;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ArrayInfo)) return false;
            ArrayInfo arrayInfo = (ArrayInfo) o;
            return listIndex == arrayInfo.listIndex && elementIndex == arrayInfo.elementIndex;
        }
        @Override
        public int hashCode() {
            return Objects.hash(listIndex, elementIndex);
        }
        
        @Override
        public int compareTo(ArrayInfo o) {
            int cmp1 = Integer.compare(this.elementValue, o.elementValue);
            if (cmp1 != 0) return cmp1;
            // does not matter here but better to use order if equal
            return Integer.compare(this.listIndex, o.listIndex);
        }
    }
    
    public static int closestInKSortedLists(List<List<Integer>> lists) {
        TreeSet<ArrayInfo> state = new TreeSet<>();
        for (int i=0; i<lists.size(); i++) {
            state.add(new ArrayInfo(i, 0, lists.get(i).get(0)));
        }
        
        int result = Integer.MAX_VALUE;
        while (true) {
            result = Math.min(result,
                Math.abs(state.first().elementValue - state.last().elementValue));
            ArrayInfo ai = state.pollFirst();
            ai.elementIndex++;
            if (ai.elementIndex == lists.get(ai.listIndex).size()) break;
            ai.elementValue = lists.get(ai.listIndex).get(ai.elementIndex);
            state.add(ai);
        }
        return result;
    }
    public static void main(String[] args) {
        List<List<Integer>> lists = Arrays.asList(Arrays.asList(5, 10, 15),
            Arrays.asList(3,6,9,12,15), Arrays.asList(8,16,24));
        System.out.println("closest interval is: " + closestInKSortedLists(lists));
    }
}
