package com.practice.ds.tree.bst.finale;

public class Easy {

}

class Basic {
    public static TreeNode min(TreeNode node) {
        if (node == null) return null;
        while (node.left != null) node = node.left;
        return node;
    }
    
    public static TreeNode max(TreeNode node) {
        if (node == null) return null;
        while (node.right != null) node = node.right;
        return node;
    }
    
    public static boolean isLeaf(TreeNode node) {
        return node != null && node.left == null && node.right == null;
    }
}

class SortedArrayToBst {
    
    public TreeNode sortedArrayToBST(int[] nums) {
        if (nums == null || nums.length == 0) return null;
        return sortedArrayToBSTRec(nums, 0, nums.length-1);
    }
    private TreeNode sortedArrayToBSTRec(int[] nums, int start, int end) {
        if (start > end) return null;
        if (start == end) return new TreeNode(nums[start]);
        int mid = start + (end-start)/2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = sortedArrayToBSTRec(nums, start, mid-1);
        root.right = sortedArrayToBSTRec(nums, mid+1, end);
        return root;
    }
    
}

class LCA {
    
    public static TreeNode lca(TreeNode root, TreeNode a, TreeNode b) {
        if (a.val > b.val) return lca(root, b, a);
        
        TreeNode iter = root;
        while (iter != null) {
            if (a.val <= iter.val && iter.val <= b.val) break;
            iter = (iter.val < a.val) ? iter.right : iter.left;
        }
        return iter;
    }
    
}

class TrimBST {
    
    public static TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) return null;
        if (root.val < low) {
            return trimBST(root.right, low, high);
        }
        if (root.val > high) {
            return trimBST(root.left, low, high);
        }
        root.left = trimBST(root.left, low, high);
        root.right = trimBST(root.right, low, high);
        return root;
    }
    
}

