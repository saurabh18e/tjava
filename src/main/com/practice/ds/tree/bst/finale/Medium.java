package com.practice.ds.tree.bst.finale;

import java.util.*;

import static com.practice.ds.tree.bst.finale.BstUtil.*;
import static com.practice.ds.tree.finale.TreeUtil.buildTreeFromLevelOrder;
import static com.practice.ds.tree.finale.TreeUtil.inorderString;

public class Medium {
}

class BinarySearchInBST {
    
    // bst binary-search
    public static TreeNode search(TreeNode root, int val) {
        return searchIter(root, val);
    }
    
    public static TreeNode searchRec(TreeNode root, int val) {
        if (root == null) return null;
        if (root.val == val) return root;
        return root.val > val ? searchRec(root.left, val) : searchRec(root.right, val);
    }
    
    public static TreeNode searchIter(TreeNode root, int val) {
        TreeNode iter = root;
        while (iter != null) {
            if (iter.val == val) return iter;
            iter = (iter.val > val) ? iter.left : iter.right;
        }
        return iter;
    }
    
    public static void testSearch() {
        TreeNode root = dummyTree();
        System.out.println(inorderString(root));
        for (int i = 0; i < 5; i++) {
            if (searchIter(root, i) != searchRec(root, i)) {
                throw new RuntimeException("diff found in iter and rec searches");
            }
            System.out.println("search: " + i + " -> " + search(root, i));
        }
    }
    
    // find first occurrence in bst
    public static TreeNode findFirst(TreeNode root, int i) {
        TreeNode curr = root, lastSeen = null;
        while (curr != null) {
            if (curr.val == i) {
                lastSeen = curr;
                curr = curr.left;
            } else if (curr.val > i) {
                curr = curr.left;
            } else {
                curr = curr.right;
            }
        }
        return lastSeen;
    }
    public static void testFindFirst() {
        TreeNode root = buildTreeFromLevelOrder(10, 9, null, 5, null, 5, 5, 5, null, null, null, 1, null, null, null);
        int key = 5;
        System.out.println(findFirst(root, key).left);
        System.out.println(findFirst(root, key).right);
    }
    
    // find greater key
    public static TreeNode nextGreater(TreeNode root, int i) {
        TreeNode curr = root, lastGreater = null;
        while (curr != null) {
            if (curr.val > i) {
                lastGreater = curr;
                curr = curr.left;
            } else {
                curr = curr.right;
            }
        }
        return lastGreater;
    }
    public static void testNextGreater() {
        TreeNode root = dummyTree();
        System.out.println(nextGreater(root, 45));
        System.out.println(nextGreater(root, 70));
    }
    
    public static void main(String[] args) {
        testNextGreater();
    }
    
}

class KthOrderStatic {
    
    // find kth order static in tree without augmentation
    // kth-min = inorder kth, kth-max = reverse inorder kth
    public TreeNode kthSmallest(TreeNode root, int k) {
        Stack<TreeNode> stack = new Stack<>();
        int nodesVisited = 0;
        TreeNode curr = root;
        while (!stack.isEmpty() || curr != null) {
            if (curr != null) {
                stack.push(curr);
                curr = curr.left;
            } else {
                curr = stack.pop();
                // visit the node
                nodesVisited++;
                if (nodesVisited == k) return curr;
                curr = curr.right;
            }
        }
        return null;
    }
    
    // recursive code
    public static List<Integer> kthLargest(TreeNode root, int k) {
        if (k <= 0) return Collections.emptyList();
        List<Integer> output = new ArrayList<>();
        kthLargestRec(root, k, output);
        return output;
    }
    
    public static void kthLargestRec(TreeNode root, int k, List<Integer> output) {
        if (root != null) {
            if (output.size() < k) kthLargestRec(root.right, k, output);
            if (output.size() < k) output.add(root.val);
            if (output.size() < k) kthLargestRec(root.left, k, output);
        }
    }
    
}

class MinDiffInBST {
    
    private static class GlobalState {
        public Integer lastVisited, minDiff;
        
    }
    private void minimumDifferenceRec(TreeNode root, GlobalState globalState) {
        if (root == null) return;
        minimumDifferenceRec(root.left, globalState);
        if (globalState.lastVisited != null) {
            // update min diff
            globalState.minDiff = Math.min(globalState.minDiff,
                Math.abs(root.val - globalState.lastVisited));
        }
        // update last visited
        globalState.lastVisited = root.val;
        minimumDifferenceRec(root.right, globalState);
    }
    
    public int minimumDifference(TreeNode root) {
        GlobalState globalState = new GlobalState();
        globalState.lastVisited = null;
        globalState.minDiff = Integer.MAX_VALUE;
        minimumDifferenceRec(root, globalState);
        return globalState.minDiff;
    }
    
}

class BSTCount {
    
    public static int countBST(int nodeCount) {
        if (nodeCount == 0) return 1;
        int sum = 0;
        // make every node a root
        for (int i = 1; i <= nodeCount; i++) {
            sum += countBST(i - 1) * countBST(nodeCount - i);
        }
        return sum;
    }
    
    public static int countBSTMemoized(int nodeCount) {
        HashMap<Integer, Integer> cache = new HashMap<>();
        cache.put(0, 1);
        cache.put(1, 1);
        return countBSTMemoized(nodeCount, cache);
    }
    private static int countBSTMemoized(int nodeCount, HashMap<Integer, Integer> cache) {
        if (cache.containsKey(nodeCount)) return cache.get(nodeCount);
        int sum = 0;
        // make every node a root
        for (int i = 1; i <= nodeCount; i++) {
            sum += countBSTMemoized(i - 1, cache) * countBSTMemoized(nodeCount - i, cache);
        }
        return sum;
    }
    
    public static int countBSTDyPro(int nodeCount) {
        if (nodeCount <= 1) return 1;
        int[] table = new int[nodeCount + 1];
        table[0] = table[1] = 1;
        
        for (int i = 2; i <= nodeCount; i++) {
            int sum = 0;
            for (int j = 1; j <= i; j++) {
                sum += table[j - 1] * table[i - j];
            }
            table[i] = sum;
        }
        
        return table[nodeCount];
    }
    
    public List<TreeNode> generateTrees(int n) {
        return allTrees(1, n);
    }
    public static List<TreeNode> allTrees(int low, int high) {
        if (low > high) return Collections.singletonList(null);
        if (low == high) return Collections.singletonList(new TreeNode(low));
        
        List<TreeNode> output = new ArrayList<>();
        // make every node a root
        for (int i = low; i <= high; i++) {
            List<TreeNode> left = allTrees(low, i - 1);
            List<TreeNode> right = allTrees(i+1, high);
            for (TreeNode l : left) {
                for (TreeNode r : right) {
                    output.add(new TreeNode(i, l, r));
                }
            }
        }
        return output;
    }
    
    public List<TreeNode> generateTreesMemoized(int n) {
        HashMap<Pair, List<TreeNode>> cache = new HashMap<>();
        return allTreesMemoized(1, n, cache);
    }
    static class Pair {
        public int low, high;
        public Pair(int low, int high) {
            this.low = low;
            this.high = high;
        }
        @Override
        public boolean equals(Object other) {
            if (other == null || !(other instanceof Pair)) return false;
            if (other == this) return true;
            Pair o = (Pair) other;
            return low == o.low && high == o.high;
        }
        @Override
        public int hashCode() {
            return Objects.hash(low, high);
        }
    
    }
    public static List<TreeNode> allTreesMemoized(int low, int high, HashMap<Pair, List<TreeNode>> cache) {
        Pair p = new Pair(low, high);
        if (cache.containsKey(p)) return cache.get(p);
        if (low > high) return Collections.singletonList(null);
        if (low == high) return Collections.singletonList(new TreeNode(low));
        
        List<TreeNode> output = new ArrayList<>();
        // make every node a root
        for (int i = low; i <= high; i++) {
            List<TreeNode> left = allTreesMemoized(low, i - 1, cache);
            List<TreeNode> right = allTreesMemoized(i+1, high, cache);
            for (TreeNode l : left) {
                for (TreeNode r : right) {
                    output.add(new TreeNode(i, l, r));
                }
            }
        }
        cache.put(p, output);
        return output;
    }
}

class DataStructureDesign {
    
    /*
        question:
            given list of page id in files, design a data structure to quickly find top-k entries
            keep in mind that the data structure will have to have streaming updates 24x7
        answer
            lookup(count by id) -> hashmap
            lookup + order static
                -> hashmap + bst -> o(k + log h), using threaded/parent pointers from max
                -> hashmap + heap -> o(1) if k is fixed i.e. top(k)
                -> hashmap + deque -> for top(1) -> LRU design
     */
    
    /*
        question:
            ref: https://leetcode.com/problems/kth-smallest-element-in-a-bst/
            design a data structure and supports fast insert, delete, sorted, k-order-static
        answer
            insert-delete -> hashmap
            insert-delete-sorted -> bst
            insert-delete-sorted+order-static
                -> order static tree -> o(k + log h)
                -> o(k), by keeping pointer to max and iterating back using parent pointer
                -> o(1), by keeping heap if k is fixed
     */
    
    
    /*
        question: max credit client
            1. 0(1) crud on client-credit
            2. add credit to all
            3. 0(1) find clients with max credit
        solution
            sol1:
                - client-to-credit hashmap
                - max-heap of credit with class(credit, Set<ClientId>)
                  (but this will make updating client-credit difficult, so use bst TreeMap)
            sol2:
                - client-to-credit hashmap
                - treeMap of credit-to-Set<ClientId>
                - bst set/map often caches min/max
            sol3:
                - we do not need to keep client sorted by credit, so no need for bst
                - client-to-credit hashmap
                - credit-to-client hashmap
                - max-heap for all credits seen so far
                - rule of thumb: if you cannot use heap, fallback to bst set/tree (TreeSet/TreeMap)
     */
}
