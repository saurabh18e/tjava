package com.practice.ds.tree.bst.finale;

import static com.practice.ds.tree.finale.TreeUtil.*;

public class BstUtil {
    
    public static TreeNode dummyTree() {
        return buildTreeFromLevelOrder(50, 40, 60, 30, 45, null, 70, null, null, 42, null, 65, null, null, null, null, 68);
    }
    
}
