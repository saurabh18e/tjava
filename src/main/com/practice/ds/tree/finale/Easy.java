package com.practice.ds.tree.finale;

import com.practice.ds.tree.bst.finale.*;

import java.util.*;

public class Easy {
}

class Basic {
    
    public static int goodNodes(TreeNode root) {
        return goodNodes(root, null);
    }
    public static int goodNodes(TreeNode root, Integer maxSoFar) {
        if (root == null) return 0;
        boolean isGoodNode = (maxSoFar == null || root.val >= maxSoFar);
        maxSoFar = (maxSoFar == null) ? root.val : Math.max(maxSoFar, root.val);
        return (isGoodNode ? 1 : 0) + goodNodes(root.left, maxSoFar) + goodNodes(root.right, maxSoFar);
    }
    
    public static int maxNodeToAncestorDiff(TreeNode root) {
        return maxNodeToAncestorDiffRec(root, null, null);
    }
    public static int maxNodeToAncestorDiffRec(TreeNode root, Integer minSoFar, Integer maxSoFar) {
        int maxDiff = 0;
        if (root == null) return maxDiff;
        
        if (minSoFar != null) {
            maxDiff = Math.max(maxDiff, Math.abs(root.val - minSoFar));
        }
        minSoFar = minSoFar == null ? root.val : Math.min(minSoFar, root.val);
    
        if (maxSoFar != null) {
            maxDiff = Math.max(maxDiff, Math.abs(root.val - maxSoFar));
        }
        maxSoFar = maxSoFar == null ? root.val : Math.max(maxSoFar, root.val);
        
        maxDiff = Math.max(maxDiff, maxNodeToAncestorDiffRec(root.left, minSoFar, maxSoFar));
        maxDiff = Math.max(maxDiff, maxNodeToAncestorDiffRec(root.right, minSoFar, maxSoFar));
        
        return maxDiff;
    }
}

class Stanford {
    
    public static boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) return false;
        if (root.left == null && root.right == null && root.val == sum) return true;
        return hasPathSum(root.left, sum-root.val) || hasPathSum(root.right, sum-root.val);
    }
    public static boolean hasPathSumIter(TreeNode root, int sum) {
        if (root == null) return false;
        Stack<TreeNode> stack = new Stack<>();
        Stack<Integer> pathSum = new Stack<>();
        stack.push(root);
        pathSum.push(root.val);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            int pathSumFromRoot = pathSum.pop();
            if (node.left == null && node.right == null) {
                if (pathSumFromRoot == sum) return true;
            }
            if (node.left != null) {
                stack.push(node.left);
                pathSum.push(pathSumFromRoot + node.left.val);
            }
            if (node.right != null) {
                stack.push(node.right);
                pathSum.push(pathSumFromRoot + node.right.val);
            }
        }
        return false;
    }
    
    /////// all root to leaf paths ////////////
    
    public static void allRootToLeafPathRec1(
        TreeNode root, List<Integer> currentPath, List<List<Integer>> allPaths) {
        if (root == null) return;;
        currentPath.add(root.val);
        if (root.left == null && root.right == null) {
            allPaths.add(new ArrayList<>(currentPath));
        } else {
            allRootToLeafPathRec1(root.left, currentPath, allPaths);
            allRootToLeafPathRec1(root.right, currentPath, allPaths);
        }
        currentPath.remove(currentPath.size()-1);
    }
    public static List<String> allRootToLeafPath1(TreeNode root) {
        List<List<Integer>> allPaths = new ArrayList<>();
        List<Integer> currentPath = new ArrayList<>();
        allRootToLeafPathRec1(root, currentPath, allPaths);
        List<String> output = new ArrayList<>(allPaths.size());
        for (List<Integer> path : allPaths) {
            StringBuilder builder = new StringBuilder();
            for (Integer node : path) {
                builder.append(node);
                builder.append("->");
            }
            if (builder.length() > 0) builder.delete(builder.length()-2, builder.length());
            output.add(builder.toString());
        }
        return output;
    }
    
    public static void allRootToLeafPathRec2(
        TreeNode root, StringBuilder currentPath, List<String> allPaths) {
        if (root == null) return;
        int len = currentPath.length();
        currentPath.append(root.val);
        if (root.left == null && root.right == null) {
            allPaths.add(currentPath.toString());
        } else {
            currentPath.append("->");
            allRootToLeafPathRec2(root.left, currentPath, allPaths);
            allRootToLeafPathRec2(root.right, currentPath, allPaths);
            currentPath.delete(currentPath.length()-2, currentPath.length());
        }
        currentPath.delete(len, currentPath.length());
    }
    public static List<String> allRootToLeafPath2(TreeNode root) {
        List<String> allPaths = new ArrayList<>();
        StringBuilder currentPath = new StringBuilder();
        allRootToLeafPathRec2(root, currentPath, allPaths);
        return allPaths;
    }
    
    //// sum of all root to leaf paths - paths as decimal numbers //////
    public static int sumOfAllRootToLeafDecimalNumbers(TreeNode root) {
        return sumOfAllRootToLeafDecimalNumbers(root, 0);
    }
    private static int sumOfAllRootToLeafDecimalNumbers(TreeNode root, int numSoFar) {
        if (root == null) return 0;
        numSoFar = numSoFar*10 + root.val;
        if (root.left == null && root.right == null) return numSoFar;
        int sum = 0;
        sum += sumOfAllRootToLeafDecimalNumbers(root.left, numSoFar);
        sum += sumOfAllRootToLeafDecimalNumbers(root.right, numSoFar);
        return sum;
    }
    
    //// sum of all root to leaf paths - path as binary numbers //////
    public static int sumOfAllRootToLeafBinaryNumbers(TreeNode root) {
        return sumOfAllRootToLeafBinaryNumbers(root, 0);
    }
    private static int sumOfAllRootToLeafBinaryNumbers(TreeNode root, int numSoFar) {
        if (root == null) return 0;
        numSoFar = numSoFar*2 + root.val;
        if (root.left == null && root.right == null) return numSoFar;
        int sum = 0;
        sum += sumOfAllRootToLeafBinaryNumbers(root.left, numSoFar);
        sum += sumOfAllRootToLeafBinaryNumbers(root.right, numSoFar);
        return sum;
    }
    
    
    /////////  tree mirror  ////////////
    
    public static TreeNode mirror(TreeNode root) {
        if (root == null) return root;
        TreeNode tmp = root.left;
        root.left = root.right;
        root.right = tmp;
        mirror(root.left);
        mirror(root.right);
        return root;
    }
    
    // check mirror symmetry
    public static boolean checkSymmetry(TreeNode root1, TreeNode root2) {
        // if atleast one is null
        if (root1 == null || root2 == null) return root1 == root2;
        // both are not null
        if (root1.val != root2.val) return false;
        return checkSymmetry(root1.left, root2.right) && checkSymmetry(root1.right, root2.left);
    }
    public static boolean checkSymmetry(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) return true;
        return checkSymmetry(root.left, root.right);
    }
    
    // check if tree are exactly same
    public boolean sameTree(TreeNode p, TreeNode q) {
        if (p == null || q == null) return p == q;
        if (p.val != q.val) return false;
        return sameTree(p.left, q.left) && sameTree(p.right, q.right);
    }
    
    /// double tree ////
    public static TreeNode doubleTree(TreeNode root) {
        if (root == null) return null;
        
        root.left = doubleTree(root.left);
        root.right = doubleTree(root.right);
        
        TreeNode nnd = new TreeNode(root.val);
        nnd.left = root.left;
        root.left = nnd;
        
        return root;
    }
}
