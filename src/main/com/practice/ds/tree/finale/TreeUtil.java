package com.practice.ds.tree.finale;

import com.practice.ds.tree.bst.finale.*;

import java.util.*;

import static com.practice.util.Util.toIntegerArray;
import static com.practice.util.Util.toStringArray;

public class TreeUtil {
    
    public static String inorderString(TreeNode tree) {
        return inorderString(tree, false);
    }
    
    public static String preorderString(TreeNode tree) {
        return preorderString(tree, false);
    }
    public static String inorderString(TreeNode tree, boolean withNull) {
        String inoderStr = Arrays.toString(toStringArray(inorder(tree)));
        if (withNull) {
            return inoderStr;
        }
        return trimNulls(inoderStr);
    }
    private static String trimNulls(String str) {
        return str.replace(", null", "").replace("null, ", "");
    }
    
    public static Integer[] inorder(TreeNode tree) {
        List<Integer> output = new ArrayList<>();
        inorderRec(tree, output);
        return toIntegerArray(output);
    }
    private static void inorderRec(TreeNode tree, List<Integer> output) {
        if (tree == null) {
            output.add(null);
        } else {
            inorderRec(tree.left, output);
            output.add(tree.val);
            inorderRec(tree.right, output);
        }
    }
    
    public static String preorderString(TreeNode tree, boolean withNull) {
        String preorderStr = Arrays.toString(toStringArray(preorder(tree)));
        if (withNull) {
            return preorderStr;
        }
        return trimNulls(preorderStr);
    }
    
    public static Integer[] preorder(TreeNode tree) {
        List<Integer> output = new ArrayList<>();
        preorderRec(tree, output);
        return toIntegerArray(output);
    }
    private static void preorderRec(TreeNode tree, List<Integer> output) {
        output.add(tree == null ? null : tree.val);
        if (tree != null) {
            preorderRec(tree.left, output);
            preorderRec(tree.right, output);
        }
    }
    
    public static void printPreorder(TreeNode root) {
        if (root == null) {
            System.out.print("null ");
        } else {
            System.out.print(root + " ");
            printPreorder(root.left);
            printPreorder(root.right);
        }
    }
    
    // helper methods
    public static TreeNode buildTreeFromLevelOrder(Integer... arr) {
        if (arr == null || arr.length == 0) return null;
        
        Deque<TreeNode> currentLevel = new LinkedList<>();
        int i = 0;
        TreeNode root = new TreeNode(arr[i++]);
        currentLevel.push(root);
        
        while (!currentLevel.isEmpty()) {
            Deque<TreeNode> nextLevel = new LinkedList<>();
            while (!currentLevel.isEmpty() && i < arr.length) {
                TreeNode curr = currentLevel.removeFirst();
                if (curr == null) continue;
                
                curr.left = arr[i] == null ? null : new TreeNode(arr[i]);
                if (curr.left != null) nextLevel.addLast(curr.left);
                i++;
                
                curr.right = arr[i] == null ? null : new TreeNode(arr[i]);
                if (curr.right != null) nextLevel.addLast(curr.right);
                i++;
            }
            currentLevel = nextLevel;
        }
        return root;
    }
    
    private static void testTreeBuildAndPrint() {
        TreeNode tree = buildTreeFromLevelOrder(4, 2, 7, 1, 3, null, null);
        System.out.println("inorder is: " + inorderString(tree));
        System.out.println("preorder is: " + preorderString(tree));
    }
    
}

class ParentUtil {
    
    public static Map<TreeNode, TreeNode> parentMap(TreeNode root) {
        Map<TreeNode, TreeNode> parent = new HashMap<>();
        populateParentMap(root, parent);
        return parent;
    }
    private static void populateParentMap(TreeNode root, Map<TreeNode, TreeNode> parent) {
        if (root == null) return;
        if (root.left != null) {
            parent.put(root.left, root);
            populateParentMap(root.left, parent);
        }
        if (root.right != null) {
            parent.put(root.right, root);
            populateParentMap(root.right, parent);
        }
    }
    
    public static int distanceToRoot(TreeNode node, Map<TreeNode, TreeNode> parent) {
        int distance = 0;
        while (parent.containsKey(node)) {
            distance++;
            node = parent.get(node);
        }
        return distance;
    }
}
