package com.practice.ds.tree.finale;

import com.practice.ds.tree.bst.finale.*;

import java.util.*;

import static com.practice.ds.tree.finale.TreeUtil.*;

public class SerDe {

}

class Codec_IN_PRE {
    
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder == null || inorder == null || preorder.length == 0) return null;
        
        HashMap<Integer, Integer> inorderIndex = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) {
            inorderIndex.put(inorder[i], i);
        }
        return buildTree(preorder, 0, preorder.length - 1, 0, inorder.length - 1, inorderIndex);
    }
    private TreeNode buildTree(int[] preorder, int pStart, int pEnd,
                               int iStart, int iEnd,
                               HashMap<Integer, Integer> inorderIndex) {
        if (pStart > pEnd) return null;
        TreeNode root = new TreeNode(preorder[pStart]);
        if (pStart == pEnd) return root;
        int i = inorderIndex.get(preorder[pStart]);
        int leftSize = i - iStart;
        root.left = buildTree(preorder, pStart + 1, pStart + leftSize,
            iStart, i - 1, inorderIndex);
        
        root.right = buildTree(preorder, pStart + leftSize + 1, pEnd,
            i + 1, iEnd, inorderIndex);
        
        return root;
    }
    
}

class Codec {
    
    /*
        complete theory:
            • general tree
                - in + pre/post
                - level order with
                - best: pre with null markers
            • special trees
                - bst: pre/post
                - perfect/complete tree: level order (reduces null)
                - full tree: follow general case
     */
    
    // algo1: preorder with null markers - S(lg n)
    // algo2: level order with null markers - S(n)
    
    private static String SENTINAL = "#";
    
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder builder = new StringBuilder();
        serialize(root, builder);
        return builder.toString();
    }
    private void serialize(TreeNode root, StringBuilder builder) {
        if (root == null) {
            builder.append(SENTINAL);
        } else {
            builder.append(root.val);
            builder.append(",");
            serialize(root.left, builder);
            builder.append(",");
            serialize(root.right, builder);
        }
    }
    
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null || data.isEmpty() || SENTINAL.equals(data)) return null;
        String[] tokens = data.split(",");
        // we can use global state for index, but lets queue and removed seen nodes
        Queue<String> queue = new LinkedList<>(Arrays.asList(tokens));
        return deserialize(queue);
    }
    private TreeNode deserialize(Queue<String> queue) {
        if (queue.isEmpty()) return null;
        String str = queue.poll();
        if (SENTINAL.equals(str)) return null;
        TreeNode node = new TreeNode(Integer.parseInt(str));
        node.left = deserialize(queue);
        node.right = deserialize(queue);
        return node;
    }
    
}

class NaryTree {
    
    static class Node {
        public int val;
        public List<Node> children;
        
        public Node() {
        }
        public Node(int val) {
            this.val = val;
        }
        public Node(int val, List<Node> children) {
            this.val = val;
            this.children = children;
        }
        
    }
    
    ;
    
    static class Codec {
        private static String SENTINAL = "#";
        
        // Encodes a tree to a single string.
        public String serialize(Node root) {
            StringBuilder builder = new StringBuilder();
            serialize(root, builder);
            return builder.toString();
        }
        private void serialize(Node root, StringBuilder builder) {
            if (root == null) {
                builder.append(SENTINAL);
            } else {
                builder.append(root.val);
                if (root.children != null) {
                    for (Node c : root.children) {
                        builder.append(",");
                        serialize(c, builder);
                    }
                }
                builder.append(",");
                builder.append(SENTINAL);
            }
        }
        
        // Decodes your encoded data to tree.
        public Node deserialize(String data) {
            if (data == null || data.isEmpty()) return null;
            String[] tokens = data.split(",");
            Queue<String> queue = new LinkedList<>(Arrays.asList(tokens));
            return deserialize(queue);
        }
        private Node deserialize(Queue<String> queue) {
            if (queue.isEmpty()) return null;
            String str = queue.poll();
            if (SENTINAL.equals(str)) return null;
            Node node = new Node(Integer.parseInt(str));
            node.children = new ArrayList<>();
            while (!SENTINAL.equals(queue.peek())) {
                node.children.add(deserialize(queue));
            }
            queue.poll();
            return node;
        }
        
    }
    
}

class MaxTree {
    
    static class GlobalState {
        public int[] arr;
        public int i;
        public GlobalState(int[] arr, int i) {
            this.arr = arr;
            this.i = i;
        }
        
    }
    
    public static TreeNode maxTree(int[] arr) {
        GlobalState gs = new GlobalState(arr, 0);
        return maxTree(gs, null);
    }
    private static TreeNode maxTree(GlobalState gs, Integer max) {
        TreeNode tree = null;
        while (gs.i < gs.arr.length && (tree == null || max == null || gs.arr[gs.i] < max)) {
            if (tree == null) {
                tree = new TreeNode(gs.arr[gs.i++]);
            } else if (tree.val < gs.arr[gs.i]) {
                TreeNode nnd = new TreeNode(gs.arr[gs.i++]);
                nnd.left = tree;
                tree = nnd;
            } else {
                tree.right = maxTree(gs, tree.val);
            }
        }
        
        return tree;
    }
    
    public static TreeNode maxTreeInsert(TreeNode root, int i) {
        if (root == null) return new TreeNode(i);
        if (root.val < i) {
            TreeNode nnd = new TreeNode(i);
            nnd.left = root;
            return nnd;
        }
        root.right = maxTreeInsert(root.right, i);
        return root;
    }
    
    public static void main(String[] args) {
        TreeNode tree = maxTree(new int[]{5, 11, 30, 9, 20});
        System.out.println("tree is: " + preorderString(tree, true));
        System.out.println("tree is: " + inorderString(tree, true));
    }
    
}
