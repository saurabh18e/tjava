package com.practice.ds.tree.finale;

import com.practice.ds.tree.bst.finale.*;

import java.util.*;

import static com.practice.ds.tree.finale.ParentUtil.*;

public class Medium {
}

class Diameter {
    
    // good example of using global state and magic recursion based update
    
    //public static int diameter(TreeNode root) {
    //    int[] maxDiameterSoFar = new int[]{0};
    //    diameterRec(root, maxDiameterSoFar);
    //    return maxDiameterSoFar[0];
    //}
    int maxDiameter = 0;
    public int diameter(TreeNode root) {
        diameterRec(root);
        return maxDiameter;
    }
    public int diameterRec(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 0;
        
        int leftHeight = diameterRec(root.left);
        int rightHeight = diameterRec(root.right);
        
        int rootDiameter = 0;
        rootDiameter += (root.left == null) ? 0 : leftHeight + 1;
        rootDiameter += (root.right == null) ? 0 : rightHeight + 1;
        maxDiameter = Math.max(maxDiameter, rootDiameter);
        return Math.max(leftHeight, rightHeight) + 1;
    }
}

class MaxDepthIterative {
    
    // good example of iterative code without modifying the TreeNode
    // since this is iterative code, so we need to keep it top-down and  use global state
    
    // note: this is top-down iteration -> so use global state
    public int maxDepth(TreeNode root) {
        if (root == null) return 0;
        int maxDepth = 0;
        Stack<TreeNode> stack = new Stack<>();
        Stack<Integer> height = new Stack<>();
        
        stack.push(root);
        height.push(1);
        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            int nodeHeight = height.pop();
            if (node.left == null && node.right == null) {
                maxDepth = Math.max(maxDepth, nodeHeight);
            }
            if (node.right != null) {
                stack.push(node.right);
                height.push(nodeHeight + 1);
            }
            
            if (node.left != null) {
                stack.push(node.left);
                height.push(nodeHeight + 1);
            }
        }
        
        return maxDepth;
    }
    
}

class Leaves {
    
    public static List<TreeNode> leaves(TreeNode root) {
        List<TreeNode> output = new ArrayList<>();
        leavesRec(root, output);
        return output;
    }
    public static void leavesRec(TreeNode root, List<TreeNode> output) {
        if (root == null) return;
        if (root.left == null && root.right == null) output.add(root);
        leavesRec(root.left, output);
        leavesRec(root.right, output);
    }
    
    // this is basically adding each node to list/level of same height
    public static List<List<Integer>> findLevelsByHeight(TreeNode root) {
        // hashmap is not needed as output is a hash table based on array
        // HashMap<Integer, List<Integer>> heightBasedMap = new HashMap<>();
        List<List<Integer>> output = new ArrayList<>();
        findLevelsByHeightRec(root, output);
        return output;
    }
    private static int findLevelsByHeightRec(
        TreeNode root, List<List<Integer>> heightBasedMap) {
        if (root == null) return -1;
        int leftHeight = findLevelsByHeightRec(root.left, heightBasedMap);
        int rightHeight = findLevelsByHeightRec(root.right, heightBasedMap);
        int rootHeight = Math.max(leftHeight, rightHeight) + 1;
        if (heightBasedMap.size() <= rootHeight)
            heightBasedMap.add(new ArrayList<>());
        heightBasedMap.get(rootHeight).add(root.val);
        return rootHeight;
    }
    
}

class LCA {
    
    //// lca without parent pointer /////
    
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        return lowestCommonAncestorV1(root, p, q);
    }
    
    // poor perf in most cases though 0(n)
    public TreeNode lowestCommonAncestorV1(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return root;
        if (root == p || root == q) return root;
        
        TreeNode leftLCA = lowestCommonAncestorV1(root.left, p, q);
        TreeNode rightLCA = lowestCommonAncestorV1(root.right, p, q);
        if (leftLCA != null && rightLCA != null) return root;
        return (leftLCA != null) ? leftLCA : rightLCA;
    }
    
    class LCAResult {
        public int nodesSeen;
        public TreeNode lca;
        public LCAResult(int nodesSeen, TreeNode lca) {
            this.nodesSeen = nodesSeen;
            this.lca = lca;
        }
        
    }
    public TreeNode lowestCommonAncestorV2(TreeNode root, TreeNode p, TreeNode q) {
        return lowestCommonAncestorV2Rec(root, p, q).lca;
    }
    public LCAResult lowestCommonAncestorV2Rec(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return new LCAResult(0, null);
        
        LCAResult leftLCA = lowestCommonAncestorV2Rec(root.left, p, q);
        if (leftLCA.nodesSeen == 2) return leftLCA;
        LCAResult rightLCA = lowestCommonAncestorV2Rec(root.right, p, q);
        if (rightLCA.nodesSeen == 2) return rightLCA;
        
        if (root == p || root == q)
            return new LCAResult(leftLCA.nodesSeen + rightLCA.nodesSeen + 1, root);
        else if (leftLCA.nodesSeen == 1 && rightLCA.nodesSeen == 1)
            return new LCAResult(2, root);
        else if (leftLCA.nodesSeen == 1)
            return leftLCA;
        else if (rightLCA.nodesSeen == 1)
            return rightLCA;
        else
            return new LCAResult(0, null);
    }
    
    ////// lca with parent pointers  /////
    
    /*
        - sol1: use hashmap and move in-tandem
        - sol2: find path length and use-fast-slow-pointer approach to move in-tandem
     */
    public static TreeNode lcaWithParentPointersV1(TreeNode root, TreeNode p, TreeNode q) {
        Map<TreeNode, TreeNode> parent = parentMap(root);
        int d1 = distanceToRoot(p, parent);
        int d2 = distanceToRoot(p, parent);
        TreeNode a = (d1 > d2) ? p : q;
        TreeNode b = (d1 > d2) ? q : p;
        
        int diff = Math.abs(d1 - d2);
        while (diff > 0) {
            a = parent.get(a);
            diff--;
        }
        while (a != b) {
            a = parent.get(a);
            b = parent.get(b);
        }
        return a;
    }
    
    // optimized for not scanning all the way to root - downside is increased space complexity
    public static TreeNode lcaWithParentPointersV2(TreeNode root, TreeNode p, TreeNode q) {
        Map<TreeNode, TreeNode> parent = parentMap(root);
        Set<TreeNode> seen = new HashSet<>();
        while (p != null || q != null) {
            if (p != null) {
                if (seen.contains(p)) return p;
                seen.add(p);
                p = parent.get(p);
            }
            
            if (q != null) {
                if (seen.contains(q)) return q;
                seen.add(q);
                q = parent.get(q);
            }
        }
        return null;
    }
}
