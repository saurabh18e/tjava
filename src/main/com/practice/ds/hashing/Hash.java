package com.practice.ds.hashing;

public class Hash { }

/*
  
  Hashing Theory
  ---------------

  - dictionary
      - used for storing dynamic sets with supported operations: Insert, Search, Delete
      - avg lookup time: 0(1) while worst case 0(n)
  - direct addressing vs hash table
    - direct address table
        - assumption: no collisions
        - S(U), U=set of universe of keys
        - examples of direct address table: arrays, bit vector
    - hash table
        - S(K), set of actual keys
        - hash functions, collisions and chaining
  - closed addressing/ chaining
    - assumption: simple uniform hashing/uniform distribution
      - each key can be mapped to any of the m slots(independent of other keys)
        i.e. distribution of keys in hash table is not affected by any pattern in the input data
    - performance: seach: T(1 + alpha), where alpha is the load factor
        - 0(1+load), assuming UNIFORM DISTRIBUTION + load=0 if size of hash table slots is proportional to size of set of keys
    - designing hash functions
      - what is a good hash function: Uniform Distribution
      - division        : h(k) = k mod m
      - multiplication  : h(k) = m(kA mod 1)
      - universal hashing: random choose a hash function at start of hashing from a set of carefully chosen hash functions H, where
                           H is such that for all h in H and key a, b in set of universe U, there are atmost \H\/m hash functions
                           such that h(a) = h(b) i.e. chances of collision of any two keys is 1/\U\
                          - random number generator: rand(seed) = ((seed*899624124 + 12345) / 2^16) % 2^15
  - open addressing / probing
    - uniform hashing: each key is likely to have any of the m! permutations as probe sequence
    - writing probing functions
      - linear probing: h(k, i) = (h(k) + i) mod m.....suffers from primary clustering
      - quadratic probing: h(k, i) = (h(k) + a*i + b*i^2) mod m.....suffers from secondary clustering
      - double hash probing: h(k, i) = (h1(k) + h2(k)*i) mod m
  - perfect hashing
    - assumption: set of keys is static i.e. no new inserts or delete of keys except given keys
    - performance: expected time and space: T(1), S(n)
    - design
      - 2 level hashing
      - each secondary hash is square the size of keys mapping to it
      - at each level, universal hashing is used
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */
