package com.practice.ds.hashing.finale;

import com.practice.ds.list.finale.*;

import java.util.*;

public class Easy {

}

class SetMembership {
    
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{map.get(target - nums[i]), i};
            }
            map.put(nums[i], i);
        }
        return new int[]{-1, -1};
    }
    
    public char repeatedCharacter(String s) {
        if (s == null) return 0;
        HashSet<Character> set = new HashSet<>();
        for (char ch : s.toCharArray()) {
            if (set.contains(ch)) return ch;
            set.add(ch);
        }
        return ' ';
    }
    
    public static ListNode checkCycleUsingHash(ListNode list) {
        HashSet<ListNode> seen = new HashSet<>();
        ListNode fast = list;
        for (ListNode iter = list; iter != null; iter = iter.next) {
            if (seen.contains(iter)) return iter;
            seen.add(iter);
        }
        return null;
    }
    
    public boolean checkIfPangram(String sentence) {
        boolean[] seen = new boolean[26];
        for (char ch : sentence.toCharArray()) {
            seen[ch - 'a'] = true;
        }
        for (boolean slot : seen) if (!slot) return false;
        return true;
    }
    public int missingNumber(int[] nums) {
        int xor = 0;
        for (int i = 1; i <= nums.length; i++) xor = xor ^ nums[i - 1] ^ i;
        return xor;
        //int sum = 0;
        //for (int a:nums) sum+=a;
        //int n=nums.length;
        //return (n*(n+1))/2 - sum;
    }
    public int countElements(int[] arr) {
        HashSet<Integer> set = new HashSet<>();
        for (int a : arr) set.add(a);
        int count = 0;
        for (int a : arr) if (set.contains(a + 1)) count++;
        return count;
    }
    public int numJewelsInStones(String jewels, String stones) {
        int count = 0;
        HashSet<Character> set = new HashSet<>();
        for (char ch : jewels.toCharArray()) {
            set.add(ch);
        }
        for (char ch : stones.toCharArray()) {
            count += set.contains(ch) ? 1 : 0;
        }
        return count;
    }
    
}

class Counting {
    
    public List<Integer> intersection(int[][] nums) {
        int n = nums.length;
        if (n < 1) return Collections.emptyList();
        HashSet<Integer> output = new HashSet<>();
        for (int a : nums[0]) {
            output.add(a);
        }
        for (int i = 1; i < nums.length; i++) {
            HashSet<Integer> set = new HashSet<>();
            for (int a : nums[i]) {
                set.add(a);
            }
            output.retainAll(set);
        }
        List<Integer> sorted = new ArrayList<>(output);
        Collections.sort(sorted);
        return sorted;
    }
    
    public static boolean areOccurrencesEqual(String s) {
        if (s == null) return false;
        HashMap<Character, Integer> map = new HashMap<>();
        for (char ch : s.toCharArray()) {
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }
        
        Integer count = map.get(s.charAt(0));
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (!count.equals(entry.getValue())) return false;
            // if (count != entry.getValue()) return false;
        }
        return true;
    }
    
    // constraint: output should be sorted but you cannot use sorting: counting/bucket sort
    public List<List<Integer>> findWinners(int[][] matches) {
        int maxSoFar = -1;
        int minSoFar = -1;
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int[] match : matches) {
            maxSoFar = Math.max(maxSoFar, Math.max(match[0], match[1]));
            minSoFar = Math.min(minSoFar, Math.min(match[0], match[1]));
            map.put(match[0], map.getOrDefault(match[0], 0));
            map.put(match[1], map.getOrDefault(match[1], 0) + 1);
        }
        List<List<Integer>> output = Arrays.asList(new ArrayList<>(), new ArrayList<>());
        for (int p = minSoFar; p <= maxSoFar; p++) {
            if (map.containsKey(p)) {
                int count = map.get(p);
                if (count == 0) output.get(0).add(p);
                if (count == 1) output.get(1).add(p);
            }
        }
        return output;
    }
    
    public int largestUniqueNumber(int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int a : nums) map.put(a, map.getOrDefault(a, 0) + 1);
        int uniqueMax = -1;
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (e.getValue() == 1) {
                uniqueMax = Math.max(uniqueMax, e.getKey());
            }
        }
        return uniqueMax;
    }
    public int maxNumberOfBalloons(String text) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (char a : text.toLowerCase().toCharArray())
            map.put(a, map.getOrDefault(a, 0) + 1);
        map.put('l', map.getOrDefault('l', 0) / 2);
        map.put('o', map.getOrDefault('o', 0) / 2);
        int count = Integer.MAX_VALUE;
        for (char a : "balon".toCharArray())
            count = Math.min(count, map.getOrDefault(a, 0));
        return count;
    }
    
    public boolean canConstruct(String ransomNote, String magazine) {
        HashMap<Character, Integer> count = new HashMap<>();
        for (char ch : ransomNote.toCharArray()) count.put(ch, count.getOrDefault(ch, 0) + 1);
        for (char ch : magazine.toCharArray()) {
            if (count.containsKey(ch)) {
                count.put(ch, count.get(ch) - 1);
                if (count.get(ch) == 0) count.remove(ch);
            }
            if (count.isEmpty()) break;
        }
        return count.isEmpty();
    }
    
    public static boolean hasPalindromicPermutation(String str) {
        if (str == null) return false;
        char[] chars = new char[256];
        int oddChars = 0;
        for (char ch : str.toCharArray()) {
            if (chars[ch] == 0) {
                chars[ch] = 1;
                oddChars++;
            } else {
                chars[ch] = 0;
                oddChars--;
            }
        }
        return oddChars <= 1;
    }
    
    public static void main(String[] args) {
        char[] arr = new char[]{'a', 'b'};
        String str = new String(arr);
        System.out.println(str);
    }
    
}

class SignatureBasedComparison {
    
    class Anagrams {
        
        public List<List<String>> groupAnagrams(String[] strs) {
            if (strs.length == 0) return new ArrayList<>();
            HashMap<String, List<String>> groups = new HashMap<>();
            for (String str : strs) {
                String signature = signature(str);
                if (!groups.containsKey(signature)) groups.put(signature, new ArrayList<>());
                groups.get(signature).add(str);
            }
            return new ArrayList<>(groups.values());
        }
        private String signature(String str) {
            char[] chars = new char[256];
            for (char ch : str.toCharArray()) {
                chars[ch]++;
            }
            StringBuilder builder = new StringBuilder();
            for (char ch : chars) {
                builder.append(ch);
                builder.append("#");
            }
            return builder.toString();
        }
        
    }
    
    class MatchRowsWithColumns {
        // match/group rows with columns
        public int equalPairsV1(int[][] grid) {
            int n = grid.length;
            if (n == 0) return 0;
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < n; i++) {
                String signature = signature(grid, i, -1);
                map.put(signature, map.getOrDefault(signature, 0) + 1);
            }
            
            int count = 0;
            for (int i = 0; i < n; i++) {
                String signature = signature(grid, -1, i);
                count += map.getOrDefault(signature, 0);
            }
            return count;
        }
        private String signature(int[][] grid, int i, int j) {
            int n = grid.length;
            StringBuilder builder = new StringBuilder();
            for (int k = 0; k < n; k++) {
                int num = (i == -1) ? grid[k][j] : grid[i][k];
                builder.append(num);
                builder.append("#");
                builder.append(k);
                builder.append("##");
            }
            return builder.toString();
        }
        
        // match/group rows with columns
        public int equalPairsV2(int[][] grid) {
            int n = grid.length;
            if (n == 0) return 0;
            HashMap<List<Integer>, Integer> map = new HashMap<>();
            for (int i = 0; i < n; i++) {
                List<Integer> signature = signatureArray(grid, i, -1);
                map.put(signature, map.getOrDefault(signature, 0) + 1);
            }
            
            int count = 0;
            for (int i = 0; i < n; i++) {
                List<Integer> signature = signatureArray(grid, -1, i);
                count += map.getOrDefault(signature, 0);
            }
            return count;
        }
        private List<Integer> signatureArray(int[][] grid, int i, int j) {
            int n = grid.length;
            List<Integer> list = new ArrayList<>();
            for (int k = 0; k < n; k++) {
                int num = (i == -1) ? grid[k][j] : grid[i][k];
                list.add(num);
            }
            return list;
        }
        
    }
    
    class EqualDigitsMaxSum {
        
        // maximum sum of two numbers if digit sum is same
        public int maximumSum(int[] nums) {
            int max = -1;
            HashMap<Integer, Integer> lastMax = new HashMap<>();
            for (int a : nums) {
                int signature = signature(a);
                if (lastMax.containsKey(signature)) {
                    Integer lm = lastMax.get(signature);
                    max = Math.max(max, lm + a);
                    if (a > lm) lastMax.put(signature, a);
                } else {
                    lastMax.put(signature, a);
                }
            }
            return max;
        }
        private int signature(int a) {
            int sum = 0;
            while (a > 0) {
                sum += a % 10;
                a = a / 10;
            }
            return sum;
        }
        
    }
    
    private static class ContactList {
        /*
            question:
                implement class called ContactList, which will store list of strings as contacts
                can store duplicates
                now implement the class so that you can hashmap the ContactList and compare it with other objects
            solution:
                implement equals and hashcode
                take care of duplicate contacts in the list during equals etc.
         */
        
        private List<String> contacts = new ArrayList<>();
        // cache the signature
        private HashSet<String> signature = null;
        
        public ContactList(List<String> contacts) {
            this.contacts = contacts;
            updateSignature();
        }
        private void updateSignature() {
            this.signature = signature(this.contacts);
        }
        
        public List<String> getContacts() {
            return contacts;
        }
        public void setContacts(List<String> contacts) {
            this.contacts = (contacts == null) ? new ArrayList<>() : contacts;
            updateSignature();
        }
        public void addContact(String contact) {
            this.contacts.add(contact);
            updateSignature();
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof ContactList)) return false;
            if (obj == this) return true;
            return this.signature.equals(((ContactList) obj).signature);
        }
        @Override
        public int hashCode() {
            return signature.hashCode();
        }
        private HashSet<String> signature(List<String> contacts) {
            return new HashSet<>(contacts);
        }
        @Override
        public String toString() {
            return "ContactList" + contacts.toString();
        }
        public static void testContactList() {
            ContactList l1 = new ContactList(Arrays.asList("a", "b", "a"));
            ContactList l2 = new ContactList(Arrays.asList("a", "b"));
            ContactList l3 = new ContactList(Arrays.asList("a", "c"));
            System.out.println(l1.equals(l2));
            System.out.println(l1.equals(l3));
            HashMap<ContactList, String> map = new HashMap<>();
            map.put(l1, "l1");
            map.put(l2, "l2");
            map.put(l3, "l3");
            System.out.println(l1);
            System.out.println(map.get(new ContactList(Arrays.asList("a", "b", "a", "a"))));
            System.out.println(map.get(new ContactList(Arrays.asList("a", "c"))));
        }
        
    }
    
}

class Misc {
    
    public int minimumCardPickup(int[] cards) {
        int min = Integer.MAX_VALUE;
        HashMap<Integer, Integer> lastSeen = new HashMap<>();
        for (int i = 0; i < cards.length; i++) {
            if (lastSeen.containsKey(cards[i])) {
                min = Math.min(min, i - lastSeen.get(cards[i]) + 1);
            }
            lastSeen.put(cards[i], i);
        }
        return min == Integer.MAX_VALUE ? -1 : min;
    }
    
    class WordDistance {
        
        private HashMap<String, List<Integer>> locationMap = new HashMap<>();
        
        public WordDistance(String[] wordsDict) {
            int i = 0;
            for (String word : wordsDict) {
                if (!locationMap.containsKey(word))
                    locationMap.put(word, new ArrayList<>());
                locationMap.get(word).add(i++);
            }
        }
        
        public int shortest(String word1, String word2) {
            List<Integer> locations1 = locationMap.getOrDefault(word1, null);
            List<Integer> locations2 = locationMap.getOrDefault(word2, null);
            if (locations1 == null || locations2 == null) return Integer.MAX_VALUE;
            int minDistance = Integer.MAX_VALUE;
            for (int i = 0, j = 0; i < locations1.size() && j < locations2.size(); ) {
                int loc1 = locations1.get(i);
                int loc2 = locations2.get(j);
                minDistance = Math.min(minDistance, Math.abs(loc1 - loc2));
                if (minDistance == 1) break;
                if (loc1 < loc2) i++;
                else j++;
            }
            return minDistance;
        }
        
    }
    
}
