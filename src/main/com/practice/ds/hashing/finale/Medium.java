package com.practice.ds.hashing.finale;

import com.practice.util.*;

import java.util.*;

public class Medium {

}

class CumulativeDiff {
    
    public int subarrayWithSumK(int[] nums, int k) {
        if (nums == null || nums.length == 0) return 0;
        int prefixSum = 0;
        int count = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        for (int a : nums) {
            prefixSum += a;
            count += map.getOrDefault(prefixSum - k, 0);
            map.put(prefixSum, map.getOrDefault(prefixSum, 0) + 1);
        }
        return count;
    }
    
    public int niceSubarrays(int[] nums, int k) {
        if (nums == null || nums.length == 0) return 0;
        int prefixCount = 0;
        int count = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        for (int a : nums) {
            prefixCount += a % 2;
            count += map.getOrDefault(prefixCount - k, 0);
            map.put(prefixCount, map.getOrDefault(prefixCount, 0) + 1);
        }
        return count;
    }
    
    public int subArraysWithEqualZeroAndOne(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        // diff -> index in array map
        HashMap<Integer, Integer> firstSeen = new HashMap<>();
        firstSeen.put(0, -1);
        int maxLength = 0;
        int diffHere = 0;
        for (int i = 0; i < nums.length; i++) {
            int a = nums[i];
            diffHere += (a == 0) ? -1 : 1;
            if (firstSeen.containsKey(diffHere)) {
                maxLength = Math.max(maxLength, i - firstSeen.get(diffHere));
            } else {
                firstSeen.put(diffHere, i);
            }
        }
        return maxLength;
    }
    
}

class Practical {
    
    public static List<Integer> findStringConcat(String s, String[] words) {
        List<Integer> output = new ArrayList<>();
        if (s == null || s.isEmpty() || words == null || words.length == 0) return output;
        HashMap<String, Integer> maxCount = new HashMap<>();
        int count = 0;
        for (String w : words) {
            count++;
            maxCount.put(w, maxCount.getOrDefault(w, 0) + 1);
        }
        int n = words[0].length();
        for (int i = 0; i + n * count <= s.length(); i++) {
            String subStr = s.substring(i, i + n);
            if (maxCount.containsKey(subStr) && isConcat(s, i, maxCount, n)) {
                output.add(i);
            }
        }
        return output;
    }
    private static boolean isConcat(String s, int i, HashMap<String, Integer> maxCount, int n) {
        HashMap<String, Integer> seen = new HashMap<>();
        int pending = maxCount.size();
        for (int j = i; j + n <= s.length(); j += n) {
            String subStr = s.substring(j, j + n);
            if (!maxCount.containsKey(subStr)) return false;
            
            int seenFreq = seen.getOrDefault(subStr, 0);
            int maxFreq = maxCount.get(subStr);
            if (seenFreq == maxFreq) return false;
            seen.put(subStr, seenFreq + 1);
            if (seenFreq + 1 == maxFreq) {
                pending--;
                if (pending == 0) return true;
            }
        }
        return false;
    }
    
    public static void main(String[] args) {
        String str = "wordgoodgoodgoodbestword";
        String[] arr = new String[]{"word", "good", "best", "good"};
        List<Integer> indexes = findStringConcat(str, arr);
        System.out.println(Arrays.toString(indexes.toArray()));
    }
    
}

class Theory {
    
    ///// average of top 3 scores for each student /////
    
    public static Map<Integer, Integer> averageOfTopThree(int[][] scores) {
        Map<Integer, PriorityQueue<Integer>> map = new HashMap<>();
        for (int[] score : scores) {
            int studentId = score[0];
            int studentScore = score[1];
            PriorityQueue<Integer> minHeap = map.getOrDefault(studentId, new PriorityQueue<>());
            minHeap.add(studentScore);
            if (minHeap.size() > 3) minHeap.remove();
            map.put(studentId, minHeap);
        }
        Map<Integer, Integer> output = new HashMap<>();
        for (Map.Entry<Integer, PriorityQueue<Integer>> e : map.entrySet()) {
            PriorityQueue<Integer> pq = e.getValue();
            if (pq.size() < 3) continue;
            int sum = 0;
            while (!pq.isEmpty()) {
                sum += pq.remove();
            }
            output.put(e.getKey(), sum / 3);
        }
        return output;
    }
    private static void testaverageOfTopThree() {
        System.out.println(averageOfTopThree(new int[][]{{1, 2}, {2, 2}, {1, 5}, {1, 5}, {1, 1}}));
    }
    
    /*
        question: given n queries, find k most frequent queries
        solution: form frequency hashmap, then form frequency array and find kth order static
     */
    
    /*
        question: implement function to serde chess board
        solution:
            there are 13 states and 64 squares
            - use sum(a[i]*p^i) for arr of 64
            - use XOR for 13x64 random 64 digit numbers
     */
}

class MyHashMap {
    
    private static class Bucket {
        private List<Tuple<Integer, Integer>> list;
    
        public Bucket() {
            this.list = new LinkedList<>();
        }
        public Integer get(int key) {
            for (Tuple<Integer, Integer> t : list) {
                if (t.first.equals(key)) return t.second;
            }
            return -1;
        }
        
        public int put(int key, int value) {
            for (Tuple<Integer, Integer> t : list) {
                if (t.first.equals(key)) {
                    t.second = value;
                    return 0;
                }
            }
            list.add(new Tuple<>(key, value));
            return 1;
        }
        
        public int remove(int key) {
            int i=0;
            for (Tuple<Integer, Integer> t : list) {
                if (t.first.equals(key)) {
                    list.remove(i);
                    return -1;
                }
                i++;
            }
            return 0;
        }
        
    }
    
    /*
        array - load factor(1) - dynamic resize to double and half -> rehash old entries
        collision resolution through chaining - double ll for easy CRUD
        hashcode and equals depend on input
     */
    
    // array list takes care of dynamically growing and reducing size
    private Bucket[] hashTable;
    private int size;
    private static int MIN_CAPACITY = 10;
    
    public MyHashMap() {
        hashTable = new Bucket[MIN_CAPACITY];
    }
    
    public int get(int key) {
        return bucket(key).get(key);
    }
    
    public void remove(int key) {
        size += bucket(key).remove(key);
        if ((size > 5) && size < hashTable.length/4) rehash(2*size);
    }
    
    public void put(int key, int value) {
        size += bucket(key).put(key, value);
        if (size == hashTable.length) rehash(2*size);
    }
    private void rehash(int n) {
        n = Math.max(MIN_CAPACITY, n);
        Bucket[] newHashTable = new Bucket[n];
        for (Bucket b : hashTable) {
            if (b == null) continue;
            for (Tuple<Integer, Integer> pair : b.list) {
                int key = pair.first;
                int value = pair.second;
                int slot = slot(key, n);
                if (newHashTable[slot] == null) newHashTable[slot] = new Bucket();
                newHashTable[slot].put(key, value);
            }
        }
        this.hashTable = newHashTable;
    }
    
    public int size() {
        return size;
    }
    private Bucket bucket(int key) {
        int slot = slot(key);
        if (hashTable[slot] == null) hashTable[slot] = new Bucket();
        return hashTable[slot];
    }
    private int slot(int key) {
        return slot(key, hashTable.length);
    }
    private int slot(int key, int n) {
        return Math.abs(key) % n;
    }
    
}
