package com.practice.ds.hashing.finale;

import java.util.*;

public class Hard {
}

class LinkedHashMapBased {
    
    public static int lengthOfLongestSubstringKDistinctV1(String s, int k) {
        if (s == null || s.isEmpty() || k <= 0) return 0;
        int maxLength = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0, j = 0; j < s.length(); ) {
            char ch = s.charAt(j++);
            map.put(ch, map.getOrDefault(ch, 0) + 1);
            while (map.size() > k && i < j) {
                char leftCh = s.charAt(i++);
                map.put(leftCh, map.get(leftCh) - 1);
                if (map.get(leftCh) == 0) map.remove(leftCh);
            }
            maxLength = Math.max(maxLength, j - i);
        }
        return maxLength;
    }
    
    // v2: greedy algo: direct jump to rightmost of char with min rightmost value
    // in order to find min rightmost i.e. earliest entry, use LinkedHashMap
    public static int lengthOfLongestSubstringKDistinctV2(String s, int k) {
        if (s == null || s.isEmpty() || k <= 0) return 0;
        int maxLength = 0;
        // default behaviour is insertion-based ordering
        Map<Character, Integer> lastSeen = new LinkedHashMap<>();
        for (int i = 0, j = 0; j < s.length(); ) {
            char ch = s.charAt(j++);
            if (lastSeen.containsKey(ch)) {
                lastSeen.remove(ch);
            }
            lastSeen.put(ch, j - 1);
            if (lastSeen.size() > k) {
                Map.Entry<Character, Integer> minIndex = lastSeen.entrySet().iterator().next();
                lastSeen.remove(minIndex.getKey());
                i = minIndex.getValue() + 1;
            }
            maxLength = Math.max(maxLength, j - i);
        }
        return maxLength;
    }
    
    ////// isdb cache: Cache with LRU based eviction on full capacity   //////
    ////// lookup, insert, delete - 0(1)
    
    class LRUCache {
        Map<String, Integer> cache;
        
        public LRUCache(int capacity) {
            int loadFactor = 1;
            boolean accessOrder = true;
            this.cache = new LinkedHashMap<String, Integer>(capacity, loadFactor, true) {
                @Override
                protected boolean removeEldestEntry(Map.Entry eldest) {
                    return size() > capacity;
                }
            };
        }
        public Integer get(String id) {
            // linked hashmap takes care of access order update
            return cache.getOrDefault(id, null);
        }
        public Integer put(String id, Integer price) {
            // linked hashmap takes care of eviction based on LRU
            // note: requirement says do not update
            if (!cache.containsKey(id)) {
                cache.put(id, price);
            }
            return cache.get(id);
        }
        
        public Integer delete(String id) {
            return cache.remove(id);
        }
        
    }
    
    public static void main(String[] args) {
        PriorityQueue<Integer> q = new PriorityQueue<>();
        q.add(1);
        q.add(1);
        q.add(2);
        q.remove(1);
        System.out.println(q);
    }
    
}

class GreedyPush {
    
    //////  longest sub-sequence interval //////
    public int longestConsecutive(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        HashSet<Integer> unvisited = new HashSet<>();
        for (int a : nums) unvisited.add(a);
        int maxLength = 0;
        for (int a : nums) {
            if (unvisited.isEmpty()) break;
            int len = directionLength(unvisited, a, 1);
            len += directionLength(unvisited, a - 1, -1);
            maxLength = Math.max(maxLength, len);
        }
        return maxLength;
    }
    private int directionLength(HashSet<Integer> set, int start, int inc) {
        int len = 0;
        while (!set.isEmpty() && set.contains(start)) {
            len++;
            set.remove(start);
            start += inc;
        }
        return len;
    }
    
    ////// length of longest substring without dups  /////////
    public int lengthOfLongestSubstringWithoutDups(String s) {
        if (s == null || s.isEmpty()) return 0;
        int[] lastSeen = new int[256];
        Arrays.fill(lastSeen, -1);
        int maxLength = 0;
        char[] chars = s.toCharArray();
        for (int i = 0, j = 0; j < chars.length; ) {
            char ch = chars[j];
            if (lastSeen[ch] != -1 && lastSeen[ch] >= i) {
                i = lastSeen[ch] + 1;
            }
            lastSeen[ch] = j++;
            maxLength = Math.max(maxLength, j - i);
        }
        return maxLength;
    }
    
    //////  smallest subarray covering all - no dups, any sequence  ///////
    public String smallestSubarrayCoveringAllV1(String s, String t) {
        if (s == null || s.isEmpty() || t == null || t.isEmpty()) return "";
        
        HashSet<Character> set = new HashSet<>();
        for (char ch : t.toCharArray()) set.add(ch);
        
        Map<Character, Integer> indexMap = new LinkedHashMap<>();
        char[] chars = s.toCharArray();
        
        /*
            - seen char seen again
                - move char last seen and start to last-inserted
            - new irrelevant char seen
                - ignore and move
            - seen new char
                - inc char count and index map
                - if all seen
                    - re-calculate min
                    - delete last-inserted and move start to second last inserted
         */
        String min = null;
        for (int i = 0, j = 0; j < s.length(); j++) {
            char ch = chars[j];
            if (!set.contains(ch)) continue;
            
            // new char seen
            if (!indexMap.containsKey(ch)) {
                indexMap.put(ch, j);
                if (indexMap.size() == t.length()) {
                    i = indexMap.entrySet().iterator().next().getValue();
                    min = min != null && (j - i + 1) > min.length() ? min : s.substring(i, j + 1);
                    indexMap.remove(chars[i]);
                }
            } else {
                indexMap.remove(ch);
                indexMap.put(ch, j);
            }
        }
        return min == null ? "" : min;
    }
    
    //////  smallest subarray covering all - with dups, any sequence ///////
    public static String smallestSubarrayCoveringAllV2(String s, String t) {
        if (s == null || s.isEmpty() || t == null || t.isEmpty()) return "";
        
        HashMap<Character, Integer> maxCount = new HashMap<>();
        for (char ch : t.toCharArray()) maxCount.put(ch, maxCount.getOrDefault(ch, 0) + 1);
        
        Map<Character, Integer> indexMap = new HashMap<>();
        HashSet<Character> seen = new HashSet<>();
        char[] chars = s.toCharArray();
        String min = null;
        
        for (int i = 0, j = 0; j < s.length(); ) {
            char ch = chars[j++];
            if (!maxCount.containsKey(ch)) continue;
            
            indexMap.put(ch, indexMap.getOrDefault(ch, 0) + 1);
            if (indexMap.get(ch).equals(maxCount.get(ch))) {
                seen.add(ch);
                while (i <= j && seen.size() == maxCount.size()) {
                    min = min != null && (j - i) > min.length() ? min : s.substring(i, j);
                    char chI = chars[i++];
                    if (!maxCount.containsKey(chI)) continue;
                    indexMap.put(chI, indexMap.get(chI) - 1);
                    if (Integer.compare(indexMap.get(chI), maxCount.get(chI)) < 0) {
                        seen.remove(chI);
                    }
                }
            }
            
        }
        return min == null ? "" : min;
    }
    
    //////  smallest subarray covering all - with dups, but sequential ///////
    //public String smallestSubarrayCoveringAllV1(String s, String t) {
    //
    //}
    
    public static void main(String[] args) {
        System.out.println(smallestSubarrayCoveringAllV2("ADOBECODEBANC", "ABC"));
    }
}
