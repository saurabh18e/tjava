package com.practice.ds.math;

public class Combination {
  
  public static long calculateCombinations(int n, int k) {
    if (n == 0 || k == 0 || n == k) {
      return 1;
    } else if (n < k) {
      return 0;
    }
    if (k > n-k) {
      k=n-k;
    }
    
    long combinations = 1;
    for (int i=1; i<=k; i++) {
      System.out.println(n + " " + k);
      combinations = (combinations*(n-k+1))/k;
    }
    
    return combinations;
  }
  
}
