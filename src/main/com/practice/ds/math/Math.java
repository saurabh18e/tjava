package com.practice.ds.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Math {
  
}


/*
  Permutation, Combinations and Subset
  -------------------------------------
  
  Permutation
  ------------
  
  - print all permutations of an array(without repetition)
  - ref: https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
  - method1: bottom up: generate all permutations of size n-1 and for each permutation, place nth element at each position
    method2: top down: choose first element and recurse to generate all permutations
  
  - given a permutation of +ve numbers, find next higher permutation
  - method: firstChar, ceil, swap and reverse(sort)
  
  - print all permutations lexicographically
  - ref: https://www.geeksforgeeks.org/lexicographic-permutations-of-string/
  - method1: sort the input and keep on finding next higher permutation
    method2: own sol: sort and shift start of each block to start and recurse
    complexity of both methods is T(n*n!)
    
  - print all permutations lexicographically, given an array with duplicates
  - ref: https://www.geeksforgeeks.org/print-all-permutations-of-a-string-with-duplicates-allowed-in-input-string/
  - method1: sort the input and keep on finding next higher permutation
    method2: own sol: sort and shift start of each block to start and recurse
    complexity of both methods is T(n*n!)
  
  
  Combinations
  -------------
  
  - print all k-combinations from an array of size n(without duplicates)
  - ref: https://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
  - method1: nCk = nC(k-1) + (n-1)C(k-1)  choose/not choose in final output....
             add all elements to hashset to remove duplicates else use duplicates
             complexity/count: T(nCr)
    method2: count from 1 to 2^n and print for all numbers having k bits on
    method3: DFS like approach: https://www.geeksforgeeks.org/make-combinations-size-k/
             (basically tail recursive call optimization and iterative version of approach1)
  
  - print all k-combinations from an array of size n(without duplicates), but in lexographic order
  - method: count from 1 to 2^n and print for all numbers having k bits on
            (natural counting by default gives lexographic order)
  
  - print all k-combinations from an array of size n(with duplicates)
  - ref: https://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
  - method: sort and add special code to handle duplicates
  
  
  Permutation of All Combinations
  --------------------------------
  
  - given a set of numbers, print permutations of all combination array of length n. what will be the complexity or count?
  - ref: https://www.geeksforgeeks.org/print-all-permutations-with-repetition-of-characters/
  - method: use recursion, complexity/count: T(n^n)
  
  - print all possible phone words
  - ref: https://www.geeksforgeeks.org/find-possible-words-phone-digits/
  - method: use recursion, complexity: T(4^n) (because 9 has 4 chars)
  
  - convert given phone words to numbers and print them in descending order
  - ref: https://www.geeksforgeeks.org/amazon-interview-experience-set-149-campus-internship/
  - method: sort the list of strings by length and then by string compare, do conversion of each element in the sorted list
  
  Power Set
  ----------
  
  - print power set of an array(all elements being unique)
  - ref: https://www.geeksforgeeks.org/power-set/
  - method1: recursion: again, this can be seen as chosen or not but this will lead to huge number of function calls
    method2: counting: count from 0 to 2^(n-1). for each i, print all elements whose corresponding bit is on
  
  - print power set of an array(with duplicates)
  - ref: https://www.geeksforgeeks.org/find-distinct-subsets-given-set/
  - method1: brute force: out of memory: generate subsets and put them in hashset
    methdo2: own sol: extended counting with cross product of all possibilities
        - while for unique numbers, we do linear counting from 0 to n
        - for each duplicate block of numbers, we keep on turning an extra bit on at each count with x|(x+1)
        - cross product of all possibilities i.e. recursion/stack for each group
          (for iterative solution, write NextNumberGenerator classes and use stack)
  
  - given a number d and size of an array as n, count and print all combination of element in the array such that first
    element of array is d and next element in the array can be +1 or -1 the previous element in the array.
  - method: for each cell we have two choices +1 or -1, for n-1 cells, there are total of 2^(n-1) possibilities
            (this can also be seen as a binary tree, where -1 means left and +1 means right)
            this can also be seen as binary number generation, bit is 0 for -1 and 1 for +1
            this means we can print all combinations using binary counting
            for (num in 0 to 2^(n-1)) {
              for (i = 1 to n-1) {
                add = (i-1 bit is zero in num) ? +1 : -1
                arr[i] = arr[i-1] + add
              }
            }
  
 */

/*
  
  Partitions of a Number
  -----------------------
  
  - generate all partitons of a number in lexographic order
  - ref: https://www.geeksforgeeks.org/generate-unique-partitions-of-an-integer/
  - method: own sol: use recursion(refer to own code)
  
  - generate all partitons of a number in lexographic order with given number of partitions and max value of each partition
  - method: own sol: use recursion(refer to own code)
  
  - generate all partitons of a number in lexographic order, given that no partitions must be same
  - method: own sol: use recursion(refer to own code)
  
  
  Partition of a Set(Sterling Number and Bell Number)
  ----------------------------------------------------
  
  note: this is different from partition of a number, here we have a set of labelled numbers, so permutation matters
  
  - Sterling Number of Second Kind: count ways to partition a set into k subsets
  - ref: https://www.geeksforgeeks.org/count-number-of-ways-to-partition-a-set-into-k-subsets/
  - method: use recursion and dynamic programming: S(n, k) = k * S(n-1, k) + S(n-1, k-1)
  
  - Bell Number: count all possible ways to create partitions of a set
  - ref: https://www.geeksforgeeks.org/bell-numbers-number-of-ways-to-partition-a-set/
  - method: Bell(n) = sum of i in [1, k] S(n, k)
  
 */

/*
  Josephus Problem
  -----------------
  
  - given n and k, find the winner of josephus problem using circular linked list
  - ref: https://www.geeksforgeeks.org/josephus-circle-using-circular-linked-list/
  - method: use circular linked list and simulate actual run of josephus problem
  
  - given n and k, find the winner of josephus problem using mathematics
  - ref: https://www.geeksforgeeks.org/josephus-problem-set-1-a-on-solution/
  - method: J(n, k) =  (J(n-1, k) + k) % n
            https://www.quora.com/What-is-the-best-solution-for-Josephus-problem-algorithm
  
  - josephus problem with k=2
  - ref: https://www.geeksforgeeks.org/josephus-problem-set-2-simple-solution-k-2/
  - method: https://www.youtube.com/watch?v=uCsD3ZGzMgE
      observe output for multiple runs for n=1 to 15
      observations:
        - if n=2^x, the 1 is always the winner
        - if n=2^x+l, then winner is 2l+1, x is max possible number such that 2^x < n
          if we remove MSB in n, then we will get l and answer is l<<2+1(i.e. moving MSB to end gives solution)
  
 */

/*
  Clock Angle
  ------------
  
  - find angle between hour and minute hands of a clock at given time
  - ref: https://www.geeksforgeeks.org/calculate-angle-hour-hand-minute-hand/
  - method: calculate degrees moved by hour and minute hands in each minute
            find diff and return acute angle
            this question is more about covering all adge cases
  
 */

/*
  
  Random shuffling an array
  --------------------------
  - shuffle an array randomly so that all n! permutations are equally likely
  - ref: https://www.geeksforgeeks.org/?p=25111
  - method: iterate and choose one element randomly
  
  Reservoir Sampling
  ------------------
  - given an infinite stream/array of numbers of unknown length, design algo to choose k random numbers
    such that each element has equal probability of being chosen
  - ref: https://www.geeksforgeeks.org/reservoir-sampling/
  - method:
    - for finite length array, choose one random number at a time till k numbers are chosen
      (this is similar to shuffling an array: https://www.geeksforgeeks.org/?p=25111)
      (another way to say the same thing is to randomly shuffle the array and then pick first k)
    - for infinite stream or cases where we have access to elements one at a time,
      generate a random number j between 0 to i, if it lies between 0 to k, then replace jth element
      (replace ith element with probability k/i)
      
  - select random node of a linked list, constraint: single traversal
  - ref: https://www.geeksforgeeks.org/select-a-random-node-from-a-singly-linked-list/
  - method: reservoir sampling
  
 */


/*
  Two Robot On Infinite Line
  ---------------------------
  
  - Two robot on an infinite line
  - ref: https://www.geeksforgeeks.org/puzzle-two-robot-parachute-line/
  - method: both move left one step with a no-op till one hits a parachute
            after this the one who found the parachute start to move left without no-op
 */

/*
  - detect if robot moving in circular path
  - ref: https://www.geeksforgeeks.org/check-if-a-given-sequence-of-moves-for-a-robot-is-circular-or-not/
  - method: own sol: NESW as doubly linked list circular queue with left as prev and right as next
                     keep count based on path. if in the end, N cancels S and E cancels W, then path is circular
 */

/*
  
  - Smallest number with D and I pattern
  - ref: https://www.careercup.com/question?id=5644221689102336
         https://hackerranksolutionc.blogspot.in/2017/10/minimum-number-by-pattern-i-d.html
  - method: own solution: each runlength of Ds calls for reversal of the corresponding part of the output

 */

/*

  Biased-Unbiased(coin/dice) and Random Number Generators
  --------------------------------------------------------
  
  - strategies
    - bits: generate n independent bits
    - bits with pruning: range generation and using specific values for probabilities(biased-unbiased)
    - range with pruning: grid/range generation and filtering extra values with looping
  
  - write code to simulate a fair coin
  - method: randInt() & 1
  
  - implement rand2 using rand1
  - ref: https://www.geeksforgeeks.org/implement-rand3-using-rand2/
  - method: bits with pruning: call rand1 twice and generate 2 bits of information(rand2 output). recurse if output>2 else repeat
  
  - implement rand1 using rand2
  - method: own sol: range with pruning: generate 0/1 using rand2 and check last bit
  
  - implement rand5 using rand1
  - ref: https://www.geeksforgeeks.org/implement-random-0-6-generator-using-the-given-random-0-1-generator/
  - method: bits with pruning: use rand1 to generate log(n) bits. if generated number < n, return the generated number else repeat
  
  - implement rand11 using rand5
  - ref: https://www.geeksforgeeks.org/implement-rand12-using-rand6-in-one-line/
  - method: generate range 0-11, with all numbers equally likely
    - range with pruning: rand5 + (6 * (rand5 & 1))
    - range with pruning: 2*rand5 - (rand5 * 1)
  
  - implement rand7(1-7) using rand5(1-5)
  - ref: https://www.quora.com/How-do-you-design-a-rand7-function-using-a-rand5-function
  - method:
      - method1: generate 3 bits of information i.e. implement rand1 using rand5 and then implement rand7 using this rand1
      - method2: expand th range: generate number in 5x5 grid, filterotu extra values and divide by 3
      - which method is better: method2 is better as it leads to probably less number of calls
      - can rand7 be implemented in deterministic way using rand5: no, why: because 5X is never divisible by 7
  
  - implement random number generator which returns a number from given array based on provided frequency
  - ref: https://www.geeksforgeeks.org/random-number-generator-in-arbitrary-probability-distribution-fashion/
  - method: create a frequency sum prefix array representing range from 1-frequencySum, now generate a random number
            in 1-frequencySum, find it's slot(ceil) in prefix array and return corresponding number
  
  
  Biased-Unbiased
  ----------------
  
  - biased using unbiased coin: 25-75 biased rand1 using unbiased rand1(create 25-75 biased coin using a fair coin)
  - ref: https://www.geeksforgeeks.org/generate-0-1-25-75-probability/
  - method: call rand1 twice to generate 2 bits of info i.e. number in range 0-3 and split this range in 25-75 ratio
            (e.g. return true if: number is 0, number is 1, number<1, number is > 0)
            (this is same as using left shift and XOR: (rand50()<1 | rand)....this can also be done using and-or)
  
  - biased to unbiased coin: given a 60-40 biased rand1, implement unbiased rand1 using this
                             also find average number of coin tosses
  - ref: https://www.geeksforgeeks.org/print-0-and-1-with-50-probability/
  - method1: check probability of generating 1 followed by 0 or 0 followed by 1
             https://stackoverflow.com/questions/5429045/c-puzzle-make-a-fair-coin-from-a-biased-coin
             expected number of trials before success = 1/p = 1/(coins are different) = 1(4/(HT + TH)) = 1/(1/2) = 2
             in each trial, we flip coins twice, therefore, number of coins toss = 2*2 = 4
 */

/*
  Probability of First Success(Geometric Progression)
  -----------------------------------------------------
  
  - prove that in geometric progression, expected number of trials for first success is 1/p,
    where p is the probability of success in each independent trial
  - ref: https://adhocery.wordpress.com/2011/02/11/expected-number-of-trials-until-success-of-a-bernoulli-random-variable/
         https://www.geeksforgeeks.org/expected-number-of-trials-before-success/
  
  - expected number of babies till first boy is born, given that probability of boy is 1/3
  - method: 1/p = 1/(1/3) = 3
  
  - expected number of throws of dice before 5 appears
  - method: 1/p = 1/(1/6) = 6
  
  - expected number of throws before all faces are different in a 6 dice throw
  - ref: https://math.stackexchange.com/questions/1282831/expected-number-of-throws-of-6-dice-until-all-6-faces-appear
  - method = 1/p = 1/[6!/(6^6)]
  
  
  Expected Value of a Random Variable
  ------------------------------------
            AND
  Linearity of Expectations
  --------------------------
  
  - ref: https://www.geeksforgeeks.org/linearity-of-expectation/
  
  - find exected value of sum of throws of two dice
  - method1: use expected value formula
    method2: use expected value of each throw of dice
  
  - expected number of heads in n tosses of a coin
  - method: E[X] = E[sum X[i]] = n*(1/2) = n/2
  
  - Coupon Collection Problem
    find expected number of coupons to collect each type of coupon
  - ref: https://www.geeksforgeeks.org/expected-number-of-trials-before-success/
         http://www.cse.iitd.ac.in/~mohanty/col106/Resources/linearity_expectation.pdf
  - method: use geometric distribution and linearity of expectations
  
  - Balls Into Bins: n balls are thrown into m bins
    - find expected number of balls in each bin
    - find expected number of empty bins
  - ref: http://www.cse.iitd.ac.in/~mohanty/col106/Resources/linearity_expectation.pdf
  - method: use random variable to define the problem and then use linearity of expectations
  
  - Hat shuffle and expected number of men with their own hat
  - method: E[X] = E[sum X[i]] = n(1/n) = 1
  
 */

/*
  Geometry
  ---------
  
  - check if two rectangles overlap or not(assumption: rectangles are horizontal)
  - ref: https://www.geeksforgeeks.org/find-two-rectangles-overlap/
  - method1: if rectangles are horizontal: check if their x and y ranges overlap or not
    method2: if rectangles are tilted: check if any corner of one rectangle lies inside other or not
  
  - check if point lies inside a triangle
  - ref: http://mathinstructor.net/2012/08/how-to-find-area-of-triangle-given-three-vertices/
  - method1: use special formula for area of triangle
    method2: use check for point inside polygon
    
  
  
  - given two points on a line, find the equation of the line
  - ref: http://www.coolmath.com/algebra/08-lines/12-finding-equation-two-points-01
  - method: slope intercept form: m = (y-y1)/(x-x1)
  
  - find orientation of three ordered points
  - ref: https://www.geeksforgeeks.org/orientation-3-ordered-points/
  - method: use slope of the three line segments
  
  - check if a point lies on a line segment or not
  - method: ensure that orientation is collinear and point is within min-max range of x and y coordinates
  
  - check if two lines intersect or not, given two points on eahc line
  - ref: https://www.topcoder.com/community/data-science/data-science-tutorials/geometry-concepts-line-intersection-and-its-applications/
  - method: derive the equation of the lines from the points
            solve the two equations to find point of intersection
  
  - check if two line segments(not lines) intersect or not
  - ref: https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
  - method1: find point of intersection of corresponding lines and check if the this point lies on both the line segments
    method2: use orientations:
  
  - check if three given points can form a triangle or not
  - ref: https://math.stackexchange.com/questions/937768/how-to-determine-if-a-triangle-can-be-drawn-with-the-given-points
  - method1: check area of the triangle, it should be non zero
    method2: check if the three points are collinear or not
  
  - check if a point lies inside a circle or not
  - ref: https://www.geeksforgeeks.org/find-if-a-point-lies-inside-or-on-circle/
  - method: use formula: (x-x1)^2 + (y-y1)^2 = r^2 i.e. find distance from centre, it should be less than radius
            https://math.stackexchange.com/questions/198764/how-to-know-if-a-point-is-inside-a-circle
  
  - find nearby cabs
  - ref: https://www.geeksforgeeks.org/finding-cabs-nearby-using-great-circle-distance-formula/
  - method: for each cab, check if the point lies in the cab's radius or not
  
  - find max count of collinear points
  - ref: https://www.geeksforgeeks.org/count-maximum-points-on-same-line/
  - method: for each point, find it's slope w.r.t other points and use hashmap to keep track of count of pairs with same slope, T(n^2)
            this questions is more about handling edge cases:
              1. how would you put slope(a decimal) in a hashmap
              2. points could be duplicate
              3. points could be vertical(infinite slope)
  
 */

/*
  
  Divisibility
  -------------
  
  - notes:
    - ref: https://en.wikipedia.org/wiki/Divisibility_rule
    - divisibility by 2: number ends with an even number
    - divisibility by 3: if sum of digits is divisible by 3
    - divisibility by 4: last two digits are divisible by 4
    - divisibility by 5: last digit is either 0 or 5
    - divisibility by 6: divisible by 2 and 3
    - divisibility by 7: 10a+b = a-2b
    - divisibility by 8: (num >= 8) + divisible by 4 and after division, divisible by 2
    - divisibility by 9: if the sum of digits is divisible by 9
    - divisibility by 10: last digit is 0
    - divisibility by 11: alternating sum of digits
    - divisibility by 12: divisible by 3 and 4
    - divisibility by 14: divisible by 2 and 7
    
  - find formula to check divisibility by 7
  - ref: https://www.geeksforgeeks.org/divisibility-by-7/
  - method: 10a+b = 20a+2b = 21a - a + 2b = -a + 2b = a - 2b
  
  - find method for divisibility check by 11
  - ref: https://www.math.hmc.edu/funfacts/ffiles/10013.5.shtml
  - method: alternating sum of digits from left to right
            (proof: 1, 100, 10000....leaves 1 as remainder on division by 11
                    10, 1000, 100000....leaves -1 as remainder on division by 11)
  
  - prove that p^2 – 1 is divisible by 24 if p is a prime number greater than 3?
  - ref: http://www.crazyforcode.com/prove-p2-1-divisible-24/
  - method: p^2-1 = p^2-1^2 = (p+1)(p-1)
            atleast one of p-1, p, p+1 must be divisible by 3 since they are consecutive
            since p is prime, either p-1 or p+1 must be divisible by 3
            also, since p is prime, it must be odd, that means p-1 and p+1 are even: division by 4
            on division by 2: (p-1)/2 or (p+1)/2 must be even: one more division by 2
            (simply said: in every consecutive pair of even numbers, one is divisible by 2 and other is divisible by 4)
  
  DFA based division
  -------------------
  
  - use DFA to check divisibility of two numbers
    (find remainder without using division)
    (check divisibility of two numbers without using division)
  - ref: https://www.geeksforgeeks.org/dfa-based-division/
  - method: build table for DFA transitions. process biggest number bit by bit, doing state transitions
  
  - check divisibility of a binary stream by a number
  - ref: https://www.geeksforgeeks.org/check-divisibility-binary-stream/
  - method: process binary stream 1 bit at a time and keep track of remainder
  
  - check if a number is divisibile by 3, constraint: T(lg n)
  - ref: https://www.geeksforgeeks.org/write-an-efficient-method-to-check-if-a-number-is-multiple-of-3/
  - method1: check if sum of digits is divisible by 3
    method2: process binary stream of input number over DFA for divisibility by 3
    method3: prove and use the fact that x is divisible by 3 if (sum of odd bits-sum of even bits is divisible by 3)
             (i.e. prove alternating sum theory for 11 and apply the same to bits, where 11 is 3 in binary)
 */

/*
  
  Factorial
  ----------
  
  - find number of trailing zeros in n!, without calculating n!
  - ref: https://www.geeksforgeeks.org/count-trailing-zeroes-factorial-number/
  - method: find highest power of 5 that divides n!, which is same as floor(n/5) + floor(n/5^2) + floor(n/5^3) + ....
  
  - find max power of prime number that divides n!
  - ref: https://stackoverflow.com/questions/23349065/given-x-y-how-to-find-whether-x-is-divisible-by-y-or-not
  - method: use above shortcut formula(proof: similar to trailing zeros in n!)
  
  - check if x! is divisible by y, given that you cannot calculate x!
  - ref: https://stackoverflow.com/questions/23349065/given-x-y-how-to-find-whether-x-is-divisible-by-y-or-not
  - method: if (y<x) true else:
            (compute prime factorization of x! an y. check if factors of y are subset of factors of x!)
              - generate all prime numbers till y and break y into it's prime factors
              - ensure for each such prime p, max power of p that divides n! < power of p in y
  
  
  
 */

/*
  Prime Numbers
  --------------
  
  - check if a number is prime or not
  - ref: https://www.geeksforgeeks.org/primality-test-set-1-introduction-and-school-method/
  - method1: naive method: check if n is divisible by 2 or any odd number between 3 to n-1
      - optimizations:
         - optimization1: check all numbers from 2 to sqrt(n), why:
            because if n is not prime, it must have atleast one factor smaller than sqrt of n
            ref: https://stackoverflow.com/questions/5811151/why-do-we-check-up-to-the-square-root-of-a-prime-number-to-determine-if-it-is-pr
         - optimization2: do pseudo prime i.e. probability based prime test first
    method2: generate all primes till sqrt(n) and check if any of them divide n
  
  - sieve of eratosthenes: generate all prime numbers till n
  - ref: https://www.geeksforgeeks.org/sieve-of-eratosthenes/
  - method: generate a list of numbers from 2 to n and keep on reducing possibilities for all primes between 2 to sqrt(n)
  
  - count all numbers in given range with prime number of set bits
  - ref: https://www.geeksforgeeks.org/prime-number-set-bits-binary-representation/
  - method: brute force:
            generate all prime numbers till 2^n(number just greater than end of range) and put in hashset
            now iterate through from start to end and check number of set bits
  
 */

/*

  Expression
  -----------
  
  ref: https://www.geeksforgeeks.org/stack-set-2-infix-to-postfix/
  
  - which form(infix, postfix, prefix) of expression is most suitable for evaluation and why?
  - ref: https://www.geeksforgeeks.org/stack-set-2-infix-to-postfix/
  - method:
      1. Infix : no, because in infix, we will have to do repeated scanning to evaluate the highest precedence operator first
      2. Prefix: no, because for evaluation, scanning has to be done from right to left(which is not so natural)
                 also, conversion of infix to prefix involves string reversals(again not so natural)
      3. Postfix: yes, both conversion from infox as well as evaluation are pretty easy
  
  
  Expression Conversions
  -----------------------
  
  Conversion: From Infix
  -----------------------
  - infix to postfix
  - ref: https://www.geeksforgeeks.org/stack-set-2-infix-to-postfix/
  - method: 1. use stack: ref: https://www.youtube.com/watch?v=vXPL6UavUeA
            2. operator: push operators on stack till lower precedence operator shows up
            3. brackets: special treatment for brackets
            4. operand: operands are always added to output
  
  - infix to prefix
  - ref: https://www.geeksforgeeks.org/convert-infix-prefix-notation/
  - method: convert brackets + reverse , use infix to postfix method, reverse again
  
  
  Conversion: To Infix
  ---------------------
  - postfix to infix
  - ref: https://www.geeksforgeeks.org/postfix-to-infix/
  - method: use stack: push on stack, once you get an operator, pop last two operands..combine and push back
  
  - prefix to infix
  - ref : https://www.geeksforgeeks.org/prefix-infix-conversion/
  - method: reverse, do postfix to infix conversion and reverse
  
  
   Conversion: Prefix-Postfix
  ----------------------------
  
  - postfix to prefix
  - ref: https://www.geeksforgeeks.org/postfix-prefix-conversion/
  - method: use postfix evaluation procedure, but instead of evalution, do creation of prefix expression
  
  
  - prefix to postfix
  - ref: https://www.geeksforgeeks.org/prefix-postfix-conversion/
  - method: reverse and use postfix evaluation procedure, but instead of evalution, do creation of postfix expression
  
  
  Expression Evaluation
  ----------------------
  
  - postfix evaluation
  - ref: https://www.geeksforgeeks.org/stack-set-4-evaluation-postfix-expression/
  - method: use stack: scan from left to right
              keep pushing on stack till you get an operator, upon which you pop 2 operands and combine them and push back the result
  
  - prefix evaluation
  - ref: https://www.geeksforgeeks.org/evaluation-prefix-expressions/
  - method: reverse and use postfix evaluation procedure
  
  - infix evaluation(using postfix)
  - method: convert to postfix expression and evaluate postfix expression
  
  - infix evaluation(without using postfix)
  - ref: https://www.geeksforgeeks.org/expression-evaluation/
  - method: the key is to do infix to postfix conversion(and do postfix evaluation during the conversion)
            this means we need to stacks: operator stack for conversion and operand stack for evaluation. basically:
              1. instead of outputting operands, push them onto operands stack
              2. if an operator needs to be outputted, evaluate it
  
  Expression Tree
  ----------------
  
  - ref: https://www.geeksforgeeks.org/expression-tree/
  
  - given an infix expression, create it's expression tree
  - method: convert infix expression into postfix expression
            then create expression with method similar to postfix expression evaluation
  
  - given prefix expression, create it's expression tree
  - method: reverse and use postfix evaluation procedure
  
  - evaluate expression tree
  - ref: https://www.geeksforgeeks.org/evaluation-of-expression-tree/
  
  
  Expression Related Questions
  -----------------------------
  
  - validate pair of braces in an expression, given there is only one type of braces: ()
  - method1: use stack
    method2: use counter
  
  - validate pair of braces in an expression with multiple types of braces: [{()}]
  - method1: use stack(works well even there are multiple types fo braces)
    method2: use a counter for each type of brace
  
  - validate pair of braces in an expression, given following precedence in barcket type: [ > { > ( i.e.
      [] can enclosed [], {} and ().....{} can enclosed {}, ().....() can enclosed only ()
  - method1: use stack
    method2: use counter for each brace type: while incrementing counter of a brace, counter for lower precedence braces should be 0
    method3: use stack of counters
  
  - minimum number of braces inversions to balance braces in expression
  - ref: https://www.geeksforgeeks.org/minimum-number-of-bracket-reversals-needed-to-make-an-expression-balanced/
  - method: use stack and check for expression balancing by reducing expression to m+n: ...}}}{{{...: ceil(m)+ceil(n)
  
  - check if two expressions are same or not, e.g. -(a+b)+(c+d) and -b-a+d+c, operands will be a-z only
  - ref: https://www.geeksforgeeks.org/check-two-expressions-brackets/
  - method: use stack to find normalized sign and hashtable to keep count of each operand
  
  - find equal point for brackets in a random string of braces
  - ref: https://www.geeksforgeeks.org/find-equal-point-string-brackets/
  - method: create leftOpen and rightClosed arrays and find index i where leftOpen[i] = rightClosed[i]
  
  - () defines an onion of depth 1, (()) of 2 and so on.
    find all onions with their depth in a given string e.g. (()) () )) ((( () () ()))))
  - method: own sol: use counter/stack. print onion when counter=0 or stack is empty
  
  - you have been given a generator string ab from which any number of strings can be generated by repeatedly
    inserting ‘ab’ at any location. check if a given string can be generated by this procedure.
    eg. Input: aabbab  Output: valid, Input: abbaab  Output: Invalid, constraint: T(single traversal)
  - method: own sol: this is same as validating balanced parenthesis....ab is ()
  
 */

/*
 */

/*

 */

/*

 */

/*
  Binomial Coefficients and Pascal's Triangle
  --------------------------------------------
  
  Binomial Coefficients
  ----------------------
  - Coefficients in equation: (a+b)^n = sum i in [0, n] nCk a^k b^(n-k)
  - Properties:
    - nCk = (n-1)C(k-1) + (n-1)Ck
    - nCk = nC(n-k)
  
  - write code to calculate binomial coefficients
  - ref: https://www.geeksforgeeks.org/space-and-time-efficient-binomial-coefficient/
  - method: use simpel loop, conver k to n-k if needed
  
  - print pascal's triangle
  - ref: https://www.geeksforgeeks.org/pascal-triangle/
  - method1: use binomial coefficient formula, T(n^3) S(1)
    method2: generate each row from previous row, T(n^2) S(n)
    method3: generate each cell from previous cell in the same row, T(n^2) S(1)
  
  - print pascal triangle and print it in triangle form
  - ref: https://www.programiz.com/c-programming/examples/pyramid-pattern
  - method: take care of spaces at start of each row, print each number with a space after it
  
 */

/*

  Tree Count
  -----------
  
  BST Count
  ----------
  
  - find count of distinct BSTs that can be formed given array of size n(having distinct values)
  - ref: https://www.geeksforgeeks.org/total-number-of-possible-binary-search-trees-with-n-keys/
  - method1: select each node as root of bst, recursively find count of left and right subtrees and add the product of count to total count
    method2: catalan numbers
  
  - print all possible BSTs with given nodes
  - https://www.geeksforgeeks.org/construct-all-possible-bsts-for-keys-1-to-n/
  - method: recursively create list of all possible BSTs and print the list
  
  Tree Count
  -----------
  
  - find count of distinct binary trees with n unlabelled nodes
    (find count of structurally distinct binary trees with n nodes)
  - https://www.geeksforgeeks.org/enumeration-of-binary-trees/
  - method: since nodes are not labelled, their ordering/permutation doesnot matter
            now we can choose each node as root and recursively create subtrees
            since all nodes are same, choosing different nodes as root will result in same tree
            i.e. it's useless to choose each nodes as root...we can choose any node as root
            and recurse with different partitions of rest of the nodes as left and right subtrees
            just like BST count, this also comes out to be catalan number
  
  - why there count of structurally different trees with n nodes is same as bst count with n nodes
  - method: because for a given structure, there is only 1 valid bst with n ordered nodes
  
  - find count of distinct binary trees with n labelled nodes
  - ref: https://www.geeksforgeeks.org/enumeration-of-binary-trees/
  - method1: an unlabelled binary tree of n nodes with a given structure can be labelled with n keys in n! ways
             => for each unlabelled tree, there are n! labelled trees
             => total labelled tree with n nodes = n! * (total unlabelled tree with n nodes) = n! * catalan number
    method2: we can also prove this based on the fact that a labelled binary tree of n nodes has n-1 edges
             ref: https://gatecse.in/number-of-binary-trees-possible-with-n-nodes/
    
 */

/*
 
  Catalan Number
  ---------------
  
  - calculate nth catalan number
  - ref: https://www.geeksforgeeks.org/program-nth-catalan-number/
  - method1: use recursive definition: c(n) = sum from 0 to n-1 [c(i)*c(n-i-1)]
    method2: use dynamic programming on top of recursion
    method3: use combinatorics based formula: c(n) = 2nCn/(n+1)
  
  APPLICATIONS OF CATALAN NUMBER
  -------------------------------
  
  - ref: https://www.geeksforgeeks.org/applications-of-catalan-numbers/
         https://en.wikipedia.org/wiki/Catalan_number
  
  - count of BSTs with n keys or count of unlabelled binary trees
  - method: choose each node as root and recurse for left and right = Cn
  
  
  - count of expressions with correctly matched pair of n paranthesis
  - ref: https://www.geeksforgeeks.org/find-number-valid-parentheses-expressions-given-length/
  - method: for i in 0 to n-1, i is number of parenthesis on left of current parenthesis and n-i-1 are inside it
  
  - count of dyck words: string on n X and Y, such that count of Y never exceeds X from left to right
  - method: this is same as paranthesis, XY being ()
  
  - count of lattice paths: monotonic paths to diagonally opposite vertex of a square, which never crosses diagonal
  - method: since the paths never cross diagonal, therefore vertical moves must always be less than horizontal modes
            this is same as valid parenthesis problem
  
  
  - number of ways to parenthsize n+1 numbers or n applications of a binary operator
  - method: we split n numbers at any index and recurse
  
  - given an expression of binary operators, find number of different ways to evaluate the expression
    e.g.: 1+2*3, can be evaluated as (1+2)*3 or 1+(2*3)
  - method: catalan number
  
  - count of full binary trees with (n+1) leaves(or n internal nodes)
  - method: treat leaves as numbers and internal nodes as binary operator
  
  - unexplained applications
    - ways to divide polygon with (n+2) sides
    - stack sortable permutations
    - permutations that avoid 123
  
  
  Related Problems
  -----------------
  
  - print all valid expressions of n pair of parenthesis. also find formula for the count
  - ref: https://www.geeksforgeeks.org/print-all-combinations-of-balanced-parentheses/
  - method1: bottom up recursion
      use catalan form of recursion to print all valid expressions. use catalan number formula to get the count
    method2: top-down recursion
      use definition of balanced parenthesis with recursions and catalan number formula
      if (can put open bracket: count of open brackets < n) put open brakcet and recurse
      if (can put closed bracket: count of open > closed) put closed brakcet and recurse
  
  - check if a given expression has valid parenthesization or not
  - https://www.geeksforgeeks.org/check-for-balanced-parentheses-in-an-expression/
  - method: use stack
  
  - count number of paths to travel from top-left corner of a grid of size nxm to diagonally opposite corner,
    given that you can only move right or down
  - ref: https://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/
  - method1: use recursion
      paths(m, n) = paths(m-1, n) + paths(m, n-1)
      paths(m, n) = 1, if m=1 or n=1
    method2: use dypro
    method3: use combinatorics: https://betterexplained.com/articles/navigate-a-grid-using-combinations-and-permutations/
      we need to take (m-1) vertical and (n-1) horizontal steps out of (m-1)+(n-1) total steps
      so total paths = (m-1 + n-1)C(m-1) = (m-1 + n-1)C(n-1)
  
  - given an infinite rectangular grid, what's the probability that a person will reach mxn cell from top-left in k steps(right or down)
  - ref: https://betterexplained.com/articles/navigate-a-grid-using-combinations-and-permutations/
  - method: total paths = 2^k
            total valid paths to bottom = (m-1 + n-1)C(n-1)
            probability = (m-1 + n-1)C(n-1)/2^k
  
 */

/*
  Tiling Problem
  ---------------
  
  - count ways to tile 2xn floor with 1x2 tiles
  - ref: https://www.geeksforgeeks.org/tiling-problem/
  - method: c(n) = c(n-1) + c(n-2), c(1)=1, c(2)=2
  
  - count ways to tile nxm floor with 1xm tiles
  - ref: https://www.geeksforgeeks.org/count-number-ways-tile-floor-size-n-x-m-using-1-x-m-size-tiles/
  - method: c(n) = c(n-1) + c(n-m), c(i, i=[0, m-1])=1, c(m)=2
  
 */

/*

  Puzzles
  --------

  (timepass, not really relevant now a days)
  
  - all puzzles: https://www.geeksforgeeks.org/category/puzzles/
  
  
  - given 2 eggs, highest floor where egg breaks, constraint: minimize the count of egg drops
  - ref: https://www.geeksforgeeks.org/puzzle-set-35-2-eggs-and-100-floors/
  - method1: linear search
    method2: binary search
    method3: search with jumps of decreasing size
  
  - chessboard and dominoes puzzle
  - ref: https://www.geeksforgeeks.org/puzzle-25chessboard-and-dominos/
  - method: no, why: because dominoes of same color have been taken out
  
  - given a pile of coins on table, with 10 coins with tail side up
    divide it in 2 piles so that each has same number of tails facing up
  - ref: https://www.geeksforgeeks.org/puzzle-39-hundred-coin-puzzle/
  - method: divide it into two piles of 90 and 10 coins, flip all 10 coins in 2nd pile
    - why this works: https://www.geeksforgeeks.org/puzzle-24-10-coins-puzzle/
  
 */

/*
  Skyline Problem
  ----------------
  
  - find skyline of a given set of buildings in a 2D coordinate system
  - ref: https://www.geeksforgeeks.org/divide-and-conquer-set-7-the-skyline-problem/
  - method1: sweep line algorithm: use heap or TreeMap: https://www.youtube.com/watch?v=GSBLe8cKu0s
             (this is similar(not same) to merging overlapping intervals)
    method2: use merge sort algo: https://www.youtube.com/watch?v=Cv0ft2dFz80
    note: take care of edge cases like:
      - building starting at same point
      - building ending at same point
      - build stating and ending at same point
      - redundant skyline points
  
 */

/*
  Largest Rectangle In Histogram
  -------------------------------
  
  - find largest rectangle in a histogram, constraint: T(n) S(n)
  - ref: https://www.geeksforgeeks.org/largest-rectangle-under-histogram/
  - method1: brute force: consider every pair of i and j, and find area between the bars: T(n^3)
    method2: for each i as starting bar, find area with rest of bars to right of it: T(n^2)
    method3: divide and conquer: split at min value in current array of bars
             (but this is over complicated as it will require RMQ using segment trees)
    method4: use find next smaller algo
  
 */

/*
  Random Number Generator
  ------------------------
  
  - write your own pseudo random number generator that generates random number in range [0, 2^15]
  - ref: https://www.quora.com/How-do-you-create-your-own-randomize-function-in-C
  - method: mult, add....mult, rem
      int seed = 1
      def rand() = abs((seed * 137423743 + 12345) * 2^16) % 2^15)
  
  - write your own random number to generate random number between 0 and 1, given rand() that generates rand between [l, h]
  - method: (rand() - l)/(h-l)
  
  - given rand that gives random number in range[0, 1], write rand that given random number in range [l, h]
  - ref: https://www.geeksforgeeks.org/generating-random-number-range-c/
  - method: rand*(l-h) + l
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  
  -
  - ref:
  - method:
  
 */

/*
  Numeric Palindromes
  --------------------
  
  - given a number n, write code to count/print number of palindromes till n
  - ref: https://www.geeksforgeeks.org/generate-palindromic-numbers-less-n/
  - method: for each number between 1 and n, generate even length and odd length palindromes till palindrome < n
  
  - next higher number using same digits
  - method:
      - find rightmost digit which is lesser than it's next digit: num1
      - find least digit higher than num1 and lying right of it: num2
      - swap num1 and num2
      - reverse all digits right of new num1 to get the next higher number using the same digits
      (if any of the above step is not feasible, then return null)
  
  - next higher palindrome using same digits
  - ref: https://www.geeksforgeeks.org/next-higher-palindromic-number-using-set-digits/
  - method: break the input array into two equal halves(ignore mid in case of odd length string)
            find next higher number for first half(special algorithm for this): num1
            return (num1 + mid + reverse(num1))
  
  - next higher palindrome
  - ref: https://www.geeksforgeeks.org/given-a-number-find-next-smallest-palindrome-larger-than-this-number/
  - method: break the input array into two equal halves(ignore mid in case of odd length string)
            case1: right half < reverse of left: mirror left half to right half
            case2: right half >= reverse of left: add 1 to left half and mirror left half to right half
  
 */

/*
  
  Modulus: (a^b)%m,
  -----------------
  
  Properties
  -----------
  - % is distributive over + and *
    (a + b)%m = (a%m + b%m) % m
    (a * b)%m = (a%m * b%m) % m
  
  
  - find a%m, where a is a large number in a file and m^2 < INT_MAX < m^3
  - ref: https://www.geeksforgeeks.org/how-to-compute-mod-of-a-big-number
  - method: if a % b = c, then a = b*n + c
            => (10a + d) % b = (10(b*n + c) + d) % b
                             = (10*b*n + 10c + d)%b
                             = (10c + d)%b
            therefore, keep on reading large number one digit at a time and keep on updating the result
  
  - find (a^b)%m efficiently, where b is very large and m^2 < INT_MAX < m^3
  - ref: https://stackoverflow.com/questions/2177781/how-to-calculate-modulus-of-large-numbers
  - method1: use the fact that % is distributive over *
             therefore, iteratively calculate %m for a, a^2, a^3, ....., a^m
    methdo2: repeated squaring by breaking into powers of 2
  
  
  - solve (a*b)%m, where a,b and m are all very large
  - ref: https://www.geeksforgeeks.org/find-abm-large/
  - method:
      - reduce a: use method of finding a%m, where a is very large stream of digits
      - reduce a^b: use repeated squaring
      - reduce (a^b)%m: use the fact that % is distributive over *
      - final solution: combine all of above: find (a^b)%m using repeated squaring
 */

/*
  Exponentiation
  ---------------
  
  - find efficient ways to calculate a^n for large values of n
  - ref: https://en.wikipedia.org/wiki/Exponentiation_by_squaring
  - method1: iterative
    method2: recursive: repeated squaring
             x^n = (x^(n/2))^2, if n is even
                 = x*(x^((n-1)/2))^2, if n is odd
    method3: tail recursive: repeated squaring
             (output, x, n) = (output, x*x, n/2), if n is even
                            = (output*x, x*x, (n-1)/2), if n is odd
    method4: breaking exponent into powers of 2(this is same as repeated squaring and using result of ith squaring if ith bit is on in n)
 */

/*
  
  Add Subtract without Arithmetic Operators
  -------------------------------------------
  
  - add two numbers without using +
  - ref: https://www.geeksforgeeks.org/add-two-numbers-without-using-arithmetic-operators/
  - method: add(x, y) = x, if y=0
                        add(x^y, (x&y)<<1)
  
  - subtract two numbers without using -
  - ref:
  - method: sub(x, y) = x, if y=0
                      = add(x^y, (~x&y)<<1)
                      
  - multiply two numbers without using multiplication and bitwise operators
  - ref: https://www.geeksforgeeks.org/multiply-two-numbers-without-using-multiply-division-bitwise-operators-and-no-loops/
  - method: use the fact that multiplication is repetitive addition
  
 */

/*
  Fibonacci Numbers
  ------------------
  
  - calculate nth fibonacci number
  - ref: https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
  - method1: use recursion, but this is exponential time, why:
             T(n) = T(n-1) + T(n-2) = 2*T(n-2) + T(n-3) > 2(2*T(n-4)) > 2^(n/2)T(1) = T(2^n)
    method2: dynamic programming with S(n).... T(n)
    method3: dynamic programming with S(1)..... T(n)
    method4: using matrix multiplication or othermathematical formulas....T(lg n), S(1)
 */

/*
  DFA
  ----
  
  - design DFA that accept string containing even a's and odd b's
  - ref: https://www.youtube.com/watch?v=N1IvyTpzJmY
  - method: design 2 bit DFA i.e. having 4 states
  
  - design DFA that accept string containing even number of a's and b's
  - method: design 2 bit DFA i.e. having 4 states
  
 */

/*
  Base System Conversions
  ------------------------
  
  - ref: https://www.tutorialspoint.com/computer_logical_organization/number_system_conversion.htm
  
  - decimal(base 10) number to binary(some other non-decimal) base
  - method: keep on finding remainders and quotients
  
  - binary to decimal
  - method: sum of products of positional values
  
  - non-decimal base to non-decimal base(example: octal/hexa decimal to binary)
  - method: non-decimal base 1 -> decimal -> non-decimal base 2
  
  - binary to 2^n base(example: binary to octal)
  - method: divide the binary number into groups of n. write char for each group
  
  - 2^n base to binary(example: octal to binary)
  - method: write binary form of each char in 2^n base
  
  Excel(base 26) to decimal
  --------------------------
  
  gotcha: excel counting starts from 1(not 0)
  
  - excel column string to decimal
  - ref: https://www.geeksforgeeks.org/find-excel-column-number-column-title/
         https://www.programcreek.com/2014/03/leetcode-excel-sheet-column-number-java/
  - method: sum of product of positional values with base 26
  
  - decimal number to excel column string(base 26)
  - ref: https://www.geeksforgeeks.org/find-excel-column-name-given-number/
         https://www.programcreek.com/2014/03/leetcode-excel-sheet-column-title-java/
  - method: use repeated remainder and quotient method
  
  Magic Number
  -------------
  
  - magic number is a number formed by adding one or more powers of 5: 5, 25, 25+5=30, 125, 125+5=130, 125+25=150, 125+25+5=155
    calculate nth magic number
  - ref: https://www.geeksforgeeks.org/find-nth-magic-number/
  - method: this is same as finding nth number in number system with base 5
            (note: here no base conversion is involved, we are directly calculating nth number in base 5, where n is in decimal number system)
  
 */

/*
  Successive Perfect Squares
  ---------------------------
  
  - given a number n, print all perfect squares <= n, constraint: you can only use + operator
    e.g. if n is 30, o/p: 1 4 9 16 25.
  - method: (i+1)^2 - i^2 = (i+1 + i)(i+1 - i) = 2i + 1
            while (prevSquare < n) nextSquare = 2*prevSquare + 1

 */

/*
  Max Product of 3 numbers
  -------------------------
  
  - given an array of +ve and -ve numbers, find max product possible with 3 numbers, constraint: T(single scan)
  - ref: https://leetcode.com/problems/maximum-product-of-three-numbers/solution/
  - method: max(max1 * max2 * max3, max1 * min1 * min2)
  
 */

/*

  Easy
  -----
  - given a cube of size n, constructed by n^3 smaller 1 unit white cubes.
    you painted the cube in black from outside, how many cubes are still in white color.
  - method: (n-2)^3(n > 1), why: because after removing outer layer of cube, length of each side of cube is n-2
  
  - count number of unique handshakes possible between n people
  - method: nC2
  
  - check endianness of a machine. does endianness matter for programmer? does it matter for file i/o? which endianness is better?
  - ref: https://www.geeksforgeeks.org/little-and-big-endian-mystery/
  - method:
    - checking endianness
      - java: print java.nio.ByteOrder.nativeOrder()
      - c: typecast unsigned int to char pointer
    - endianness matters for programmers in case of network i/o
    - it doesnot matter in case of file i/o since file stores chars in single byte
    - there is no specific benefit of one endianness over another, both are equal
  
  - given an array of integers, replace each element with the product of the remaining elements, without using division
  - method: store cumulative product from right and left
  
  - find prefect square closest to a given number
  - ref: https://www.careercup.com/question?id=14467683
  - method: find square root of n say r, return min(abs(r^2 - num), abs((r+1)^2) - num)
  
  - given n pencils, each having length l. each pencil can write 4 km and gets reduced to length l/4.
    you can join 4 pencils and make 1 pencil again. you can’t make pencil if pieces are < 4.
    calculate total length that you can write
  - method: own sol: use recursion
     - sol(full, partial) = 4*full + sol( (full/4 + p)/4, (full/4 + p) mod 4 )
       answer = sol(n, 0)
  
  - some people are standing in a queue and only even people are selected.
    again out of these only even people are selected.This happens until we are left with one.
    find out the position of that person in the original queue.
  - method: run a few examples
      observation: assuming 1 based indexing, last number left will be the highest power of 2 closest to size of initial queue
  
  - check for integer overflow and underflow in addition
  - ref: https://www.geeksforgeeks.org/check-for-integer-overflow/
  - method1:
      only two numbers with same sign can cause overflow/underflow in addition
      so just add the two numbers and check if sign of result is different from that of numbers
    method2: check for over/underflow before addition: a+b>INT_MAX => a > INT_MAX - b
  
  - n people in party exchange gifts and the array of the number of gifts each person receives is given to you.
    check if such an arrangement is possible and if it’s possible then enumerate gift exchanges
  - method: own sol: if arrangement is feasible, then no number should repeat and all numbers should be there
                     find cycles of exchange of gifts and print them
  
  - count or enumerate all possible string decodings of a number string
  - ref: https://www.geeksforgeeks.org/count-possible-decodings-given-digit-sequence/
  - method: use recursion(can use memoization for efficiency)
  
 */
