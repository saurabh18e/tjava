package com.practice.ds.list;

public class MultiList {

  /*
    
    List Of List: Two Level List
    -----------------------------
    
    - flatten list of list i.e. 2 level list into a SORTED list
    - ref: https://www.geeksforgeeks.org/flattening-a-linked-list/
    - method:
        - use recursion on next node
        - now do sorted merge of two lists:
            - current node list with down pointers
            - recursive node list with down(or right) pointers
   */
  
  
  /*
    
    Recursive Linked List (Multi Level Linked List)
    ------------------------------------------------
    
    note: this type of list is similar to a binary tree(but not exactly a binary tree)
          so we can traverse this linked list in DFS or BFS order
    
    - flatten recursive linked list(preorder depth wise i.e DFS traversal)
    - ref: https://www.geeksforgeeks.org/flatten-a-multi-level-linked-list-set-2-depth-wise/
    - method1: use recursion to flatten child list(recursion with return list with end pointer for optimization)
               use recursion/iteration to flatten siblings
               merge the lists
        
    - flatten recursive linked list level by level(breadth wise i.e. BFS traversal)
    - ref: https://www.geeksforgeeks.org/flatten-a-linked-list-with-next-and-child-pointers/
    - method: use queue
    
    
   */
   
}
