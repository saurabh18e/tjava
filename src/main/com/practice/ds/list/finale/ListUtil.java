package com.practice.ds.list.finale;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListUtil {
    
    public static ListNode deleteNext(ListNode head) {
        ListNode n = head.next;
        head.next = head.next.next;
        n.next = null;
        return n;
    }
    
    public static void addNode(ListNode head, ListNode n) {
        n.next = head.next;
        head.next = n;
    }
    
    public static ListNode find(ListNode list, int n) {
        while (list != null) {
            if (list.val == n) return list;
            list = list.next;
        }
        return list;
    }
    
    public static ListNode toList(int... arr) {
        ListNode head = new ListNode(0);
        ListNode tail = head;
        for (int a : arr) {
            ListNode n = new ListNode(a);
            addNode(tail, n);
            tail = n;
        }
        return head.next;
    }
    
    public static ListNode dummyNode(ListNode l1) {
        return new ListNode(Integer.MIN_VALUE, l1);
    }
    
    public static ListNode dummyNode() {
        return dummyNode(null);
    }
    
    public static void moveNextToHead(ListNode head, ListNode curr) {
        ListNode n = curr.next;
        curr.next = n.next;
        n.next = head.next;
        head.next = n;
    }
    
    public static String getString(ListNode head) {
        List<Integer> l = new ArrayList<>();
        if (head != null && head.val == Integer.MIN_VALUE) head = head.next;
        while (head != null) {
            l.add(head.val);
            head = head.next;
        }
        return Arrays.toString(l.toArray());
    }
    
    public static int distance(ListNode n1, ListNode n2) {
        int count = 0;
        while (n1 != n2) {
            count++;
            n1 = n1.next;
        }
        return count;
    }
    
    public static int length(ListNode list) {
        int count = 0;
        while (list != null) {
            count++;
            list = list.next;
        }
        return count;
    }
}
