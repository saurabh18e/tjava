package com.practice.ds.list.finale;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

import static com.practice.ds.list.finale.ListUtil.*;

public class Easy {
}

class MergeSortedLists {
    
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        
        ListNode head = dummyNode();
        ListNode tail = head;
        ListNode list1 = dummyNode(l1);
        ListNode list2 = dummyNode(l2);
        
        while (list1.next != null && list2.next != null) {
            ListNode n = (list1.next.val <= list2.next.val) ? deleteNext(list1) : deleteNext(list2);
            addNode(tail, n);
            tail = n;
        }
        tail.next = (list1.next != null) ? list1.next : list2.next;
        return head.next;
    }
    
    public static ListNode mergeTwoLists2(ListNode list1, ListNode list2) {
        if (list1 == null) return list2;
        if (list2 == null) return list1;
        
        ListNode head = dummyNode();
        ListNode tail = head;
        
        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                tail.next = list1;
                list1 = list1.next;
            } else {
                tail.next = list2;
                list2 = list2.next;
            }
            tail = tail.next;
            tail.next = null;
        }
        tail.next = (list1 != null) ? list1 : list2;
        return head.next;
    }
    
    public static void main(String[] args) {
        ListNode list1 = mergeTwoLists(toList(1, 2, 5), toList(1, 3, 4));
        System.out.println("merged list is: " + getString(list1));
    }
}

class Reverse {
    
    /*
        - move to start-prev...since start might be head, so we need dummy head
        - reverse next k-1 nodes if they exist
     */
    public static ListNode reverseSublist(ListNode l, int start, int end) {
        if (l == null || start == end) return l;
        
        ListNode head = dummyNode(l);
        ListNode subListHead = head;
        for (int k = 1; k < start; k++) {
            subListHead = subListHead.next;
        }
        
        ListNode curr = subListHead.next;
        for (int i = start; i < end; i++) {
            moveNextToHead(subListHead, curr);
        }
        
        return head.next;
    }
    
    public static ListNode reverselist(ListNode l) {
        if (l == null) return l;
        ListNode head = dummyNode(l);
        for (ListNode curr = head.next; curr.next != null; ) {
            moveNextToHead(head, curr);
        }
        return head.next;
    }
    public static ListNode reverse(ListNode l) {
        if (l == null) return l;
        ListNode prev = null, curr = l, next = null;
        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }
    
    public static ListNode reverseSublistChunks(ListNode l, int k) {
        if (l == null || k == 1) return l;
        
        ListNode head = dummyNode(l);
        ListNode subListHead = head;
        boolean lastLeg = false;
        while (subListHead.next != null) {
            ListNode curr = subListHead.next;
            int count = 1;
            for (; count < k && curr.next != null; count++) {
                moveNextToHead(subListHead, curr);
            }
            
            if (count == k) {
                subListHead = curr;
            } else if (!lastLeg) {
                lastLeg = true;
            } else {
                break;
            }
        }
        
        return head.next;
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        ListNode list1 = reverseSublist(toList(arr), 3, 5);
        System.out.println("sublist-reversed list is: " + getString(list1));
        
        ListNode list2 = reverselist(toList(arr));
        System.out.println("reversed list is: " + getString(list2));
        
        ListNode list3 = reverseSublistChunks(toList(arr), 2);
        System.out.println("k-group-reversed list is: " + getString(list3));
    }
}

class CyclesAndOverlap {
    
    public static int cycleLength(ListNode list) {
        if (list == null) return 0;
        int length = 1;
        for (ListNode curr = list.next; curr != list; curr = curr.next, length++) {
        }
        return length;
    }
    
    public static ListNode checkCycle(ListNode list) {
        // check cycle: using fast-slow: rabbit-hare algo
        ListNode slow = list;
        ListNode fast = list;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) break;
        }
        if (fast == null || fast.next == null) return null;
        return fast;
    }
    
    public static ListNode cycleStart(ListNode list) {
        // check cycle: using fast-slow: rabbit-hare algo
        ListNode fast = checkCycle(list);
        if (fast == null) return null;
        
        // cycle start: using fast-slow: move-ahead algo
        int length = cycleLength(fast);
        fast = list;
        for (int i = 0; i < length; i++) fast = fast.next;
        ListNode slow = list;
        while (slow != fast) {
            slow = slow.next;
            fast = fast.next;
        }
        return fast;
    }
    
    public static String cycleString(ListNode list, ListNode cycleStart) {
        boolean cycleStartSeen = false;
        StringBuilder builder = new StringBuilder();
        while (true) {
            if (list == cycleStart) {
                if (!cycleStartSeen) {
                    cycleStartSeen = true;
                } else {
                    builder.append(list.val);
                    builder.append("*");
                    break;
                }
            }
            
            builder.append(list.val);
            builder.append("->");
            list = list.next;
        }
        return builder.toString();
    }
    
    public static ListNode overlapStart(ListNode list1, ListNode list2) {
        if (list1 == null || list2 == null) return null;
        int length1 = length(list1);
        int length2 = length(list2);
        if (length1 < length2) {
            ListNode tmp = list1;
            list1 = list2;
            list2 = tmp;
        }
        
        int diff = Math.abs(length1 - length2);
        for (int i = 0; i < diff; i++) list1 = list1.next;
        while (list1 != null && list2 != null && list1 != list2) {
            list1 = list1.next;
            list2 = list2.next;
        }
        
        return (list1 == list2) ? list1 : null;
    }
    
    public static ListNode overlapStartWithCycles(ListNode list1, ListNode list2) {
        ListNode start1 = cycleStart(list1);
        ListNode start2 = cycleStart(list1);
        
        if (start1 == null && start2 == null) {
            // case1: both are non-cyclic
            return overlapStart(list1, list2);
        } else if ((start1 != null && start2 == null) || (start1 == null && start2 != null)) {
            // case2: only one is cyclic
            return null;
        }
        // case3: both are cyclic
        
        // case 3.1: cycles are not same
        ListNode tmp = start1.next;
        for (; tmp != start1 && tmp != start2; tmp = tmp.next) ;
        if (tmp == start1) return null;
        
        // case 3.2: cycles are same
        // fast-slow algo: move-ahead algo
        int d1 = distance(list1, start1);
        int d2 = distance(list2, start2);
        int diff = Math.abs(d1 - d2);
        if (d1 > d2) {
            for (; diff > 0; diff--) list1 = list1.next;
        } else {
            for (; diff > 0; diff--) list2 = list2.next;
        }
        while (list1 != list2 && list1 != start1 && list2 != start2) {
            list1 = list1.next;
            list2 = list2.next;
        }
        return (list1 == list2) ? list1 : start1;
    }
    
    public static void test1() {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7};
        ListNode list = toList(arr);
        System.out.println("finding cycle is list: " + list);
        System.out.println("cycle start in non-cyclic: " + cycleStart(list));
    }
    
    public static void test2() {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7};
        ListNode list = toList(arr);
        System.out.println("adding cycle is list: " + list);
        ListNode cycleStart = find(list, 4);
        find(list, 7).next = cycleStart;
        System.out.println("finding cycle is list: " + cycleString(list, cycleStart));
        System.out.println("cycle start in non-cyclic: " + cycleStart(list).val);
    }
    
    public static void main(String[] args) {
        // test1();
        test2();
    }
    
}

class FastSlow {
    
    public static ListNode middleNode(ListNode head) {
        ListNode fast = head, slow = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
    
    // 1-based index
    public static ListNode removeKthFromEnd(ListNode list, int k) {
        if (list == null || k <= 0) return list;
        
        ListNode head = dummyNode(list);
        ListNode fast = head;
        ListNode slow = head;
        
        for (int i = 0; fast != null; i++) {
            fast = fast.next;
            if (i > k) slow = slow.next;
        }
        
        deleteNext(slow);
        
        return head.next;
    }
    
    // no need for dummy head as head will not change
    public static ListNode removeDupsFromSortedList(ListNode list) {
        if (list == null) return null;
        
        for (ListNode node = list; node.next != null; ) {
            if (node.next.val == node.val) {
                deleteNext(node);
            } else {
                node = node.next;
            }
        }
        
        return list;
    }
}

class SplitMerge {
    
    // gotcha: instead moving one node at a time, it's better to split and merge
    public static ListNode rotateList(ListNode list, int k) {
        int n = length(list);
        if (n <= 0) return null;
        
        k = k % n;
        if (k == 0) return list;
        
        ListNode slow = list, fast = list;
        for (int i = 0; fast.next != null; i++) {
            fast = fast.next;
            if (i >= k) slow = slow.next;
        }
        
        ListNode tmp = slow.next;
        slow.next = null;
        fast.next = list;
        list = tmp;
        return list;
    }
    
    public static ListNode evenOddMerge(ListNode list) {
        if (list == null) return list;
        
        ListNode evenHead = dummyNode(), oddHead = dummyNode();
        ListNode evenTail = evenHead, oddTail = oddHead;
        
        ListNode head = dummyNode(list);
        while (true) {
            if (head.next == null) break;
            moveNextToHead(evenTail, head);
            evenTail = evenTail.next;
            
            if (head.next == null) break;
            moveNextToHead(oddTail, head);
            oddTail = oddTail.next;
        }
        
        evenTail.next = oddHead.next;
        return evenHead.next;
    }
    
    public static boolean isPalindrome(ListNode list) {
        if (list == null || list.next == null) return true;
        
        ListNode slow = list, fast = list, slowPrev = null;
        while (fast != null && fast.next != null) {
            slowPrev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        
        ListNode list1 = list;
        ListNode list2 = slowPrev.next;
        slowPrev.next = null;
        list2 = Reverse.reverselist(list2);
        while (list1 != null && list2 != null) {
            if (list1.val != list2.val) return false;
            list1 = list1.next;
            list2 = list2.next;
        }
        return true;
    }
    
    public int maxTwinPairSum(ListNode list) {
        if (list == null) return Integer.MIN_VALUE;
        
        ListNode slowPrev = null, slow = list, fast = list;
        while (fast != null && fast.next != null) {
            slowPrev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        if (slowPrev != null) slowPrev.next = null;
        
        ListNode l1 = list, l2 = Reverse.reverse(slow);
        int maxSum = Integer.MIN_VALUE;
        while (l1 != null && l2 != null) {
            maxSum = Math.max(maxSum, l1.val + l2.val);
            l1 = l1.next;
            l2 = l2.next;
        }
        return maxSum;
    }
    
    public static ListNode partition(ListNode list, int pivot) {
        ListNode lessThanHead = dummyNode(), equalToHead = dummyNode(), greaterThanHead = dummyNode(), head = dummyNode(list);
        ListNode lessThanTail = lessThanHead, equalToTail = equalToHead, greaterThanTail = greaterThanHead;
        while (head.next != null) {
            int data = head.next.val;
            if (data < pivot) {
                moveNextToHead(lessThanTail, head);
                lessThanTail = lessThanTail.next;
            } else if (data == pivot) {
                moveNextToHead(equalToTail, head);
                equalToTail = equalToTail.next;
            } else {
                moveNextToHead(greaterThanTail, head);
                greaterThanTail = greaterThanTail.next;
            }
        }
        lessThanTail.next = equalToHead.next;
        equalToTail.next = greaterThanHead.next;
        return lessThanHead.next;
    }
    
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        
        ListNode head = dummyNode();
        ListNode tail = head;
        int carry = 0;
        while (l1 != null || l2 != null) {
            int sum = carry;
            if (l1 != null) {
                sum += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                sum += l2.val;
                l2 = l2.next;
            }
            tail.next = new ListNode(sum % 10);
            tail = tail.next;
            carry = sum / 10;
        }
        if (carry > 0) tail.next = new ListNode(carry);
        return head.next;
    }
    
    public ListNode swapPairs(ListNode list) {
        ListNode swapListHead = dummyNode(), head = dummyNode(list);
        ListNode swapListTail = swapListHead;
        while (head.next != null) {
            ListNode n1 = deleteNext(head);
            if (head.next != null) {
                moveNextToHead(swapListTail, head);
                swapListTail = swapListTail.next;
            }
            swapListTail.next = n1;
            swapListTail = swapListTail.next;
        }
        return swapListHead.next;
    }
}
