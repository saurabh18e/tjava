package com.practice.ds.list.finale;

import java.util.List;

public class Medium {
}

class MergeKSortedList {
    // refer to code in heaps-hard chapter
}

class ListWithRandomPointers {
    
    static class Node {
        int val;
        Node next;
        Node random;
        
        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }
    
    public static Node copy(Node head) {
        if (head == null) return null;
        
        // first pass: clone each node
        for (Node iter = head; iter != null; ) {
            Node nnd = new Node(iter.val);
            nnd.next = iter.next;
            iter.next = nnd;
            iter = iter.next.next;
        }
        
        // second pass: copy random pointers
        for (Node iter = head; iter != null; ) {
            iter.next.random = iter.random != null ? iter.random.next : null;
            iter = iter.next.next;
        }
        
        // third pass: split the lists
        Node cloneHead = new Node(0);
        Node tail = cloneHead;
        for (Node iter = head; iter != null; ) {
            Node nnd = iter.next;
            iter.next = iter.next.next;
            iter = iter.next;
            
            tail.next = nnd;
            tail = tail.next;
            tail.next = null;
        }
        return cloneHead.next;
    }
    
}

