package com.practice.ds.list.finale;

public class Hard {
}

class ListSort {
    
    // algo1: top-down divide and conquer i.e. merge sort
    // algo2: bottom up iterative approach
    
    static class Chunk {
        public ListNode start, end;
        
        public Chunk(ListNode start, ListNode end) {
            this.start = start;
            this.end = end;
        }
    }
    private static int length(ListNode head) {
        int count = 0;
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }
    
    public ListNode sortBottomUp(ListNode head) {
        int n = length(head);
        for (int size = 1; size < n; size = size * 2) {
            head = mergeChunks(head, size);
        }
        return head;
    }
    public ListNode mergeChunks(ListNode head, int size) {
        if (head == null) return null;
        ListNode mergedHead = new ListNode(0), mergedTail = mergedHead;
        while (head != null) {
            Chunk c1 = getChunk(head, size);
            if (c1.start == null) break;
            head = c1.end.next;
            c1.end.next=null;
            
            Chunk c2 = getChunk(head, size);
            if (c2.start != null) {
                head = c2.end.next;
                c2.end.next=null;
            }
            
            Chunk mergedChunk = mergeChunks(c1, c2);
            mergedTail.next = mergedChunk.start;
            mergedTail = mergedChunk.end;
        }
        return mergedHead.next;
    }
    
    private static Chunk getChunk(ListNode head, int size) {
        ListNode iter = head, prev = null;
        for (int i=0; iter != null && i<size; i++) {
            prev = iter;
            iter = iter.next;
        }
        return new Chunk(head, prev);
    }
    
    private Chunk mergeChunks(Chunk c1, Chunk c2) {
        if (c2 == null) return c1;
        ListNode head = new ListNode(0);
        ListNode tail = head;
        
        ListNode s1 = c1.start, s2 = c2.start;
        while (s1 != null && s2 != null) {
            ListNode node = null;
            if (s1.val <= s2.val) {
                node = s1;
                s1 = s1.next;
            } else {
                node = s2;
                s2 = s2.next;
            }
            tail.next = node;
            tail = tail.next;
            tail.next = null;
        }
        if (s1 != null) {
            tail.next = s1;
            tail = c1.end;
        } else {
            tail.next = s2;
            tail = c2.end;
        }
        
        return new Chunk(head.next, tail);
    }
}

