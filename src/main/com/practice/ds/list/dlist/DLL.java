package com.practice.ds.list.dlist;

// dll implementation with head and tail pointers and nodes
// https://leetcode.com/problems/lru-cache/solution/
public class DLL {
    
    public DLLNode head;
    public DLLNode tail;
    
    public DLL() {
        head = new DLLNode();
        tail = new DLLNode();
        head.next = tail;
        tail.prev = head;
    }
    
    public void addToTail(int val) {
        DLLNode nnd = new DLLNode(val);
        nnd.prev = tail.prev;
        nnd.next = tail;
        tail.prev = nnd;
    }
    
    public void addToFront(DLLNode nnd) {
        nnd.prev = head;
        nnd.next = head.next;
        nnd.next.prev = nnd;
        head.next = nnd;
    }
    
    public void remove(DLLNode node) {
        if (head == tail || node == null) return;
        
        DLLNode prev = node.prev;
        DLLNode next = node.next;
        node.prev = node.next = null;
        
        next.prev = prev;
        prev.next = next;
    }
    
    public void moveToHead(DLLNode node) {
        if (head == tail || node == null) return;
        
        remove(node);
        addToFront(node);
    }
}
