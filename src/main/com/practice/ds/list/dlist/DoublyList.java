package com.practice.ds.list.dlist;

public class DoublyList {

    public static DoublyListNode duplicateNodesInline(DoublyListNode list) {
        DoublyListNode curr = list;
        while(curr != null) {
            DoublyListNode nnd = new DoublyListNode();
            nnd.val = curr.val;
            nnd.next = curr.next;
            curr.next = nnd;
            curr = curr.next.next;
        }
        return list;
    }

    public static DoublyListNode copyRandomPointers(DoublyListNode list) {
        DoublyListNode curr = list;
        while(curr != null) {
            curr.next.prev = (curr.prev == null) ? null : curr.prev.next;
            curr = curr.next.next;
        }
        return list;
    }

    public static DoublyListNode splitDuplicateNodes(DoublyListNode list) {
        DoublyListNode head = (list == null) ? list : list.next;
        DoublyListNode curr = list;
        while (curr != null) {
            DoublyListNode next = curr.next;
            curr.next = next.next;
            curr = curr.next;
            next.next = (next.next == null) ? null : next.next.next;
        }
        return head;
    }

    public static DoublyListNode copyListWithRandomPointers(DoublyListNode list) {
        DoublyListNode output = duplicateNodesInline(list);
        output = copyRandomPointers(output);
        output = splitDuplicateNodes(output);
        return output;
    }
}

class DoublyListNode {
    int val;
    DoublyListNode next;
    DoublyListNode prev;
}
