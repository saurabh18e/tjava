package com.practice.ds.list.dlist;

public class DLLNode {
    public int val;
    public DLLNode prev;
    public DLLNode next;
    
    public DLLNode() {
        this(0);
    }
    public DLLNode(int val) {
        this(val, null, null);
    }
    
    public DLLNode(int val, DLLNode prev, DLLNode next) {
        this.val = val;
        this.prev = prev;
        this.next = next;
    }
}
