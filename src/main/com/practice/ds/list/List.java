package com.practice.ds.list;

/*
    Lists
    ------
    - dynamic set operations: bag of items: insert, delete, search
    - types of lists
        - arrays: arrays and arrayList
        - linked list: variants: doubly, circular(head-tail pointers), sorted, XOR
        - stacks and queues, priority queues
 */


class ListNode {

    int value;
    ListNode next;

    public ListNode(int value) {
        this(value, null);
    }

    public ListNode(int value, ListNode next) {
        this.value = value;
        this.next = next;
    }

    @Override
    public String toString() {
        return this.value + "";
    }
}

public class List {

    ListNode head;

    public List() {
        this(null, null);
    }

    public List(ListNode head) {
        this(head, null);
    }

    public List(ListNode head, ListNode tail) {
        this.head = head;
    }

    public void add(int val) {
        insert(val);
    }

    public void insert(int val) {
        insert(new ListNode(val));
    }

    public void insert(ListNode node) {
        if (head == null) {
            this.head = node;
        } else {
            node.next = this.head;
            this.head = node;
        }
    }

    public void delete(int val) {
        ListNode prev = null;
        ListNode curr = this.head;
        while (curr != null) {
            if (curr.value == val) {
                if (curr == head) {
                    this.head = curr.next;
                    curr = curr.next;
                } else {
                    prev.next = curr.next;
                    curr = curr.next;
                }
            } else {
                prev = curr;
                curr = curr.next;
            }
        }
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer("[");
        boolean first = true;
        ListNode curr = this.head;
        while (curr != null) {
            if (first) {
                first = false;
                buffer.append(curr.value);
            } else {
                buffer.append(", " + curr.value);
            }
            curr = curr.next;
        }
        buffer.append("]");
        return buffer.toString();
    }

    public ListNode find(int val) {
        ListNode curr = this.head;
        while (curr != null) {
            if (curr.value == val) break;
            curr = curr.next;
        }
        return curr;
    }

    public int findIndex(int val) {
        ListNode curr = this.head;
        int i = 0;
        while (curr != null) {
            if (curr.value == val) break;
            curr = curr.next;
            i++;
        }
        return i;
    }

    public static ListNode reverse(ListNode head) {
        ListNode prev = null;
        ListNode curr = head;
        ListNode next = null;

        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }

        return prev;
    }
    public List reverse() {
        this.head = reverse(this.head);
        return this;
    }
    // ref: add numbers in linked list: https://leetcode.com/explore/interview/card/facebook/6/linked-list/319/
    public static List add(List list1, List list2) {
        if (list1 == null) {
            return list2;
        } else if (list2 == null) {
            return list1;
        } else {
            // System.out.println("adding: list1: " + list1 + " and list2: " + list2);
            list1.reverse();
            list2.reverse();
            List output = new List();
            int carry = 0;
            int sum = 0;
            ListNode curr1 = list1.head;
            ListNode curr2 = list2.head;
            while (curr1 != null || curr2 != null) {
                sum = carry + (curr1 == null ? 0 : curr1.value) + (curr2 == null ? 0 : curr2.value);
                carry = sum / 10;
                output.add(sum % 10);
                curr1 = (curr1 == null ? null : curr1.next);
                curr2 = (curr2 == null ? null : curr2.next);
            }
            if (carry > 0) output.add(carry);
            list1.reverse();
            list2.reverse();
            output.reverse();
            // System.out.println("addition: " + output);
            return output;
        }
    }

    // merge sort
    public static ListNode merge(ListNode l1, ListNode l2) {
        ListNode head = null;
        ListNode tail = null;
        while (l1 != null || l2 != null) {
            ListNode nodeToMege = null;
            if (l1 == null) {
                nodeToMege = l2;
                l2 = l2.next;
            } else if (l2 == null) {
                nodeToMege = l1;
                l1 = l1.next;
            } else if (l1.value < l2.value) {
                nodeToMege = l1;
                l1 = l1.next;
            } else {
                nodeToMege = l2;
                l2 = l2.next;
            }
            if (head == null) {
                head = tail = nodeToMege;
            } else {
                tail.next = nodeToMege;
                tail = nodeToMege;
            }
            nodeToMege.next = null;
        }
        return head;
    }
    public static List merge(List l1, List l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        return new List(merge(l1.head, l2.head));
    }

    public static ListNode findMidPrev(ListNode list) {
        ListNode slowPrev = null, slow = list, fast = list;
        while (fast != null && fast.next != null) {
            slowPrev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        return slowPrev;
    }
    public ListNode findMidPrev() {
        return findMidPrev(this.head);
    }

    public static ListNode findMid(ListNode list) {
        ListNode slow = list, fast = list;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
    public ListNode findMid() {
        return findMid(this.head);
    }

    public static ListNode mergeSort(ListNode list) {
        if (list != null) {
            // split
            ListNode midPrev = findMidPrev(list);
            if (midPrev == null) return list;
            ListNode first = list;
            ListNode second = midPrev.next;
            midPrev.next = null;
            // recurse
            first = mergeSort(first);
            second = mergeSort(second);
            // merge
            return merge(first, second);
        }
        return null;
    }
    public static ListNode sort(ListNode list) {
        return mergeSort(list);
    }
    public List sort() {
        this.head = sort(this.head);
        return this;
    }

    // alternate splits and merges
    public static ListNode alternateMerge(ListNode l1, ListNode l2) {
        ListNode head = null, tail = null;
        while (l1 != null || l2 != null) {
            if (l1 != null) {
                if (head == null) {
                    head = tail = l1;
                } else {
                    tail.next = l1;
                    tail = tail.next;
                }
                l1 = l1.next;
            }

            if (l2 != null) {
                if (head == null) {
                    head = tail = l2;
                } else {
                    tail.next = l2;
                    tail = tail.next;
                }
                l2 = l2.next;
            }
        }
        return head;
    }

    public static ListNode midSplitAndAlternateMerge(ListNode list) {
        ListNode midPrev = findMid(list);
        if (midPrev == null) return list;
        ListNode first = list;
        ListNode second = midPrev.next;
        midPrev.next = null;
        return alternateMerge(first, second);
    }
    public List midSplitAndAlternateMerge() {
        this.head = midSplitAndAlternateMerge(this.head);
        return this;
    }

    public static ListNode midSplitAndAlternateReverseMerge(ListNode list) {
        ListNode midPrev = findMid(list);
        if (midPrev == null) return list;
        ListNode first = list;
        ListNode second = midPrev.next;
        midPrev.next = null;
        return alternateMerge(first, reverse(second));
    }
    public List midSplitAndAlternateReverseMerge() {
        this.head = midSplitAndAlternateReverseMerge(this.head);
        return this;
    }

    // testing code

    public static List testData() {
        return list(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
    }

    public static List list(int[] arr) {
        List list = new List();
        for (int i = 0; i < arr.length; i++) list.add(arr[i]);
        list.reverse();
        return list;
    }

    public static void addTest() {
        System.out.println("list addition[1]: " + add(list(new int[]{}), list(new int[]{})));
        System.out.println("list addition[2]: " + add(list(new int[]{1, 2, 3, 4}), list(new int[]{9, 8})));
    }

    public static void reverseTest() {
        List list = testData();
        System.out.println("List before reverse: " + list);
        list.reverse();
        System.out.println("List after reverse: " + list);
    }

    public static void insertDeleteTest() {
        List list = new List();
        System.out.println("list before inserts: " + list);
        for (int i = 0; i < 5; i++) {
            System.out.println(list);
            list.add(i);
        }
        System.out.println("list after inserts: " + list);
        System.out.println("list before deletes: " + list);
        for (int i = 0; i < 5; i++) {
            System.out.println(list);
            list.delete(i);
        }
        System.out.println("list after deletes: " + list);
    }

    public static void mergeTest() {
        System.out.println("merge list[1]: " + merge(list(new int[]{}), list(new int[]{})));
        System.out.println("merge list[2]: " + merge(list(new int[]{1, 3, 5, 6, 7, 8}), list(new int[]{})));
        System.out.println("merge list[3]: " + merge(list(new int[]{}), list(new int[]{2, 4})));
        System.out.println("merge list[4]: " + merge(list(new int[]{1, 3, 5, 6, 7, 8}), list(new int[]{2, 4})));
    }

    public static void findMidTest() {
        System.out.println("find mid[1]: " + list(new int[]{}).findMid());
        System.out.println("find mid[2]: " + list(new int[]{1}).findMid());
        System.out.println("find mid[3]: " + list(new int[]{1, 2}).findMid());
        System.out.println("find mid[4]: " + list(new int[]{1, 2, 3}).findMid());
        System.out.println("find mid[5]: " + list(new int[]{1, 2, 3, 4}).findMid());
        System.out.println("find mid[6]: " + list(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}).findMid());
    }

    public static void findMidPrevTest() {
        System.out.println("find mid prev[1]: " + list(new int[]{}).findMidPrev());
        System.out.println("find mid prev[2]: " + list(new int[]{1}).findMidPrev());
        System.out.println("find mid prev[3]: " + list(new int[]{1, 2}).findMidPrev());
        System.out.println("find mid prev[4]: " + list(new int[]{1, 2, 3}).findMidPrev());
        System.out.println("find mid prev[5]: " + list(new int[]{1, 2, 3, 4}).findMidPrev());
        System.out.println("find mid prev[6]: " + list(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}).findMidPrev());
    }

    public static void sortingTest() {
        System.out.println("sorting[1]: " + list(new int[]{}).sort());
        System.out.println("sorting[2]: " + list(new int[]{1}).sort());
        System.out.println("sorting[3]: " + list(new int[]{2, 1}).sort());
        System.out.println("sorting[4]: " + list(new int[]{6, 5, 1, 3, 2}).sort());
    }

    public static void midSplitAndAlternateMergeTest() {
        System.out.println("midSplitAndAlternateMerge[1]: " + list(new int[]{}).midSplitAndAlternateMerge());
        System.out.println("midSplitAndAlternateMerge[2]: " + list(new int[]{1}).midSplitAndAlternateMerge());
        System.out.println("midSplitAndAlternateMerge[3]: " + list(new int[]{1, 2}).midSplitAndAlternateMerge());
        System.out.println("midSplitAndAlternateMerge[4]: " + list(new int[]{1, 2, 3}).midSplitAndAlternateMerge());
        System.out.println("midSplitAndAlternateMerge[5]: " + list(new int[]{1, 2, 3, 4}).midSplitAndAlternateMerge());
        System.out.println("midSplitAndAlternateMerge[6]: " + list(new int[]{1, 2, 3, 4, 5}).midSplitAndAlternateMerge());
        System.out.println("midSplitAndAlternateMerge[7]: " + list(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}).midSplitAndAlternateMerge());
    }

    public static void midSplitAndAlternateReverseMergeTest() {
        System.out.println("midSplitAndAlternateReverseMerge[1]: " + list(new int[]{}).midSplitAndAlternateReverseMerge());
        System.out.println("midSplitAndAlternateReverseMerge[2]: " + list(new int[]{1}).midSplitAndAlternateReverseMerge());
        System.out.println("midSplitAndAlternateReverseMerge[3]: " + list(new int[]{1, 2}).midSplitAndAlternateReverseMerge());
        System.out.println("midSplitAndAlternateReverseMerge[4]: " + list(new int[]{1, 2, 3}).midSplitAndAlternateReverseMerge());
        System.out.println("midSplitAndAlternateReverseMerge[5]: " + list(new int[]{1, 2, 3, 4}).midSplitAndAlternateReverseMerge());
        System.out.println("midSplitAndAlternateReverseMerge[6]: " + list(new int[]{1, 2, 3, 4, 5}).midSplitAndAlternateReverseMerge());
        System.out.println("midSplitAndAlternateReverseMerge[7]: " + list(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}).midSplitAndAlternateReverseMerge());
    }

    public static void main(String[] args) {
        midSplitAndAlternateReverseMergeTest();
    }
}
