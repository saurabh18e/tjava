package com.practice.ds.stack.finale;

import java.util.*;

public class Medium {
}

class CanonicalPath {
    
    public static String canonicalPath(String path) {
        if (path == null || path.isEmpty()) return path;
        
        String[] tokens = path.split("/");
    
        Deque<String> stack = new LinkedList<>();
        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].isEmpty()) {
                if (i == 0) stack.push("/");
            } else if (tokens[i].equals(".")) {
                continue;
            } else if (tokens[i].equals("..")) {
                if (stack.isEmpty() || stack.peek().equals("..")) {
                    stack.push(tokens[i]);
                } else if (stack.peek().equals("/")) {
                    throw new IllegalArgumentException("cannot go above root dir in path");
                } else {
                    stack.pop();
                }
            } else {
                stack.push(tokens[i]);
            }
        }
        
        StringBuilder builder = new StringBuilder();
        while (!stack.isEmpty()) {
            String token = stack.removeLast();
            builder.append(token);
            if (token.equals("/")) continue;
            if (!stack.isEmpty()) builder.append("/");
        }
        return builder.toString();
    }
    
    public static void main(String[] args) {
        String str1 = "/a/././../b//c";
        System.out.println("canonical path of " + str1 + " is: " + canonicalPath(str1));
        
        //String[] tokens = "/a//b/".split("/");
        //String str1 = Arrays.toString(tokens);
        //String str2 = String.join("|", tokens);
        //System.out.println("tokens are: " + str1);
        //System.out.println("tokens are: [" + str2 + "]");
    }
}

class SimplifyPath {
    
    public static String simplifyPath(String path) {
        if (path == null || path.isEmpty()) return path;
    
        Stack<String> stack = new Stack<>();
        for (String token : path.split("/")) {
            if (token.isEmpty() || token.equals(".")) {
                continue;
            } else if (token.equals("..")) {
                if (!stack.isEmpty()) stack.pop();
            } else {
                stack.push(token);
            }
        }
        
        // Stich together all the directory names together
        StringBuilder result = new StringBuilder();
        for (String dir : stack) {
            result.append("/");
            result.append(dir);
        }
        
        return result.length() > 0 ? result.toString() : "/" ;
    }
    
}
