package com.practice.ds.stack.finale;

import com.practice.ds.list.finale.ListNode;

import java.util.*;

public class Easy {
}

class MyStack<T> {
    // stack impl is all about implementing the interface
    // worst case, implement your own linked-list/dynamic array
    
    // use dynamic array list/linked list/deque
    List<T> list = new ArrayList<>();
    
    public boolean push(T e) {
        list.add(e);
        return true;
    }
    
    public T pop() {
        return list.isEmpty() ? null : list.remove(list.size() - 1);
    }
    
    public T peek() {
        return list.isEmpty() ? null : list.get(list.size() - 1);
    }
    
    public int size() {
        return list.size();
    }
}

class Basic {
    
    public void printReverse(ListNode head) {
        Stack<ListNode> stack = new Stack<>();
        while (!stack.isEmpty() || head != null) {
            if (head == null) {
                System.out.println(stack.pop().val);
            } else {
                stack.push(head);
                head = head.next;
            }
        }
    }
    
    public static boolean checkBracesValidity(String s) {
        if (s == null || s.isEmpty()) return true;
        
        HashMap<Character, Character> map = new HashMap<>();
        map.put('[', ']');
        map.put('{', '}');
        map.put('(', ')');
        
        Stack<Character> stack = new Stack<>();
        for (Character ch : s.toCharArray()) {
            if (map.containsKey(ch)) {
                stack.push(ch);
            } else {
                if (stack.isEmpty() || ch != map.get(stack.peek())) return false;
                stack.pop();
            }
        }
        return stack.isEmpty();
    }
    
}

class PolishNotations {
    
    // note: reverse polish notation = postfix operator form of an infix expression
    public static int evalRPN(String expr) {
        HashSet<Character> validOperators = new HashSet<>(Arrays.asList('+', '-', '*', '/'));
        Stack<Integer> stack = new Stack<>();
        String[] tokens = expr.trim().split(" ");
        for (String token : tokens) {
            if (token.length() == 1 && validOperators.contains(token.charAt(0))) {
                int num2 = stack.pop(); // note the order of pop
                int num1 = stack.pop();
                char operator = token.charAt(0);
                stack.push(calculate(operator, num1, num2));
            } else {
                stack.push(Integer.parseInt(token));
            }
        }
        return stack.pop();
    }
    
    public static int calculate(char operator, int num1, int num2) {
        int output = 0;
        switch (operator) {
            case '+':
                output = num1 + num2;
                break;
            case '-':
                output = num1 - num2;
                break;
            case '*':
                output = num1 * num2;
                break;
            case '/':
                output = num1 / num2;
                break;
            default:
                throw new IllegalArgumentException("invalid operator: |" + operator + "|");
        }
        return output;
    }
    
    // note: polish notation = prefix operator form of an infix expression
    public static int evalPolishNotation(String expr) {
        HashSet<Character> validOperators = new HashSet<>(Arrays.asList('+', '-', '*', '/'));
        Stack<Integer> stack = new Stack<>();
        String[] tokens = expr.trim().split(" ");
        for (String token : tokens) {
            if (token.length() == 1 && validOperators.contains(token.charAt(0))) {
                stack.push((int) token.charAt(0));
            } else {
                int num2 = Integer.parseInt(token);
                while (!stack.isEmpty() && !validOperators.contains((char) (int) stack.peek())) {
                    int num1 = stack.pop();
                    char operator = (char) (int) stack.pop();
                    num2 = calculate(operator, num1, num2);
                }
                stack.push(num2);
            }
        }
        while (stack.size() > 1) {
            int num2 = stack.pop();
            int num1 = stack.pop();
            char operator = (char) (int) stack.pop();
            stack.push(calculate(operator, num1, num2));
        }
        return stack.pop();
    }
    
    private static void printStack(Stack<Integer> stack) {
        List<String> list = new ArrayList<>();
        Stack<Integer> cl = (Stack<Integer>) stack.clone();
        while (!cl.isEmpty()) {
            int i = cl.pop();
            list.add(i > 20 ? (char) i + "" : i + "");
        }
        System.out.println(Arrays.toString(list.toArray()));
    }
    
    public static void testPolishNotations() {
        HashMap<String, String> map = new HashMap<>();
        map.put("2 1 + 3 *", "* + 2 1 3");
        map.put("4 13 5 / +", "+ 4 / 13 5");
        map.put("10 6 9 3 + -11 * / * 17 +", "+ * 10 / 6 * + 9 3 -11 17");
        // map.put("10 6 9 3 + -11 * / * 17 + 5 +", "+ + * 10 / 6 * + 9 3 -11 17 5");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println("evaluating: |" + entry.getKey() + "|");
            int num1 = evalRPN(entry.getKey());
            // System.out.println("rpn result: " + num1);
            int num2 = evalPolishNotation(entry.getValue());
            // System.out.println("polish result: " + num2);
            if (num1 != num2) throw new RuntimeException("check failed for expression: "
                + entry.getKey() + " as " + num1 + " != " + num2);
            System.out.println("evaluation success");
        }
    }
    
    public static void main(String[] args) {
        testPolishNotations();
    }
}

class JumpFirstOrder {
    
    static class JumpNode {
        public int data;
        public JumpNode next;
        public JumpNode jump;
        public int order = -1;
        
        public JumpNode(int data, JumpNode next) {
            this.data = data;
            this.next = next;
        }
    }
    
    static JumpNode dummyList() {
        JumpNode n4 = new JumpNode(4, null);
        JumpNode n3 = new JumpNode(3, n4);
        JumpNode n2 = new JumpNode(2, n3);
        JumpNode n1 = new JumpNode(1, n2);
        n1.jump = n4;
        n2.jump = n4;
        n3.jump = n3;
        n4.jump = null;
        return n1;
    }
    
    public static void main(String[] args) {
        List<Integer> l1 = jumpFirstOrder(dummyList());
        System.out.println("recursive jump first order is: " + Arrays.toString(l1.toArray()));
        List<Integer> l2 = jumpFirstOrderIterative(dummyList());
        System.out.println("iterative jump first order is: " + Arrays.toString(l2.toArray()));
        for (int i = 0; i < l1.size(); i++) {
            if (l1.get(i) != l2.get(i)) throw new RuntimeException("list are not equal");
        }
    }
    
    private static List<Integer> jumpFirstOrderIterative(JumpNode head) {
        Stack<JumpNode> stack = new Stack<>();
        List<Integer> output = new ArrayList<>();
        int order = 0;
        while (!stack.isEmpty() || head != null) {
            if (head == null) {
                head = stack.pop();
            } else {
                if (head.order == -1) {
                    head.order = order++;
                    output.add(head.data);
                    stack.push(head.next);
                    stack.push(head.jump);
                }
                head = null;
            }
        }
        return output;
    }
    
    private static List<Integer> jumpFirstOrder(JumpNode head) {
        List<Integer> list = new ArrayList<>();
        return jumpFirstOrderDFS(head, list, 0);
    }
    
    private static List<Integer> jumpFirstOrderDFS(JumpNode head, List<Integer> output, int order) {
        if (head != null && head.order == -1) {
            output.add(head.data);
            head.order = order++; // this is like visited flag
            jumpFirstOrderDFS(head.jump, output, order);
            jumpFirstOrderDFS(head.next, output, order);
        }
        return output;
    }
}

class StackWithMax {
    
    /*
        ref: https://leetcode.com/problems/min-stack/
        stack with max: only peek max
        sol1: stack node with cached max - T(1), S(n)
        sol2: auxiliary max-stack - T(1), S(max-chain)
     */
    
    static class NodeWithCount {
        public Integer data;
        public int count;
        
        public NodeWithCount(Integer data, int count) {
            this.data = data;
            this.count = count;
        }
    }
    
    Stack<Integer> dataStack = new Stack<>();
    Stack<NodeWithCount> maxStack = new Stack<>();
    
    // push/pop/peek/max/size
    public boolean push(Integer data) {
        if (data == null) return false;
        dataStack.push(data);
        if (maxStack.isEmpty() || maxStack.peek().data < data) {
            maxStack.push(new NodeWithCount(data, 1));
        } else if (maxStack.peek().data == data) {
            maxStack.peek().count++;
        }
        return true;
    }
    
    public Integer pop() {
        if (dataStack.isEmpty()) return null;
        Integer data = dataStack.pop();
        if (maxStack.peek().data == data) {
            maxStack.peek().count--;
            if (maxStack.peek().count == 0) maxStack.pop();
        }
        return data;
    }
    
    public Integer peek() {
        return (dataStack.isEmpty()) ? null : dataStack.peek();
    }
    
    public Integer peekMax() {
        return maxStack.isEmpty() ? null : maxStack.peek().data;
    }
    
    public Integer popMax() {
        if (maxStack.isEmpty()) return null;
        NodeWithCount max = maxStack.peek();
        max.count--;
        if (max.count == 0) maxStack.pop();
        return max.data;
    }
    
    public int size() {
        return dataStack.size();
    }
    
}

class StackWithMaxPop {
    
    /*
        ref: https://leetcode.com/problems/max-stack/
            stack with max: peek and pop max
        sol1: auxiliary max-stack with lazy delete and timestamp
    */
    
    static class Node {
        public int data;
        public boolean isDeleted;
        public long timestamp;
        
        public Node(Integer data, long timestamp) {
            this.data = data;
            this.timestamp = timestamp;
        }
    
        @Override
        public String toString() {
            return "[" + data + ", " + isDeleted + "," + timestamp + "]";
        }
    
    }
    
    Stack<Node> dataStack;
    PriorityQueue<Node> maxHeap;
    static Integer EMPTY_MAX = Integer.MIN_VALUE;
    public static long timestamp = 0;
    
    public StackWithMaxPop() {
        this.dataStack = new Stack<>();
        this.maxHeap = new PriorityQueue<>((a, b) -> {
            int cmp1 = Integer.compare(b.data, a.data);
            if (cmp1 != 0) return cmp1;
            return Long.compare(b.timestamp, a.timestamp);
        });
    }
    
    public void push(int data) {
        Node node = new Node(data, timestamp++);
        
        flushStack(dataStack);
        dataStack.push(node);
        
        flushHeap(maxHeap);
        maxHeap.add(node);
    }
    
    private void flushStack(Stack<Node> stack) {
        while (!stack.isEmpty() && stack.peek().isDeleted) stack.pop();
    }
    private void flushHeap(PriorityQueue<Node> maxHeap) {
        while (!maxHeap.isEmpty() && maxHeap.peek().isDeleted) maxHeap.remove();
    }
    
    public int pop() {
        flushStack(dataStack);
        if (dataStack.isEmpty()) return EMPTY_MAX;
        Node node = dataStack.pop();
        node.isDeleted = true;
        
        flushHeap(maxHeap);
        return node.data;
    }
    
    public int top() {
        flushStack(dataStack);
        if (dataStack.isEmpty()) return EMPTY_MAX;
        return dataStack.peek().data;
    }
    
    public int peekMax() {
        flushHeap(maxHeap);
        return maxHeap.isEmpty() ? EMPTY_MAX : maxHeap.peek().data;
    }
    
    public int popMax() {
        flushHeap(maxHeap);
        if (maxHeap.isEmpty()) return EMPTY_MAX;
        
        Node node = maxHeap.remove();
        node.isDeleted = true;
        
        flushStack(dataStack);
        return node.data;
    }
    
    public static void main(String[] args) {
        StackWithMaxPop stack = new StackWithMaxPop();
        stack.push(5);
        stack.push(1);
        stack.push(5);
        
        // "top","popMax","top","peekMax","pop","top"]
        if (stack.top() != 5) throw new RuntimeException();
        if (stack.popMax() != 5) throw new RuntimeException();
        if (stack.top() != 1) throw new RuntimeException();
        if (stack.peekMax() != 5) throw new RuntimeException();
        if (stack.pop() != 1) throw new RuntimeException();
        if (stack.top() != 5) throw new RuntimeException();
    }
}

class SunsetView {
    
    public static List<Integer> sunsetViewLeftToRight(List<Integer> heights) {
        List<Integer> buildingsWithView = new ArrayList<>();
        int maxSofar = Integer.MIN_VALUE;
        for (Integer height : heights) {
            if (height > maxSofar) {
                buildingsWithView.add(height);
                maxSofar = height;
            }
        }
        return buildingsWithView;
    }
    
    // monotonic decreasing stack
    public static List<Integer> sunsetViewRightToLeft(List<Integer> heights) {
        Stack<Integer> buildingsWithView = new Stack<>();
        for (int i = heights.size() - 1; i >= 0; i--) {
            int height = heights.get(i);
            while (!buildingsWithView.isEmpty() && height >= buildingsWithView.peek())
                buildingsWithView.pop();
            buildingsWithView.add(height);
        }
        List<Integer> output = new ArrayList<>();
        while (!buildingsWithView.isEmpty()) output.add(buildingsWithView.pop());
        return output;
    }
    
    public static void main(String[] args) {
        List<Integer> heights = Arrays.asList(2, 3, 2, 1, 5, 2, 6, 8, 2, 10, 5);
        List<Integer> l1 = sunsetViewLeftToRight(heights);
        System.out.println("sunsetViewLeftToRight: " + Arrays.toString(l1.toArray()));
        List<Integer> l2 = sunsetViewRightToLeft(heights);
        System.out.println("sunsetViewRightToLeft: " + Arrays.toString(l2.toArray()));
        for (int i = 0; i < l1.size(); i++) {
            if (l1.get(i) != l2.get(i)) throw new RuntimeException("list are not equal");
        }
    }
}

class StackForMatchingProblems {
    
    // warning: poor perf if char[] is input since in that case we can use char[] as stack
    public String repeatedDuplicateRemoval1(String str) {
        if (str == null || str.length() == 0) return str;
        Stack<Character> stack = new Stack<>();
        for (char ch : str.toCharArray()) {
            if (stack.isEmpty() || ch != stack.peek()) {
                stack.push(ch);
            } else if (ch == stack.peek()) {
                stack.pop();
            }
        }
        StringBuilder builder = new StringBuilder();
        while (!stack.isEmpty()) builder.append(stack.pop());
        return builder.reverse().toString();
    }
    
    public static String repeatedDuplicateRemoval2(char[] arr) {
        int i = -1;
        for (int j = 0; j < arr.length; j++) {
            if (i < 0) {
                // buffer stack is empty
                arr[++i] = arr[j]; // push
            } else {
                if (arr[i] == arr[j]) {
                    i--; // pop
                } else {
                    arr[++i] = arr[j]; // push
                }
            }
        }
        
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j <= i; j++) builder.append(arr[j]);
        return builder.toString();
    }
    
    // wrong code: example: "ab##" "c#d#"
    // reason: code violates LIFO order of backspace
    public boolean compareStringWithBackspaces1(String str1, String str2) {
        char BACKSPACE = '#';
        if (str1 == null && str2 == null) return true;
        if (str1 != null && str2 == null || str1 == null && str2 != null) return false;
        int i = -1, j = -1;
        for (i = str1.length() - 1, j = str2.length() - 1; i >= 0 && j >= 0; ) {
            char ch1 = str1.charAt(i), ch2 = str2.charAt(j);
            if (ch1 == BACKSPACE || ch2 == BACKSPACE) {
                if (ch1 == BACKSPACE) i = i - 2;
                if (ch2 == BACKSPACE) j = j - 2;
            } else {
                if (ch1 != ch2) return false;
                i--;
                j--;
            }
        }
        return (i == -1 && j == -1);
    }
    
    public boolean compareStringWithBackspaces(String str1, String str2) {
        return shrink(str1).equals(shrink(str2));
    }
    
    private String shrink(String str) {
        if (str == null) return "";
        char BACKSPACE = '#';
        Stack<Character> stack = new Stack<>();
        for (char ch : str.toCharArray()) {
            if (ch == BACKSPACE) {
                if (!stack.isEmpty()) stack.pop();
            } else {
                stack.push(ch);
                
            }
        }
        StringBuilder builder = new StringBuilder();
        while (!stack.isEmpty()) builder.append(stack.pop());
        return builder.toString();
    }
    
    // warning: poor perf if char[] is input since in that case we can use char[] as stack
    public static String goodString1(String str) {
        if (str == null || str.isEmpty()) return str;
        Stack<Character> stack = new Stack<>();
        for (char ch : str.toCharArray()) {
            if (stack.isEmpty()) {
                stack.push(ch);
            } else {
                char ch1 = stack.peek();
                char ch2 = ch;
                if (Character.isLetter(ch1) && Character.isLetter(ch2)
                    && Character.toLowerCase(ch1) == Character.toLowerCase(ch2)
                    && ch1 != ch2) {
                    stack.pop();
                } else {
                    stack.push(ch);
                }
            }
        }
        
        StringBuilder builder = new StringBuilder();
        while (!stack.isEmpty()) builder.append(stack.pop());
        return builder.reverse().toString();
    }
    
    public static String goodString2(String str) {
        if (str == null || str.isEmpty()) return str;
        char[] arr = str.toCharArray();
        int i = -1;
        for (int j = 0; j < str.length(); j++) {
            if (i < 0) {
                // buffer stack is empty
                arr[++i] = arr[j]; // push
            } else {
                if (notGood(arr[i], arr[j])) {
                    i--; // pop
                } else {
                    arr[++i] = arr[j]; // push
                }
            }
        }
        
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j <= i; j++) builder.append(arr[j]);
        return builder.toString();
    }
    
    private static boolean notGood(char ch1, char ch2) {
        return (Character.isLetter(ch1) && Character.isLetter(ch2)
            && Character.toLowerCase(ch1) == Character.toLowerCase(ch2)
            && ch1 != ch2);
    }
    
    public static boolean validateStackSequences(int[] pushed, int[] popped) {
        if (popped.length > pushed.length) return false;
        Stack<Integer> stack = new Stack<>();
        int i = 0, j = 0;
        while (j < popped.length) {
            // if we can pop, then pop
            if (!stack.isEmpty() && stack.peek() == popped[j]) {
                stack.pop();
                j++;
            } else {
                // if we cannot pop, then try to push
                if (i < pushed.length) {
                    stack.push(pushed[i++]);
                } else {
                    // if we cannot pop, nor can we push, then simulation is done
                    // break and check final result
                    break;
                }
            }
        }
        return j == popped.length;
    }
    
}

class Monotonic {
    
    // monotonic decreasing stack
    public static int[] nextWarmerDay(int[] temperatures) {
        int[] warmer = new int[temperatures.length];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < temperatures.length; i++) {
            while (!stack.isEmpty() && temperatures[i] > temperatures[stack.peek()]) {
                warmer[stack.peek()] = (i - stack.peek());
                stack.pop();
            }
            stack.push(i);
        }
        while (!stack.isEmpty()) warmer[stack.pop()] = 0;
        return warmer;
    }
    
    // monotonic decreasing
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        /*
            algo:
                process num2 using monotonic stack and store results in hashmap for fast lookup
                constraint: all elements are unique
         */
        Stack<Integer> decreasing = new Stack<>();
        HashMap<Integer, Integer> nextGreater = new HashMap<>();
        for (int i = 0; i < nums2.length; i++) {
            while (!decreasing.isEmpty() && nums2[decreasing.peek()] < nums2[i]) {
                nextGreater.put(nums2[decreasing.pop()], nums2[i]);
            }
            decreasing.push(i);
        }
        // while (!decreasing.isEmpty()) nextGreater.put(nums2[decreasing.pop()], -1);
        int[] ans = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            ans[i] = nextGreater.getOrDefault(nums1[i], -1);
        }
        return ans;
    }
    
    class StockSpanner {
        // monotonic decreasing
        Stack<List<Integer>> stack = new Stack<>();
        
        public int next(int price) {
            int span = 1;
            while (!stack.isEmpty() && stack.peek().get(0) <= price) {
                List<Integer> list = stack.pop();
                span += list.get(1);
            }
            stack.push(Arrays.asList(price, span));
            return span;
        }
    }
    
}
