package com.practice.ds.stack;

import java.util.*;

public class EPI {
    
    private static class StackWithMaxApi {
        /*
            question:
                EPI 9.1: stack with max api
            algo:
                if max is poped, we need to find next max
                keep track of previous max using stack
                also, since elements are not unique, so use counter as well
         */
        
        public class MaxNode {
            int value;
            int count;
            
            public MaxNode(int value, int count) {
                this.value = value;
                this.count = count;
            }
        }
        
        public class StackWithMax {
            
            private Deque<Integer> dataStack = new LinkedList<>();
            private Deque<MaxNode> maxStack = new LinkedList<>();
            
            public void push(int data) {
                if (isEmpty()) {
                    dataStack.push(data);
                    maxStack.push(new MaxNode(data, 1));
                } else {
                    dataStack.push(data);
                    MaxNode max = maxStack.peek();
                    if (data > max.value) {
                        // case 1: new > max: add new max
                        maxStack.add(new MaxNode(data, 1));
                    } else if (data == max.value) {
                        // case 2: new == max: inc existing max
                        max.count++;
                    } else {
                        // case 3: new < max: do nothing
                    }
                }
            }
            
            public Integer pop() {
                if (isEmpty()) return null;
                // case1: pop != max: do nothing
                // case2: pop == max: decrease count and pop max if count == 0
                Integer data = dataStack.pop();
                MaxNode max = maxStack.peek();
                if (data == max.value) {
                    max.count--;
                    if (max.count == 0) maxStack.pop();
                }
                return maxStack.peek().value;
            }
            
            public Integer max() {
                if (isEmpty()) return null;
                return maxStack.peek().value;
            }
            
            public boolean isEmpty() {
                return dataStack.isEmpty();
            }
        }
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class RPNEvaluator {
        /*
            question:
                EPI 9.2
            algo:
                use stack
         */
        
        public static int evaluateRPN(String rpnStr) {
            if (rpnStr == null || rpnStr.isEmpty()) return 0;
            
            Set<String> validOperations = new HashSet<>(Arrays.asList("+", "-", "*", "/"));
            Deque<Integer> stack = new LinkedList<>();
            String[] tokens = rpnStr.split(",");
            for (String token : tokens) {
                if (validOperations.contains(token)) {
                    int x = stack.pop();
                    int y = stack.pop();
                    switch (token) {
                        case "+":
                            stack.push(x + y);
                            break;
                        case "-":
                            stack.push(x - y);
                            break;
                        case "*":
                            stack.push(x * y);
                            break;
                        case "/":
                            stack.push(y / x);
                            break;
                        default:
                            throw new IllegalArgumentException("invalid operation: " + token);
                    }
                } else {
                    stack.push(Integer.parseInt(token));
                }
            }
            
            return stack.pop();
        }
        
        private static void printRPNEvaluation(String str) {
            System.out.println(str + " evaluated value is: " + evaluateRPN(str));
        }
        
        public static void main(String[] args) {
            printRPNEvaluation("3,4,+,2,*,1,+");
        }
    }
    
    private static class CheckBracesValidity {
        /*
            question:
                EPI 9.3
            algo:
                use stack
         */
        
        public static boolean isValidBracesExpression(String str) {
            Map<Character, Character> leftToRightBracketMap = new HashMap<>();
            leftToRightBracketMap.put('[', ']');
            leftToRightBracketMap.put('{', '}');
            leftToRightBracketMap.put('(', ')');
            
            Deque<Character> stack = new LinkedList<>();
            for (char ch : str.toCharArray()) {
                if (leftToRightBracketMap.containsKey(ch)) {
                    stack.push(ch);
                } else {
                    if (leftToRightBracketMap.get(stack.pop()) != ch) return false;
                }
            }
            return stack.isEmpty();
        }
        
        
        private static void printBracesValidity(String str) {
            System.out.println(str + " validity check result: " + isValidBracesExpression(str));
        }
        
        public static void main(String[] args) {
            printBracesValidity("(({[](){}()}))");
            printBracesValidity("(({[](){()}))");
        }
    }
    
    private static class JumpOrderTraversal {
        /*
            question:
                EPI 9.5
            algo:
                use recursion/iter for preorder traversal
         */
        
        public static class ListNode {
            int data;
            int order = -1;
            ListNode next, jump;
            
            public ListNode(int data) {
                this.data = data;
            }
            
            public ListNode(int data, ListNode next, ListNode jump) {
                this.data = data;
                this.next = next;
                this.jump = jump;
            }
        }
        
        // jump link pre-order visit using recursion
        public static void jumpOrderVisitRecHelper(ListNode head, int order) {
            // case 1: null -> nothing to visit
            if (head == null) return;
            // case 2: already visited/being visited -> nothing to visit
            if (head.order != -1) return;
            // case 3: unvisited -> do visit
            head.order = ++order; // preorder traversal, so mark node order before visiting children
            // visit children with priority to jump
            jumpOrderVisitRecHelper(head.jump, order);
            jumpOrderVisitRecHelper(head.next, order);
        }
        public static void jumpOrderVisitRec(ListNode head) {
            jumpOrderVisitRecHelper(head, 0);
        }
        
        /*
            // general algo for converting recursive code to iterative
        
            public static void recToIter() {
                Stack stack = new Stack();
                stack.push(first call)
                while (stack is not empty) {
                    context = stack.pop();
                    if (any of the base case) {
                        // handle base case(s)
                    } else {
                        // handle recursive case
                        1. do pre-recursion processing
                        2. push post processing context back
                        3. push recursive cases on to stack
                    }
                }
                
            }
         */
        public static void jumpOrderVisitIter(ListNode head) {
            Deque<ListNode> stack = new LinkedList();
            stack.push(head);
            int order = 0;
            while (!stack.isEmpty()) {
                ListNode curr = stack.pop();
                // base case1: curr == null
                if (curr == null) continue;
                // base case2: curr already visited
                if (curr.order != -1) continue;
                // non-base case: curr is not visited
                // step1: preprocess
                curr.order = ++order;
                // step2: push post processing context -> none in this case
                // step3: push recursive calls on to the stack
                // we need jump order first, so push it last i.e. make it top
                stack.push(curr.next);
                stack.push(curr.jump);
            }
        }
        
        private static ListNode dummyList() {
            ListNode n1 = new ListNode(1);
            ListNode n2 = new ListNode(2);
            ListNode n3 = new ListNode(3);
            ListNode n4 = new ListNode(4);
    
            n1.next = n2;
            n2.next = n3;
            n3.next = n4;
    
            n1.jump = n3;
            n2.jump = n4;
            n3.jump = n2;
            n4.jump = n4;
            return n1;
        }
        public static void printList(ListNode head) {
            while (head != null) {
                System.out.println("[" + head.data + ", " + head.order + "]");
                head = head.next;
            }
        }
        private static void print() {
            System.out.println("jump order using recursion is: ");
            ListNode l1 = dummyList();
            jumpOrderVisitRec(l1);
            printList(l1);
            
            System.out.println("jump order using iteration is: ");
            ListNode l2 = dummyList();
            jumpOrderVisitRec(l2);
            printList(l2);
        }
        
        public static void main(String[] args) {
            print();
        }
    }
    
    private static class BuildingsWithSunsetView {
        /*
            question:
                EPI 9.6
            algo:
                use stack: monotonic decreasing
         */
        
        public static List<Integer> buildingWithSunsetView(List<Integer> arr) {
            Deque<Integer> stack = new LinkedList<>();
            for (int i=arr.size()-1; i>=0; i--) {
                Integer e = arr.get(i);
                if (stack.isEmpty()) {
                    stack.push(e);
                } else {
                    while (!stack.isEmpty() && stack.peek() <= e) stack.pop();
                    stack.push(e);
                }
            }
            return new ArrayList<>(stack);
        }
        
        private static void printBuildingWithSunsetView(Integer[] arr) {
            System.out.println("buildings with sunset view in: " + Arrays.toString(arr)
                + " are: " + buildingWithSunsetView(Arrays.asList(arr)));
        }
        
        public static void main(String[] args) {
            printBuildingWithSunsetView(new Integer[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques4 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques5 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques6 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques7 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques8 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques9 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques10 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
}
