package com.practice.ds.stack;


/*
  Stack implementations
  ----------------------
  - stack using queues
  - ref: https://www.geeksforgeeks.org/implement-stack-using-queue/
  - method: use two queues: q1 and q2
      - push: enqueue element to queue
      - pop: while queue q1 size > 1: dequeue and enqueue to q2, dequeue result from q1, swap q1 and q2 queues
  
 */

/*
  Augmented Stack
  ----------------
  
  - stack with 0(1) min, constraint: can use stack data structure only: T(1), S(n)
  - ref: https://www.geeksforgeeks.org/design-and-implement-special-stack-data-structure/
  - method: use an extra stack to keep track of mins
            (or: augment stack nodes to keep track of previous min)
            (edge case: handling duplicates, push to min stack if new data <= min so far...mind <=)
  
 */

/*
  Sorting or Reversing Stacks
  ----------------------------
  
  Reverse A Stack
  ----------------
  
  - reverse a stack
  - ref: https://www.geeksforgeeks.org/reverse-a-stack-using-recursion/
  - method1: use an auxiliary stack
  
  - reverse stack using recursion
  - ref: https://www.geeksforgeeks.org/reverse-a-stack-using-recursion/
  - method:
      - bottom up recursion1: pop element and reverse rest of stack
      - bottom up recursion2: insert poped element at bottom of reversed stack
  
  
  Sort A Stack
  -------------
  
  - sort a stack using stack data structure only
  - ref: https://www.geeksforgeeks.org/sort-stack-using-temporary-stack/
  - method: use an auxiliary stack as a sorted stack
        while(stack1 is not empty)
           pop element and insert in sorted stack2 using recursion
  
  - sort a stack using recursion only(no auxiliary stack allowed)
  - ref: https://www.geeksforgeeks.org/sort-a-stack-using-recursion/
  - method:
      - bottom up recursion1: pop element and sort rest of stack
      - bottopm up recursion2: insert poped element in sorted stack using recursion
  
  - sort a stack without using recursion and using only one auxiliary stack
  - ref: https://www.geeksforgeeks.org/sort-a-stack-using-recursion/
  - method:
        while(stack1 is not empty)
          pop element from stack1
          while(top of stack2 is > poped element)
            pop and push elment back to stack1
          push element to stack2
 */
