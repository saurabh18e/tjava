package com.practice.ds.sort.finale;

import com.practice.ds.heap.finale.*;
import com.practice.ds.list.finale.*;

import java.util.*;

import static com.practice.util.Util.*;

public class Medium {
}

class Intervals {
    // use data structure: hashmap, stack or queue and see if we can avoid with sweep-line
    // sorting generally helps to process endpoints in some order
    // take special care of end points being open/close and what the behaviour should be
    
    class MaxConcurrentIntervals {
        
        private class Endpoint implements Comparable<Endpoint> {
            public int timestamp;
            public boolean isStart;
            public Endpoint(int timestamp, boolean isStart) {
                this.timestamp = timestamp;
                this.isStart = isStart;
            }
            @Override
            public int compareTo(Endpoint o) {
                int cmp1 = Integer.compare(this.timestamp, o.timestamp);
                if (cmp1 != 0) return cmp1;
                // if interviewer says that start end on same time means extra meeting room
                // then start should come first
                if (isStart && !o.isStart) return -1;
                if (!isStart && o.isStart) return 1;
                return 0;
            }
            
        }
        public int minMeetingRoomsV1(int[][] intervals) {
            // we can use hashmap to count max going on
            // but use short cut: sweeping line/manual diff/count algo
            
            if (intervals.length == 0) return 0;
            Endpoint[] endPoints = new Endpoint[intervals.length * 2];
            for (int i = 0, j = 0; i < intervals.length; ) {
                endPoints[j++] = new Endpoint(intervals[i][0], true);
                endPoints[j++] = new Endpoint(intervals[i++][1], false);
            }
            Arrays.sort(endPoints);
            int count = 0, max = Integer.MIN_VALUE;
            for (Endpoint e : endPoints) {
                count += (e.isStart) ? 1 : -1;
                max = Math.max(max, count);
            }
            return max;
        }
        
        public int minMeetingRoomsV2(int[][] intervals) {
            // optimization: do not mix start and finish endpoints
            if (intervals.length == 0) return 0;
            int[] start = new int[intervals.length];
            int[] finish = new int[intervals.length];
            for (int i = 0; i < intervals.length; i++) {
                start[i] = intervals[i][0];
                finish[i] = intervals[i][1];
            }
            Arrays.sort(start);
            Arrays.sort(finish);
            int count = 0, max = Integer.MIN_VALUE;
            for (int i = 0, j = 0; i < intervals.length; ) {
                if (start[i] < finish[j]) {
                    count++;
                    i++;
                    max = Math.max(max, count);
                } else if (start[i] == finish[j]) {
                    // if interviewer says that start end on same time
                    // does not mean extra meeting room then start and finish cancells each other
                    i++;
                    j++;
                } else {
                    count--;
                    j++;
                }
            }
            return max;
        }
        
    }
    
    class MergeIntervalsIntoDisjointSets {
        
        public int[][] merge(int[][] intervals) {
            // use stack to keep track of currently open interval
            // a new interval either updates the top or gets pushed a stop itself
            // shortcut: since we only need stack top, we can just keep current as variable
            if (intervals.length == 0) return intervals;
            
            // Arrays.sort(intervals, (i1, i2) -> Integer.compare(i1[0], i2[0]));
            Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));
            ArrayList<int[]> output = new ArrayList<>();
            int[] curr = new int[]{intervals[0][0], intervals[0][1]};
            for (int[] next : intervals) {
                if (curr[1] < next[0]) {
                    output.add(curr);
                    curr = new int[]{next[0], next[1]};
                } else {
                    curr[1] = Math.max(curr[1], next[1]);
                }
            }
            output.add(curr);
            return output.toArray(new int[output.size()][]);
        }
        
        private class EndPoint {
            public int timestamp;
            public boolean isClosed;
            public EndPoint(int timestamp, boolean isClosed) {
                this.timestamp = timestamp;
                this.isClosed = isClosed;
            }
            
        }
        
        private class Interval {
            public EndPoint start;
            public EndPoint end;
            public Interval(EndPoint start, EndPoint end) {
                this.start = start;
                this.end = end;
            }
            
        }
        
        private class IntervalComparator implements Comparator<Interval> {
            @Override
            public int compare(Interval i1, Interval i2) {
                // we want to sort by start end point for merging
                // also closed start endpoint should be considered before open start endpoint
                int cmp1 = Integer.compare(i1.start.timestamp, i2.start.timestamp);
                if (cmp1 != 0) return cmp1;
                return Integer.compare(i1.start.isClosed ? 1 : -1, i2.start.isClosed ? 1 : -1);
            }
            
        }
        public List<Interval> mergeOpenClosedIntervals(List<Interval> intervals) {
            if (intervals.size() == 0) return intervals;
            Collections.sort(intervals, new IntervalComparator());
            List<Interval> output = new ArrayList<>();
            Interval curr = null;
            for (Interval next : intervals) {
                if (curr == null) {
                    curr = next;
                } else if (overlap(curr, next)) {
                    // already sorted by left end point(time and closure), so update right
                    if (curr.end.timestamp < next.end.timestamp) {
                        curr.end.timestamp = next.end.timestamp;
                        curr.end.isClosed = next.end.isClosed;
                    } else if (curr.end.timestamp == next.end.timestamp) {
                        curr.end.isClosed = next.end.isClosed || curr.end.isClosed;
                    }
                } else {
                    output.add(curr);
                    curr = next;
                }
            }
            output.add(curr);
            return output;
        }
        private boolean overlap(Interval i1, Interval i2) {
            // overlap : i1.start <= i2.end && i1.end >= i2.start
            // we are already sorted on left end point i.e. i1.start <= e2.end
            // so overlap only needs to check: i1.end >= i2.start
            if (i1.end.timestamp > i2.start.timestamp) {
                return true;
            } else if (i1.end.timestamp == i2.start.timestamp) {
                return i1.end.isClosed || i2.start.isClosed;
            }
            return false;
        }
        
    }
    
    class MergeSingleInterval {
        
        private class Interval {
            public int start, end;
            
        }
        public List<Interval> mergeSingleInterval(Interval i, List<Interval> intervals) {
            Collections.sort(intervals, Comparator.comparingInt(l -> l.start));
            List<Interval> output = new ArrayList<>();
            for (Interval j : intervals) {
                if (i == null) {
                    // if i is processed, then nothing to do
                    output.add(j);
                } else if (j.end < i.start) {
                    output.add(j);
                } else if (j.start <= i.end) { // j.end >= start && j.start <= end
                    // merge j with i
                    i.start = Math.min(i.start, j.start);
                    i.end = Math.max(i.end, j.end);
                } else {
                    // output i and signal merge finish
                    output.add(i);
                    i = null;
                }
            }
            return output;
        }
        
    }
    
}

class SortingBasicComparison {
    
    /*
        sorting benchmarks
            • stable or not
            • in place or not
            • extra space
            • worst and average case perf
     */
    
    /*
        - ref: https://en.wikipedia.org/wiki/Bubble_sort
        - algo: bubble out the max to end at each iteration
        - O(n^2) comparisons and O(n^2) swaps, S(1)
        - non stable
     */
    public void buubleSort(int[] arr) {
        // bubble out max to end
        for (int i = 0; i < arr.length - 1; i++) {
            boolean swapped = false;
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j, j + 1);
                    swapped = true;
                }
            }
            if (!swapped) break;
        }
    }
    
    /*
        - ref: https://en.wikipedia.org/wiki/Selection_sort
        - algo: find max in each iteration and insert at end
        - advantage
            - better than bubble since less swaps are done: O(n^2) comparisons and O(n) swaps, S(1)
            - stable sort
     */
    public void selectionSort(int[] arr) {
        // select max and move to end (no bubbling - avoid swaps)
        for (int i = 0; i < arr.length - 1; i++) {
            int maxIndex = 0;
            for (int j = 1; j < arr.length - i; j++) {
                if (arr[j] >= arr[maxIndex]) maxIndex = j;
            }
            if (maxIndex != arr.length - 1 - i) swap(arr, maxIndex, arr.length - 1 - i);
        }
    }
    
    /*
    - ref: https://en.wikipedia.org/wiki/Insertion_sort
    - also: insert one element at a time into a sorted list
    - advantage
        - better than bubble if list is already sorted: O(n^2) comparisons and O(n) swaps, S(1)
        - stable sort
 */
    public void insertionSort(int[] arr) {
        // insert each element in right place in sorted window
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = arr.length - 1 - (i + 1); j < arr.length - 1; j++) {
                if (arr[j] <= arr[j + 1]) break;
                swap(arr, j, j + 1);
            }
        }
    }
    
    private int[] testData() {
        return new int[]{5, 6, 1, 2, 3};
    }
    
    public static void main(String[] args) {
        SortingBasicComparison sorter = new SortingBasicComparison();
        // basic comparison sorts
        int[] arr = sorter.testData();
        // sorter.buubleSort(arr);
        // sorter.selectionSort(arr);
        sorter.insertionSort(arr);
        System.out.println(Arrays.toString(arr));
    }
    
}

class SortingAdvancedComparison {
    /*
        1. BIS: bubble sort, insertion sort, selection sort
        2. MHQ: merge sort, heap sort, quick sort(and randomized quick sort)
        3. CBR: non comparison sorts: counting sort, bucket sort and radix sort
        4. Selection/Order Static: Min-Max, nth max
     */
    
    private void heapSort(int[] arr) {
        if (arr.length < 1) return;
        // use own implementation since we want to reuse space
        Theory.MaxHeap maxHeap = new Theory.MaxHeap(arr);
        maxHeap.heapSort();
    }
    
    private void mergeSort(int[] arr) {
        mergeSortRec(arr, 0, arr.length - 1);
    }
    private void mergeSortRec(int[] arr, int start, int end) {
        if (start >= end) return;
        int mid = start + (end - start) / 2;
        mergeSortRec(arr, start, mid);
        mergeSortRec(arr, mid + 1, end);
        merge(arr, start, mid, end);
    }
    
    private void merge(int[] arr, int start, int mid, int end) {
        int[] tmp = new int[end - start + 1];
        for (int i = start, j = mid + 1, k = 0; k <= end - start; ) {
            if (i <= mid && (j > end || arr[j] > arr[i])) {
                tmp[k++] = arr[i++];
            } else {
                tmp[k++] = arr[j++];
            }
        }
        for (int i = start, k = 0; k <= end - start; ) {
            arr[i++] = tmp[k++];
        }
    }
    
    private void quickSort(int[] arr) {
        if (arr.length < 1) return;
        randomizedQuickSort(arr, 0, arr.length - 1);
    }
    private void randomizedQuickSort(int[] arr, int start, int end) {
        if (start >= end) return;
        Random r = new Random();
        int pivotIndex = start + r.nextInt(end - start + 1);
        pivotIndex = pivot(arr, start, end, pivotIndex);
        randomizedQuickSort(arr, start, pivotIndex - 1);
        randomizedQuickSort(arr, pivotIndex + 1, end);
    }
    private int pivot(int[] arr, int start, int end, int pivotIndex) {
        int pivot = arr[pivotIndex];
        int i = start - 1, j = start, k = end + 1;
        while (j < k) {
            if (arr[j] == pivot) j++;
            else if (arr[j] < pivot) swap(arr, ++i, j++);
            else swap(arr, j, --k);
        }
        return i + 1;
    }
    
    private int[] testData() {
        return new int[]{5, 6, 1, 2, 3};
    }
    
    public static void main(String[] args) {
        SortingAdvancedComparison sorter = new SortingAdvancedComparison();
        int[] arr = sorter.testData();
        // sorter.heapSort(arr);
        // sorter.mergeSort(arr);
        sorter.quickSort(arr);
        System.out.println(Arrays.toString(arr));
    }
    
}

class SortingNonComparison {
    
    /*
        Counting Sort
        --------------
        - assumptions
            - all numbers like in a given range [0, k], k = 0(n)
        - algo
            - count the number of occurrences of each element using an array of size k
            - it is a stable sort
            - O(n+k), S(k)
     */
    public void countingSort(int[] arr) {
        if (arr.length == 0) return;
        int min = arr[0], max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            min = Math.min(min, arr[i]);
            max = Math.max(max, arr[i]);
        }
        // can reduce range using min and max, but it's ok for now
        countingSort(arr, max + 1);
    }
    public void countingSort(int[] arr, int k) {
        // step1: count
        int[] counts = new int[k];
        for (int i = 0; i < arr.length; i++) counts[arr[i]] += 1;
        
        // step2: count cumulative i.e. calculate offsets
        // (we don't need this as we can directly copy
        // count array as output, but what if input is String[])
        for (int i = 1; i < counts.length; i++) counts[i] += counts[i - 1];
        
        // step3: make use of counting
        // note: we follow groupBy algo and avoid extra space, but for now, it's ok
        // note: process in reverse order to make counting sort stable
        int[] tmp = new int[arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            tmp[counts[arr[i]] - 1] = arr[i];
            counts[arr[i]]--;
        }
        for (int i = 0; i < arr.length; i++) arr[i] = tmp[i];
    }
    
    public String sortString(String str) {
        // idea is to use counting sort for sorting chars in a string
        int[] arr = new int[256];
        for (char ch : str.toCharArray()) arr[ch] += 1;
        
        // no need to reuse space or copy back or making things table
        // produce the output and return
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            char ch = (char) i;
            for (int j = 0; j < arr[i]; j++) builder.append(ch);
        }
        return builder.toString();
    }
    
    /*
        Bucket Sort
        ------------
        - assumption: all n numbers are uniformly distributed between [0,k]
        - algorithm
            - normalize the numbers to space [0, 1[ by dividing each number by (k+1)
            - create n buckets, each of size 1/n and put the numbers in the buckets in a list
            - use insertion sort to sort each list
            - concat the lists to produce sorted output
            - 0(n), S(n)
     */
    public void bucketSort(int[] arr) {
        // step1: calculate and init buckets
        if (arr.length <= 1) return;
        int min = arr[0], max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            min = Math.min(min, arr[i]);
            max = Math.max(max, arr[i]);
        }
        int k = max - min + 1;
        int bucketCount = arr.length;
        List<Integer>[] buckets = new List[bucketCount];
        
        // step 2: insert in buckets
        for (int i = 0; i < arr.length; i++) {
            int bucketNumber = arr[i] / k;
            if (buckets[bucketNumber] == null) buckets[bucketNumber] = new ArrayList<>();
            buckets[bucketNumber].add(arr[i]);
        }
        
        // sort each bucket and copy back to output
        int j = 0;
        for (int i = 0; i < bucketCount; i++) {
            if (buckets[i] == null) continue;
            Collections.sort(buckets[i]);
            for (int a : buckets[i]) {
                arr[j++] = a;
            }
        }
    }
    
    /*
        Radix Sort
        -----------
        - assumption: all numbers are of d digits
        - algorithm
            - for each digit from smallest to highest d:
                sort the numbers in array using counting sort based on digit d
            - stable sort
            - 0(d(n+k)), S(k)...practially for
            - base 10 32 bit integers have 10 digits max and 64 bit long have 20 digits
            - 0(n), S(1): for ints and longs
     */
    private void radixSort(int[] arr) {
        if (arr.length == 0) return;
        int max = arr[0];
        for (int a:arr) max = Math.max(max, Math.abs(a));
        int factor = 1;
        while (max > 0) {
            radixSortDigits(arr, factor);
            max = max / 10;
            factor = factor * 10;
        }
        handleNegativeNumbers(arr);
    }
    private void handleNegativeNumbers(int[] arr) {
        int[] output = new int[arr.length];
        int j = 0;
        for (int i = arr.length - 1, k = arr.length - 1; i >= 0; ) {
            if (arr[i] > 0) output[k--] = arr[i--];
            else output[j++] = arr[i--];
        }
        for (int i = 0; i < arr.length; i++) arr[i] = output[i];
    }
    private void radixSortDigits(int[] arr, int factor) {
        // counting sort
        int[] counts = new int[10];
        for (int a : arr) counts[countsIndex(a, factor)]++;
        for (int i = 1; i < counts.length; i++) counts[i] += counts[i - 1];
        
        int[] tmp = new int[arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            int index = countsIndex(arr[i], factor);
            tmp[counts[index] - 1] = arr[i];
            counts[index]--;
        }
        for (int i = 0; i < arr.length; i++) arr[i] = tmp[i];
    }
    private int countsIndex(int a, int factor) {
        return (Math.abs(a) / factor) % 10;
    }
    
    private int[] testData() {
        return new int[]{5, 6, 1, 2, 3};
    }
    
    public static void main(String[] args) {
        SortingNonComparison sorter = new SortingNonComparison();
        int[] arr = sorter.testData();
        // sorter.countingSort(arr);
        // sorter.bucketSort(arr);
        arr = new int[]{35, 447, 147, 500, 0, -2, -500, -599, -50, 0, 95, 195, 595};
        arr = new int[]{-1, 2, -8, -10};
        // arr = new int[]{1, 2, 8, 10};
        sorter.radixSort(arr);
        System.out.println(Arrays.toString(arr));
        
        // sort string chars
        // System.out.println(sorter.sortString("b;ljdnckbasb"));
    }
    
}

class SortingLinkedList {
    
    // algo1: top-down divide and conquer i.e. merge sort
    // algo2: bottom up iterative approach
    
    // merge sort is most appropriate sorting algo for linked list
    // due to linked structure of list
    public ListNode mergeSort(ListNode head) {
        if (head == null || head.next == null) return head;
        
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        ListNode head2 = slow.next;
        slow.next = null;
        return merge(mergeSort(head), mergeSort(head2));
    }
    
    private ListNode merge(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        ListNode dummyHead = new ListNode(0), iter = dummyHead;
        while (l1 != null || l2 != null) {
            if (l1 != null && (l2 == null || l1.val <= l2.val)) {
                iter.next = l1;
                l1 = l1.next;
            } else {
                iter.next = l2;
                l2 = l2.next;
            }
            iter = iter.next;
            iter.next = null;
        }
        return dummyHead.next;
    }
    
    static class Chunk {
        public ListNode start, end;
        
        public Chunk(ListNode start, ListNode end) {
            this.start = start;
            this.end = end;
        }
        
    }
    private static int length(ListNode head) {
        int count = 0;
        while (head != null) {
            count++;
            head = head.next;
        }
        return count;
    }
    
    public ListNode mergeSortBottomUp(ListNode head) {
        int n = length(head);
        for (int size = 1; size < n; size = size * 2) {
            head = mergeChunks(head, size);
        }
        return head;
    }
    public ListNode mergeChunks(ListNode head, int size) {
        if (head == null) return null;
        ListNode mergedHead = new ListNode(0), mergedTail = mergedHead;
        while (head != null) {
            Chunk c1 = getChunk(head, size);
            if (c1.start == null) break;
            head = c1.end.next;
            c1.end.next = null;
            
            Chunk c2 = getChunk(head, size);
            if (c2.start != null) {
                head = c2.end.next;
                c2.end.next = null;
            }
            
            Chunk mergedChunk = mergeChunks(c1, c2);
            mergedTail.next = mergedChunk.start;
            mergedTail = mergedChunk.end;
        }
        return mergedHead.next;
    }
    
    private static Chunk getChunk(ListNode head, int size) {
        ListNode iter = head, prev = null;
        for (int i = 0; iter != null && i < size; i++) {
            prev = iter;
            iter = iter.next;
        }
        return new Chunk(head, prev);
    }
    
    private Chunk mergeChunks(Chunk c1, Chunk c2) {
        if (c1.start == null) return c2;
        if (c2.start == null) return c1;
        ListNode dummyHead = new ListNode(0), iter = dummyHead;
        ListNode l1 = c1.start, l2 = c2.start;
        while (l1 != null || l2 != null) {
            if (l1 != null && (l2 == null || l1.val <= l2.val)) {
                iter.next = l1;
                l1 = l1.next;
            } else {
                iter.next = l2;
                l2 = l2.next;
            }
            iter = iter.next;
            iter.next = null;
        }
        return new Chunk(dummyHead.next, iter);
    }
    
}
