package com.practice.ds.sort.finale;

import java.util.*;

public class Easy {
}

class Basic {
    
    public static int[] intersectionOfSorted(int[] arr1, int[] arr2) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0, j = 0; i < arr1.length && j < arr2.length; ) {
            if (arr1[i] == arr2[j]) {
                list.add(arr1[i]);
                i++;
                j++;
            } else if (arr1[i] < arr2[j]) {
                i++;
            } else {
                j++;
            }
        }
        return list.stream().mapToInt(i -> i).toArray();
    }
    
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (n == 0) return;
        
        int i = m - 1, j = n - 1, k = m + n - 1;
        for (; k >= 0; k--) {
            if (i >= 0 && (j < 0 || (nums1[i] >= nums2[j]))) {
                nums1[k] = nums1[i--];
            } else {
                nums1[k] = nums2[j--];
            }
        }
    }
    
    public boolean teamPhotoDay(int[] teamA, int[] teamB) {
        /*
           question: can we place team A in a row behind row of team B
           solution:
            greedy: place shortest of A with shortest of B
            so sort and process, length of diff does not matter
         */
        Arrays.sort(teamA);
        Arrays.sort(teamB);
        for (int i=0; i<teamA.length && i<teamB.length; i++) {
            if (teamA[i] >= teamB[i]) return false;
        }
        return true;
    }
}

