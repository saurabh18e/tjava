package com.practice.ds.array.junk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MergeIntervals {
  
  public static class Interval {
    
    public int start;
    public int end;
    
    public Interval(int start, int end) {
      this.start = start;
      this.end = end;
    }
    
    @Override public String toString() {
      return "[" + start + ", " + end + "]";
    }
  }
  
  public boolean overlap(Interval i1, Interval i2) {
    if (i1 == null || i2 == null) return false;
    return !(i1.start > i2.end || i2.start > i1.end);
  }
  
  public Interval merge(Interval i1, Interval i2) {
    return new Interval(Math.min(i1.start, i2.start), Math.max(i1.end, i2.end));
  }
  
  public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
    System.out.println(intervals);
    
    Collections.sort(intervals, new Comparator<Interval>() {
      @Override
      public int compare(Interval i1, Interval i2) {
        if (i1.start == i2.start) {
          return 0;
        } else if (i1.start < i2.start) {
          return -1;
        } else {
          return 1;
        }
      }
    });
    System.out.println(intervals);
    
    ArrayList<Interval> output = new ArrayList<Interval>();
    for (int i=0; i<intervals.size();) {
      Interval interval=intervals.get(i);
      Interval mergedInterval = interval;
      i++;
      while (i<intervals.size() && overlap(mergedInterval, intervals.get(i))) {
        mergedInterval = merge(mergedInterval, intervals.get(i));
        i++;
      }
      output.add(mergedInterval);
    }
    return output;
  }
  
  public static void main(String[] args) {
    ArrayList<Interval> intervals = new ArrayList<Interval>();
    intervals.add(new Interval(1, 10));
    intervals.add(new Interval(2, 9));
    intervals.add(new Interval(3, 8));
    intervals.add(new Interval(4, 7));
    intervals.add(new Interval(5, 6));
    intervals.add(new Interval(6, 6));
    
    MergeIntervals mergeIntervals = new MergeIntervals();
    mergeIntervals.merge(intervals);
  }
  
}
