package com.practice.ds.array.junk;

import java.util.Arrays;

public class Partitions {
  
  static int count=0;
  
  public static void partitionsLexo(int n, int maxVal, int[] partition, int index) {
    
    if (n==0) {
      StringBuffer buff = new StringBuffer();
      for(int i=0; i<index;i++) {
        buff.append(partition[i] + " ");
      }
      System.out.println(buff);
      count++;
    }
    
    if (n == 0 || maxVal == 0) {
      return;
    }
    
    partitionsLexo(n, maxVal-1, partition, index);
    
    if (maxVal <= n) {
      partition[index] = maxVal;
      partitionsLexo(n-maxVal, maxVal, partition, index+1);
    }
  }
  
  public static void partitionsLexo(int n) {
    partitionsLexo(n, n, new int[n], 0);
  }
  
  public static void partitionsWithoutRep(int n, int maxVal, int[] partition, int index) {
    
    if (n==0) {
      StringBuffer buff = new StringBuffer();
      for(int i=0; i<index;i++) {
        buff.append(partition[i] + " ");
      }
      System.out.println(buff);
      count++;
    }
    
    if (n == 0 || maxVal == 0) {
      return;
    }
  
    partitionsWithoutRep(n, maxVal-1, partition, index);
    
    if (maxVal <= n) {
      partition[index] = maxVal;
      partitionsWithoutRep(n-maxVal, maxVal-1, partition, index+1);
    }
  }
  
  public static void partitionsWithoutRep(int n) {
    partitionsWithoutRep(n, n, new int[n], 0);
  }
  
  
  private static void partitionsLexo(int n, int maxVal, int[] partition, int index, int maxPartitions) {
    if (n==0) {
      StringBuffer buff = new StringBuffer();
      for(int i=0; i<index;i++) {
        buff.append(partition[i] + " ");
      }
      System.out.println(buff);
      count++;
    }
    
    if (n == 0 || maxVal == 0) {
      return;
    }
  
    partitionsLexo(n, maxVal-1, partition, index, maxPartitions);
  
    if (maxVal <= n && index < maxPartitions) {
      partition[index] = maxVal;
      partitionsLexo(n-maxVal, maxVal, partition, index+1, maxPartitions);
    }
  }
  public static void partitionsLexo(int n, int maxVal, int maxPartitions) {
    partitionsLexo(n, Math.min(maxVal, n), new int[maxPartitions], 0, maxPartitions);
  }
  
  public static void main(String[] args) {
    partitionsWithoutRep(10);
    System.out.println("partitions: " + count);
  }
}
