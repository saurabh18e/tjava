package com.practice.ds.array.junk;

import java.util.ArrayList;
import java.util.Arrays;

public class Combination {
  
  static int count = 0;
  
  static void combinations(ArrayList<Integer> input, int start, ArrayList<Integer> output, int k) {
    if (k == 0) {
      count++;
      System.out.println(output);
      return;
    } else if (start == input.size()) {
      return;
    }
    
    // choose current element
    output.add(input.get(start));
    combinations(input, start + 1, output, k - 1);
    output.remove(output.size() - 1);
    
    // skip current element
    combinations(input, start + 1, output, k);
  }
  
  static void combinations(ArrayList<Integer> list, int k) {
    combinations(list, 0, new ArrayList<>(), k);
  }
  
  static void combinationsWithDuplicates(ArrayList<Integer> input, int start, ArrayList<Integer> output, int k) {
    if (k == 0) {
      count++;
      System.out.println(output);
      return;
    } else if (start == input.size()) {
      return;
    }
    
    // choose current element
    output.add(input.get(start));
    combinationsWithDuplicates(input, start + 1, output, k - 1);
    output.remove(output.size() - 1);
    
    // skip current element(with duplicates)
    while(start + 1 < input.size() && input.get(start).equals(input.get(start+1))) {
      start++;
    }
    combinationsWithDuplicates(input, start + 1, output, k);
  }
  static void combinationsWithDuplicates(ArrayList<Integer> list, int k) {
    combinationsWithDuplicates(list, 0, new ArrayList<>(), k);
  }
  
  public static void main(String[] args) {
    ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1, 1, 1, 2, 2));
    combinationsWithDuplicates(list, 2);
    System.out.println("total combinations: " + count);
  }
  
}
