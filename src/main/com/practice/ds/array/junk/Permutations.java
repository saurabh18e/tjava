package com.practice.ds.array.junk;

import java.util.ArrayList;
import java.util.Arrays;

public class Permutations {
  
  static int count = 0;
  
  public static void permutation(ArrayList<Integer> arr, int start, int end) {
    if (start == end) {
      count++;
      System.out.println(arr);
      return;
    }
    for (int i = start; i <= end; i++) {
      swap(arr, start, i);
      permutationLexo(arr, start + 1, end);
      swap(arr, start, i);
    }
  }
  
  public static void permutationLexo(ArrayList<Integer> arr, int start, int end) {
    if (start == end) {
      count++;
      System.out.println(arr);
      return;
    }
    // note: since shifting is needed, it's better to use doubly linked list
    for (int i = start; i <= end; i++) {
      shiftToStart(arr, start, i);
      permutationLexo(arr, start + 1, end);
      shiftToEnd(arr, start, i);
    }
  }
  private static void shiftToStart(ArrayList<Integer> arr, int start, int end) {
    Integer tmp = arr.get(end);
    while (end > start) {
      arr.set(end, arr.get(end - 1));
      end--;
    }
    arr.set(start, tmp);
  }
  private static void shiftToEnd(ArrayList<Integer> arr, int start, int end) {
    Integer tmp = arr.get(start);
    while (end > start) {
      arr.set(start, arr.get(start + 1));
      start++;
    }
    arr.set(end, tmp);
  }
  
  public static void permutationLexoWithDuplicates(ArrayList<Integer> arr, int start, int end) {
    if (start == end) {
      count++;
      System.out.println(arr);
      return;
    }
    // note: since shifting is needed, it's better to use doubly linked list
    for (int i = start; i <= end; i++) {
      shiftToStart(arr, start, i);
      permutationLexoWithDuplicates(arr, start + 1, end);
      shiftToEnd(arr, start, i);
      while (i + 1 <= end && arr.get(i).equals(arr.get(i + 1))) {
        i++;
      }
    }
  }
  
  private static void swap(ArrayList<Integer> arr, int i1, int i2) {
    Integer tmp = arr.get(i1);
    arr.set(i1, arr.get(i2));
    arr.set(i2, tmp);
  }
  
  public static void main(String[] args) {
    ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1, 1, 2, 2, 3, 3, 4, 4, 4));
    permutationLexoWithDuplicates(list, 0, list.size() - 1);
    System.out.println("total permutations: " + count);
  }
  
}
