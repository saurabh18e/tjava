package com.practice.ds.array.prob;

import javax.swing.text.html.ListView;
import java.util.HashMap;
import java.util.Map;
public class NumToWords {
    
    public static Map<Integer, String> map = mapInit();
    public static Map<Integer, String> mapInit() {
        map = new HashMap<>();
        map.put(0, "Zero");
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        map.put(5, "Five");
        map.put(6, "Six");
        map.put(7, "Seven");
        map.put(8, "Eight");
        map.put(9, "Nine");
        map.put(10, "Ten");
        map.put(11, "Eleven");
        map.put(12, "Twelve");
        map.put(13, "Thirteen");
        map.put(14, "Fourteen");
        map.put(15, "Fifteen");
        map.put(16, "Sixteen");
        map.put(17, "Seventeen");
        map.put(18, "Eighteen");
        map.put(19, "Nineteen");
        map.put(20, "Twenty");
        map.put(30, "Thirty");
        map.put(40, "Forty");
        map.put(50, "Fifty");
        map.put(60, "Sixty");
        map.put(70, "Seventy");
        map.put(80, "Eighty");
        map.put(90, "Ninety");
        return map;
    }
    public static String space(String output) {
        return (!output.isEmpty()) ? " " : "";
    }
    
    public static String hundredsToWords(int num) {
        String output = "";
        if (num >= 100) {
            output = output + map.get(num / 100) + " Hundred";
        }
        int tens = num % 100;
        if (tens > 0) {
            if (map.containsKey(tens)) {
                output = output + space(output) + map.get(tens);
            } else {
                output = output + space(output) + map.get(tens - tens % 10) + " " + map.get(tens % 10);
            }
        }
        return output;
    }
    
    // break in chunks
    // develop lib for 999 = x hundred + 99, x>0
    // 99 = special handling from 1-19 and then tens
    public static String numberToWords(int num) {
        String output = "";
        if (num >= 1000000000) {
            int billion = (num / 1000000000) % 1000;
            num = num % 1000000000;
            output = output + space(output) + hundredsToWords(billion) + " Billion";
        }
        if (num >= 1000000) {
            int million = (num / 1000000) % 1000;
            num = num % 1000000;
            output = output + space(output) + hundredsToWords(million) + " Million";
        }
        if (num >= 1000) {
            int thousand = (num / 1000) % 1000;
            num = num % 1000;
            output = output + space(output) + hundredsToWords(thousand) + " Thousand";
        }
        if (num > 0) {
            int hundreds = num % 1000;
            output = output + space(output) + hundredsToWords(hundreds);
        } else {
            output = output + ((output.isEmpty()) ? map.get(0) : "");
        }
        return output;
    }
    
    // move zeros: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/262/
    public void moveZeroes(int[] nums) {
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[j++] = nums[i];
            }
        }
        for (; j < nums.length; j++) {
            nums[j] = 0;
        }
    }
    
    // longest substring with k distinct: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3017/
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        int maxLength = 0;
        HashMap<Character, Integer> window = new HashMap<>();
        for (int l = 0, r = 0; r < s.length(); r++) {
            char rightChar = s.charAt(r);
            if (!window.containsKey(rightChar)) window.put(rightChar, 0);
            window.put(rightChar, window.get(rightChar) + 1);
            if (window.size() <= k) {
                maxLength = Math.max(maxLength, r - l + 1);
            } else {
                while (window.size() > k && l <= r) {
                    char leftChar = s.charAt(l);
                    window.put(leftChar, window.get(leftChar) - 1);
                    if (window.get(leftChar) == 0) window.remove(leftChar);
                    l++;
                }
            }
        }
        return maxLength;
    }
    
    // min windows with all chars: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/285/
    public String minWindow(String s, String t) {
        HashMap<Character, Integer> expectedCount = new HashMap<>();
        for (char ch : t.toCharArray()) {
            if (!expectedCount.containsKey(ch)) expectedCount.put(ch, 0);
            expectedCount.put(ch, expectedCount.get(ch) + 1);
        }
        
        int minSize = -1;
        int minLeft = -1;
        int minRight = -1;
        
        int found = 0;
        HashMap<Character, Integer> currentCount = new HashMap<>();
        for (int l = 0, r = 0; r < s.length(); r++) {
            char rightChar = s.charAt(r);
            if (!expectedCount.containsKey(rightChar)) continue;
            if (!currentCount.containsKey(rightChar)) currentCount.put(rightChar, 0);
            currentCount.put(rightChar, currentCount.get(rightChar) + 1);
            if (currentCount.get(rightChar).equals(expectedCount.get(rightChar))) found++;
            
            while (found == expectedCount.size() && l <= r) {
                if (minSize == -1 || minSize > r - l + 1) {
                    minSize = r - l + 1;
                    minLeft = l;
                    minRight = r;
                }
                char leftChar = s.charAt(l);
                if (expectedCount.containsKey(leftChar)) {
                    currentCount.put(leftChar, currentCount.get(leftChar) - 1);
                    if (currentCount.get(leftChar) < expectedCount.get(leftChar)) found--;
                }
                l++;
            }
        }
        
        if (minSize == -1) return "";
        StringBuilder buff = new StringBuilder();
        for (int i = minLeft; i <= minRight; i++) buff.append(s.charAt(i));
        return buff.toString();
    }
    
    // palindrome: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/288/
    public static boolean isPalindrome(String s) {
        if (s == null) return true;
        return isPalindrome(s, 0, s.length() - 1);
    }
    public static boolean isPalindrome(String s, int i, int j) {
        while (i < j) {
            while (i < j && !Character.isLetterOrDigit(s.charAt(i))) i++;
            while (i < j && !Character.isLetterOrDigit(s.charAt(j))) j--;
            if (i < j) {
                if (Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j))) return false;
                i++;
                j--;
            }
        }
        return true;
    }
    
    // palindrome with deletion: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/289/
    public static boolean validPalindrome(String s) {
        int i = 0, j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return isPalindrome(s, i + 1, j) || isPalindrome(s, i, j - 1);
            }
            i++;
            j--;
        }
        return true;
    }
    
    public static void main(String[] args) {
        String str = "cupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupucu";
        int n = str.length();
//        for (int i = 0; i < n; i++) {
//            System.out.println("found conflict at: " + i + " & " + j);
//        }
        
        System.out.println(validPalindrome(str));
    }
    
    // classic: sub array sum as k: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3019/
    /*
        - approach1: brute force: for all subarrays, find sum
        - approach2: subarray(i,j) sum = suffix(j)-suffix(i)....for each suffix(j)...substract all suffix(0,j-1)
        - appraoch3: in approach2 + caching, ....cumulative array and then try all possibilities
        - approach4: apprach3+direct lookup: use hashmap
     */
    public int subarraySum(int[] nums, int k) {
        int count = 0;
        int sum = 0;
        HashMap<Integer, Integer> sums = new HashMap<>();
        sums.put(0, 1);
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i];
            count += sums.getOrDefault(sum - k, 0);
            sums.put(sum, sums.getOrDefault(sum, 0) + 1);
        }
        return count;
    }
    
    // classic: max sum subarray: https://leetcode.com/problems/maximum-subarray/
    public int maxSubArray(int[] nums) {
        int maxSumSoFar = nums[0];
        int maxSumHere = nums[0];
        for (int i = 1; i < nums.length; i++) {
            int n = nums[i];
            maxSumHere = Math.max(maxSumHere + n, n);
            maxSumSoFar = Math.max(maxSumSoFar, maxSumHere);
        }
        return maxSumSoFar;
    }
}
