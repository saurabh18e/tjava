package com.practice.ds.array;

import java.util.*;

public class Array {
    
    public static int[] rotateArray(int[] arr, int rotations) {
        int size = arr.length;
        reverse(arr, 0, size - rotations - 1);
        reverse(arr, size - rotations, size - 1);
        reverse(arr, 0, size - 1);
        return arr;
    }
    
    private static void reverse(int[] arr, int start, int end) {
        while (start < end) {
            int tmp = arr[start];
            arr[start] = arr[end];
            arr[end] = tmp;
            start++;
            end--;
        }
    }
    
    public static List<Integer> rotateArray(List<Integer> list, int rotations) {
        int size = list.size();
        rotations = rotations % size;
        reverse(list, 0, size - rotations - 1);
        reverse(list, size - rotations, size - 1);
        reverse(list, 0, size - 1);
        return list;
    }
    
    private static void reverse(List<Integer> list, int start, int end) {
        while (start < end) {
            int tmp = list.get(start);
            list.set(start, list.get(end));
            list.set(end, tmp);
            start++;
            end--;
        }
    }
    
    public int coverPoints(List<Integer> A, List<Integer> B) {
        int minSteps = 0;
        for (int i = 0; i < A.size() - 1; i++) {
            int x1 = A.get(i), y1 = B.get(i), x2 = A.get(i + 1), y2 = B.get(i + 1);
            int horizontalDistance = Math.abs(x1 - x2);
            int verticalDistance = Math.abs(y1 - y2);
            minSteps += Math.max(horizontalDistance, verticalDistance);
        }
        return minSteps;
    }
    
    /*
      - longest substring without repeating chars
      - ref: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3008/
     */
    public static int longestSubstringWithoutDup(String str) {
        if (str == null) return 0;
        Map<Character, Integer> lastIndex = new HashMap<>();
        int len = 0;
        for (int i = 0, j = 0; j < str.length(); j++) {
            char ch = str.charAt(j);
            if (lastIndex.containsKey(ch)) {
                i = Math.max(i, lastIndex.get(ch) + 1);
            }
            lastIndex.put(ch, j);
            len = Math.max(len, j - i + 1);
        }
        return len;
    }
    
    /*
      - roman to int
      - ref: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3010/
     */
    public static int romanToInt(String str) {
        Map<String, Integer> map = new HashMap<>();
        map.put("I", 1);
        map.put("IV", 4);
        map.put("V", 5);
        map.put("IX", 9);
        map.put("X", 10);
        map.put("XL", 40);
        map.put("L", 50);
        map.put("XC", 90);
        map.put("C", 100);
        map.put("CD", 400);
        map.put("D", 500);
        map.put("CM", 900);
        map.put("M", 1000);
        
        int value = 0;
        if (str == null || str.isEmpty()) return value;
        for (int i = 0; i < str.length(); i++) {
            if (i + 1 < str.length()) {
                String key = "" + str.charAt(i) + str.charAt(i + 1);
                if (map.containsKey(key)) {
                    value = value + map.get(key);
                    i++;
                    continue;
                }
            }
            String key = str.charAt(i) + "";
            value = value + map.get(key);
        }
        return value;
    }
    
    // remove dups: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3011/
    public int removeDuplicates(int[] nums) {
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[i] != nums[j]) {
                i++;
                nums[i] = nums[j];
            }
        }
        return i + 1;
    }
    
    // group anagrams: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3014/
    public List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String, List<String>> groups = new HashMap<>();
        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);
            if (!groups.containsKey(key)) {
                groups.put(key, new ArrayList<>());
            }
            groups.get(key).add(str);
        }
        List<List<String>> output = new ArrayList<>();
        for (List<String> group : groups.values()) output.add(group);
        return output;
    }
    
    // add binary strings: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/263/
    public String addBinary(String a, String b) {
        if (a == null || a.isEmpty()) return b;
        if (b == null || b.isEmpty()) return a;
        char[] output = new char[Math.max(a.length(), b.length())];
        int carry = 0;
        for (int i = a.length() - 1, j = b.length() - 1; i >= 0 || j >= 0; i--, j--) {
            int num1 = (i >= 0) ? a.charAt(i) - '0' : 0;
            int num2 = (j >= 0) ? b.charAt(j) - '0' : 0;
            int sum = num1 + num2 + carry;
            char ch = (char) ((sum % 2) + '0');
            carry = sum / 2;
            output[i > j ? i : j] = ch;
        }
        return carry == 0 ? new String(output) : (char) (carry + '0') + new String(output);
    }
    
    // inplace sorted merge: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/309/
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1;
        int j = n - 1;
        int k = m + n - 1;
        while (j >= 0) {
            if (i >= 0 && nums1[i] > nums2[j]) {
                nums1[k--] = nums1[i--];
            } else {
                nums1[k--] = nums2[j--];
            }
        }
    }
    
    // check valid palindrome: https://leetcode.com/problems/valid-palindrome/solution/
    public boolean isPalindrome(String s) {
        if (s == null || s.isEmpty()) return true;
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            while (i < j && !Character.isLetterOrDigit(s.charAt(i))) i++;
            while (i < j && !Character.isLetterOrDigit(s.charAt(j))) j--;
            if (i < j) {
                if (Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j))) return false;
                i++;
                j--;
            }
        }
        return true;
    }
    
    private static char[] buf4 = new char[4];
    private static int bufPtr = -1;
    private static int bufSize = 0;
    private static boolean fileReadPending = true;
    private static int callNumber = 0;
    public static int read4(char[] buf) {
        callNumber++;
        if (callNumber == 1) {
            buf[0] = 'a';
            buf[1] = 'b';
            buf[2] = 'c';
            return 3;
        }
        return 0;
    }
    public static int read(char[] buf, int n) {
        int totalCharsRead = 0;
        while (totalCharsRead < n && (bufPtr != -1 || fileReadPending)) {
            if (bufPtr == -1 && fileReadPending) {
                bufSize = read4(buf4);
                if (bufSize > 0) bufPtr = 0;
                if (bufSize < 4) fileReadPending = false;
            }
            for (; bufPtr >= 0 && bufPtr < bufSize && totalCharsRead < n; ) {
                buf[totalCharsRead++] = buf4[bufPtr++];
            }
            if (bufPtr == bufSize) bufPtr = -1;
        }
        return totalCharsRead;
    }
    
    public boolean isOneEditDistance(String s, String t) {
        s = (s == null) ? "" : s;
        t = (t == null) ? "" : t;
        
        if (Math.abs(s.length() - t.length()) > 1) return false;
        
        int distance = 0;
        int i = 0, j = 0;
        for (; i < s.length() && j < t.length() && Math.abs(distance) <= 1; ) {
            if (s.charAt(i) == t.charAt(i)) {
                i++;
                j++;
            } else {
                distance++;
                if (s.length() == t.length()) {
                    i++;
                    j++;
                } else if (s.length() > t.length()) {
                    // delete from s
                    i++;
                } else {
                    // delete from t
                    j++;
                }
            }
        }
        return Math.abs(distance) + (s.length() - i) + (t.length() - j) == 1;
    }
    
    // array product: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/3016/
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
    
        int[] suffixProduct = new int[n];
        for (int i = 0; i < n; i++) {
            int product = (i == 0) ? 1 : suffixProduct[i - 1];
            int num = (i == 0) ? 1 : nums[i - 1];
            suffixProduct[i] = product * num;
        }
    
        int[] prefixProduct = new int[n];
        for (int i = n - 1; i >= 0; i--) {
            int product = (i == n - 1) ? 1 : prefixProduct[i + 1];
            int num = (i == n - 1) ? 1 : nums[i + 1];
            prefixProduct[i] = product * num;
        }
    
        int[] products = new int[n];
        for (int i = 0; i < n; i++) {
            products[i] = prefixProduct[i] * suffixProduct[i];
        }
        return products;
    }
    
    // max length without duplicates
    public static int maxLenWithDup(String s) {
        if (s == null) return 0;
        
        int low=0, high=-1, maxSoFar=0;
        HashMap<Character, Integer> set = new HashMap<>();
        // move high...keep updating maxSoFar
        while (high+1 < s.length()) {
            high++;
            char ch = s.charAt(high);
            if (set.containsKey(ch)) {
                // move low as far as possible
                low = Math.max(low, set.get(ch) + 1);
            }
            set.put(ch, high);
            maxSoFar = Math.max(maxSoFar, high-low+1);
        }
        
        return maxSoFar;
    }
    
    public static void main(String[] args) {
        System.out.println(read(new char[4], 4));
        System.out.println(read(new char[4], 1));
    }
}

    
    /*
        three sum
        ----------
        - three sum: https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/283/
        - two sum
            - ref1: https://leetcode.com/problems/two-sum/solution/
            - ref2: https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
            - approaches: brute force, sorting, hash table
     */

/*
  Sorting an Array
  -----------------
  
  - implement all sorting algorithms for an array:
    1. bubble sort, insertion sort, selection sort
    2. merge sort
    3. quick sort and randomized quick sort
    4. heap sort
    5. non comparison sorts: CBR: counting sort, bucket sort and radix sort
  
  - name stable sort algorithms
  - method: bubble, insertion, merge, counting and radix
  
  - how to optimize bubble sort
  - method: check if no swapping was done
  
  - which sorting algorithm best suits find top k elements in an array
  - method: quick sort(followed by bubble sort), why: randomized quick sort partitioning gives result in expected linear time
            (using medians of medians, this can be made guaranteed linear time)
  
  - which sorting algorithm best suits find top k elements in an array, when k is a very small constant
  - method: bubble sort, why: k iterations of bubble sort with given solution in linear time
  
  - state worst, avg and best time complexities of various sorting algorithms
  - ref: https://www.geeksforgeeks.org/know-sorting-algorithm-set-1-sorting-weapons-used-programming-languages/
  - compare quicksort vs heapsort
  - method: quicksort is best for arrays and mergesort is better for linked lists, why:
              https://www.geeksforgeeks.org/why-quick-sort-preferred-for-arrays-and-merge-sort-for-linked-lists/
  
  - compare quicksort vs heapsort
  - ref: https://stackoverflow.com/questions/2467751/quicksort-vs-heapsort
  - method: heapsort is 0(n lg n) expected time and quicksort is 0(n^2) worst case, but
      - average running time of quicksort is 0(n lg n) with randomized version
      - better in terms of locality of memory/cache and hence works better for large arrays(i.e. if array exceeds virtual page size)
      - hidden constant factor is small as compared to heapsort due to less number of comparisons, why:
        because heapsort always does rearrangement and swaps irrespective of whether the array is sorted or not
      - that is the one used most frequently and is the one used in most libraries
    
  
  - quicksort phd(ignore)
  - ref: https://www.geeksforgeeks.org/quick-sort/
  - mergesort phd(ignore)
  - ref: https://www.geeksforgeeks.org/quicksort-better-mergesort/
  - quicksort and mergesort iterative versions/tail call optimizations(ignore)
  - ref: https://www.geeksforgeeks.org/quicksort-tail-call-optimization-reducing-worst-case-space-log-n/
         https://www.geeksforgeeks.org/iterative-quick-sort/
         https://www.geeksforgeeks.org/iterative-merge-sort/
         
  - pancake sort: sort an array using only flip/reverse opertion: flip(0, i)
  - ref: https://www.geeksforgeeks.org/pancake-sorting/
  - method: use iteration: in each iteration, find max and place it at right position using flip operations
  
 */

/*
  Merge Sorted Arrays
  ---------------------
  
  - merge k sorted arrays into one sorted array
  - ref: https://www.geeksforgeeks.org/merge-k-sorted-arrays/
  - method: use heap
  
 */

/*
  External Merge Sort
  --------------------
  
  - ref: https://www.geeksforgeeks.org/external-sorting/
         https://en.wikipedia.org/wiki/External_sorting
  
  
 */




/*
  - find element in array for which right and left subarrays have same sum
    - constraints: T(n) S(1)
  - ref: https://www.geeksforgeeks.org/find-element-array-sum-left-array-equal-sum-right-array/
  - method1: brute-force
    method2: prefix sum and suffix sum arrays
    method3: current prefix and suffix sum traversal
 */

/*
  - given an array containing numbers from 1 to n but one missing number, find the number
    - https://www.geeksforgeeks.org/find-the-missing-number/
  - method1: sum
    method2: XOR
   
  - given an array of numbers from 1 to n, with 2 missing numbers, find the number
    extend the solution to k missing numbers
    - https://www.geeksforgeeks.org/find-two-missing-numbers-set-2-xor-based-solution/
  - method1: XOR
    method2: sum and product
  - follow: extend to k missing numbers from an array of 1 to n...using XOR
  
  - given an array of size 2n+1 where n integers are repeated twice and only one integer occurs once. find that integer.
    given an array of size 2n+2 where n integers are repeated twice and two integers occurs only once. find both of them.
  - method for problem1: XOR
    method for problem2: XOR based division
  
  Billion numbers
  -----------------
  
  - given 4 billion numbers in a file from 1 to n, find the only missing number
  - ref: https://stackoverflow.com/questions/19398423/programming-pearls-finding-the-missing-integer-in-a-file-of-4-billion-integers
  - method: use XOR
  
  - given 4 billion numbers in a file from 1 to n, find first missing number with memory limit of 1 GB.
  - ref: https://www.careercup.com/question?id=14804692
  - method: use bitmap of size 512 MB(note: hashtable/array cannot be used as it required atleast 16 GB memory just to keep the numbers)
  
 */


/*
  - find first k largest element in an array
  - method1: sorting: TS(sorting)
    method2: min heap of largest k elements: T(n log k), S(k)
    method3: find kth order static: TS(order static algorithm)
  - ref:
    https://www.geeksforgeeks.org/kth-smallestlargest-element-unsorted-array/
    https://www.geeksforgeeks.org/kth-smallestlargest-element-unsorted-array-set-2-expected-linear-time/
    https://www.geeksforgeeks.org/kth-smallestlargest-element-unsorted-array-set-3-worst-case-linear-time/
    
  - follow: code for quick select and median of medians
*/


/*
  Single array pair
  ------------------
  
  - find pair in array whose sum is equal to a given number
    - ref: https://www.geeksforgeeks.org/write-a-c-program-that-given-a-set-a-of-n-numbers-and-another-number-x-determines-whether-or-not-there
    -exist-two-elements-in-s-whose-sum-is-exactly-x/
  - method1: sorting....T(sorting) > 0(n), S(sorting)
             (why this method works? successive sums from each index forms a row-column sorted matrix)
    method2: hashing....T(n) S(n)
  - follow: code
  
  - find pair in array with given diff
  - ref: https://www.geeksforgeeks.org/find-a-pair-with-the-given-difference/
  - method1: sorting....T(sort), S(sort)
             (why this method works? successive diff from each index forms a row-column sorted matrix)
    method2: hashing
  
  - find pair with product equal to a given number
  - method: this is same as pair with sum equal to a given number
  
  - find pair with given diff
  - ref: https://www.geeksforgeeks.org/find-minimum-difference-pair/
  - method: sort and consider only consecutive numbers
            (why this approach works? firstly it's pretty intuitive but in consecutive diff matrix, min diff lies in first element of each row)
  
  - find pair with sum equal to rest of the array
  - ref: https://www.geeksforgeeks.org/check-exist-two-elements-array-whose-sum-equal-sum-rest-array/
  - method: this is same as finding a+b=(sum of array)/2
  
  - find pair with closest sum to a given number
  - ref: https://www.geeksforgeeks.org/given-sorted-array-number-x-find-pair-array-whose-sum-closest-x/
  
  
  Single array triplets
  ----------------------
  - triplet such that sum of two is equal to third number
    - ref: https://www.geeksforgeeks.org/find-triplet-sum-two-equals-third-element/
  - method1: brute-force: T(n^3), S(1)
    method2: sorting: T(n^2), S(sorting)
  
  - triplet such that sum of three numbers is k
  - ref: https://www.geeksforgeeks.org/find-a-triplet-that-sum-to-a-given-value/
  - method: this is same as finding triplet usch that a+b=k-c
  
  - pythagorean triplet in an array
    - ref: https://www.geeksforgeeks.org/find-pythagorean-triplet-in-an-unsorted-array/
  - do square of all the numbers, then use algo to find triplet such that a+b=c
    sorting method is good here as run time will T(n^2) even with hashing
  
  Single array quadruple
  -----------------------
  - find four numbers in an array such that: a+b=c+d
  - ref: https://www.geeksforgeeks.org/find-four-elements-a-b-c-and-d-in-an-array-such-that-ab-cd/
  - method1: sorting
    method2: hashing
  
  Two array pair
  ---------------
  
  - pair with given diff
  - method: consecutive diff approach
  
  - find pair of elements to be swapped from two arrays, so that sum becomes equal
  - ref: https://www.geeksforgeeks.org/find-a-pair-swapping-which-makes-sum-of-two-arrays-same/
  - method: find difference in sum, then sort the arrays and find pair (a, b) such that (a - b = sum)
  
  - smallest diff pair
  - ref: https://www.geeksforgeeks.org/smallest-difference-pair-values-two-unsorted-arrays/
  - method: sort both arrays and iterate smartly for possible pairs
            (again use approach based on matrix of consecutive diff)
  
  - pair from array with closest sum
  - ref: https://www.geeksforgeeks.org/given-two-sorted-arrays-number-x-find-pair-whose-sum-closest-x/
  - method: sort both arrays and iterate smartly for possible pairs
            (why this approach works? consider cumulative array on one array for each number from other array)
  
  
  
  Triplets from three arrays
  ---------------------------
  
  - find triplets from given three arrays with sum equal to a given number
  - ref: https://stackoverflow.com/questions/3987264/interview-question-three-arrays-and-onn
  - method: extend the two array solution: iterate smartly for first two arrays for each element in third array
  
 */

/*
  - rotate array k times, constraints: T(n), S(1)
    - ref: https://www.geeksforgeeks.org/program-for-array-rotation-continued-reversal-algorithm/
  - method1: brute force: rotate array one at a time: T(kn), S(1)
    method2: copy over last k elements to a buffer of size k: T(n), S(k)
    method3: rotation using reversal: T(n), S(1)
  - follow: code for method3
 */

/*
  - binary search in a sorted array
  - follow: code both recursive and iterative versions
  
  - given a sorted array of integers, count occurrences of a number in it
  - ref: https://www.geeksforgeeks.org/count-number-of-occurrences-or-frequency-in-a-sorted-array/
  - method:
      - use binary search to find lower index
      - use binary search to find upper index
       (jumping binary search can be used to find upper index more efficiently, but code would get complicated)
  - follow: code
  
  - given a sorted array of integers(with duplicates), find first index of an element
  - modified binary search
  
  - search a sorted array for a number closest to a given number
  - ref: https://www.geeksforgeeks.org/find-closest-number-array/
  - method: modified binary search, check closest at each iteration or just at end of iteration
  
  Rotated array searches(note: these work only if array has no duplicates)
  -------------------------------------------------------------------------
  
  - search element in a sorted array that has been rotated
  - ref: https://www.geeksforgeeks.org/search-an-element-in-a-sorted-and-pivoted-array/
  - method1: binary search with slope check...but this will require two binary searches
    method2: binary search with PARTITION CHECKING
  - follow: code
  
  - a sorted array which has been rotated n number of times. find the value of n
  - ref: https://www.geeksforgeeks.org/find-rotation-count-rotated-sorted-array/
  - method: (binary search with SLOPE CHECKING) or (binary search with partition checking)
  - follow: code
  
  - given an array of integers which is initially increasing and then decreasing, find the maximum value in the array
  - ref: https://www.geeksforgeeks.org/find-the-maximum-element-in-an-array-which-is-first-increasing-and-then-decreasing/
  - method: this is not same as rotated array search(because this array can contain duplicates)
            (e.g.  1 2 3 4 5 8 6 3 2 1 is not a sorted array that has been rotated)
    - method1: use binary search with jumps....but why incur cost for out of index exceptions when we know the size
    - method2: modified binary search for rotated arrays to look for max
  - follow: code
  
  - find all pairs with given sum in a sorted but rotated array
  - ref: https://www.geeksforgeeks.org/given-a-sorted-and-rotated-array-find-if-there-is-a-pair-with-a-given-sum/
  - method: find pivot point or index of max element after rotation
            extend the approach for normal sorted arrays, but not this array is circular
  
  - find type of increasing-decreasing array
  - ref: https://www.geeksforgeeks.org/type-array-maximum-element/
         https://stackoverflow.com/questions/42105746/an-array-is-given-and-it-can-be-of-four-types
  - method: own sol: compare first with mid and mid with last, 4 cases will arise and that will determine the type of array
                     based on type, either return first or last element or do search for max in rotated array
  
  
  Infinite array searches
  ------------------------
  - find position of an element in sorted array of infinite numbers
  - ref: https://www.geeksforgeeks.org/find-position-element-sorted-array-infinite-numbers/
  - method: binary search with jumps + normal binary search
  - follow: code
  
  - index of first 1 in an infinite array of 0s and 1s which is sorted as well
  - ref: https://www.geeksforgeeks.org/find-index-first-1-infinite-sorted-array-0s-1s/
  - method: binary search with jumps + normal binary search
  
  - infinite sorted array of 0, 1 and 2, of unknown length, find transition points
  - method: binary search with jumps
  
  
  Unknown length sorted array
  ----------------------------
  
  - search for a number in a finite sorted array of unknown length
  - ref: http://www.ardendertat.com/2011/11/21/programming-interview-questions-17-search-unknown-length-array/
  - method: binary search with jumps
  - follow: code
  
  Ternary Search and Point Of Inflection
  ---------------------------------------
  - an int function which is monotonically increasing then decreasing, find point of inflection
  - ref: https://www.hackerearth.com/practice/notes/how-find-the-maximum-of-the-function-f-x-on-the-interval-l-r/
  - method:
    - if range of domain of function is given: ternary search
    - if range of domain of function is not given: binary search with jumps, then ternary search between current and prevToPrev

 */

/*
  Repeated elements in range 0 to n-1
  ------------------------------------
  - given an array of n integers ranging from 0 to N-1. find maximum repeating integer. constraints: T(n) S(1)
  - ref: https://www.geeksforgeeks.org/?p=7953
         https://www.geeksforgeeks.org/find-duplicates-in-on-time-and-constant-extra-space/
         https://www.geeksforgeeks.org/duplicates-array-using-o1-extra-space-set-2/
  - method 1 and 2: naive: brute force and counting
  - method 3 and 4: maths: XOR and (sum-product equations)
  - method 5 and 6: array as index: negation and multiplication
  - follow: code
  
  - find two numbers with odd occurrences
  - ref: https://www.geeksforgeeks.org/find-the-two-numbers-with-odd-occurences-in-an-unsorted-array/
  - method: XOR
  - follow: code
 */

/*
  Majority, Peak, Fixed
  ----------------------
  - find majority element in a array
  - ref: https://www.geeksforgeeks.org/majority-element/
  - method 1: find count of all elements using hashmap and check max count....T(n) S(hash)
    method 2: voting algorithm
  
  
  - find majority element in a sorted array
  - ref: https://www.geeksforgeeks.org/check-for-majority-element-in-a-sorted-array/
  - method: find lower index using binary search and check element at (low_index + n/2)
  
  
  - peak element in array
  - ref: https://www.geeksforgeeks.org/find-a-peak-in-a-given-array/
  - method: binary search, either the mid is peak or it has a peak in the direction in which neighbour is greater than it
    https://www.youtube.com/watch?v=a7D77DdhlFc&list=PLamzFoFxwoNjPfxzaWqs7cZGsPYy0x_gI&index=3&spfreload=10
  - follow: code
  
  - fixed element(value=index) in a sorted array
  - ref: https://www.geeksforgeeks.org/find-a-fixed-point-in-a-given-array/
  - method: specialized binary search
  
  - single in sorted array of pairs. constraint: cannot use XOR.
  - ref: https://www.geeksforgeeks.org/find-the-element-that-appears-once-in-a-sorted-array/
  - method: specialized binary search
 */

/*
  Dutch National Flag Problem: Sort array of 0s, 1s and 2s
  ---------------------------------------------------------
  - ref: http://users.monash.edu/~lloyd/tildeAlgDS/Sort/Flag/
  
  - sort an array of 0s and 1s, constraints: T(n) S(1) and SINGLE TRAVERSAL
  - ref: https://www.geeksforgeeks.org/segregate-0s-and-1s-in-an-array-by-traversing-array-once/
  - method1: counting sort
    method2: two index travel and swapping
    method3: shrink the unknown space
  - follow: code
  
  - sort an array of 0s, 1s and 2s
  - ref: https://www.geeksforgeeks.org/sort-an-array-of-0s-1s-and-2s/
  - method: shrink the unknown space
  - follow: code
  
 */

/*
  - Iterative array sum
    Given positive numbers a1,a2…an, you can sum up the elements in the following way: 1*a(i%n)+2*a((i+1)%n)…+n*a((i+n)%n)
    where i ranges from 0 to n-1. Find the value of i at which this sum would be the maximum.
    e.g. 5,6,7....the various sums can be 1*5+2*6+3*7, 1*6+2*7+3*5, and 1*7+2*5+3*6. The answer is 38.
    constraint: T(n) S(1)
  - method: find pattern to calculate successive sums
    We can solve this in O(n). We initially calculate the sum for i being 0. We make the following observation.
      i=0, sum0 = 1*a1+2*a2+3*a3…n*an.
      i=1, sum1 = n*a1+1*a2+2*a3…(n-1)an
    sum1 = sum0 – (a1+a2+…an) + n*a1
    Generalizing the equation: sumi  = sumi-1 – (a1+a2+…an) + n*ai , where i ranges from 1 to n-1.
    The maximum of the sum0 to sumn-1 is the result.
 */

/*
  - sort on array based on another
  - ref: https://www.geeksforgeeks.org/sort-array-according-order-defined-another-array/
  - method: use custom comparator for sorting, where comparator compares weight of each element(fetched from hashmap)
  - follow: code
 */


/*
  Set Operations
  ---------------
  
  - intersection, union and diff of two sets in array
  - ref: https://www.geeksforgeeks.org/find-union-and-intersection-of-two-unsorted-arrays/
  - method1: naive
    method2: sorting both
    (sorting one and search for other. used when array size differs a lot, but code is simple for sorting both)
    method3: hashing
  - follow: code
  
  - set operation implementation for two sorted arrays
  - ref: https://www.geeksforgeeks.org/union-and-intersection-of-two-sorted-arrays-2/
  
  
  - common elements from 3 sorted arrays
  
  - common elements from n sorted arrays

 */


/*
  Sliding Window
  ---------------
  - sliding window max
  - ref: https://www.geeksforgeeks.org/sliding-window-maximum-maximum-of-all-subarrays-of-size-k/
  - method: dequeue
  - follow: code
  
  
  - sliding window min-max sum
  - ref: : https://www.geeksforgeeks.org/sum-minimum-maximum-elements-subarrays-size-k/
  - method: similar approach except we maintain 2 dequeues
  
  
  - count of distinct elements in sliding window
  - ref: https://www.geeksforgeeks.org/count-distinct-elements-in-every-window-of-size-k/
  - method: use hashmap
 */

/*
  - min and 2nd min in least comparisons
  - ref: https://www.geeksforgeeks.org/second-minimum-element-using-minimum-comparisons/
  - method1: compare two elements at a time: 0(3n/2)
    method2: tournament tree (n-1 + lg n -1)
 */

/*
  - buildings facing sunset
  - ref: https://www.geeksforgeeks.org/number-buildings-facing-sun/
  - method1: iterate from start and use max-so-far approach
    method2: iterate from end and put elements on stack to maintain increasing sequence on stack
    (assumption: a tall building can hide any number of buildings shorted than it)
  
  - given an array of integers, replace each element with the next greater element
  - ref: https://www.geeksforgeeks.org/replace-every-element-with-the-greatest-on-right-side/
  - method: iterate from end and replace each element by max-so-far
  
  - given an array of integers, replace each element with the next IMMEDIATE greater element
    constraint: T(n), S(n)
  - ref: https://www.geeksforgeeks.org/next-greater-element/
  - method: iterate from end use stack to maintain increasing seq on stack
 */

/*
  
  Infinite numbers
  -----------------
  
  - infinite stream of numbers, find first non-repeating number in stream seen so far
  - ref: https://www.geeksforgeeks.org/find-first-non-repeating-character-stream-characters/
  - method: linked hashmap
  
  - infinite stream of numbers, design DS to find count of elements smaller than current element
  - ref: https://stackoverflow.com/questions/33046646/count-of-previously-smaller-elements-encountered-in-an-input-stream-of-integers
  - method: order static tree
  
  - infinite stream of numbers, design DS to add an int n to all elements seen so far in 0(1)
    any future add should take care of the added value if needed
  - ref: http://codeforces.com/blog/entry/54141
  - method: augmented linked list with a variable called delta
 */

/*
  
  Minimum Hike Of Employees
  --------------------------
  
  - There are n employees in a company, each having some ratings(>0). The employees are given a hike(>0 and minimum 1) in their salary based on
    their ratings, i.e employee with higher rating will get the higher raise. An employee can only know the hike and
    rating of two of his neighbors, one on the left and other on the right. Given an array of size n, specifying the
    ratings of n employees, find the minimum hike that should be raised for each employee, such that no employee feels unfair.
    Example: 1 3 5 4        Output: 1+2+3+2 = 7
    Example: 5 3 4 2 1 6    Output: 2+1+3+2+1+2 = 11.
  - method: own sol: subarrays which are monotonically increasing and decreasing have one solution only
                     so divide the array into series of increasing-decreasing sequences and assign minimum hike possible
                     largest, then next largest and so on
                     example: 12 8 7 1 2 5 4 6 9 11.....[12 8 7 1] [2 5] [4 6 9 11]....[4 3 2 1] [2 3] [1 2 3 4]
 
 */

/*
  
  Minimum Number Of Platforms / Maximum Overlapping Interval Count
  -----------------------------------------------------------------
  
  - minimum number of platforms
  - ref: https://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station/
  - method1: own sol: created a sorted list of all trains by their arrival time
                      now for each arrival, insert the train in a list and remove all trains that must have departed by that time
                      (this means we need to keep the list sorted by departure time). keep track of max list size at any time.
    method2: optimized: instead of keeping a list we can just keep the counter
                        create a sorted list of arrival and departure events by timestamp
                        for each arrival event, increment the counter and for each departure, decrement the event. keep track of max at any time
    
  - merge overlapping intervals
  - ref: https://www.geeksforgeeks.org/merging-intervals/
         https://gist.github.com/zac-xin/4349436
         https://www.interviewbit.com/problems/merge-overlapping-intervals/
  - method: sort by start time and merge in linear time
  
  - is complete youtube video watched or not?
    given a customer watching a youtube video in random order, you have given the start and end time of
    various intervals he watched. check if the customer watched the complete video or not
  - method: own sol: keep on merging intervals, check if it converts into single interval covering entire video length
  
 */

/*
  
  French Playing Cards
  ---------------------
  
  - given a deck of 52 french playing cards with one missing card, find the missing card with suit and number
  - method: keep sum of cards per deck. in the end, for each deck, find: sum(1 to 13) - (sum for the deck)
  
 */

/*
  
  Maximum Continuous Range
  -------------------------
  
  - given and array of integers, find maximum range such that all elements of the range are in the array, constraint T(n) S(n)
  - ref: https://www.geeksforgeeks.org/longest-consecutive-subsequence/
  - method1: sorting, T(n lg n)
    method2: put elements in hashset and for each element x, check for element in x-n and x+n range, T(n) S(n)
  
 */

/*
  Sum divisible by K
  -------------------
  
  - check if an array(with -ve and +ve numbers) can be divided into pairs each having sum divisible by k, constraint: T(linear)
  - ref: https://www.geeksforgeeks.org/check-if-an-array-can-be-divided-into-pairs-whose-sum-is-divisible-by-k/
  - method: create hash of remainders of elements in the array
  
  - count of all subarrays(with -ve and +ve numbers) having sum divisible by k, constraint: T(linear)
  - ref: https://www.geeksforgeeks.org/count-sub-arrays-sum-divisible-k/
  - method1: create cumulative array and try out all possible sub-arrays. T(n^2)
    method2: create cumulative array(reduced to remainders) and use hashmap to find out matching pairs of remainders
  
 */

/*
  
  Search an Array of Consecutive Elements
  ----------------------------------------
  
  - given an array where consecutive elements differ by +1 or -1, search an element in it
  - ref: https://www.geeksforgeeks.org/efficient-search-in-an-array-where-difference-between-adjacent-is-1/
  - method: linear search with jumps of abs(arr[i] - searchElement)
  
  - given an array where consecutive elements differ by +M or -M, search an element in it
  - method: generalization of above problem but approach is same
            one optimization can be done: diff at every iteration should be divisible by M else element cannot be there in array
  
  - given an array where consecutive elements differ by atmost +M or -M i.e. a[i]-M <= a[i+1] <= a[i]+M, search an element in it
  - method: this is even further generalization, but approach remains same
            search for element at (i + ceil(abs(a[i]-x)/M))
    
 */

/*
 
  Matching nuts and bolts
  -------------------------
  
  - given n nuts and bolts(keys and locks) in two arrays in random order, match them with each other
  - ref: https://www.geeksforgeeks.org/nuts-bolts-problem-lock-key-problem/
  - method: use quicksort technique
  
 */

/*
  
  Find Longest Subsequence Range(Super Cool Question)
  ----------------------------------------------------
  
  - find longest subsequence in an array, representing a contiguous range, constraint: T(n), S(n)
  - ref: https://stackoverflow.com/questions/5415305/finding-contiguous-ranges-in-arrays
  - method1: sorting, but this will either be non linear in time or space
    method2: connected components of a graph, but this will require S(n^2) for representing the graph
    method3: hashset: for each element i, check for i-n and i+n in hashset.....
             this algorithm in T(n). why, because in all iterations we either break the loop or process some element from hashset
  
  - check if an array(with duplicates and negative numbers) is a set of contiguous integers or not, constraint: T(n), S(n)
  - ref:
      https://www.geeksforgeeks.org/check-if-array-elements-are-consecutive/
      https://www.geeksforgeeks.org/check-array-contains-contiguous-integers-duplicates-allowed/
  - method1: sorting, T(n lg n)
    method2: hashtable/ visited array, S(n)....hashtable of [min, min+n] and max=min+1
             (how to extend this method if there are -ve numbers or duplicates)
             (method: shift array to +ve range. use visited array approach, but make number -ve when it is visited, S(1))
    method3: hashset, S(n)
    method4: AP sum(assumption: no duplicates)
  
 */

/*
  
  Swapping Array Elements
  ------------------------
  
  - find minimum number of swaps to sort an array of integers from 1 to n
  - method: find number of distinct cycles of elements that have swap dependency. output = sum((length-1) of all cycles)
  
  - find minimum number of swaps to sort an array of integers(integers are random and not in 1 to n)
  - ref: https://www.geeksforgeeks.org/minimum-number-swaps-required-sort-array/
  - method: replace each element by it's index in sorted array. now find min swaps to sort this array
  
  - find minimum number of swaps to make two arrays identical
  - ref: https://www.geeksforgeeks.org/minimum-swaps-to-make-two-array-identical/
  - method: replace each element in second array by it's index in first array and find swaps to sort this array
  
  - code to rearrange array elements as per given indexes
  - ref: https://www.geeksforgeeks.org/reorder-a-array-according-to-given-indexes/
  - method: use cyclic linear time replacement algorithm
  
  - given an array of elements from 0 to n-1, rearrange elements so that a[i] is replaced by a[a[i]]
  - ref: https://www.geeksforgeeks.org/rearrange-given-array-place/
  - method: for each element, a[i] = a[i] + (a[a[i]]%n)*n
  
  - min swaps for arranging pairs
  - ref: https://www.geeksforgeeks.org/minimum-number-of-swaps-required-for-arranging-pairs-adjacent-to-each-other/
  - method:
  
  
  Adjacent Swaps/ Inversions in an Array
  ---------------------------------------
  
  - find minimum number of adjacent swaps to sort an array
  - ref: https://www.geeksforgeeks.org/number-swaps-sort-adjacent-swapping-allowed/
  - method: this is same as counting number of inversions in an array
  
  - count number of inversions in an array
  - ref: https://www.geeksforgeeks.org/counting-inversions/
  - method1: brute force
    method2: merge sort
  
 */

/*
  Minima-Maxima Array
  --------------------
  
  - convert array to zig-zag array pattern i.e. alternate minima and maxima pattern, constraint: T(n), S(1)
  - ref: https://www.geeksforgeeks.org/convert-array-into-zig-zag-fashion/
  - method1: brute force, but this will be T(n^2)
    method2: partition around median and alternate merge(or cyclic rearrange), but this will be S(n)
    method3: sort and create alternate minima and maxima, but this will be T(n lg n)
    method3: minima-maxima iterative approach: fixing next minima/maxima without affecting previous one
  
  Local Minima/Maxima of an Array
  --------------------------------
  
  - find any local minima in an array, constraint: T(lg n)
  - ref: https://www.geeksforgeeks.org/find-local-minima-array/
  - method: special binary search
  
 */

/*
  
  Min Steps Path to Cover all Cells in Matrix
  --------------------------------------------
  
  - count min steps to cover a given set of cells in a given order
  - ref: https://www.interviewbit.com/problems/min-steps-in-infinite-grid/
  - method: own sol: problem can be seen as finding min steps to go from a given cell to next cell
                      = max(horizontal distance, vertical distance)
  
 */

/*
  Array Maximization Problems: No DyPro
  --------------------------------------
  
  Largest Subarray With 0s and 1s
  --------------------------------
  
  - largest subarray with 0s and 1s
  - https://www.geeksforgeeks.org/largest-subarray-with-equal-number-of-0s-and-1s/
  - method: convert all 0s to -1, find cumulative sum array. find pairs with equal sum and at max distance or index with 0 in cumulative sum array
  
  
  
 */

/*
  Split Array into Two Equal Parts
  ---------------------------------
  
  - split matrix into two equal parts of cherries
  - ref: https://www.hackerearth.com/practice/basic-programming/implementation/basics-of-implementation/practice-problems/algorithm/bear-and-chocolate/
  - method: own solution: calculate sums of each row into rowSumArray and try to split it into two equal parts. do the same for columns.
  
  - split a given array into two subarrays with equal sum
  - method1: use cumulative arrays from left and right
    method2: find complete sum and use left cumulative sum at each iteration
    
 */

/*
  Triangle Count in an Array
  ---------------------------
  
  - count number of possible triangles from array elements, constraint: T(n^2)
  - ref: https://www.geeksforgeeks.org/find-number-of-triangles-possible/
  - method1: brute force, T(n^3)
    method2: sort and do binary search for each pair, T(n^2 * lg n)
    method3: sort with incremental linear search, T(n^2)
    
 */

/*
  Min Distance Between Two Numbers in Array
  -------------------------------------------
  
  - given an array of numbers, find min distance between the two numbers in array, constraint: T(n)
  - ref: https://www.geeksforgeeks.org/find-the-minimum-distance-between-two-numbers/
  - method: smart scan from left to right
  
 */

/*
  Unique/Duplicate Rows in Matrix
  ---------------------------------
  
  - print all unique rows of a matrix of 0s and 1s
  - ref: https://www.geeksforgeeks.org/print-unique-rows/
  - method1: brute force
    method2: treat each row as numbers. sort(radix sort) or put in hashset
    method3: use trie
  
  - print all duplicate rows in a matrix of 0s and 1s
  - method: this is similar to finding unique rows in a matrix
  
 */

/*
  Common Elements in Matrix/Arrays
  ---------------------------------
  
  - find elements which are common in all the rows of a matrix, given that all rows are sorted, constraint: T(mn)
  - ref: https://www.geeksforgeeks.org/find-common-element-rows-row-wise-sorted-matrix/
  - method: simultaneous iteration through all rows
            maintain an array current_row_index. at each iteration either a common element is found
            or all indexes are incremented except for the row with max element
  
  - find elements which are common in all the rows of a matrix, constraint: T(mn)
  - ref: https://www.geeksforgeeks.org/common-elements-in-all-rows-of-a-given-matrix/
  - method1: brute force, T(m * n^2)
    method2: sort all rows, then binary search for each element in first row in all other rows, T(m*n*lg n) + T(m*n*lg n)
    method3: sort all rows, then use simultaneous iteration through all rows, T(m*n) + T(m*n*lg n)....but S(lg n+m)
    method4: use hashset, T(mn), S(n)
  
  - common elements in n sorted arrays
  - method: this is same as common elements in rows of a matrix with sorted rows
  
  - common elements in 3 sorted arrays
  - ref: https://www.geeksforgeeks.org/find-common-elements-three-sorted-arrays/
  - method: this is same as n sorted arrays but coding can be made simple
  
 */

/*
  Problems with Special Structure
  ---------------------------------
  
  - problems with special structure
    - reduce problem space:
    - combine operations for all e.g. find sorted sequence of 3
  
  - find a subsequence of size 3 in an array, such that arr[i] < arr[j] < arr[k], constraint: T(n)
  - ref: https://www.geeksforgeeks.org/find-a-sorted-subsequence-of-size-3-in-linear-time/
  - method: find next greater and previous lesser element for every element in array

  - find a subsequence of size 3 in an array, such that arr[i] < arr[j] > arr[k], constraint: T(n)
  - method: find next lesser and previous lesser elements
  
  - find a sorted subsequence of size 4 in an array, constraint: T(n)
  - ref: https://stackoverflow.com/questions/17654673/find-a-sorted-subsequence-of-size-4-in-an-array-in-linear-time
  - method: own sol: For each element, find next greater element index else -1 Now think of this as a graph
                     and find a path of length k(if it exist) This can be done easily in linear time using hashtable and memoization.
  
  
  
 */


/*
  Easy
  -----
  
  - implement circular linked list in array
  - method: keep first and last index...logic to increment/decrement them, taking care of rotation
  
  - equilibrium point in an array(sum or multiplication), constraint: T(n), S(1)
  - ref: https://www.geeksforgeeks.org/equilibrium-index-of-an-array/
  - method1: calculate cumulative array of left sum and right sum, for each index i, check if left and right cumulative sums are equal or not
    method2: we don't need to cumulative arrays as they can be calculated on the fly during each iteration
  
  - cumulative sum(product): given an array, create cumulative sum array from left to right, constraint: T(single traversal), S(1)
  - method: just traverse from left to right, keeping track of cumulative sum
  
  - cumulative all except current: given an array, create an array of sums(product), such that ith element is sum of all except a[i]
  - method1: find sum of array, now iterate from left to right, doing: a[i] = sum - a[i]
  
  - cumulative all except current: given an array, create an array of sums(product), such that ith element is sum of all except a[i]
    constraint: cannot use subtraction(division)
  - method: create cumulative left sum array and cumulative right sum array, except current element
            for each element in array: a[i] = cumulative left + cumulative right
  
  - find partition elements in an array, constraint: T(single traversal)
  - ref: https://www.geeksforgeeks.org/find-the-element-before-which-all-the-elements-are-smaller-than-it-and-after-which-all-are-greater-than-it/
  - method1: brute force
    method2: naive approach: create leftMaxSoFar and rightMinSoFar arrays
    method3: maxSoFar variable and all possible output stack
  
  - given a string of 0s and 1s, count the number of substrings starting and ending at 1
  - ref: https://www.geeksforgeeks.org/given-binary-string-count-number-substrings-start-end-1/
  - method: if k is the number of 1s in the string, then return (nC2 + n) (assuming each 1 itself counts as a substring) else it's just nC2
  
  - min iterations to remove all 0s from array based on neighbour based flipping
  - ref: https://www.geeksforgeeks.org/fill-array-1s-minimum-iterations-filling-neighbors/
  - method: find blocks of zeros, consider various cases and keep track of max iterations needed
  
  
  - find min sum of squares for a string after removing k chars
  - ref: https://www.geeksforgeeks.org/minimum-sum-squares-characters-counts-given-string-removing-k-characters/
  - method1: use hashtable to count frequency of each char and then sor chars based on frequency
    method2: hashtable + heap
    method3: hashtable + counting sort

  - given an online game with millions of players online at any time, design data structure to
    find closest match for a given player, given each player is assigned a rank.
  - method: own sol: to find closest matchm we can keep a sorted list and do and binary search.
                     since we will also need to do insert and delete frequently, we can keep a sorted linked list.
                     but that binary search in linked list is not efficient, so best solution is to keep a bst
  
  - given two unsorted arrays, for each element in A count elements <= to it in B
  - ref: https://www.geeksforgeeks.org/element-1st-array-count-elements-less-equal-2nd-array/
  - method1: naive approach: sort 1st array and do binary search for each element in 2nd array: T(m * (lg m + lg n))
                             (if the two arrays differ a lot in size, choose the small array)
             (question: why not use bst here: because bst will take more memory and has lower locality of reference than array)
    method2: we don't need to do binary search again and again, we can sort both arrays and just iterate
             through both arrays once: T(n lg n + m lg m).....this works well if both arrays are of same size
  
  - count number of elements in given range, constraint: this range query will be made billions of time >> size of array
  - ref: https://www.geeksforgeeks.org/queries-counts-array-elements-values-given-range/
  - method: sort the array and do binary search to find the count of elements in given range
            since query will be made billions of time(>> size of array), overall cost will be:
             T(n lg n + B lg n) = T(B lg n), where B = number of times range query is made
  
  - print string by placing 0 or 1 space between chars
  - ref: https://www.geeksforgeeks.org/print-possible-strings-can-made-placing-spaces/
  - method: use top-down recursion
  
  - parse chemical formula: for chemical formula C6H2(Cl3(OH2)3)3, print: C-6 H-20 O-9 Cl-9(i.e. number of atoms of each element)
  - ref: https://repl.it/@robturtle/iterative-parsing
  - method: use iterative paring and stack with hashmap
  
 */
