package com.practice.ds.array;

import java.util.ArrayList;
import java.util.List;

public class Matrix {
  
  public static void printDiagonally(int matrix[][], int rows, int cols) {
    int diagonalCount = rows + cols - 1;
    
    for (int diagonalNumber=1; diagonalNumber <= diagonalCount; diagonalNumber++) {
      int rowNum = Math.min(diagonalNumber-1, rows-1);
      int colNum = Math.max(0, diagonalNumber - rows);
      while(rowNum>=0 && colNum < cols) {
        System.out.print(matrix[rowNum][colNum] + " ");
        rowNum--;
        colNum++;
      }
      System.out.println();
    }
    
  }
  public static void printZigZagDiagonally(int matrix[][], int rows, int cols) {
    int diagonalCount = rows + cols - 1;
    
    for (int diagonalNumber=1; diagonalNumber <= diagonalCount; diagonalNumber++) {
      if (diagonalNumber%2 != 0) {
        int rowNum = Math.min(diagonalNumber-1, rows-1);
        int colNum = Math.max(0, diagonalNumber - rows);
        while(rowNum>=0 && colNum < cols) {
          System.out.print(matrix[rowNum][colNum] + " ");
          rowNum--;
          colNum++;
        }
      } else {
        int rowNum = Math.max(0, diagonalNumber - cols);
        int colNum = Math.min(diagonalNumber-1, cols-1);
        while(rowNum<rows && colNum >= 0) {
          System.out.print(matrix[rowNum][colNum] + " ");
          rowNum++;
          colNum--;
        }
      }
      System.out.println();
    }
    
  }
  
  public static void printZigZagRowWise(int matrix[][], int rows, int cols) {
    for (int row=0; row<rows; row++) {
      if (row%2==0) {
        for(int col=0; col<cols; col++) {
          System.out.print(matrix[row][col] + " ");
        }
      } else {
        for(int col=cols-1; col>=0; col--) {
          System.out.print(matrix[row][col] + " ");
        }
      }
      System.out.println();
    }
  }
  
  public static void printSpiralHelper(int matrix[][], int rowStart, int rowEnd, int colStart, int colEnd) {
    if (rowStart > rowEnd || colStart > colEnd) {
      return;
    }
    
    for(int col=colStart; col<=colEnd; col++) {
      System.out.print(matrix[rowStart][col] + " ");
    }
    for (int row=rowStart+1; row<rowEnd; row++) {
      System.out.print(matrix[row][colEnd] + " ");
    }
    if (rowEnd != rowStart) {
      for(int col=colEnd; col>=colStart; col--) {
        System.out.print(matrix[rowEnd][col] + " ");
      }
    }
    if (colEnd != colStart) {
      for (int row = rowEnd - 1; row > rowStart; row--) {
        System.out.print(matrix[row][colStart] + " ");
      }
    }
    printSpiralHelper(matrix, rowStart+1, rowEnd-1, colStart+1, colEnd-1);
  }
  public static void printSpiral(int matrix[][], int rows, int cols) {
    printSpiralHelper(matrix, 0, rows-1, 0, cols-1);
  }
  
  public static void printSpiralIterative(int matrix[][], int rows, int cols) {
    int rowStart=0, rowEnd=rows-1, colStart=0, colEnd=cols-1;
    while(rowStart <= rowEnd && colStart <= colEnd) {
      for(int col=colStart; col<=colEnd; col++) {
        System.out.print(matrix[rowStart][col] + " ");
      }
      for (int row=rowStart+1; row<=rowEnd; row++) {
        System.out.print(matrix[row][colEnd] + " ");
      }
      if (rowEnd != rowStart) {
        for (int col = colEnd - 1; col >= colStart; col--) {
          System.out.print(matrix[rowEnd][col] + " ");
        }
      }
      if (colEnd != colStart) {
        for (int row = rowEnd - 1; row > rowStart; row--) {
          System.out.print(matrix[row][colStart] + " ");
        }
      }
      rowStart=rowStart+1;
      rowEnd=rowEnd-1;
      colStart=colStart+1;
      colEnd=colEnd-1;
    }
  }
  
  public static void printAntiSpiralHelper(int matrix[][], int rowStart, int rowEnd, int colStart, int colEnd) {
    if (rowStart > rowEnd || colStart > colEnd) {
      return;
    }
    
    printAntiSpiralHelper(matrix, rowStart+1, rowEnd-1, colStart+1, colEnd-1);
    
    for (int row=rowStart+1; row<rowEnd; row++) {
      System.out.print(matrix[row][colStart] + " ");
    }
    for(int col=colStart; col<colEnd; col++) {
      System.out.print(matrix[rowEnd][col] + " ");
    }
    if (rowEnd != rowStart) {
      for (int row = rowEnd; row > rowStart; row--) {
        System.out.print(matrix[row][colEnd] + " ");
      }
    }
    if (colEnd != colStart) {
      for (int col = colEnd; col >= colStart; col--) {
        System.out.print(matrix[rowStart][col] + " ");
      }
    }
  }
  public static void printAntiSpiral(int matrix[][], int rows, int cols) {
    printAntiSpiralHelper(matrix, 0, rows-1, 0, cols-1);
  }
  
  public static void printAntiSpiralIterative(int matrix[][], int rows, int cols) {
    // todo: finish this code
    int spiralCount = Math.min(rows/2, cols/2);
    int rowStart=spiralCount-1, rowEnd=rows-spiralCount, colStart=spiralCount-1, colEnd=cols-spiralCount;
    
    while(rowStart>=0 && rowEnd<rows && colStart>=0 && colEnd<cols) {
      for (int row=rowStart+1; row<rowEnd; row++) {
        System.out.print(matrix[row][colStart] + " ");
      }
      for(int col=colStart; col<colEnd; col++) {
        System.out.print(matrix[rowEnd][col] + " ");
      }
      for (int row=rowEnd; row>rowStart; row--) {
        System.out.print(matrix[row][colEnd] + " ");
      }
      for(int col=colEnd; col>=colStart; col--) {
        System.out.print(matrix[rowStart][col] + " ");
      }
      rowStart=rowStart-1; rowEnd=rowEnd+1;
      colStart=colStart-1; colEnd=colEnd+1;
    }
  }
  
  public static int matrix1[][] = {
      {1, 2, 3, 4},
      {5, 6, 7, 8},
      {9, 10, 11, 12},
      {13, 14, 15, 16},
      {17, 18, 19, 20},
  };
  
  public static int matrix2[][] = {
      {1, 2, 3, 4}
  };
  public static int matrix3[][] = {
      {1}
  };
  public static int matrix4[][] = {
  
  };

    public List<Integer> spiralOrder(final List<List<Integer>> A) {
        int rowStart = 0, rowEnd = A.size()-1, colStart = 0, colEnd = A.get(0).size()-1;
        ArrayList<Integer> output = new ArrayList<Integer>();
        while(rowStart <= rowEnd && colStart <= colEnd) {
            for (int i=colStart; i<=colEnd; i++) {
                output.add(A.get(rowStart).get(i));
            }
            for (int i=rowStart+1; i<=rowEnd-1; i++) {
                output.add(A.get(i).get(colEnd));
            }
            if (rowStart < rowEnd) {
                for (int i=colEnd; i>=colStart; i--) {
                    output.add(A.get(rowEnd).get(i));
                }
            }
            if (colStart < colEnd) {
                for (int i=rowEnd-1; i>=rowStart+1; i--) {
                    output.add(A.get(i).get(colStart));
                }
            }
            rowStart=rowStart+1; rowEnd=rowEnd-1; colStart=colStart+1; colEnd=colEnd-1;
        }
        return output;
    }

    public static void main(String[] args) {
    printSpiral(matrix4, 0, 0);
    System.out.println();
    printSpiral(matrix3, 1, 1);
    System.out.println();
    printSpiral(matrix2, 1, 4);
    System.out.println();
    printSpiral(matrix1, 5, 4);
    System.out.println();
    
    printSpiralIterative(matrix4, 0, 0);
    System.out.println();
    printSpiralIterative(matrix3, 1, 1);
    System.out.println();
    printSpiralIterative(matrix2, 1, 4);
    System.out.println();
    printSpiralIterative(matrix1, 5, 4);
    System.out.println();
    
    printAntiSpiral(matrix4, 0, 0);
    System.out.println();
    printAntiSpiral(matrix3, 1, 1);
    System.out.println();
    printAntiSpiral(matrix2, 1, 4);
    System.out.println();
    printAntiSpiral(matrix1, 5, 4);
    System.out.println();
    
    //    printSpiralIterative(matrix, rows, cols);
    //    System.out.println();
    //
    //    System.out.println();
    //
    //    printAntiSpiral(matrix, rows, cols);
    //    System.out.println();
    //    printAntiSpiralIterative(matrix, rows, cols);
  }
  
}

/*
  Matrix Traversals
  ------------------
  - diagonal: printDiagonally: https://www.geeksforgeeks.org/zigzag-or-diagonal-traversal-of-matrix/
  - zigzag diagonally: printDiagonallyZigZag: https://www.geeksforgeeks.org/print-matrix-diagonal-pattern/
  - zigzag row wise: https://www.geeksforgeeks.org/print-given-matrix-zigzag-form/
  - spiral: https://www.geeksforgeeks.org/print-a-given-matrix-in-spiral-form/
    (recursive and iterative)
  - antispiral: https://www.geeksforgeeks.org/print-matrix-antispiral-form/
*/

/*
  Sorted Matrix(row and column wise)
  ------------------------------------
  - given a matrix which is sorted row and column wise, search a number in it
  - ref: https://www.geeksforgeeks.org/search-in-row-wise-and-column-wise-sorted-matrix/
  - method: use special procedure for sorted matrix search
  
  - given a matrix which is sorted row and column wise, search a number in it using divide and conquer
  - ref: https://www.geeksforgeeks.org/divide-conquer-set-6-search-row-wise-column-wise-sorted-2d-array/
  - note: this approach is not efficient than linear approach
  
  - a row and column wise sorted matrix, where all element in a row are greater than elements in previous row,
    search a number in it in log time
  - ref: https://www.geeksforgeeks.org/search-element-sorted-matrix/
  - method: approach: this is basically a sorted array chopped into matrix
            own sol: do binary search in array but map indexes to matrix cells
            complexity: T(log mn) = T(log m) + T(log n)
  - follow: code
  
  - find kth smallest in a row and column wise sorted matrix
  - ref: https://www.geeksforgeeks.org/kth-smallest-element-in-a-row-wise-and-column-wise-sorted-2d-array-set-1/
  - method: use min heap
  - follow: code
  
  - given a matrix which is sorted row and column wise, print it as single sorted array
    what if row count >> column count
  - method: use min heap, T(n^2 log n)
    since time complexity is dependent on n, in case n >> m, we should transpose the matrix and then traverse
  
  - count negative numbers in a matrix, which is sorted row and column wise
  - ref: https://www.geeksforgeeks.org/count-negative-numbers-in-a-column-wise-row-wise-sorted-matrix/
  - method: follow algo similar to searching an element

  
  
  
  Sorted Matrix(row wise)
  -------------------------

  - a matrix of 0s and 1s, sorted row-wise only(not column wise), find row with max 1s
  - ref: https://www.geeksforgeeks.org/find-row-number-binary-matrix-maximum-number-1s/
  - method: similar to searching in row and
  
  
 */

/*
  - find path between two cells in a matrix
  - ref: https://www.geeksforgeeks.org/find-whether-path-two-cells-matrix/
  - method1: use DFS(simple recursion)
    methods: use BFS(queue based code)
 */

/*
  - kth element in spiral traversal of a matrix
  - ref: https://www.geeksforgeeks.org/print-kth-element-spiral-form-matrix/
  - method: use maths and recursion
  - follow: code
 */

/*
  
  - print unique rows in a matrix of 0s and 1s
  - ref: https://www.geeksforgeeks.org/print-unique-rows/
  - method: basic approach is to partition rows based on 0 or 1 at index i and do it recursively
            this approach basically leads to trie data structure
 */

/*
  - given n points in 2D plane, find k nearest neighbour of a given point from n points. constraint: T(n)
  - ref: https://stackoverflow.com/questions/20398799/find-k-nearest-points-to-point-p-in-2-dimensional-plane
  - method1: max heap. time: T(n lo k)
    method2: find distance of all points from given point and then find kth order static of the array of distances. time: T(n)
 */

/*
  Islands of 1s
  --------------
  
  - count islands of 1s in a matrix of 0s and 1s
  - ref: https://www.geeksforgeeks.org/find-number-of-islands/
  - method: DFS/BFS of the graph represented by matrix
  
  - find largest size island of 1s in a matrix of 0s and 1s
  - ref: https://www.geeksforgeeks.org/find-length-largest-region-boolean-matrix/
  - method: DFS
  
 */

/*
  Largest Sub-Square
  -------------------
  - in a rectangular matrix of 0s and 1s, find max size square sub matrix of 1s
  - ref: https://www.geeksforgeeks.org/maximum-size-sub-matrix-with-all-1s-in-a-binary-matrix/
  - method: dynamic programming: for each cell, find largest square sub matrix ending/starting at it
  - follow: code
  
 */

/*
  
  Paths in matrix
  ----------------
  - count all possible paths in a matrix from top left to bottom right. allowed movements are right and down(and diagonal)
    (also known as monotonic paths in matrix)
  - ref: https://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/
  - method1: recursion(dynamic programming with memoization)
  - method2: use mathematics: ((m-1) + (n-1)) C (n-1)
             why: because in mxn matrix, we get (m-1) horizontal and (n-1) vertical steps,
                  so we need to choose (n-1) horizontal steps out of (m-1 + n-1) steps
  - follow: code
  
  - print all possible paths in a matrix from top left to bottom right. allowed movements are right and down(and diagonal)
  - ref: https://www.geeksforgeeks.org/print-all-possible-paths-from-top-left-to-bottom-right-of-a-mxn-matrix/
  - method: recursion with backtracking
  - follow: code
  
  - count all possible paths in a matrix with obstacels from top left to bottom right. allowed movements are right and down(and diagonal)
  - ref: https://www.geeksforgeeks.org/unique-paths-in-a-grid-with-obstacles/
  - method: recursion(dynamic programming with memoization)
  - follow: code

  - boggle game: find all possible paths(no cell reused) in matrix which form valid words from a given dictionary
  - ref: https://www.geeksforgeeks.org/boggle-find-possible-words-board-characters/
         https://www.geeksforgeeks.org/boggle-set-2-using-trie/
  - method: use recursion(dfs) with visited matrix. at every step if path so far forms a valid word in dictionary.
            for efficiency, implement dictionary using trie.
  
 */

/*
  DFS in Matrix
  --------------
  
  - search for a word in matrix, given that word should be found in given direction only(not zig zag)
  - ref: https://www.geeksforgeeks.org/search-a-word-in-a-2d-grid-of-characters/
  - method: use recursion(dfs)
  
  - search for a word in a matrix, given that search can go in any direction and cycles are also allowed
  - ref: https://www.geeksforgeeks.org/find-all-occurrences-of-the-word-in-a-matrix/
  - method: use recursion(dfs)
  
 */

/*
  
  - min time to rot all oranges in a matrix
  - ref: https://www.geeksforgeeks.org/minimum-time-required-so-that-all-oranges-become-rotten/
  - method: BFS
  - follow: code
  
 */

/*
  Sum Area Table
  ---------------
  - find an efficient way to find sum of any aXb sub matrix in an mXn matrix
  - ref: https://stackoverflow.com/questions/2277749/calculate-the-sum-of-elements-in-a-matrix-efficiently
  - method: use dypro to create sum area table
  - follow: code
  
 */

/*

 */

/*

 */

/*

 */

/*

 */

/*

 */

/*

 */
