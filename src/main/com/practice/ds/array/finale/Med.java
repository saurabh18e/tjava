package com.practice.ds.array.finale;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Med {
}

class ThreeSum {
    
    // three sum: https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/776/
    // two sum: https://leetcode.com/problems/two-sum/solution/#approach-3-one-pass-hash-table
    private static List<List<Integer>> threeSum(int[] nums, int sum) {
        if (nums == null || nums.length < 3) return Collections.emptyList();
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i <= nums.length - 3; i++) {
            twoSum(nums, sum, i, result);
            while (i <= nums.length - 3 && nums[i + 1] == nums[i]) i++;
        }
        return result;
    }
    
    private static void twoSum(int[] nums, int sum, int i, List<List<Integer>> result) {
        int j = i + 1, k = nums.length - 1;
        sum = sum - nums[i];
        while (j < k) {
            if (nums[j] + nums[k] == sum) {
                result.add(Arrays.asList(nums[i], nums[j], nums[k]));
                while (j < k && nums[j] == nums[j+1]) j++;
                j++;
            } else if (nums[j] + nums[k] > sum) {
                while (j < k && nums[k] == nums[k-1]) k--;
                k--;
            } else {
                while (j < k && nums[j] == nums[j+1]) j++;
                j++;
            }
        }
    }
    
    private static void printThreeSum(int[] arr, int sum) {
        // twoSum(arr, 0, 0, new ArrayList<>());
        
        List<List<Integer>> partitions = threeSum(arr, sum);
        System.out.println("valid partitions of " + Arrays.toString(arr) + " are:");
        for (List<Integer> partition : partitions) {
            System.out.println(Arrays.toString(partition.toArray()));
        }
    }
    
    public static void main(String[] args) {
        printThreeSum(new int[]{-1, 0, 1, 2, -1, -4}, 0);
    }
    
}

class TripleLIS {
    public static boolean increasingTriplet(int[] nums) {
        int first_num = Integer.MAX_VALUE;
        int second_num = Integer.MAX_VALUE;
        for (int n: nums) {
            if (n <= first_num) {
                first_num = n;
            } else if (n <= second_num) {
                second_num = n;
            } else {
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(increasingTriplet(new int[]{5,10,6,7}));
    }
    
}

