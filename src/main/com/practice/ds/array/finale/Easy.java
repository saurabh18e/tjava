package com.practice.ds.array.finale;

import com.practice.util.*;

import java.util.*;

public class Easy {
    
    public static class DutchNationalFlag {
        
        // dutch national flag problem
        
        // EPI: 6.1
        public static int[] groupBy(int[] arr, int pivot) {
            if (arr == null || arr.length <= 1) return arr;
            
            int n = arr.length;
            int start = -1, end = n;
            for (int i = 0; i < n && i < end; ) {
                if (arr[i] == pivot) {
                    i++;
                } else if (arr[i] < pivot) {
                    Util.swap(arr, ++start, i++);
                } else {
                    Util.swap(arr, --end, i);
                }
            }
            return arr;
        }
        
        public static void main(String[] args) {
            System.out.println(Arrays.toString(groupBy(new int[]{2, 1, 3, 1, 3, 2, 2, 1, 3, 1}, 2)));
            System.out.println(Arrays.toString(groupBy(new int[]{2, 1, 3}, 2)));
        }
    }
    
}


class AddBigNumbers {
    
    // EPI: 6.2
    // using array lists
    private static List<Integer> add(List<Integer> num1, List<Integer> num2) {
        if (num1 == null || num1.size() == 0) return num2;
        if (num2 == null || num2.size() == 0) return num1;
        
        // 0-padding
        List<Integer> output = new ArrayList<Integer>();
        int carry = 0, sum = 0;
        for (int n = num1.size() - 1, m = num2.size() - 1; n >= 0 || m >= 0; n--, m--) {
            int a = n >= 0 ? num1.get(n) : 0;
            int b = m >= 0 ? num2.get(m) : 0;
            sum = a + b + carry;
            carry = sum / 10;
            output.add(sum % 10);
        }
        if (carry > 0) output.add(carry);
        
        Collections.reverse(output);
        return output;
    }
    
    public static void main(String[] args) {
        List<Integer> sum1 = add(Arrays.asList(9, 2, 3), Arrays.asList(9, 9));
        System.out.println(Arrays.toString(sum1.toArray()));
    }
}

class JumpArray {
    
    // EPI 6.4: https://leetcode.com/problems/jump-game/
    private boolean canJump(int[] nums) {
        if (nums == null || nums.length == 0) return true;
        int maxReachSofar = 0;
        for (int i = 0; i < nums.length && i <= maxReachSofar; i++) {
            maxReachSofar = Math.max(maxReachSofar, i + nums[i]);
            if (maxReachSofar >= nums.length - 1) return true;
        }
        return false;
    }
    
}

class DeleteDups {
    // EPI 6.5
    private static List<Integer> deleteDups(List<Integer> list) {
        if (list == null || list.size() == 0) return list;
        int i = 0;
        for (int j = 1; j < list.size(); j++) {
            if (list.get(j) != list.get(i)) {
                list.set(++i, list.get(j));
            }
        }
        return list.subList(0, i + 1);
    }
    
    public static void main(String[] args) {
        System.out.println(Arrays.toString(deleteDups(Arrays.asList(1, 2, 2, 3, 4, 4, 4, 4, 5, 5)).toArray()));
        System.out.println(Arrays.toString(deleteDups(Arrays.asList(5, 5)).toArray()));
        System.out.println(Arrays.toString(deleteDups(Arrays.asList(1, 2)).toArray()));
    }
}

