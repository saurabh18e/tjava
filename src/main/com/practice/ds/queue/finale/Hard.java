package com.practice.ds.queue.finale;

import java.util.*;

public class Hard {
}

class LargestSubArrayWithMaxDiff {
    
    // monotonic decreasing queue
    public int maxSubarraySize(int[] arr, int maxDiff) {
        Deque<Integer> maxInWindow = new LinkedList<>();
        Deque<Integer> minInWindow = new LinkedList<>();
        
        int maxSubarraySize = 0;
        int start = 0;
        for (int end = 0; end < arr.length; end++) {
            // update min-max monotonic queues for current window
            while (!maxInWindow.isEmpty() && maxInWindow.getLast() < arr[end])
                maxInWindow.removeLast();
            maxInWindow.add(arr[end]);
    
            while (!minInWindow.isEmpty() && minInWindow.getLast() > arr[end])
                minInWindow.removeLast();
            minInWindow.add(arr[end]);
            
            // maintain window property for current window
            while (maxInWindow.getFirst() - minInWindow.getFirst() > maxDiff) {
                if (arr[start] == maxInWindow.getFirst()) maxInWindow.removeFirst();
                if (arr[start] == minInWindow.getFirst()) minInWindow.removeFirst();
                start++;
            }
            
            // update max subarray size
            maxSubarraySize = Math.max(maxSubarraySize, end-start+1);
        }
        
        return maxSubarraySize;
    }
    
}
