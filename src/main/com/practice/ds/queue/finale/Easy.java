package com.practice.ds.queue.finale;

import com.practice.ds.tree.bst.finale.*;

import java.util.*;

import static com.practice.ds.queue.finale.ConnectingNodesLevelWise.Direction.*;

public class Easy {
}

class RecentCounter {
    // sliding window example -> always use Deque
    
    int SlidingWindowSize = 3000;
    
    Deque<Integer> slidingWindow = new LinkedList<>();
    
    public RecentCounter() {
    
    }
    
    public int ping(int t) {
        while (!slidingWindow.isEmpty() && slidingWindow.getFirst() < t - SlidingWindowSize)
            slidingWindow.removeFirst();
        slidingWindow.addLast(t);
        return slidingWindow.size();
    }
    
}

class Monotonic {
    
    // monotonic decreasing queue
    public int[] slidingWindowMax(int[] nums, int k) {
        int n = nums.length - k + 1;
        int[] max = new int[n];
        Deque<Integer> queue = new LinkedList<>();
        
        for (int i = 0; i < nums.length; i++) {
            while (!queue.isEmpty() && nums[queue.getLast()] < nums[i]) queue.removeLast();
            while (!queue.isEmpty() && queue.getFirst() <= i - k) queue.removeFirst();
            queue.addLast(i);
            if (i >= k - 1) {
                max[i - k + 1] = nums[queue.getFirst()];
            }
        }
        
        return max;
    }
    
}

class QueueWithMax {
    // monotonic decreasing deque
    
    public static class MaxNode {
        public int data, count;
        
        public MaxNode(int data, int count) {
            this.data = data;
            this.count = count;
        }
        
    }
    
    Queue<Integer> dataQueue = new LinkedList<>();
    Deque<MaxNode> maxQueue = new LinkedList<>();
    
    public void enqueue(int i) {
        dataQueue.add(i);
        while (!maxQueue.isEmpty() && maxQueue.getLast().data < i) {
            maxQueue.removeLast();
        }
        if (!maxQueue.isEmpty() && maxQueue.getLast().data == i) {
            maxQueue.getLast().count++;
        } else {
            maxQueue.addLast(new MaxNode(i, 1));
        }
    }
    
    public int dequeue() {
        if (maxQueue.isEmpty()) throw new NoSuchElementException("can not remove from empty queue");
        if (maxQueue.getFirst().data == dataQueue.peek()) {
            maxQueue.getFirst().count--;
            if (maxQueue.getFirst().count == 0) maxQueue.removeFirst();
        }
        return dataQueue.remove();
    }
    
    public int max() {
        if (maxQueue.isEmpty()) throw new NoSuchElementException("can not return max from empty queue");
        return maxQueue.getFirst().data;
    }
    
    public boolean isEmpty() {
        return dataQueue.isEmpty();
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{5, 8, 3, 9, 2, 4};
        QueueWithMax q = new QueueWithMax();
        
        System.out.println("queue while enqueue phase: ");
        for (int a : arr) {
            q.enqueue(a);
            Integer max = q.isEmpty() ? null : q.max();
            System.out.println(a + " added to queue with max: " + max);
        }
        
        System.out.println("queue while dequeue phase: ");
        while (!q.isEmpty()) {
            int a = q.dequeue();
            Integer max = q.isEmpty() ? null : q.max();
            System.out.println(a + " removed from queue with max: " + max);
        }
    }
    
}

class QueueUsingStacks {
        /*
            question:
                EPI 9.9
            algo:
                use enqueue and dequeue stacks
         */
    
    private Stack<Integer> enqueueStack = new Stack<>();
    private Stack<Integer> dequeueStack = new Stack<>();
    
    public void enqueue(int a) {
        enqueueStack.push(a);
    }
    
    public Integer dequeue() {
        if (!dequeueStack.isEmpty()) {
            return dequeueStack.pop();
        } else {
            while (!enqueueStack.isEmpty()) dequeueStack.push(enqueueStack.pop());
            return (dequeueStack.isEmpty()) ? null : dequeueStack.pop();
        }
    }
    
}

class StackUsingQueue {
    
    private Queue<Integer> queue = new LinkedList<>();
    
    //public void push(int i) {
    //    queue.add(i);
    //}
    //public Integer pop() {
    //    if (queue.isEmpty()) return null;
    //    for (int i=0; i<queue.size()-1; i++) {
    //        queue.add(queue.remove());
    //    }
    //    return queue.remove();
    //}
    
    public void push(int e) {
        queue.add(e);
        for (int i = 0; i < queue.size() - 1; i++) {
            queue.add(queue.remove());
        }
    }
    public Integer pop() {
        if (queue.isEmpty()) return null;
        return queue.remove();
    }
    public Integer peek() {
        if (queue.isEmpty()) return null;
        return queue.peek();
    }
    
}

class LevelOfTree {
    
    public static List<List<TreeNode>> levelOrder(TreeNode root) {
        List<List<TreeNode>> levels = new ArrayList<>();
        if (root == null) return levels;
        Deque<TreeNode> currentLevel = new LinkedList<>();
        currentLevel.add(root);
        
        while (!currentLevel.isEmpty()) {
            levels.add(new ArrayList<>(currentLevel));
            Deque<TreeNode> nextLevel = new LinkedList<>();
            while (!currentLevel.isEmpty()) {
                TreeNode node = currentLevel.removeFirst();
                if (node.left != null) nextLevel.addLast(node.left);
                if (node.right != null) nextLevel.addLast(node.right);
            }
            currentLevel = nextLevel;
        }
        
        return levels;
    }
    
    // note that we can use preorder traversal +
    // hashmap to find levels inorder tor educe space to 0(h)
    public static List<List<TreeNode>> levelOrderUsingPreorder(TreeNode root) {
        HashMap<Integer, List<TreeNode>> levelMap = new HashMap<>();
        preorderVisit(root, 0, levelMap);
        List<List<TreeNode>> output = new ArrayList<>();
        for (int i = 0; levelMap.containsKey(i); i++) {
            output.add(i, levelMap.get(i));
        }
        return output;
    }
    private static void preorderVisit(TreeNode root, int levelNum,
                                      HashMap<Integer, List<TreeNode>> levelMap) {
        if (root == null) return;
        if (!levelMap.containsKey(levelNum)) levelMap.put(levelNum, new ArrayList<>());
        levelMap.get(levelNum).add(root);
        preorderVisit(root.left, levelNum + 1, levelMap);
        preorderVisit(root.right, levelNum + 1, levelMap);
    }
    
    public static List<Integer> largestInEachLevel(TreeNode root) {
        List<Integer> output = new ArrayList<>();
        if (root == null) return output;
        
        Deque<TreeNode> queue = new LinkedList<>();
        queue.addLast(root);
        queue.addLast(null);
        while (!queue.isEmpty()) {
            int levelMax = Integer.MIN_VALUE;
            while (!queue.isEmpty()) {
                TreeNode node = queue.removeFirst();
                if (node == null) break;
                levelMax = Math.max(node.val, levelMax);
                if (node.left != null) queue.addLast(node.left);
                if (node.right != null) queue.addLast(node.right);
            }
            output.add(levelMax);
            if (!queue.isEmpty()) queue.addLast(null);
        }
        
        return output;
    }
    
    public static int deepestLeavesSum(TreeNode root) {
        if (root == null) return 0;
        Deque<TreeNode> queue = new LinkedList<>();
        queue.addLast(root);
        queue.addLast(null);
        int levelSum = 0;
        while (!queue.isEmpty()) {
            levelSum = 0;
            while (!queue.isEmpty()) {
                TreeNode node = queue.removeFirst();
                if (node == null) break;
                levelSum += node.val;
                if (node.left != null) queue.addLast(node.left);
                if (node.right != null) queue.addLast(node.right);
            }
            if (queue.isEmpty()) break;
            queue.addLast(null);
        }
        return levelSum;
    }
    
}

class ConnectingNodesLevelWise {
    
    /////// general case //////
    /*
        solution 1: usual level order traversal
            - use current and next levels
            - use null as dilimeter
            - use current level count
            - problem: this is S(n)....in perfect binary tree
        solution 2:
            - use level-next field as queue for level order traversal
     */
    
    private static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;
        
        public Node() {
        }
        
        public Node(int val) {
            this.val = val;
        }
        public Node(int val, Node left, Node right, Node next) {
            this.val = val;
            this.left = left;
            this.right = right;
            this.next = next;
        }
        
    }
    
    public Node connectNodesLevelWiseGeneralTree(Node root) {
        if (root == null) return root;
        
        Node currentLevelIter = root;
        Node nextLevelIter = null;
        Node nextLevelStart = null;
        while (currentLevelIter != null) {
            while (currentLevelIter != null) {
                if (nextLevelStart == null) {
                    nextLevelStart = currentLevelIter.left != null ? currentLevelIter.left : currentLevelIter.right;
                }
                
                if (currentLevelIter.left != null) {
                    if (nextLevelIter != null) {
                        nextLevelIter.next = currentLevelIter.left;
                        nextLevelIter = nextLevelIter.next;
                    } else {
                        nextLevelIter = currentLevelIter.left;
                    }
                }
                if (currentLevelIter.right != null) {
                    if (nextLevelIter != null) {
                        nextLevelIter.next = currentLevelIter.right;
                        nextLevelIter = nextLevelIter.next;
                    } else {
                        nextLevelIter = currentLevelIter.right;
                    }
                }
                
                currentLevelIter = currentLevelIter.next;
            }
            currentLevelIter = nextLevelStart;
            nextLevelIter = null;
            nextLevelStart = null;
        }
        return root;
    }
    
    public Node connectNodesLevelWisePerfectTree(Node root) {
        if (root == null) return null;
        Deque<Node> queue = new LinkedList<>();
        queue.addLast(root);
        
        Node currentIter = root;
        Node nextStart = null;
        while (currentIter != null) {
            nextStart = currentIter.left;
            while (currentIter != null && nextStart != null) {
                currentIter.left.next = currentIter.right;
                currentIter.right.next = currentIter.next == null ? null : currentIter.next.left;
                currentIter = currentIter.next;
            }
            currentIter = nextStart;
            nextStart = null;
        }
        return root;
    }
    
    static enum Direction {Zig, Zag}
    public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        if (root == null) return Collections.emptyList();
        
        List<List<Integer>> output = new ArrayList<>();
        Deque<TreeNode> currentLevel = new LinkedList<>();
        currentLevel.add(root);
        Direction direction = Zig;
        output.add(levelOutput(currentLevel, direction));
        
        Deque<TreeNode> nextLevel = null;
        while (!currentLevel.isEmpty()) {
            nextLevel = new LinkedList<>();
            while (!currentLevel.isEmpty()) {
                TreeNode node = currentLevel.removeFirst();
                if (node.left != null) nextLevel.add(node.left);
                if (node.right != null) nextLevel.add(node.right);
            }
            currentLevel = nextLevel;
            direction = (direction == Zig) ? Zag : Zig;
            if (!currentLevel.isEmpty()) output.add(levelOutput(currentLevel, direction));
        }
        return output;
    }
    private static List<Integer> levelOutput(
        Deque<TreeNode> level, Direction direction) {
        Object[] arr = level.toArray();
        List<Integer> levelOutput = new LinkedList<>();
        for (int i = 0; i < level.size(); i++) {
            TreeNode node = (TreeNode) ((direction == Zig) ? arr[i] : arr[arr.length - 1 - i]);
            levelOutput.add(node.val);
        }
        return levelOutput;
    }
    
}

//class Solution {
//
//    Node prev, leftmost;
//
//    public void processChild(Node childNode) {
//
//        if (childNode != null) {
//            if (this.prev != null) {
//                this.prev.next = childNode;
//            } else {
//                this.leftmost = childNode;
//            }
//
//            this.prev = childNode;
//        }
//    }
//
//    public Node connect(Node root) {
//
//        if (root == null) {
//            return root;
//        }
//
//        this.leftmost = root;
//        Node curr = leftmost;
//
//        while (this.leftmost != null) {
//            this.prev = null;
//            curr = this.leftmost;
//            this.leftmost = null;
//
//            while (curr != null) {
//                this.processChild(curr.left);
//                this.processChild(curr.right);
//                curr = curr.next;
//            }
//        }
//
//        return root ;
//    }
//}