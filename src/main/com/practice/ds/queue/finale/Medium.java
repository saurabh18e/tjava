package com.practice.ds.queue.finale;

import java.util.Arrays;

public class Medium {
}

class CircularQueueInDynamicArray {
    
    private int[] arr;
    private int count;
    private int head;
    private int tail;
    
    public CircularQueueInDynamicArray(int initialCapacity) {
        this.count = 0;
        this.arr = new int[initialCapacity];
    }
    
    public void enqueue(int e) {
        if (arr.length == count) {
            int[] buff = new int[Math.max(10, arr.length * 2)];
            for (int i = 0; i < arr.length; i++) buff[i] = arr[((head + i) % arr.length)];
            this.arr = buff;
            head = 0;
            tail = count;
        }
        arr[tail] = e;
        tail = (tail + 1) % arr.length;
        count++;
    }
    
    public Integer dequeue() {
        if (count == 0) return null;
        int val = arr[head];
        head = (head + 1) % arr.length;
        count--;
        return val;
    }
    
    public int front() {
        return isEmpty() ? -1 : arr[head];
    }
    
    public int rear() {
        int index = (tail - 1 + arr.length) % arr.length;
        return isEmpty() ? -1 : arr[index];
    }
    
    public boolean isEmpty() {
        return count == 0;
    }
    
    public boolean isFull() {
        return count == arr.length;
    }
    
    public static void main(String[] args) {
        CircularQueueInDynamicArray q = new CircularQueueInDynamicArray(3);
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        System.out.println(Arrays.toString(q.arr));
        System.out.println(q.tail);
    }
}
