package com.practice.ds.queue;

public class Queue { }

/*
  Queue implementations
  ----------------------
  - implement queue using linked list
  - method: use linked list with head and end pointers so that both enqueue and dequeue are 0(1)
  
  - implement queue using array such that there is no wastage of space even after 1000s of random add and remove
  - method: circular queue in array
  
  - implement queue using stacks
  - ref: https://www.geeksforgeeks.org/queue-using-stacks/
  - method: use two stacks: enqueue stack and dequeue stack
      - enqueue: push element on enqueue stack
      - dequeue: pop element from dequeue stack if it is not empty else pop and push all elements from enqueue stack
  
  - implement queue using one stack and other stack as function call stack
  - ref: https://www.geeksforgeeks.org/queue-using-stacks/
  - method:
      - enqueue: push element to stack
      - dequeue: if(last element of stack) pop and return else {pop, recurse, push back and return recursion result}
  
 */

/*
 
 */
