package com.practice.ds.search;

import java.lang.reflect.Array;
import java.util.*;
/*
    - linear search
    - binary search + exponential jump search
    - constant time lookup
 */
public class Search {
    
    public static int linearSearch(int[] nums, int x) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == x) return i;
        }
        return -1;
    }
    
    public static int binarySearch(int[] nums, int x) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (l + r) / 2;
            if (nums[m] == x) {
                return m;
            } else if (nums[m] > x) {
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return -1;
    }
    public static int binarySearchRec(int[] nums, int x) {
        return binarySearchRec(nums, x, 0, nums.length);
    }
    public static int binarySearchRec(int[] nums, int x, int l, int r) {
        if (l > r) return -1;
        int m = l + (r - l) / 2;
        if (nums[m] == x) {
            return m;
        } else if (nums[m] > x) {
            return binarySearchRec(nums, x, l, m - 1);
        } else {
            return binarySearchRec(nums, x, m + 1, r);
        }
    }
    
    // rotated search: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/279/
    public int rotatedSearch(int[] nums, int target) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (l + r) / 2;
            if (nums[m] == target) {
                return m;
            } else if (nums[m] < nums[r]) {
                if (target > nums[m] && target <= nums[r]) {
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            } else {
                if (target < nums[m] && target >= nums[l]) {
                    r = m - 1;
                } else {
                    l = m + 1;
                }
            }
        }
        return -1;
    }
    
    // boundary search using binary: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3030/
    public static int[] searchRange(int[] nums, int target) {
        int left = -1, right = -1;
        left = findBoundary(nums, target, true);
        if (left != -1) {
            right = findBoundary(nums, target, false);
        }
        return new int[]{left, right};
    }
    public static int findBoundary(int[] nums, int target, boolean findLeft) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            int m = (l + r) / 2;
            if (target == nums[m]) {
                if (findLeft) {
                    if (l == m) return l;
                    r = m;
                } else {
                    if (l == m) return nums[r] == target ? r : l;
                    l = m;
                }
            } else if (target < nums[m]) {
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return -1;
    }
    
    // first bad version: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/272/
    public static boolean isBadVersion(int version) {
        return version >= 4;
    }
    public static int firstBadVersion(int n) {
        int l = 1;
        int r = n;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (isBadVersion(m)) {
                if (l == m) return l;
                r = m;
            } else {
                if (l == m) return r;
                l = m;
            }
        }
        return -1;
    }
    
    // find any peak: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3032/
    public int findPeakElement(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        while (l <= r) {
            if (l == r) return l;
            int m = (l + r) / 2;
            if (nums[m] < nums[m + 1]) {
                l = m + 1;
            } else {
                r = m;
            }
        }
        return -1;
    }
    
    // fast exponential: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3031/
    public double myPow(double x, int n) {
        return (n < 0) ? myPowItr(1 / x, -(long) n) : myPowItr(x, n);
    }
    public double myPowRec(double x, long n) {
        if (n == 0) return 1.0;
        double res = myPowRec(x, n / 2);
        return res * res * ((n % 2 == 1) ? x : 1);
    }
    public double myPowItr(double x, long n) {
        /*
            - keep dividing n by 2....essentially doing a left shift of MSB till MSB becomes 0
            - to take care of all 1 bits before MSB, we always check 1st bit(because bits are being shifted)
              we also keep calculating the factor to load if bit is 1
         */
        double result = 1;
        double msbResult = x;
        for (long i = n; i > 0; i = i / 2) {
            result = result * ((i % 2 == 1) ? msbResult : 1);
            msbResult = msbResult * msbResult;
        }
        return result;
    }
    
    // intersection: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/3033/
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet set1 = new HashSet();
        for (int i = 0; i < nums1.length; i++) set1.add(nums1[i]);
        ArrayList<Integer> outputList = new ArrayList();
        for (int i = 0; i < nums2.length; i++) {
            int n = nums2[i];
            if (set1.contains(n)) {
                set1.remove(n);
                outputList.add(n);
            }
        }
        int[] output = new int[outputList.size()];
        int j = 0;
        for (int n : outputList) {
            output[j++] = n;
        }
        return output;
    }
    
    // intersection with dups: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/270/
    /*
        - approach1: sorting
        - approach2: hashmap with count
     */
    public int[] intersect(int[] nums1, int[] nums2) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i : nums1) map.put(i, map.getOrDefault(i, 0) + 1);
        ArrayList<Integer> outputList = new ArrayList();
        for (int i : nums2) {
            if (map.getOrDefault(i, 0) > 0) {
                map.put(i, map.getOrDefault(i, 0) - 1);
                outputList.add(i);
            }
        }
        int[] output = new int[outputList.size()];
        int j = 0;
        for (int n : outputList) {
            output[j++] = n;
        }
        return output;
    }
    
    // merge intervals: https://leetcode.com/explore/interview/card/facebook/54/sorting-and-searching-3/310/
    /*
        - approach1: connected components of a graph
        - approach2: sorting by start time
     */
    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return Integer.compare(o1[0], o2[0]);
            }
        });
    
        ArrayList<int[]> outputList = new ArrayList<>();
        for (int i = 0; i < intervals.length; ) {
            int min = intervals[i][0];
            int max = intervals[i][1];
            int j = i+1;
            while (j < intervals.length) {
                if (intervals[j][0] > max) {
                    break;
                } else {
                    min = Math.min(min, intervals[j][0]);
                    max = Math.max(max, intervals[j][1]);
                    j++;
                }
            }
            outputList.add(new int[]{min, max});
            i = j;
        }
        
        int[][] output = new int[outputList.size()][];
        int i=0;
        for (int[] arr : outputList) {
            output[i++] = arr;
        }
        return output;
    }
}
