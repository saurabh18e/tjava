package com.practice.ds.search.finale;

import java.util.*;

public class Easy {
}

class Basic {
    
    public int searchInSorted(int[] nums, int target) {
        return binarySearch(nums, target);
    }
    private int binarySearch(int[] nums, int target) {
        if (nums == null || nums.length == 0) return -1;
        int lower = 0, upper = nums.length - 1;
        while (lower <= upper) {
            int mid = lower + (upper - lower) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] < target) {
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        return -1;
    }
    
    private int binarySearchCorner(int[] nums, int target, boolean leftCorner) {
        if (nums == null || nums.length == 0) return -1;
        int lower = 0, upper = nums.length - 1;
        while (lower <= upper) {
            int mid = lower + (upper - lower) / 2;
            if (nums[mid] == target) {
                if (leftCorner) {
                    if (mid == lower || nums[mid-1] != target) return mid;
                    upper = mid - 1;
                } else {
                    if (mid == upper || nums[mid+1] != target) return mid;
                    lower = mid + 1;
                }
            } else if (nums[mid] < target) {
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        
        return -1;
    }
    private int binarySearchFirst(int[] nums, int target) {
        return binarySearchCorner(nums, target, true);
    }
    private int binarySearchLast(int[] nums, int target) {
        return binarySearchCorner(nums, target, false);
    }
    private int[] binarySearchFirstAndLast(int[] nums, int target) {
        int first = binarySearchFirst(nums, target);
        int last = (first >= 0) ? binarySearchLast(nums, target) : -1;
        return new int[] {first, last};
    }
    private int occurrenceCount(int[] nums, int target) {
        int first = binarySearchFirst(nums, target);
        int last = (first >= 0) ? binarySearchLast(nums, target) : -1;
        return (first >= 0) ? last-first+1 : 0;
    }
    
    // search first with min comparisons
    private int binarySearchFirstV2(int[] nums, int target) {
        if (nums == null || nums.length == 0) return -1;
        int lower = 0, upper = nums.length - 1;
        while (lower < upper) {
            int mid = lower + (upper - lower) / 2;
            if (nums[mid] < target) {
                lower = mid + 1;
            } else {
                // in case of equality, reduce upper
                upper = mid - 1;
            }
        }
        return nums[lower] == target ? lower : -1;
    }
    private int binarySearchFirstV3(int[] nums, int target) {
        if (nums == null || nums.length == 0) return -1;
        int lower = 0, upper = nums.length - 1, result = -1;
        while (lower <= upper) {
            int mid = lower + (upper - lower) / 2;
            if (nums[mid] == target) {
                result = mid;
                upper = mid - 1; // nothing on right can be first
            } else if (nums[mid] < target) {
                lower = mid + 1;
            } else {
                // in case of equality, reduce upper
                upper = mid - 1;
            }
        }
        return result;
    }
    
    private boolean isBadVersion(int n) {return n > 20;}
    private int firstBadVersion(int n) {
        int lower = 0, upper = n, result = -1;
        while (lower <= upper) {
            int mid = lower + (upper - lower) / 2;
            if (isBadVersion(mid)) {
                result = mid;
                upper = mid - 1; // nothing on right can be the first
            } else {
                lower = mid + 1;
            }
        }
        return result;
    }
    
    public int searchInsertionIndex(int[] nums, int target) {
        // insertion point = first greater
        int lower = 0, upper = nums.length-1, result = nums.length;
        while (lower <= upper) {
            int mid = lower + (upper - lower) / 2;
            if (nums[mid] >= target) {
                result = mid;
                upper = mid - 1; // nothing on right can be the insertion point
            } else {
                lower = mid + 1;
            }
        }
        return result;
    }
    
    
    public int[] spellsAndPortionsMatch(int[] spells, int[] potions, long success) {
        Arrays.sort(potions);
        int[] sol = new int[spells.length];
        for (int i = 0; i < spells.length; i++) {
            int l = 0, u = potions.length - 1;
            long spell = spells[i];
            
            int result = potions.length;
            while (l <= u) {
                int mid = l + (u - l) / 2;
                if (potions[mid] * spell >= success) {
                    result = mid;
                    u = mid - 1;
                } else {
                    l = mid + 1;
                }
            }
            sol[i] = potions.length - result;
        }
        return sol;
    }
    
    
    public static boolean isPrefixString(String s, String[] words) {
        if (s.equals("")) return true;
        if (words == null || words.length == 0) return false;
        
        // lexicographic ordering -> first diff char decides till non null, then length
        Arrays.sort(words);
        int l = 0, u=words.length-1;
        while (l <= u) {
            int mid = l + (u-l)/2;
            String str2 = words[mid];
            if (isPrefixString(s, str2)) return true;
            int cmp = s.compareTo(str2);
            if (cmp < 0) {
                u=mid-1;
            } else {
                l=mid+1;
            }
        }
        return false;
    }
    private static boolean isPrefixString(String s1, String s2) {
        if (s2.length() < s1.length()) return false;
        String s3 = s2.substring(0, s1.length());
        return s1.equalsIgnoreCase(s3);
    }
    public static void main(String[] args) {
        System.out.println(isPrefixString("abcd", new String[]{"def", "ab", "abb", "bc", "abcccc"}));
    }
    
}

class SearchStudentList {
    
    static class Student implements Comparable<Student> {
        public String name;
        public int score;
        public Student(String name, int score) {
            this.name = name;
            this.score = score;
        }
        @Override
        public int compareTo(Student o) {
            // return Integer.compare(this.score, o.score);
            return this.name.compareTo(o.name);
        }
        @Override
        public String toString() {
            return "[" + name + ", " + score + ']';
        }
    }
    
    static class StudentScoreComparator implements Comparator<Student> {
        @Override
        public int compare(Student o1, Student o2) {
            int cmp = Integer.compare(o1.score, o2.score);
            if (cmp != 0) return cmp;
            return o1.name.compareTo(o2.name);
        }
    }
    
    private static Student searchStudentByName(List<Student> students, Student student) {
        int index = Collections.binarySearch(students, student);
        return  (index >= 0) ? students.get(index) : null;
    }
    private static Student searchStudentByScore(List<Student> students, Student student) {
        int index = Collections.binarySearch(students, student, new StudentScoreComparator());
        return  (index >= 0) ? students.get(index) : null;
    }
    
    public static void testStudentList() {
        Student s1 = new Student("s1", 1);
        Student s2 = new Student("s2", 1);
        Student s3 = new Student("s3", 3);
        Student s4 = new Student("s4", 2);
        Student s5 = new Student("s5", 3);
        List<Student> students = Arrays.asList(s1, s2, s3, s4, s5);
        
        // sort by name and search
        Collections.sort(students);
        System.out.println(students);
        System.out.println(searchStudentByName(students, s1));
        System.out.println(searchStudentByName(students, s3));
        System.out.println(searchStudentByName(students, s5));
        System.out.println(searchStudentByName(students, new Student("s6", 6)));
        
        // sort by score and search
        Collections.sort(students, new StudentScoreComparator());
        System.out.println(students);
        System.out.println(searchStudentByScore(students, new Student("s1", 1)));
        System.out.println(searchStudentByScore(students, new Student("s2", 1)));
        System.out.println(searchStudentByScore(students, new Student("s4", 2)));
        System.out.println(searchStudentByScore(students, new Student("s3", 3)));
        System.out.println(searchStudentByScore(students, new Student("s5", 2)));
        System.out.println(searchStudentByScore(students, new Student("s6", 4)));
    }
    
    public static void main(String[] args) {
        testStudentList();
    }
}


class BinarySearchVariations {

    public static int findEntryEqualsIndex(int[] nums) {
        int l=0, u = nums.length-1;
        while (l <= u) {
            int mid = l + (u-l)/2;
            if (nums[mid] == mid) {
                return mid;
            } else if (nums[mid] < mid) {
                l = mid + 1;
            } else {
                u = mid - 1;
            }
        }
        return -1;
    }
    
    private static class ArrayReader {
        private int[] arr;
        public ArrayReader(int[] arr) {
            this.arr = arr;
        }
        public int get(int i) {
            return  (i < arr.length) ? arr[i] : Integer.MAX_VALUE;
        }
    }
    public static int searchArrayOfUnknownLength(ArrayReader reader, int target) {
        // find upper boundary using binary search
        int lower = 0, upper = 1;
        if (reader.get(lower) == target) return lower;
        while (reader.get(upper) < target) {
            lower = upper;
            upper *= 2;
        }
        
        // do simple binary search between two boundaries
        while (lower<=upper) {
            int mid = lower + (upper-lower)/2;
            if (reader.get(mid) == target) {
                return mid;
            } else if (reader.get(mid) < target) {
                lower = mid+1;
            } else {
                upper = mid-1;
            }
        }
        return -1;
    }
    
    public static final int findAnyPeak(int[] arr) {
        if (arr == null || arr.length == 0) throw new IllegalArgumentException("cannot find peak in empty array");
        int l=0, u=arr.length-1;
        while (l<=u) {
            int mid = l+(u-l)/2;
            if (l==u) {
                return arr[l];
            } else {
                if (arr[mid] < arr[mid+1]) {
                    l=mid+1;
                } else {
                    u=mid;
                }
            }
        }
        return -1;
    }
    public int findAnyPeakLinear(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] > nums[i + 1])
                return i;
        }
        return nums.length - 1;
    }
}
