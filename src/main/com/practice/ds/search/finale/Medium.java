package com.practice.ds.search.finale;

import java.util.*;

import static com.practice.util.Util.*;

public class Medium {

}

class RotatedArraySearch {
    
    public int searchInRotatedArray(int[] nums, int target) {
        int l = 0, u = nums.length - 1;
        while (l <= u) {
            int mid = l + (u - l) / 2;
            if (nums[mid] == target) {
                return mid;
            } else {
                // if first half is sorted
                if (nums[l] <= nums[mid]) {
                    // if target lies in first half
                    if (nums[l] <= target && target <= nums[mid]) u = mid - 1;
                    else l = mid + 1;
                } else {
                    // second half is sorted
                    if (nums[mid] <= target && target <= nums[u]) l = mid + 1;
                    else u = mid - 1;
                }
            }
        }
        return -1;
    }
    public int searchInRotatedArrayWithDups(int[] nums, int target) {
        int l = 0, u = nums.length - 1;
        while (l <= u) {
            int mid = l + (u - l) / 2;
            if (nums[mid] == target) {
                return mid;
            } else {
                if (nums[l] == nums[mid]) {
                    l = l + 1; // no decision, except that l cannot be it
                } else if (nums[l] <= nums[mid]) {
                    // if target lies in first half
                    if (nums[l] <= target && target <= nums[mid]) u = mid - 1;
                    else l = mid + 1;
                } else {
                    // second half is sorted
                    if (nums[mid] <= target && target <= nums[u]) l = mid + 1;
                    else u = mid - 1;
                }
            }
        }
        return -1;
    }
    
    public static int minInRotatedArray(int[] nums) {
        int l = 0, u = nums.length - 1;
        while (l <= u) {
            int mid = l + (u - l) / 2;
            if (l == u) {
                return nums[l];
            } else {
                if (nums[mid] <= nums[u]) {
                    u = mid; // anything after mid cannot be min
                } else {
                    l = mid + 1; // mid or anything before it cannot be min
                }
            }
        }
        return -1; // code should not reach here
    }
    public static int minInRotatedArrayWithDups(int[] nums) {
        int l = 0, u = nums.length - 1;
        while (l <= u) {
            int mid = l + (u - l) / 2;
            if (l == u) {
                return nums[l];
            } else {
                if (nums[mid] == nums[u]) {
                    u = u - 1; // no decision except u is not the min
                } else if (nums[mid] < nums[u]) {
                    u = mid; // anything after mid cannot be min
                } else {
                    l = mid + 1; // mid or anything before it cannot be min
                }
            }
        }
        return -1; // code should not reach here
    }
    
    private static int findMaxInIncDecArray(int... arr) {
        int l = 0, u = arr.length - 1;
        while (l <= u) {
            int mid = l + (u - l) / 2;
            if (l == u) {
                return arr[l];
            } else {
                if (arr[mid] > arr[u]) {
                    u = mid; // max connot lie after mid
                } else {
                    l = mid + 1;
                }
            }
        }
        return -1;
    }
    
    public static void main(String[] args) {
        System.out.println(findMaxInIncDecArray());
        System.out.println(findMaxInIncDecArray(1));
        System.out.println(findMaxInIncDecArray(1, 2));
        System.out.println(findMaxInIncDecArray(2, 1));
        System.out.println(findMaxInIncDecArray(1, 2, 3, 4, 5, 6, 3, 2, 0, -1, 3));
    }
    
}

class SearchSortedMatrix {
    
    public boolean binarySearchSingleArrayMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) return false;
        int n = matrix.length, m = matrix[0].length;
        int lower = 0, upper = n * m - 1;
        while (lower <= upper) {
            int mid = lower + (upper - lower) / 2;
            int row = mid / m;
            int column = mid % m;
            if (matrix[row][column] == target) {
                return true;
            } else if (matrix[row][column] < target) {
                lower = mid + 1;
            } else {
                upper = mid - 1;
            }
        }
        return false;
    }
    
    public boolean linearSearchMatrix(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) return false;
        int n = matrix.length, m = matrix[0].length;
        int row = 0, column = m - 1;
        while (row >= 0 && row < n && column >= 0 && column < m) {
            int cell = matrix[row][column];
            if (cell == target) {
                return true;
            } else if (cell < target) {
                row++; // discard row
            } else {
                column--; // discard column
            }
        }
        return false;
    }
    
}

class KthOrderStaticInSortedMatrix {
    
    // 0(k log k) = 0(n^2 log(n^2)) = 0(n^2 log(n))
    class kthSmallestInMatrixV1 {
        private class ArrayState {
            public int arrayNumber;
            public int index;
            public int value;
            public ArrayState(int arrayNumber, int index, int value) {
                this.arrayNumber = arrayNumber;
                this.index = index;
                this.value = value;
            }
            
        }
        public int kthSmallestInMatrixV1(int[][] matrix, int k) {
            if (matrix.length == 0 || matrix[0].length == 0
                || k > matrix.length * matrix.length || k < 0) throw new IllegalArgumentException("cannot find k smallest in matrix");
            
            // treat each row as sorted list and keep on moving min till kth using min-heap
            PriorityQueue<ArrayState> minHeap = new PriorityQueue<>(
                (a1, a2) -> Integer.compare(a1.value, a2.value));
            for (int i = 0; i < Math.min(matrix.length, k); i++) {
                minHeap.add(new ArrayState(i, 0, matrix[i][0]));
            }
            while (!minHeap.isEmpty()) {
                ArrayState state = minHeap.remove();
                if (--k == 0) return state.value;
                if (state.index + 1 < matrix[state.arrayNumber].length) {
                    int val = matrix[state.arrayNumber][state.index + 1];
                    state = new ArrayState(state.arrayNumber, state.index + 1, val);
                    minHeap.add(state);
                }
            }
            throw new IllegalArgumentException("cannot find k smallest in matrix");
        }
        
    }
    
    // 0(n log(min-max))....mind that log is generally very small..log(2^32)=10
    // so 0(n log(min-max)) is almost 0(n)
    class kthSmallestInMatrixV2 {
        
        public int kthSmallestInMatrix(int[][] matrix, int k) {
            int n = matrix.length;
            int lo = matrix[0][0], hi = matrix[n - 1][n - 1];
            // we have a low-high range, we keep moving low/high and make it converge
            // such that both low and high move towards k
            while (lo <= hi) {
                int mid = lo + (hi - lo) / 2;
                int count = countLessThanOrEqual(matrix, mid);
                if (count < k) {
                    lo = mid + 1;
                } else if (count > k) {
                    hi = mid - 1;
                } else {
                    hi = mid - 1; // in case of equality move high, since mid is leaned towards low
                }
            }
            return lo;
        }
        
        private int countLessThanOrEqual(int[][] matrix, int val) {
            int count = 0;
            int n = matrix.length, row = n - 1, colummn = 0;
            while (row >= 0 && colummn < n) {
                if (matrix[row][colummn] > val) {
                    row--; // discard everything from this row
                } else {
                    count += (row + 1); // all elements in column are <= val
                    colummn++;
                }
            }
            return count;
        }
        
    }
    
    class kthSmallestInMatrixV3 {
        
        public int kthSmallest(int[][] matrix, int k) {
            int n = matrix.length;
            int start = matrix[0][0], end = matrix[n - 1][n - 1];
            while (start < end) {
                int mid = start + (end - start) / 2;
                int[] smallLargePair = {matrix[0][0], matrix[n - 1][n - 1]};
                int count = this.countLessEqual(matrix, mid, smallLargePair);
                if (count == k) return smallLargePair[0];
                if (count < k) start = smallLargePair[1]; // search higher
                else end = smallLargePair[0]; // search lower
            }
            return start;
        }
        
        private int countLessEqual(int[][] matrix, int mid, int[] smallLargePair) {
            int count = 0;
            int n = matrix.length, row = n - 1, col = 0;
            while (row >= 0 && col < n) {
                if (matrix[row][col] > mid) {
                    // as matrix[row][col] is bigger than the mid, let's keep track of the
                    // smallest number greater than the mid
                    smallLargePair[1] = Math.min(smallLargePair[1], matrix[row][col]);
                    row--;
                } else {
                    // as matrix[row][col] is less than or equal to the mid, let's keep track of the
                    // biggest number less than or equal to the mid
                    smallLargePair[0] = Math.max(smallLargePair[0], matrix[row][col]);
                    count += row + 1;
                    col++;
                }
            }
            return count;
        }
        
    }
    
}

class Partition {
    
    // dutch national flag problem - unstable
    public static void dutchNationalFlagUnstable(int[] nums, int pivot) {
        int i = -1, j = 0, k = nums.length;
        while (j < k) {
            if (nums[j] == pivot) {
                j++;
            } else if (nums[j] < pivot) {
                swap(nums, ++i, j++);
            } else {
                swap(nums, j, --k);
            }
        }
    }
    public static void sortColors(int[] nums) {
        dutchNationalFlagUnstable(nums, 1);
    }
    
    // dutch national flag problem - stable
    /*
        method1: use extra o(n) array space and 3 pass
        method2: use bucket sort: hashmap or array of size 3
        overall: method 1 offers good worstcase space complexity and locality, no use of objects
     */
    public static void dutchNationalFlagStable(int[] nums, int pivot) {
        if (nums.length <= 1) return;
        int i = 0, j = 0, k = 0;
        // count
        for (int a : nums) {
            if (a < pivot) {
                i++;
            } else if (a == pivot) {
                j++;
            } else {
                k++;
            }
        }
        
        k = i + j;
        j = i;
        i = 0;
        int[] arr = new int[nums.length];
        for (int a : nums) {
            if (a < pivot) {
                arr[i++] = a;
            } else if (a == pivot) {
                arr[j++] = a;
            } else {
                arr[k++] = a;
            }
        }
        for (int a = 0; a < nums.length; a++) {
            nums[a] = arr[a];
        }
    }
    
    // array partitioning - stable
    public static void partitionUnstable(int[] arr, int pivot) {
        dutchNationalFlagUnstable(arr, pivot);
    }
    public static void partitionStable(int[] arr, int pivot) {
        dutchNationalFlagStable(arr, pivot);
    }
    
}

class KthOrderStaticV1 {
    /*
        question: kth order static (without dups)
        solution: randomized partitioning
        
        question: kth order static (with dups)
        solution: randomized stable partitioning
        
     */
    public static int kthOrderStatic(int[] nums, int k) {
        return kthOrderStaticWithDups(nums, k);
    }
    public static int kthOrderStaticWithDups(int[] nums, int k) {
        /*
            select a random element as pivot and partition array around it
            check return pivotIndex
            recurse/repeat as needed
         */
        int start = 0, end = nums.length - 1;
        while (k >= 1) {
            int pivotIndex = randomPartitioning(nums, start, end);
            
            int countSeen = pivotIndex + 1 - start;
            if (countSeen == k) {
                return nums[pivotIndex];
            } else if (countSeen < k) {
                // kth is on right of pivot
                
                // take care of dups skip equal elements on right
                while (pivotIndex < end && nums[pivotIndex] == nums[pivotIndex + 1]) {
                    countSeen++;
                    pivotIndex++;
                    if (pivotIndex + 1 - start == k) return nums[pivotIndex];
                }
                
                k = k - countSeen;
                start = pivotIndex + 1;
            } else {
                // kth is on left of pivot
                end = pivotIndex - 1;
            }
        }
        throw new IllegalArgumentException("unable to find kth-order static");
    }
    private static int randomPartitioning(int[] nums, int start, int end) {
        Random r = new Random();
        int pivotIndex = start + r.nextInt(end - start + 1);
        return partitionStable(nums, start, end, pivotIndex);
    }
    public static int partitionStable(int[] nums, int start, int end, int pivotIndex) {
        int pivot = nums[pivotIndex];
        if (nums.length <= 1) return pivotIndex;
        int i = 0, j = 0, k = 0;
        // count
        for (int l = start; l <= end; l++) {
            int a = nums[l];
            if (a < pivot) {
                i++;
            } else if (a == pivot) {
                j++;
            }
        }
        
        k = i + j;
        j = i;
        i = 0;
        int[] arr = new int[end - start + 1];
        for (int l = 0; l <= end - start; l++) {
            int a = nums[start + l];
            if (a < pivot) {
                arr[i++] = a;
            } else if (a == pivot) {
                arr[j++] = a;
            } else {
                arr[k++] = a;
            }
        }
        for (int l = 0; l <= end - start; l++) {
            nums[start + l] = arr[l];
        }
        return start + i;
    }
    
    public static void testKthSmallest() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("\n\n\n\nstarting iteration: " + i);
            int[] arr = new int[]{2, 1};
            int kth = kthOrderStatic(arr, 1);
            if (kth != 1) {
                System.out.println(kth);
                System.out.println(Arrays.toString(arr));
                System.out.println("----broken----");
                break;
            }
        }
    }
    public static void main(String[] args) {
        // testPartition();
        testKthSmallest();
    }
    
}

class KthOrderStaticV2 {
    /*
        question: kth order static (with or without dups)
        solution: randomized partitioning with modified dutch flag partition
     */
    
    public static int kthOrderStatic(int[] nums, int k) {
        return kthOrderStaticWithDups(nums, k);
    }
    public static int kthOrderStaticWithDups(int[] nums, int k) {
        int start = 0, end = nums.length - 1;
        while (k >= 1) {
            // System.out.println(k + ":" + start + ":" + end + ":" + Arrays.toString(nums));
            int pivotIndex = randomPartitioning(nums, start, end);
            // System.out.println(k + ":" + pivotIndex + "->" + start + ":" + end + ":" + Arrays.toString(nums));
            
            int countSeen = pivotIndex + 1 - start;
            if (countSeen == k) {
                return nums[pivotIndex];
            } else if (countSeen < k) {
                // kth is on right of pivot
                k = k - countSeen;
                start = pivotIndex + 1;
            } else {
                // kth is on left of pivot
                end = pivotIndex - 1;
            }
        }
        throw new IllegalArgumentException("unable to find kth-order static");
    }
    private static int randomPartitioning(int[] nums, int start, int end) {
        Random r = new Random();
        int pivotIndex = start + r.nextInt(end - start + 1);
        return partitionUnStable(nums, start, end, pivotIndex);
    }
    public static int partitionUnStable(int[] nums, int start, int end, int pivotIndex) {
        int pivot = nums[pivotIndex];
        if (nums.length <= 1) return pivotIndex;
        int i = start, j = start, k = end;
        while (j <= k) {
            int a = nums[j];
            if (a < pivot) {
                swap(nums, i++, j++);
            } else if (a == pivot) {
                j++;
            } else {
                swap(nums, k--, j);
            }
        }
        return i;
    }
    
    public static void testKthSmallest() {
        for (int i = 0; i < 100; i++) {
            System.out.println("\n\n\n\nstarting iteration: " + i);
            int[] arr = new int[]{3, 2, 1, 5, 6, 4};
            int kth = kthOrderStatic(arr, 5);
            if (kth != 5) {
                System.out.println(kth);
                System.out.println(Arrays.toString(arr));
                System.out.println("----broken----");
                break;
            }
        }
    }
    
    public static int[] topKFrequent(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int a : nums) map.put(a, map.getOrDefault(a, 0) + 1);
        int[] arr = map.values().stream().mapToInt(i -> i).toArray();
        int kth = kthOrderStatic(arr, arr.length - k + 1);
        int[] output = new int[k];
        int j = 0;
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            if (e.getValue() >= kth) {
                output[j++] = e.getKey();
                j = j % k; // keep processing as there might be dups even in top-k
            }
        }
        return output;
    }
    
    // assume arr.length > 2 and is even
    public static Integer[] minAndMax(int[] arr) {
        //if (arr.length < 1) return new Integer[]{null, null};
        //if (arr.length == 1) return new Integer[]{arr[0], arr[0]};
        Integer min = null, max = null;
        if (arr.length % 2 == 1) {
            min = max = arr[0];
        } else if (arr[0] < arr[1]) {
            min = arr[0];
            max = arr[1];
        } else {
            min = arr[1];
            max = arr[0];
        }
        int i = (arr.length % 2 == 1) ? 1 : 2;
        for (; i < arr.length; i+=2) {
            int a = arr[i], b=arr[i+1];
            if (a < b) {
                min = Math.min(min, a);
                max = Math.max(max, b);
            } else {
                min = Math.min(min, b);
                max = Math.max(max, a);
            }
        }
        return new Integer[]{min, max};
    }
    public static void testFindMinAndMax() {
        int[] arr = new int[]{4, 1, -1, 2, -1, 2, 3};
        Integer[] minMax = minAndMax(arr);
        System.out.println("min: " + minMax[0] + ", max: " + minMax[1]);
    }
    
    public static void main(String[] args) {
        // testPartition();
        // testKthSmallest();
        // topKFrequent(new int[]{4, 1, -1, 2, -1, 2, 3}, 2);
        testFindMinAndMax();
    }
    
}

class GroupBy {
    
    public static void groupBy(int[] arr, int[] groupIds) {
        if (arr.length <= 1) return;
        HashMap<Integer, Integer[]> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            int groupId = groupIds[i];
            if (!map.containsKey(groupId)) map.put(groupId, new Integer[]{0, 0, 0});
            map.get(groupId)[0] = map.get(groupId)[0] + 1;
        }
        
        int groupOffset = 0;
        for (Map.Entry<Integer, Integer[]> e : map.entrySet()) {
            e.getValue()[1] = groupOffset;
            groupOffset += e.getValue()[0];
        }
        // printMap(map);
        for (int i = 0; i < arr.length; ) {
            Integer[] destGroupInfo = map.get(groupIds[i]);
            int start = destGroupInfo[1], done = destGroupInfo[2];
            
            // check if i was already placed correctly
            if (i >= start && i < start + done) {
                i++;
            } else {
                // swap
                int destIndex = start + done;
                swap(arr, i, destIndex);
                swap(groupIds, i, destIndex);
                destGroupInfo[2]++;
            }
        }
    }
    private static void printMap(HashMap<Integer, Integer[]> map) {
        for (Map.Entry<Integer, Integer[]> e : map.entrySet()) {
            System.out.print(e.getKey() + "=" + Arrays.toString(toIntArray(e.getValue())) + "  ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int[] arr = new int[]{0, 1, 2, 3, 4, 5};
        int[] groupIds = new int[]{2, 1, 3, 3, 3, 2};
        System.out.println(Arrays.toString(arr));
        groupBy(arr, groupIds);
        System.out.println(Arrays.toString(arr));
    }
    
}

class SearchMissingAndDuplicates {
    
    /*
        - these problems inherently asks for reducing space else we can just use hashmap
        - most of the solutions are related to bits
            - bit based bucket count
                - bitset: reduce hashmap space
                - bitmask: find missing/duplicate (group based on bit diff)
         - other hacky/not so uselful algos are:
            - using equations: sum/product/squares -> easy number overflow error
            - using array as hashtable/hashset/visit cache (okayish approach)
     */
    
    class FindMissingAndDup {
        
        public int missingNumber(int[] nums) {
            int xor = 0;
            for (int i = 1; i <= nums.length; i++) xor = xor ^ nums[i - 1] ^ i;
            return xor;
        }
        
        public int[] dupAndMissing(int[] arr) {
            return dupAndMissingV1(arr);
        }
        public int[] dupAndMissingV1(int[] arr) {
            int dupXorMiss = 0;
            for (int i = 0; i < arr.length; i++) {
                dupXorMiss = dupXorMiss ^ (i + 1) ^ arr[i];
            }
            
            int diffBit = dupXorMiss & ~(dupXorMiss - 1);
            int mask1 = 0;
            for (int i = 0; i < arr.length; i++) {
                if ((arr[i] & diffBit) == 0) mask1 = mask1 ^ arr[i];
                if (((i + 1) & diffBit) == 0) mask1 = mask1 ^ (i + 1);
            }
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == mask1) return new int[]{mask1, dupXorMiss ^ mask1};
            }
            return new int[]{dupXorMiss ^ mask1, mask1};
        }
        
        public int[] dupAndMissingV2(int[] arr) {
            int dup = 0, missing = 0;
            for (int i = 0; i < arr.length; i++) {
                int e = Math.abs(arr[i]);
                if (arr[e - 1] < 0) {
                    dup = e;
                } else {
                    arr[e - 1] = -arr[e - 1];
                }
            }
            
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] > 0) {
                    missing = i + 1;
                } else {
                    arr[i] = -arr[i];
                }
            }
            
            return new int[]{dup, missing};
        }
        
    }
    
    class FindAnyMissing {
        /*
            question: given 1 billion 32 bit ip address, find a missing ip address(32 bit)
                      constraints: S(kilo bytes) and T(2 scans)
                      note that there could be 1000s of missing up addresses, so xor won't work
            answer:
                sol1: hashmap - S(4 GB) / bitset (0.5 GB)
                sol2: bit masking based split: 32 passes
                sol3: bucket count and bitset
                    • 16-MSB based bucket counting - 2^16 numbers -> 256 kb
                    • 16-LSB BitSet - 16 bits
                    • S(0.25 MB), T(2 scans)
         */
        public Integer findAnyMissingInBillionNumbers(int[] arr) {
            // 16-MSB based bucket count
            int numberOfBuckets = 1 << 16;
            int[] bucketCounts = new int[numberOfBuckets];
            for (int a : arr) {
                int bucketNumber = a >>> 16;
                bucketCounts[bucketNumber]++;
            }
            
            // find any bucket with missing number and bitmask
            Integer bucketWithMissingNumber = null;
            for (int i = 0; i < bucketCounts.length; i++) {
                if (bucketCounts[i] < numberOfBuckets) {
                    bucketWithMissingNumber = i;
                    break;
                }
            }
            if (bucketWithMissingNumber == null) return null;
            
            // scan again and use 2^16 bits in BitSet/Hashmap
            BitSet bitSet = new BitSet(1 << 16);
            for (int a : arr) {
                if (a >>> 16 == bucketWithMissingNumber) {
                    bitSet.set((a << 16) >>> 16);
                }
            }
            // not find missing number
            for (int i = 0; i < (1 << 16); i++) {
                if (!bitSet.get(i)) {
                    return bucketWithMissingNumber << 16 | i;
                }
            }
            return null;
        }
        
    }
    
}
