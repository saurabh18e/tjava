package com.practice.ds.greedy;

import java.util.Arrays;

public class Leet1 {
    
    private static class Ques1 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques2 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques3 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques4 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques5 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques6 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques7 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques8 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques9 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques10 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    
}
