package com.practice.ds.greedy;

import java.util.*;

public class EPI {
    
    private static class TaskPairing {
        /*
            question:
                EPI 18.1
            algo:
                greedily it makes sense to pair up longest with smallest and then recurse(optimal substructure)
                reasoning: contradiction: lets pair up longest with anything task x, then replacing x with smallest reduces time
                optimal substructure: iterative solve subproblem and then add...again prove with contradiction
         */
        
        private static class TaskPair {
            public int task1, task2 = -1;
    
            public TaskPair(int task1, int task2) {
                this.task1 = task1;
                this.task2 = task2;
            }
        }
        
        public static List<TaskPair> taskPairing(int[] arr) {
            Arrays.sort(arr);
            List<TaskPair> taskPairs = new ArrayList<>();
            for (int i=0, j=arr.length-1; i<j; i++, j--) {
                taskPairs.add(new TaskPair(arr[i], arr[j]));
            }
            return taskPairs;
        }
        
        private static void print(int[] arr) {
            List<TaskPair> taskPairs = taskPairing(arr);
            System.out.println("optial pairing of tasks is: ");
            for (TaskPair taskPair: taskPairs) System.out.println("[" + taskPair.task1 + ", " + taskPair.task2 + "]");
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class MinWaitingTime {
        /*
            question:
                EPI 18.2
            algo:
                greedily choose the shortest task
         */
        
        public static int minTotalWaitingTime(int[] arr) {
            if (arr == null || arr.length == 0) return 0;
            Arrays.sort(arr);
            int totalWaitingTime = 0;
            int waitingTimeSoFar = 0;
            for (int t : arr) {
                // process current task (wait)
                int currentTaskTime = waitingTimeSoFar + t;
                // update state
                totalWaitingTime += currentTaskTime;
                waitingTimeSoFar += t;
            }
            return totalWaitingTime;
        }
        
        private static void print(int[] arr) {
            System.out.println("min waiting time for task list: " + Arrays.toString(arr) + " is: " + minTotalWaitingTime(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class IntervalCovering {
        /*
            question:
                EPI 18.3
            algo:
                CP(S) = min (end time of all intervals) + CP(S - all non overlapping intervals)
                ....this means we should sort by end time or use heap
         */
        
        public static class Interval {
            public int start, end;
            
            public Interval(int start, int end) {
                this.start = start;
                this.end = end;
            }
            
        }
        
        public static List<Integer> minPointsToCoverIntervals(int[][] arr) {
            if (arr == null || arr.length == 0) return Collections.emptyList();
            
            List<Interval> intervals = new ArrayList<>();
            for (int[] e: arr) intervals.add(new Interval(e[0], e[1]));
            
            PriorityQueue<Interval> minHeap = new PriorityQueue<>(new Comparator<Interval>() {
                @Override
                public int compare(Interval o1, Interval o2) {
                    return Integer.compare(o1.end, o2.end);
                }
            });
            minHeap.addAll(intervals);
            
            //Collections.sort(intervals, new Comparator<Interval>() {
            //    @Override
            //    public int compare(Interval o1, Interval o2) {
            //        return Integer.compare(o1.end, o2.end);
            //    }
            //});
    
            List<Integer> coverPoints = new ArrayList<>();
            int currentCoverPoint = minHeap.peek().end;
            coverPoints.add(currentCoverPoint);
            while (!minHeap.isEmpty()) {
                if (minHeap.peek().start <= currentCoverPoint) {
                    minHeap.remove();
                } else {
                    currentCoverPoint = minHeap.peek().end;
                    coverPoints.add(currentCoverPoint);
                }
            }
            
            return coverPoints;
        }
        
        private static void printCoverPoints(int[][] arr) {
            List<Integer> coverPoints = minPointsToCoverIntervals(arr);
            System.out.println("\nfound " + coverPoints.size() + " cover points: ");
            for (Integer e: coverPoints) System.out.print(e + " ");
        }
        
        public static void main(String[] args) {
            printCoverPoints(new int[][]{{1, 1}, {1, 1}, {1, 1}, {1, 1}, {1, 1}});
            printCoverPoints(new int[][]{{1, 2}, {2, 3}, {4, 7}, {3, 5}, {6, 8}});
        }
    }
    
    private static class Ques4 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
    private static class Ques5 {
        /*
            question:
            
            algo:
            
         */
        
        public static int answer(int[] arr) {
            return 0;
        }
        
        private static void print(int[] arr) {
            System.out.println("min : " + Arrays.toString(arr) + " is: " + answer(arr));
        }
        
        public static void main(String[] args) {
            print(new int[]{5, 8, 3, 9, 2, 4});
        }
    }
    
}
