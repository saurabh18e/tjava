package com.practice.ds.graph.prob.clone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

// ref: https://leetcode.com/explore/interview/card/facebook/52/trees-and-graphs/277/

class Node {
    public int val;
    public List<Node> neighbors;
    
    public Node() {
        this(-1);
    }
    public Node(int val) {
        this(val, new ArrayList<Node>());
    }
    public Node(int _val, ArrayList<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
    @Override
    public String toString() {
        return "Node{" +
                "val=" + val +
                ", neighbors=" + neighbors +
                '}';
    }
}

class State {
    HashMap<Integer, Node> visited;
    
    public State() {
        this.visited = new HashMap<Integer, Node>();
    }
}

public class Clone {
    
    public static Node cloneGraph(Node node) {
        if (node == null) return null;
        return dfsClone(node, new State());
    }
    
    public static Node dfsClone(Node node, State state) {
        if (state.visited.containsKey(node.val)) {
            return state.visited.get(node.val);
        } else {
            Node nnd = new Node(node.val);
            state.visited.put(nnd.val, nnd);
            node.neighbors.forEach(n -> {
                nnd.neighbors.add(dfsClone(n, state));
            });
            return nnd;
        }
    }
    
    public static void main(String[] args) {
        // [[2,4],[1,3],[2,4],[1,3]]
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
    
        n1.neighbors.add(n2);
        n1.neighbors.add(n4);
        n2.neighbors.add(n1);
        n2.neighbors.add(n3);
        n3.neighbors.add(n2);
        n3.neighbors.add(n4);
        n4.neighbors.add(n1);
        n4.neighbors.add(n3);
        
        Node n1C = cloneGraph(n1);
        // System.out.println(n1C);
    }
}
