package com.practice.ds.graph.prob.island;

import java.util.LinkedList;
import java.util.Queue;

class Tuple {
    public int first;
    public int second;
    
    public Tuple(int first, int second) {
        this.first = first;
        this.second = second;
    }
}

public class IslandCount {
    
    public int numIslands(char[][] grid) {
        int islandCount = 0;
        if (grid != null && grid.length > 0) {
            int rows = grid.length;
            int cols = grid[0].length;
            for (int i=0; i<rows; i++) {
                for (int j=0; j<cols; j++) {
                    if (grid[i][j] == '1') {
                        islandCount++;
                        Queue<Tuple> q = new LinkedList();
                        checkAndVisit(grid, q, i, j, rows, cols);
                        bfs(grid, q, rows, cols);
                    }
                }
            }
        }
        return islandCount;
    }
    
    private void bfs(char[][] grid, Queue<Tuple> q, int rows, int cols) {
        while (!q.isEmpty()) {
            Tuple tpl = q.remove();
            int i = tpl.first;
            int j = tpl.second;
            checkAndVisit(grid, q, i-1, j, rows, cols);
            checkAndVisit(grid, q, i+1, j, rows, cols);
            checkAndVisit(grid, q, i, j-1, rows, cols);
            checkAndVisit(grid, q, i, j+1, rows, cols);
        }
    }
    private void checkAndVisit(char[][] grid, Queue<Tuple> q, int i, int j, int rows, int cols) {
        if (i >= 0 && i < rows && j >=0 && j < cols && grid[i][j] == '1') {
            grid[i][j] = '0';
            q.add(new Tuple(i, j));
        }
    }
    
}
