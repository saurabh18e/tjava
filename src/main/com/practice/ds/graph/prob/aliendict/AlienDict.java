package com.practice.ds.graph.prob.aliendict;

import java.util.*;

class State {
    public Map<Character, Integer> color = new HashMap<>();
    public LinkedList<Character> topologicalOrder = new LinkedList<>();
}

class Graph {
    Map<Character, ArrayList<Character>> edges = new HashMap<>();
    
    public void addEdge(char u, char v) {
        addVertex(u);
        edges.get(u).add(v);
    }
    public void addVertex(char u) {
        if (!edges.containsKey(u)) edges.put(u, new ArrayList<>());
    }
}

public class AlienDict {
    
    public static String alienOrder(String[] words) {
        if (words == null || words.length == 0) return "";
        Graph g = new Graph();
        for (int i = 0; i < words.length; i++) {
            char[] currWord = words[i].toCharArray();
            for (char c : currWord) g.addVertex(c);
            if (i > 0) {
                char[] prevWord = words[i-1].toCharArray();
                int len = Math.min(currWord.length, prevWord.length);
                for (int j = 0; j < len; j++) {
                    if (currWord[j] != prevWord[j]) {
                        g.addEdge(prevWord[j], currWord[j]);
                        break;
                    }
                }
            }
        }
    
        State state = dfsForest(g);
        if (state == null) return "";
        StringBuilder buff = new StringBuilder();
        for(char ch : state.topologicalOrder) buff.append(ch);
        return buff.toString();
    }
    
    public static State dfsForest(Graph g) {
        State state = new State();
        for (char  u : g.edges.keySet()) {
            if (!state.color.containsKey(u)) {
                if (dfs(u, g, state)) return null;
            }
        }
        return state;
    }
    
    public static boolean dfs(char u, Graph g, State state) {
        state.color.put(u, 1);
        for (char v : g.edges.get(u)) {
            if (!state.color.containsKey(v)) {
                if (dfs(v, g, state)) return true;
            } else {
                // grey node i.e. back edge means cycle is detected
                if (state.color.get(v) == 1) return true;
            }
        }
        state.color.put(u, 2);
        state.topologicalOrder.add(0, u);
        return false;
    }
    
    public static void main(String[] args) {
        // happy path
        String[] words1 = {"wrt","wrf","er","ett","rftt"};
        System.out.println("words1: " + Arrays.toString(words1));
        String result1 = alienOrder(words1);
        System.out.println("result: " + result1 + "\n");
        assert(result1.equals("wertf"));
    
        // cyclic
        String[] words2 = {"z","x","z"};
        System.out.println("words2: " + Arrays.toString(words2));
        String result2 = alienOrder(words2);
        System.out.println("result: " + alienOrder(words2) + "\n");
        if(!alienOrder(words2).equals("")) {
            throw new RuntimeException(" != ");
        }
    
        // forest
        String[] words3 = {"abc","ab"};
        System.out.println("words3: " + Arrays.toString(words3));
        System.out.println("result: " + alienOrder(words3) + "\n");
        assert(alienOrder(words3).equals("abc"));
        
        // forest
        String[] words4 = {"zx","zy"};
        System.out.println("words4: " + Arrays.toString(words4));
        System.out.println("result: " + alienOrder(words4) + "\n");
        assert(alienOrder(words4).equals("zxy"));
    }
}
