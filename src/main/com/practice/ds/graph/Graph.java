package com.practice.ds.graph;


public class Graph {

}

/*
  Theory Questions
  -----------------
  - question
    calculate in-degree and out-degree of each node in a graph
  - solution
    out-degree: sum of each row
    in-degree: sum of each column

  - question
    how would you inverse a graph i.e. reverse each edge in a graph given matrix representation
  - solution
    calculate transpose of the matrix

  - question
    celebrity problem/universal sink: given adjacency matrix, find the celebrity in linear time
    i.e. node which has an incoming edge from every other node, but that node has no outgoing edge
  - solution
    traverse the matrix smartly: at each iteration we can discard a node, hence atmost 0(V) lookups will be needed


  Bipartite Graph
  ----------------
  - question
    given n wrestlers with r rivalries, find of it's possible to divide them into 2 groups
    such that all rivalries are between wrestlers of opposite group
  - solution
    use bfs and at each level assign opposite color, if u find a child node with color of current node, then it's not possible
    note: since it can be forest, so do above till you have unvisited nodes

  Diameter of a graph
  --------------------
  - question
    find diameter of a graph: max of min distance between any two nodes
  - solution
    to find distances, use bfs till there are unvisited node
    then calculate diameter of each tree using bottom up recursion
    take max of diameters of all trees

  Word Ladder
  ------------
  
  - given a dictionary of strings of same length, find length of shortest chain to convert
    a given string into another by following a path of 1 char change strings
  - ref: https://www.geeksforgeeks.org/word-ladder-length-of-shortest-chain-to-reach-a-target-word/
  - method: create a graph of 1-edit distance and do bfs for shortest path

 */
