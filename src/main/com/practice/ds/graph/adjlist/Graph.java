package com.practice.ds.graph.adjlist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class Vertex {
    public int id;
    public int parent;
    public Color color;
    public int distance;
    public int startTime;
    public int finishTime;
    public int root;
    
    public Vertex(int id) {
        this.id = id;
        clearState();
    }
    public void clearState() {
        updateState(-1, Color.White, -1, -1, -1);
    }
    public void updateState(int parent, Color color, int distance, int time, int root) {
        this.parent = parent;
        this.color = color;
        this.distance = distance;
        
        if (this.color == Color.White) {
            this.startTime = this.finishTime = time;
        } else if (this.color == Color.Grey) {
            this.startTime = time;
        } else if (this.color == Color.Black) {
            this.finishTime = time;
        }
        
        this.root = root;
    }
    
    @Override
    public String toString() {
        return "Vertex[id=" + id + ", parent=" + parent + ", root=" + root +
                ", color=" + color + ", distance=" + distance
                + ", startTime=" + startTime + ", finishTime=" + finishTime+ ']';
    }
}

class State {
    
    public int vertexCount;
    public Vertex[] vertices;
    public int time;
    
    public State(int vertexCount) {
        this.vertexCount = vertexCount;
        this.vertices = new Vertex[vertexCount];
        for (int i = 0; i < vertexCount; i++) {
            this.vertices[i] = new Vertex(i);
        }
        this.time = 0;
    }
}

enum Color {White, Grey, Black}

public class Graph {
    
    int vertexCount;
    public ArrayList<Integer>[] edges;
    
    public Graph(int vertexCount) {
        this.vertexCount = vertexCount;
        this.edges = new ArrayList[vertexCount];
        for (int i = 0; i < vertexCount; i++) {
            this.edges[i] = new ArrayList();
        }
    }
    
    public void deleteEdge(int i, int j) {
        this.edges[i].removeIf(e -> e == j);
    }
    public void addEdge(int i, int j) {
        this.edges[i].add(j);
    }
    public void addEdges(int[][] newEdges) {
        for (int i = 0; i < newEdges.length; i++) {
            addEdge(newEdges[i][0], newEdges[i][1]);
        }
    }
    public void addEdgeUndirected(int i, int j) {
        addEdge(i, j);
        addEdge(j, i);
    }
    public void addEdgesUndirected(int[][] newEdges) {
        for (int i = 0; i < newEdges.length; i++) {
            addEdgeUndirected(newEdges[i][0], newEdges[i][1]);
        }
    }
    
    public State bfs() {
        return bfsForest();
    }
    public State bfsForest() {
        State state = new State(this.vertexCount);
        for (int i = 0; i < vertexCount; i++) {
            Vertex u = state.vertices[i];
            if (u.color == Color.White) {
                Queue<Vertex> queue = new LinkedList<>();
                u.updateState(-1, Color.Grey, 0, state.time++, i);
                queue.add(u);
                bfsTree(queue, state);
            }
        }
        return state;
    }
    
    public void bfsTree(Queue<Vertex> queue, State state) {
        while (!queue.isEmpty()) {
            Vertex u = queue.remove();
            this.edges[u.id].forEach(i -> {
                Vertex v = state.vertices[i];
                if (v.color == Color.White) {
                    v.updateState(u.id, Color.Grey, u.distance + 1, state.time++, u.root);
                    queue.add(v);
                }
            });
            u.updateState(u.parent, Color.Black, u.distance, state.time++, u.root);
        }
    }
    
    public State dfs() {
        return dfsForest();
    }
    public State dfsForest() {
        State state = new State(this.vertexCount);
        for (int i = 0; i < vertexCount; i++) {
            Vertex u = state.vertices[i];
            if (u.color == Color.White) {
                dfsTree(u, null, state);
            }
        }
        return state;
    }
    public void dfsTree(Vertex u, Vertex p, State state) {
        int parent = (p != null) ? p.id : -1;
        int distance = (p != null) ? p.distance + 1 : 0;
        int root = (p != null) ? p.root : u.id;
        
        u.updateState(parent, Color.Grey, distance, state.time++, root);
        this.edges[u.id].forEach(i -> {
            Vertex v = state.vertices[i];
            if (v.color == Color.White) {
                dfsTree(v, u, state);
            }
        });
        u.updateState(parent, Color.Black, distance, state.time++, root);
    }
    
    public void print() {
        int n = this.vertexCount;
        for (int i = 0; i < n; i++) {
            System.out.println( i + ": " + this.edges[i]);
        }
    }
    public void print(State state) {
        for (int i = 0; i < state.vertexCount; i++) {
            System.out.println(state.vertices[i]);
        }
    }
    public ArrayList pathFromRoot(int id, State state) {
        ArrayList path = new ArrayList();
        while (id != -1) {
            Vertex u = state.vertices[id];
            path.add(0, u.id);
            id = u.parent;
        } ;
        return path;
    }
    public void printPathFromRoot(int id, State state) {
        System.out.println("path from root for vertex " + id + " is: " + pathFromRoot(id, state));
    }
}
