package com.practice.ds.heap.finale;

import java.util.*;

public class Medium {
}

class SortAlmostSortedList {
        /*
            question:
                EPI 11.3
            algo:
                track k+1 elments in min-heap
         */
    
    public static List<Integer> sort1(List<Integer> arr, int k) {
        if (k < 0 || arr == null || arr.size() == 0) return arr;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>(Comparator.naturalOrder());
        List<Integer> sortedList = new ArrayList<>(arr.size());
        for (Integer e : arr) {
            minHeap.add(e);
            if (minHeap.size() > k) {
                sortedList.add(minHeap.remove());
            }
        }
        while (!minHeap.isEmpty()) sortedList.add(minHeap.remove());
        return sortedList;
    }
    
    public static int[] sort(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k <= 0) return arr;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i = 0; i < arr.length || !minHeap.isEmpty(); i++) {
            if (i < arr.length) {
                minHeap.add(arr[i]);
            }
            if (i >= k) {
                arr[i - k] = minHeap.remove();
            }
        }
        return arr;
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{2, 3, 1, 5, 6, 4, 8, 7};
        System.out.println("sorted version of list : " +
            Arrays.toString(arr) + " is:\n " + Arrays.toString(sort(arr, 2)));
    }
}

class StreamWithMedian {
    
    // invariant: in case of odd size, first half will have the extra element
    PriorityQueue<Integer> firstHalf = new PriorityQueue<>(Comparator.reverseOrder());
    PriorityQueue<Integer> secondHalf = new PriorityQueue<>(Comparator.naturalOrder());
    
    public StreamWithMedian() {
    
    }
    
    // efficient but more code
    // choose based on incoming data value
    public void add(int num) {
        // check whether to add to first or second half
        if (firstHalf.isEmpty() || num <= firstHalf.peek()) {
            firstHalf.add(num);
            // ensure invariant
            if (firstHalf.size() > secondHalf.size() + 1) {
                secondHalf.add(firstHalf.remove());
            }
        } else {
            secondHalf.add(num);
            // ensure invariant
            if (secondHalf.size() > firstHalf.size()) {
                firstHalf.add(secondHalf.remove());
            }
        }
    }
    
    // invariant: 0 <= first half size - second half size <= 1
    // choose based on heap sizes
    public void add1(int e) {
        if (firstHalf.size() == secondHalf.size()) {
            // if size are same, then first half size has to be increased
            // we need to add new min to firstHalf: min(e, secondHalf)
            secondHalf.add(e);
            firstHalf.add(secondHalf.remove());
        } else {
            // if size are not same, then second half size has to be increased
            // we need to add new max to secondHalf: max(e, maxHeap)
            firstHalf.add(e);
            secondHalf.add(firstHalf.remove());
        }
    }
    
    // brute force: not efficient
    public void add2(int num) {
        // check with first half
        firstHalf.add(num);
        // check the new max with second half
        secondHalf.add(firstHalf.remove());
        // maintain invariant
        if (secondHalf.size() > firstHalf.size()) {
            firstHalf.add(secondHalf.remove());
        }
    }
    
    public double median() {
        if (firstHalf.size() == 0) throw new IllegalArgumentException("cannot find median of empty stream");
        if (firstHalf.size() == secondHalf.size()) {
            return ((firstHalf.peek() + secondHalf.peek()) / 2.0);
        } else {
            return firstHalf.peek();
        }
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{5, 8, 3, 9, 2, 4};
        StreamWithMedian stream = new StreamWithMedian();
        for (int e : arr) stream.add(e);
        System.out.println("median of stream : " + Arrays.toString(arr) + " is: " + stream.median());
    }
    
}

class KLargestInMaxHeap {
    
    public static List<Integer> kLargest(int[] arr, int k) {
        List<Integer> list = new ArrayList<>();
        if (arr.length <= k) {
            for (int i : arr) list.add(i);
            return list;
        }
        
        PriorityQueue<Integer> auxMaxHeap = new PriorityQueue<>((i, j) -> Integer.compare(arr[j], arr[i]));
        auxMaxHeap.add(0);
        while (list.size() < k) {
            int p = auxMaxHeap.remove();
            list.add(arr[p]);
            if (2 * p + 1 < arr.length) auxMaxHeap.add(2 * p + 1); // left child
            if (2 * p + 2 < arr.length) auxMaxHeap.add(2 * p + 2); // right child
        }
        
        return list;
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{561, 314, 401, 28, 156, 359, 271, 11, 3};
        int k = 5;
        
        System.out.println("k-largest in max-heap are: ");
        for (int e : kLargest(arr, k)) {
            System.out.print(e + " ");
        }
        
    }
}

class StackQueueUsingHeap {
    
    static class HeapNode implements Comparable<HeapNode> {
        public int data, priority;
        
        public HeapNode(int data, int priority) {
            this.data = data;
            this.priority = priority;
        }
        
        @Override
        public int compareTo(HeapNode other) {
            return Integer.compare(this.priority, other.priority);
        }
        
        @Override
        public String toString() {
            return "[" + data + ", " + priority + "]";
        }
    }
    
    static class StackUsingHeap {
        
        PriorityQueue<HeapNode> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());
        // private int insertTimestamp;
        
        public void push(int data) {
            // maxHeap.add(new StackEntry(i, insertTimestamp++));
            maxHeap.add(new HeapNode(data, maxHeap.size() + 1));
        }
        
        public int pop() {
            if (maxHeap.isEmpty()) throw new IllegalArgumentException("cannot pop from empty stack");
            
            //StackEntry e = maxHeap.remove();
            //// take care of int overflow
            //if (maxHeap.isEmpty()) {
            //    insertTimestamp = 0;
            //}
            //return e.value;
            
            return maxHeap.remove().data;
        }
        
        public static void main(String[] args) {
            StackUsingHeap stack = new StackUsingHeap();
            stack.push(5);
            stack.push(8);
            stack.push(3);
            System.out.println(stack.pop());
            System.out.println(stack.pop());
            System.out.println(stack.pop());
        }
        
    }
    
}
