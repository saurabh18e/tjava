package com.practice.ds.heap.finale;

import java.util.*;
import java.util.stream.Collectors;

public class Easy {
}

class RepeatedMinMax {
    
    public int lastStoneWeight(int[] stones) {
        if (stones == null || stones.length == 0) return 0;
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());
        for (int i : stones) maxHeap.add(i);
        while (maxHeap.size() > 1) {
            int x = maxHeap.remove();
            int y = maxHeap.remove();
            if (x != y) maxHeap.add(x - y);
        }
        return maxHeap.isEmpty() ? 0 : maxHeap.peek();
    }

}

class TopKInStream {
    
    private static class LongestKStrings {
    
        // note: if array is given then it's better to use kth order static
        public static List<String> longestK(List<String> list, int k) {
            if (list == null || list.size() <= k) return list;
            if (k <= 0) return Collections.emptyList();
            
            PriorityQueue<String> maxHeap = new PriorityQueue<>(new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return Integer.compare(s2.length(), s1.length());
                }
            });
            
            for (String s : list) {
                maxHeap.add(s);
                if (maxHeap.size() > k) maxHeap.remove();
            }
            
            return new ArrayList<>(maxHeap);
        }
    
    }
    
    private static class StreamWithKthLargest {
        /*
            question:
                https://leetcode.com/problems/kth-largest-element-in-a-stream/
            algo:
                maintain k-largest so far and be ready to remove the min of these k-largest
         */
        
        private PriorityQueue<Integer> minHeap = new PriorityQueue<>(Comparator.naturalOrder());
        private int k;
        
        public StreamWithKthLargest(int k) {
            if (k <= 0) throw new IllegalArgumentException("k should be greater than 0");
            this.k = k;
        }
        
        public void add(int e) {
            if (minHeap.size() < k || e >= minHeap.peek()) {
                minHeap.add(e);
            }
            if (minHeap.size() > k) minHeap.remove();
        }
        
        public int kthLargest() {
            if (minHeap.size() < k) throw new IllegalArgumentException("cannot find kth max in stream of size less than k");
            return minHeap.peek();
        }
        
        private static void printKthLargest(int[] arr, int k) {
            StreamWithKthLargest stream = new StreamWithKthLargest(k);
            for (int e : arr) stream.add(e);
            System.out.println(k + " largest of : " + Arrays.toString(arr) + " is: " + stream.kthLargest());
            Arrays.sort(arr);
            assert (arr[k] == stream.kthLargest());
        }
        
        public static void main(String[] args) {
            for (int i = 1; i <= 6; i++) {
                printKthLargest(new int[]{5, 8, 3, 9, 2, 4}, i);
            }
        }
    }
    
    private static class KClosest2D {
        // note that this code is pretty generic and can be extended to to N-dimension
        
        /*
            question:
                https://leetcode.com/explore/featured/card/leetcodes-interview-crash-course-data-structures-and-algorithms/708/heaps/4642/
            algo:
                use maxHeap
         */
        
        public static double distance(int[] p) {
            return Math.sqrt(Math.pow(p[0], 2) + Math.pow(p[1], 2));
        }
        
        public static int[][] kClosest(int[][] points, int k) {
            PriorityQueue<int[]> maxHeap = new PriorityQueue<>((p1, p2) -> Double.compare(distance(p2), distance(p1)));
            for (int[] point : points) {
                maxHeap.add(point);
                if (maxHeap.size() > k) maxHeap.remove();
            }
            int[][] res = new int[maxHeap.size()][2];
            int n = maxHeap.size();
            for (int i = 0; i < n; i++) res[i] = maxHeap.remove();
            return res;
        }
        
        private static void printKClosest(int[][] arr, int k) {
            System.out.println(k + " closest point are : ");
            for (int[] a : kClosest(arr, k)) System.out.println(a[0] + " " + a[1]);
        }
        
        public static void main(String[] args) {
            for (int i = 0; i < 5; i++) {
                printKClosest(new int[][]{{2, 3}, {5, 6}, {1, 0}, {11, 6}}, i);
            }
        }
    }
    
    private static class KClosest3D {
        
        static class Star implements Comparable<Star> {
            public int x, y, z;
            
            public Star(int x, int y, int z) {
                this.x = x;
                this.y = y;
                this.z = z;
            }
            
            @Override
            public int compareTo(Star other) {
                return Double.compare(distance(this), distance(other));
            }
            
            public static double distance(Star other) {
                return Math.sqrt(Math.pow(other.x, 2) + Math.pow(other.y, 2) + Math.pow(other.z, 2));
            }
            
            @Override
            public String toString() {
                return "Start[" + x + ", " + y + ", " + z + "]";
            }
        }
        
        public static List<Star> closestStars(List<Star> stars, int k) {
            if (k <= 0 || stars == null || stars.isEmpty()) return Collections.emptyList();
            
            PriorityQueue<Star> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());
            for (Star star : stars) {
                maxHeap.add(star);
                if (maxHeap.size() > k) maxHeap.remove();
            }
            
            //List<Star> closest = new ArrayList<>(maxHeap);
            //Collections.sort(closest);
            List<Star> closest = new ArrayList<>(k);
            while (!maxHeap.isEmpty()) closest.add(maxHeap.remove());
            return closest;
        }
        
        private static void printClosestStars(int[][] arr, int k) {
            List<Star> stars = Arrays.stream(arr).map(e -> new Star(e[0], e[1], e[2])).collect(Collectors.toList());
            System.out.println("distance of input stars: ");
            for (Star star : stars) {
                System.out.println(star + " -> " + Star.distance(star));
            }
            System.out.println(k + " closest stars are : ");
            for (Star star : closestStars(stars, k)) {
                System.out.println(star);
            }
        }
        
        public static void main(String[] args) {
            printClosestStars(new int[][]{{0, 0, 1}, {6, 5, 7}, {-3, 0, 1},
                {10, 5, 1}, {1, 0, -1}, {-1, 4, 1}}, 3);
        }
        
    }
    
    private static class KFrequentWords {
        
        public static List<String> topKFrequentWords(String[] words, int k) {
            if (k <= 0 || words == null || words.length == 0) return Collections.emptyList();
            
            // count frequency for each word using hashmap
            // use minheap to find top k using custom comparator
            
            HashMap<String, Integer> wordFrequency = new HashMap<>();
            for (String word : words) wordFrequency.put(word, wordFrequency.getOrDefault(word, 0) + 1);
            
            // note: we can use kth order static here for optimization but that will require more space
            
            //PriorityQueue<String> minHeap = new PriorityQueue<>(new Comparator<String>() {
            //    @Override
            //    public int compare(String str1, String str2) {
            //        int cmp1 = Integer.compare(wordCount.get(str1), wordCount.get(str2));
            //        return (cmp1 != 0) ? cmp1 : str2.compareTo(str1);
            //    }
            //});
            PriorityQueue<String> minHeap = new PriorityQueue<>((w1, w2) -> {
                int cmp1 = Integer.compare(wordFrequency.get(w1), wordFrequency.get(w2));
                return (cmp1 != 0) ? cmp1 : w2.compareTo(w1); // use reverse lex since will reverse
            });
            
            for (String word : wordFrequency.keySet()) {
                minHeap.add(word);
                if (minHeap.size() > k) minHeap.remove();
            }
            
            // List<String> list = new ArrayList<>(minHeap); // elements get added in min-heap order
            List<String> list = new ArrayList<>(minHeap.size());
            while (!minHeap.isEmpty()) list.add(minHeap.remove());
            Collections.reverse(list); // we need decreasing order
            return list;
        }
    }
}

class HeapsInGreedy {

    // greedy often needs repeated min/max -> use heap
    
    public int halveArray(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        
        double sum = 0;
        PriorityQueue<Double> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());
        for (int i : nums) {
            sum += i;
            maxHeap.add((double)i);
        }
        double target = sum / 2.0;
        int count = 0;
        while (target < sum) {
            double half = maxHeap.remove()/2.0;
            sum -= half;
            maxHeap.add(half);
            count++;
        }
        
        return count;
    }
    
    public int connectSticks(int[] arr) {
        if (arr == null || arr.length == 0) return 0;
        
        int totalMinCostSoFar = 0;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i: arr) minHeap.add(i);
        while (minHeap.size() > 1) {
            int stickJoinCost = minHeap.remove() + minHeap.remove();
            minHeap.add(stickJoinCost);
            totalMinCostSoFar = totalMinCostSoFar + stickJoinCost;
        }
        
        return totalMinCostSoFar;
    }
    
    public int minStoneSum(int[] piles, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());
        for (int p : piles) maxHeap.add(p);
        while (k-- > 0) maxHeap.add((int)Math.ceil(maxHeap.remove()/2.0));
        int sum = 0;
        while (!maxHeap.isEmpty()) sum += maxHeap.remove();
        return sum;
    }
    
}