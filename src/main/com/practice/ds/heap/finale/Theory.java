package com.practice.ds.heap.finale;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Theory {
    
    public static class MaxHeap {
        /*
          Heap
          ----
          - a complete binary tree modeled in an array
          - nearly complete binary tree
          - each element >= all of it's children
          - root element is always the max/min
       */
        
        private int[] arr;
        private int size;
        
        public MaxHeap() {
            this(new int[]{});
        }
        
        public MaxHeap(int[] arr) {
            this(arr, arr.length);
        }
        
        public MaxHeap(int[] arr, int length) {
            this.arr = arr;
            this.size = length;
        }
        
        // indexing base functions
        public static int leftChild(int i) {
            return 2 * i + 1;
        }
        
        public static int rightChild(int i) {
            return 2 * i + 2;
        }
        
        public static int parent(int i) {
            return (i - 1) / 2;
        }
        
        // shift down and shift up
        public void shiftDown(int index) {
            if (index >= size) return;
            int maxIndex = index;
            if (leftChild(index) < size) {
                maxIndex = arr[index] < arr[leftChild(index)] ? leftChild(index) : maxIndex;
            }
            if (rightChild(index) < size) {
                maxIndex = arr[maxIndex] < arr[rightChild(index)] ? rightChild(index) : maxIndex;
            }
            if (index != maxIndex) {
                int tmp = arr[index];
                arr[index] = arr[maxIndex];
                arr[maxIndex] = tmp;
                shiftDown(maxIndex);
            }
        }
        
        public void shiftUp(int i) {
            if (i <= 0) return;
            int p = parent(i);
            if (arr[p] >= arr[i]) return;
            int tmp = arr[p];
            arr[p] = arr[i];
            arr[i] = tmp;
            shiftUp(p);
        }
        
        // functions for building heap and doing heapsort
        // prove : build heap is T(n) and not T(n lg n)
        public void buildHeap() {
            for (int i = size / 2; i >= 0; i--) shiftDown(i);
        }
        
        public void heapSort() {
            buildHeap();
            int n = this.size; // save original size
            for (int i = 0; i < n - 1; i++) {
                this.arr[n - 1 - i] = extractMax();
            }
            this.size = n; // restore original size
        }
        
        // insert and remove/get max functions needed for priority queue
        public int extractMax() {
            if (size == 0) throw new NoSuchElementException("cannot extract max from empty heap");
            int max = arr[0];
            arr[0] = arr[--size];
            shiftDown(0);
            return max;
        }
        
        public int max() {
            if (size == 0) throw new NoSuchElementException("cannot find max in empty heap");
            return arr[0];
        }
        
        public void add(int i) {
            if (size == arr.length) {
                int[] buff = new int[arr.length * 2];
                for (int j = 0; j < arr.length; j++) buff[j] = arr[j];
                arr = buff;
            }
            arr[size] = i;
            shiftUp(size);
            size++;
        }
        
        public static void main(String[] args) {
            int[] arr = new int[]{5, 0, 1, 3, 2, 0, 6};
            MaxHeap heap = new MaxHeap(arr, 5);
            System.out.println("Before Heap Sort: " + Arrays.toString(heap.arr));
            heap.buildHeap();
            System.out.println("After Heapify: " + Arrays.toString(heap.arr));
            heap.heapSort();
            System.out.println("After Heap Sort: " + Arrays.toString(heap.arr));
        }
        
    }
    
    public static class PQ{
        /*
            - Built on top of heap data structure
            - added functionality: ExtractMax, IncreaseKey, InsertKey

            - Note: for using heaps when size will change, it's better to use dynamic arrays(ArrayList) rather than array
         */
        
        private MaxHeap heap;
        
        public PQ(int[] arr) {
            this.heap = new MaxHeap(arr);
            this.heap.buildHeap();
        }
        
        public static void main(String[] args) {
            PQ q = new PQ(new int[]{5, 6, 1, 2, 3});
            System.out.println("Heap Before Delete: " + Arrays.toString(q.heap.arr));
            for (int i = 0; i < 5; i++) {
                System.out.println("Heap After Delete[" + i + "]: " + Arrays.toString(q.heap.arr));
                System.out.println(q.next());
            }
            System.out.println("Heap After All Deletes: " + Arrays.toString(q.heap.arr));
            q.insert(5);
            q.insert(6);
            q.insert(1);
            q.insert(2);
            q.insert(3);
            System.out.println("Heap Before Delete: " + Arrays.toString(q.heap.arr));
            for (int i = 0; i < 5; i++) {
                System.out.println("Heap After Re-Delete[" + i + "]: " + Arrays.toString(q.heap.arr));
                System.out.println(q.next());
            }
            System.out.println("Heap After All Re-Deletes: " + Arrays.toString(q.heap.arr));
        }
        
        public boolean hasNext() {
            return this.heap.size >= 0;
        }
        
        public int next() {
            return this.heap.extractMax();
        }
        
        public void insert(int item) {
            this.heap.add(item);
        }
    }
}
