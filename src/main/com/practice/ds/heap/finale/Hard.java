package com.practice.ds.heap.finale;

import java.util.*;

import static com.practice.ds.heap.finale.SortIncreasingDecreasingArray.Direction.Decreasing;
import static com.practice.ds.heap.finale.SortIncreasingDecreasingArray.Direction.Increasing;

public class Hard {
}

class MergeSortedFiles {

    /*
        - algo1: min heap: T(n log k), S(k)
        - algo2: divide and conquer: T(n log k), S(k)
        - algo2: bottom up repeated merge: T(n log k), S(1)
     */
    
    // algo1: using min heap
    static class FileIndex {
        public int fileNumber;
        public int lineNumber;
        
        public FileIndex(int fileNumber, int lineNumber) {
            this.fileNumber = fileNumber;
            this.lineNumber = lineNumber;
        }
    }
    
    public static List<Integer> mergeSortedFiles(List<List<Integer>> files) {
        int k = files.size();
        if (k <= 1) return Collections.emptyList();
        
        PriorityQueue<FileIndex> minHeap = new PriorityQueue<>((f1, f2) -> {
            int num1 = files.get(f1.fileNumber).get(f1.lineNumber);
            int num2 = files.get(f2.fileNumber).get(f2.lineNumber);
            return Integer.compare(num1, num2);
        });
        
        for (int i = 0; i < files.size(); i++) {
            List<Integer> file = files.get(i);
            if (!file.isEmpty()) minHeap.add(new FileIndex(i, 0));
        }
        
        List<Integer> sortedData = new ArrayList<>();
        while (!minHeap.isEmpty()) {
            FileIndex fileIndex = minHeap.remove();
            List<Integer> file = files.get(fileIndex.fileNumber);
            sortedData.add(file.get(fileIndex.lineNumber));
            fileIndex.lineNumber++;
            if (fileIndex.lineNumber < file.size()) minHeap.add(fileIndex);
        }
        return sortedData;
    }
    
    // algo2: divide and conquer
    public static List<Integer> mergeUsingRecursion(List<List<Integer>> files) {
        if (files.size() == 0) return Collections.emptyList();
        else if (files.size() == 1) return files.get(0);
        
        int n = files.size();
        return mergeUsingRecursion(files, 0, n - 1);
    }
    
    public static List<Integer> mergeUsingRecursion(List<List<Integer>> files, int start, int end) {
        if (start > end) return Collections.emptyList();
        else if (start == end) return files.get(0);
        
        int mid = start + (end - start) / 2;
        List<Integer> firstHalf = mergeUsingRecursion(files.subList(start, mid + 1));
        List<Integer> secondHalf = mergeUsingRecursion(files.subList(mid + 1, end + 1));
        return merge(firstHalf, secondHalf);
    }
    
    private static List<Integer> merge(List<Integer> l1, List<Integer> l2) {
        if (l1.isEmpty()) return l2;
        if (l2.isEmpty()) return l1;
        
        List<Integer> output = new ArrayList<>();
        int i = 0, j = 0;
        while (i < l1.size() && j < l2.size()) {
            if (l1.get(i) <= l2.get(j)) {
                output.add(l1.get(i++));
            } else {
                output.add(l2.get(j++));
            }
        }
        while (i < l1.size()) output.add(l1.get(i++));
        while (j < l2.size()) output.add(l2.get(j++));
        return output;
    }
    
    // algo2: divide and conquer
    public static List<Integer> mergeBottomUpIterative(List<List<Integer>> files) {
        if (files.size() == 0) return Collections.emptyList();
        
        while (files.size() > 1) {
            List<List<Integer>> mergedLists = new ArrayList<>();
            for (int i = 0; i < files.size(); i += 2) {
                if (i + 1 < files.size()) mergedLists.add(merge(files.get(i), files.get(i + 1)));
                else mergedLists.add(files.get(i));
            }
            files = mergedLists;
        }
        return files.get(0);
    }
    
    public static void main(String[] args) {
        List<List<Integer>> files = new ArrayList<>();
        files.add(Arrays.asList(5, 8, 15));
        files.add(Arrays.asList(2, 9, 10));
        files.add(Arrays.asList(2, 4, 8));
        // System.out.println("merge: " + merge(files.get(0), files.get(1)));
        System.out.println("merged list is: " + mergeUsingRecursion(files));
        System.out.println("merged list is: " + mergeBottomUpIterative(files));
    }
    
}

class SortIncreasingDecreasingArray {
    
    enum Direction {Increasing, Decreasing}
    
    public static int[] sort(int[] arr) {
        if (arr == null || arr.length <= 1) return arr;
        
        List<List<Integer>> sortedLists = new ArrayList<>();
        Direction direction = Decreasing;
        int startIndex = 0;
        for (int endIndex = 1; endIndex <= arr.length; endIndex++) {
            if (endIndex == arr.length
                || direction == Decreasing && arr[endIndex] > arr[endIndex - 1]
                || direction == Increasing && arr[endIndex] < arr[endIndex - 1]) {
                
                List<Integer> subList = new ArrayList<>();
                for (int i=startIndex; i<endIndex; i++) subList.add(arr[i]);
                if (direction == Decreasing) Collections.reverse(subList);
                sortedLists.add(subList);
                
                startIndex = endIndex;
                direction = direction == Decreasing ? Increasing : Direction.Decreasing;
            }
        }
        
        List<Integer> sortedList = MergeSortedFiles.mergeBottomUpIterative(sortedLists);
        for (int i = 0; i < sortedList.size(); i++) {
            arr[i] = sortedList.get(i);
        }
        return arr;
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 4, 3, 2, 4, 4, 5, 5, 3, 1, 1, 2, 1, 1, 0};
        // int[] arr = new int[]{1, 2, 3, 4, 1, 0};
        System.out.println("sorted version of list : " + Arrays.toString(arr) + " is: \n"
            + Arrays.toString(sort(arr)));
    }
}
