package com.practice.intbit.list;

public class InsertionSort {
  
  public ListNode insertionSortList(ListNode A) {
    ListNode sStart=null;
    
    while(A != null) {
      ListNode tmp=A;
      A=A.next;
      tmp.next=null;
      sStart = sortedInsert(sStart, tmp);
    }
    
    return sStart;
  }
  private ListNode sortedInsert(ListNode sStart, ListNode tmp) {
    if (sStart==null) {
      return tmp;
    }
    
    ListNode prev=null, current=sStart;
    while (current!=null && current.val<tmp.val) {
      prev=current;
      current=current.next;
    }
    
    if (prev==null) {
      tmp.next=current;
      return tmp;
    } else {
      prev.next=tmp;
      tmp.next=current;
      return sStart;
    }
  }
}
