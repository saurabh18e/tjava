package com.practice.intbit.list;

import static com.practice.intbit.list.Reverse.reverse;

public class ReverseAlternate {
  
  public ListNode reorderList(ListNode A) {
    if(A == null || A.next==null) {
      return A;
    }
    
    ListNode slowPrev=null, slow=A, fast=A;
    while(fast != null && fast.next!=null) {
      slowPrev=slow;
      slow=slow.next;
      fast=fast.next.next;
    }
    slowPrev.next=null;
    ListNode B=reverse(slow);
    return mergeAlternateItr(A, B);
  }
  
  private ListNode mergeAlternateItr(ListNode a, ListNode b) {
    ListNode start=null, end=null;
    while(a!=null && b!=null) {
      ListNode tmp1 = a.next;
      ListNode tmp2 = b.next;
      
      a.next=b;
      b.next=null;
      if (start == null) {
        start=a;
        end=b;
      } else {
        end.next=a;
        end=b;
      }
      
      a=tmp1;
      b=tmp2;
    }
    
    if (a == null && b == null) {
      return start;
    }
    
    end.next=(a!=null)?a:b;
    return start;
  }
  
  private ListNode mergeAlternate(ListNode a, ListNode b) {
    if (a == null) {
      return b;
    } else if(b == null) {
      return a;
    } else {
      ListNode tmp1 = a.next;
      ListNode tmp2 = b.next;
      a.next=b;
      b.next=mergeAlternate(tmp1, tmp2);
      return a;
    }
  }
  
}
