package com.practice.intbit.list;

public class Rotate {
  
  /*
    - null
    - non-null with k<size
    - non-null with k=size
    - non-null with k>size
   */
  
  public ListNode rotateRight(ListNode head, int k) {
    ListNode slow=head, fast=head;
    while(k>0 && fast!=null) {
      fast=fast.next;
      k--;
      if (k > 0 && fast==null) {
        fast=head;
      }
    }
    if (k>0 || fast==null) {
      return head;
    }
    
    while(fast.next != null) {
      slow=slow.next;
      fast=fast.next;
    }
    
    fast.next=head;
    head=slow.next;
    slow.next = null;
    return head;
  }

}
