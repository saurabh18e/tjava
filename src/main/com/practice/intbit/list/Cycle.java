package com.practice.intbit.list;

public class Cycle {
  
  /*
    - ref: https://www.geeksforgeeks.org/detect-and-remove-loop-in-a-linked-list/
           https://www.geeksforgeeks.org/detect-loop-in-a-linked-list/
    1. hashset/visited falg
    2. floyd's cycle detection algo with fast and slow pointers
    3. own method: linked list intersection
    4. complicated maths
   */
  
  public ListNode detectCycle(ListNode head) {
    ListNode slow=head, fast=head;
    while(fast != null && fast.next != null) {
      slow=slow.next;
      fast=fast.next.next;
      if (slow==fast) {
        break;
      }
    }
    if (fast==null || fast.next == null) {
      return null;
    }
    
    int len1 = cycleLength(fast);
    int len2 = listLength(head, fast);
    int diff=-1;
    ListNode big=null, small=null;
    if (len1 > len2) {
      big=fast;
      small=head;
    } else {
      big=head;
      small=fast;
    }
    diff = Math.abs(len1-len2);
    while(diff>0) {
      diff--;
      big=big.next;
    }
    while(big!=small) {
      big=big.next;
      small=small.next;
    }
    return big;
  }
  
  private int listLength(ListNode start, ListNode end) {
    int len=0;
    while(start != end) {
      len++;
      start=start.next;
    }
    return len;
  }
  private int cycleLength(ListNode start) {
    if (start == null) {
      return 0;
    }
    int len=1;
    ListNode tmp=start.next;
    while(tmp != start) {
      len++;
      tmp=tmp.next;
    }
    return len;
  }
  
  public static void main(String[] args) {
    ListNode start = ListUtil.generateList(1, 6);
    start.next.next.next.next.next.next = start.next.next.next;
    Cycle main=new Cycle();
    System.out.println(main.detectCycle(start).val);
  }
}
