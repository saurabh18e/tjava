package com.practice.intbit.list;

public class ListArithmetic {
  
  public ListNode addTwoNumbers(ListNode A, ListNode B) {
    if (A == null) {
      return B;
    } else if (B == null) {
      return A;
    }
    
    int sum=0, carry=0;
    ListNode start = null, end=null;
    
    while(A != null || B != null || carry != 0) {
      int a = (A==null)?0:A.val;
      int b = (B==null)?0:B.val;
      
      sum = a+b+carry;
      carry = sum/10;
      
      ListNode nnd = new ListNode(sum%10);
      if (start==null) {
        start=end=nnd;
      } else {
        end.next=nnd;
        end = end.next;
      }
      
      A = (A==null)?A:A.next;
      B = (B==null)?B:B.next;
    }
    
    return start;
  }

}
