package com.practice.intbit.list;

public class Reverse {
  
  public static ListNode reverse(ListNode A) {
    return ListUtil.reverse(A);
  }

}
