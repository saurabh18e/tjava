package com.practice.intbit.list;

public class NthFromEnd {
  
  public ListNode removeNthFromEnd(ListNode A, int n) {
    ListNode slow=A, fast=A;
    while (n>0 && fast!=null) {
      n--;
      fast=fast.next;
    }
    if (n>0 || fast==null) {
      ListNode tail=A.next;
      A.next=null;
      return tail;
    }
    
    while(fast.next != null) {
      slow=slow.next;
      fast=fast.next;
    }
    ListNode tmp = slow.next;
    slow.next=slow.next.next;
    tmp.next=null;
    return A;
  }

}
