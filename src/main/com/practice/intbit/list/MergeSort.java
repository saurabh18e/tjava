package com.practice.intbit.list;

public class MergeSort {
  
  public ListNode mergeSort(ListNode A) {
    if (A == null || A.next == null) {
      return A;
    }
    
    ListNode slowPrev=null, slow=A, fast=A;
    while(fast != null && fast.next!=null) {
      slowPrev=slow;
      slow=slow.next;
      fast=fast.next.next;
    }
    
    ListNode first=A, second=slowPrev.next;
    slowPrev.next=null;
    return mergeTwoLists(mergeSort(first), mergeSort(second));
  }
  
  public ListNode mergeTwoLists(ListNode A, ListNode B) {
    ListNode start=null, end=null;
    while(A != null && B != null) {
      ListNode smaller = null;
      if (A.val < B.val) {
        smaller=A;
        A=A.next;
      } else {
        smaller=B;
        B=B.next;
      }
      smaller.next=null;
      if (end==null) {
        start=end=smaller;
      } else {
        end.next=smaller;
        end=smaller;
      }
    }
    
    ListNode tail = (A!=null)?A:B;
    if (end==null) {
      start=end=tail;
    } else {
      end.next=tail;
    }
    return start;
  }

}
