package com.practice.intbit.list;

public class DeDup {
  
  public ListNode deleteDuplicates(ListNode A) {
    if (A == null) {
      return A;
    }
    ListNode start=A;
    while (A.next != null) {
      if (A.val == A.next.val) {
        A.next = A.next.next;
      } else {
        A = A.next;
      }
    }
    return start;
  }
  
  public ListNode removeRepeated(ListNode A) {
    if (A == null || A.next == null) {
      return A;
    } else if (A.val == A.next.val) {
      return removeRepeated(removePrefix(A));
    } else {
      A.next = removeRepeated(A.next);
      return A;
    }
  }
  private ListNode removePrefix(ListNode a) {
    while(a.next != null && a.val == a.next.val) {
      a=a.next;
    }
    ListNode suffix = a.next;
    a.next=null;
    return suffix;
  }
}
