package com.practice.intbit.list;

import java.util.Stack;

import static com.practice.intbit.list.Reverse.reverse;

public class PalindromeCheck {
  
  public int lPalin(ListNode A) {
    if (A==null || A.next==null) {
      return 1;
    }
    
    ListNode slow=A, fast=A, slowPrev=null;
    while(fast!=null && fast.next!=null) {
      slowPrev=slow;
      slow=slow.next;
      fast=fast.next.next;
    }
    
    ListNode first = A;
    slowPrev.next=null;
    ListNode second = reverse(slow);
    ListNode secondStart = second;
    int result = -1;
    while(first != null && second != null) {
      if (first.val != second.val) {
        result = 0;
        break;
      }
      first=first.next;
      second=second.next;
    }
    if (second!=null) {
      second=second.next;
    }
    
    slowPrev.next = reverse(secondStart);
    
    if (result != -1) {
      return result;
    }
    return (first == second) ? 1 : 0;
  }
  
}
