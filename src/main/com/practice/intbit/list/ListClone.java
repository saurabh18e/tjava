package com.practice.intbit.list;

import java.util.HashMap;

public class ListClone {
  
  private class RandomListNode {
    int label;
    RandomListNode next, random;
    
    RandomListNode(int x) {
      this.label = x;
    }
  }
  
  public RandomListNode copyRandomListUsingHash(RandomListNode head) {
    RandomListNode cStart=null, cEnd=null;
    HashMap<RandomListNode, RandomListNode> map = new HashMap<>();
    
    for (RandomListNode current=head; current!=null; current=current.next) {
      RandomListNode nnd = new RandomListNode(current.label);
      if (cStart==null) {
        cStart=cEnd=nnd;
      } else {
        cEnd.next=nnd;
        cEnd=cEnd.next;
      }
      map.put(current, nnd);
    }
    
    for (RandomListNode list1=head, list2=cStart; list1!=null;
         list1=list1.next, list2=list2.next) {
      list2.random = map.get(list1.random);
    }
    return cStart;
  }
  
  public RandomListNode copyRandomList(RandomListNode head) {
    if (head==null) {
      return null;
    }
    RandomListNode oStart=null, oEnd=null;
    RandomListNode cStart=null, cEnd=null;
    
    // clone all nodes
    for (RandomListNode current=head; current!=null; current=current.next.next) {
      RandomListNode nnd = new RandomListNode(current.label);
      nnd.next=current.next;
      current.next=nnd;
    }
    
    // fix the random pointers
    for (RandomListNode current=head; current!=null; current=current.next.next) {
      current.next.random=(current.random==null)?null:current.random.next;
    }
    
    // split the lists
    for (RandomListNode current=head; current!=null; ) {
      if (oStart==null) {
        oStart=oEnd=current;
        cStart=cEnd=current.next;
      } else {
        oEnd.next=current;
        oEnd=oEnd.next;
        cEnd.next=current.next;
        cEnd=cEnd.next;
      }
      current=current.next.next;
    }
    oEnd.next=null;
    cEnd.next=null;
    
    return cStart;
  }
}
