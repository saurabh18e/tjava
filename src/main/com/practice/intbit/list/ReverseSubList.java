package com.practice.intbit.list;

import java.util.Arrays;

import static com.practice.intbit.list.ListUtil.makeList;
import static com.practice.intbit.list.ListUtil.print;

public class ReverseSubList {
  
  public ListNode reverseBetween(ListNode head, int m, int n) {
    ListNode pStart=null, pEnd=null, rStart=null, rEnd=null, prev=null, current=null, next=null;
    
    current=head;
    for(int i=0; i<m-1 && current!=null; i++) {
      ListNode tmp = current;
      current=current.next;
      tmp.next=null;
      if (pStart==null) {
        pStart=pEnd=tmp;
      } else {
        pEnd.next=tmp;
        pEnd=tmp;
      }
    }
    
    if (current == null) {
      return pStart;
    }
    
    for(int i=0; i<=(n-m) && current != null; i++) {
      ListNode tmp = current;
      current=current.next;
      tmp.next=null;
      if (rStart==null) {
        rStart=rEnd=tmp;
      } else {
        tmp.next=rStart;
        rStart=tmp;
      }
    }
    
    if (pEnd != null) {
      pEnd.next=rStart;
    } else {
      pStart=rStart;
    }
    
    rEnd.next=current;
    
    return pStart;
  }
  
  public static void main(String[] args) {
    ReverseSubList main = new ReverseSubList();
  
    print(main.reverseBetween(makeList(Arrays.asList(1, 2, 3)), 1, 2));
    print(main.reverseBetween(makeList(Arrays.asList(1, 2, 3)), 2, 3));
  }
  
}
