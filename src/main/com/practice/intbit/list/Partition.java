package com.practice.intbit.list;

public class Partition {
  
  public ListNode partition(ListNode A, int B) {
    ListNode pStart=null, pEnd=null;
    ListNode sStart=null, sEnd=null;
    
    while(A != null) {
      ListNode tmp = A;
      A=A.next;
      tmp.next=null;
      
      if (tmp.val < B) {
        if (pEnd==null) {
          pStart=pEnd=tmp;
        } else {
          pEnd.next=tmp;
          pEnd = tmp;
        }
      } else {
        if (sEnd==null) {
          sStart=sEnd=tmp;
        } else {
          sEnd.next=tmp;
          sEnd = tmp;
        }
      }
    }
    
    if (pEnd == null) {
      A=sStart;
    } else {
      pEnd.next=sStart;
      A=pStart;
    }
    return A;
  }

}
