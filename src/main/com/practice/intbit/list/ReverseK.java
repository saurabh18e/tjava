package com.practice.intbit.list;

public class ReverseK {
  
  public ListNode reverseK(ListNode head, int k) {
  
    ListNode rStart=null, rEnd=null, tStart=null, tEnd=null;
    
    while(head != null) {
      // cut k chunk prefix
      tStart = tEnd = head;
      for(int i=1; i<k && tEnd.next != null; i++) {
        tEnd=tEnd.next;
      }
      head = tEnd.next;
      tEnd.next = null;
      
      // reverse k chunk prefix
      ListNode prev=null, current=tStart, next=null;
      while(current != null) {
        next=current.next;
        current.next=prev;
        prev=current;
        current=next;
      }
      
      // add to new list end
      if(rStart==null) {
        rStart=tEnd;
        rEnd=tStart;
      } else {
        rEnd.next=tEnd;
        rEnd=tStart;
      }
    }
    
    return rStart;
  }
  
  public ListNode swapPairs(ListNode A) {
    ListNode start=null, prevStart=null, currentStart=A;
    
    while (currentStart!=null && currentStart.next!=null) {
      ListNode node1=currentStart;
      ListNode node2=currentStart.next;
      currentStart=currentStart.next.next;
      
      node2.next=node1;
      node1.next=currentStart;
      
      if (start==null) {
        start=node2;
      }
      
      if (prevStart!=null) {
        prevStart.next = node2;
      }
      prevStart=node1;
    }
    
    return (start==null) ? A : start;
  }
  
  public ListNode swapPairsRec(ListNode A) {
    if (A==null || A.next==null) {
      return A;
    }
    ListNode tail=A.next.next;
    ListNode newHead=A.next;
    newHead.next=A;
    A.next = swapPairs(tail);
    return newHead;
  }
}
