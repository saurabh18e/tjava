package com.practice.intbit.list.stack.expression;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Stack;

public class PostfixEval {
  
  private HashSet<String> hs = new HashSet<>(Arrays.asList("+", "-", "*", "/"));
  
  public int evalRPN(ArrayList<String> A) {
    int value=0;
    Stack<Integer> stack = new Stack<>();
    for(int i=0; i<A.size(); i++) {
      if (!hs.contains(A.get(i))) {
        stack.push(Integer.parseInt(A.get(i)));
      } else {
        int v2=stack.pop();
        int v1=stack.pop();
        stack.push(calculate(v1, v2, A.get(i)));
      }
    }
    return stack.pop();
  }
  
  private Integer calculate(int v1, int v2, String s) {
    if (s.equals("+")) {
      return v1+v2;
    } else if (s.equals("-")) {
      return v1-v2;
    } else if (s.equals("*")) {
      return v1*v2;
    } else {
      return v1/v2;
    }
  }
}
