package com.practice.intbit.list.stack.expression;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Stack;

public class RedundantBraces {
  
  HashSet<Character> operators = new HashSet<>(Arrays.asList('+', '-', '*', '/'));
  
  public int braces(String A) {
    if (A==null || A.isEmpty()) {
      return 1;
    }
    Stack<Character> stack=new Stack<>();
    for (char ch: A.toCharArray()) {
      if (ch == ')') {
        boolean matched = false;
        boolean redundant = true;
        while (!stack.isEmpty() && !matched) {
          char pop = stack.pop();
          if (pop == '(') {
            matched = true;
          } else if(operators.contains(pop)) {
            redundant = false;
          }
        }
        if (!matched || redundant) {
          return 1;
        }
      } else {
        stack.push(ch);
      }
    }
    return 0;
  }
  
  public static void main(String[] args) {
    RedundantBraces main=new RedundantBraces();
    System.out.println(main.braces("((a + b))"));
    System.out.println(main.braces("(a + (a + b))"));
  }
}
