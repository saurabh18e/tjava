package com.practice.intbit.list.stack.expression;

import java.util.HashMap;
import java.util.Stack;

public class CheckParenthesis {
  
  public int isValid(String A) {
    if (A==null || A.isEmpty()) {
      return 1;
    }
    
    HashMap<Character, Character> map = new HashMap<>();
    map.put(')', '(');
    map.put(']', '[');
    map.put('}', '{');
    
    Stack<Character> stack = new Stack();
    for (char ch: A.toCharArray()) {
      if (ch=='[' || ch=='(' || ch=='{') {
        stack.push(ch);
      } else {
        if (stack.isEmpty() || stack.pop() != map.get(ch)) {
          return 0;
        }
      }
    }
    
    return stack.isEmpty()?1:0;
  }
  public static void main(String[] args) {
    CheckParenthesis main = new CheckParenthesis();
    System.out.println(main.isValid("()[]{}"));
  }
}
