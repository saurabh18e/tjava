package com.practice.intbit.list.stack;

import java.util.*;

public class SlidingWindowMax {
  
  private class Element {
    int val;
    int index;
    public Element(int val, int index) {
      this.val = val;
      this.index = index;
    }
  }
  
  private void addToQueue(Deque<Element> deque, int val, int index, int window) {
    while(!deque.isEmpty() && deque.getLast().val < val) {
      deque.removeLast();
    }
    while(!deque.isEmpty() && deque.getFirst().index <= index - window) {
      deque.removeFirst();
    }
    deque.addLast(new Element(val, index));
  }
  
  public ArrayList<Integer> slidingMaximum(final List<Integer> arr, int w) {
    ArrayList<Integer> output = new ArrayList<>();
    Deque<Element> deque = new ArrayDeque<>();
    for (int i=0; i<arr.size(); i++) {
      addToQueue(deque, arr.get(i), i, w);
      if (i>=w-1) {
        output.add(deque.getFirst().val);
      }
    }
    if(arr.size() < w && !deque.isEmpty()) {
      output.add(deque.getFirst().val);
    }
    
    return output;
  }
  
  public ArrayList<Integer> slidingMaximum2(final List<Integer> a, int w) {
    ArrayList<Integer> ans = new ArrayList<Integer>();
    LinkedList<Integer> q = new LinkedList<Integer>();
    for(int i = 0;i<a.size();i++) {
      while(!q.isEmpty() && ( q.peekFirst() + w <=i || a.get(q.peekFirst()) <= a.get(i))) {
        q.peekFirst();
      }
      
      while(!q.isEmpty() && (q.peekLast() + w <=i || a.get(q.peekLast()) <= a.get(i))) {
        q.pollLast();
      }
      
      q.addLast(i);
      if(i >= w - 1) {
        ans.add(a.get(q.peekFirst()));
      }
      
    }
    return ans;
  }
}
