package com.practice.intbit.list.stack;

import java.util.Stack;

public class Misc {
  
  public String reverseString(String A) {
    Stack<Character> stack=new Stack<>();
    for (char ch: A.toCharArray()) {
      stack.push(ch);
    }
    StringBuilder builder = new StringBuilder();
    while(!stack.isEmpty()) {
      builder.append(stack.pop());
    }
    return builder.toString();
  }
  
  public static void main(String[] args) {
    Misc main=new Misc();
    System.out.println(main.reverseString("12345"));
  }
}
