package com.practice.intbit.list.stack;

import java.util.ArrayList;
import java.util.Stack;

public class NearestSmaller {
  
  public ArrayList<Integer> prevSmaller(ArrayList<Integer> A) {
    ArrayList<Integer> output = new ArrayList<>();
    Stack<Integer> stack = new Stack<>();
    for(int i=0; i<A.size();i++) {
      while (!stack.isEmpty() && A.get(stack.peek()) >= A.get(i)) {
        stack.pop();
      }
      output.add(stack.isEmpty() ? -1 : A.get(stack.peek()));
      stack.push(i);
    }
    
    return output;
  }
  
}
