package com.practice.intbit.list.stack;

import java.util.Stack;

public class StackWithMin {
  
  /*
    - Stack with min.....S(1)
    - ref: https://www.geeksforgeeks.org/design-a-stack-that-supports-getmin-in-o1-time-and-o1-extra-space/
    - method: minEle = 2*NewMin-CurrentMin
   */
  
  class Element {
    int val;
    public Element(int val) {
      this.val = val;
    }
  }
  
  private Stack<Element> stack = new Stack<>();
  private Stack<Element> minStack = new Stack<>();
  
  public void push(int x) {
    Element e=new Element(x);
    if (stack.isEmpty() || minStack.peek().val > e.val) {
      minStack.push(e);
    }
    stack.push(e);
  }
  
  public void pop() {
    if (!stack.isEmpty()) {
      Element e = stack.pop();
      if (minStack.peek() == e) {
        minStack.pop();
      }
    }
  }
  
  public int top() {
    return (stack.isEmpty() ? -1 : stack.peek().val);
  }
  
  public int getMin() {
    return (stack.isEmpty() ? -1 : minStack.peek().val);
  }

}
