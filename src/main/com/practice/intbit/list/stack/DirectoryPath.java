package com.practice.intbit.list.stack;

import java.util.Stack;

public class DirectoryPath {
  
  public String simplifyPath(String A) {
    if (A==null || A.isEmpty() || A.charAt(0) != '/') {
      return A;
    }
    
    String[] dirs = A.split("/");
    Stack<String> stack=new Stack<>();
    for(String dir: dirs) {
      switch(dir) {
        case "" : {
          break;
        }
        case "." : {
          break;
        }
        case "..": {
          if (!stack.isEmpty()) {
            stack.pop();
          }
          break;
        }
        default: {
          stack.push(dir);
        }
      }
    }
    
    if(stack.isEmpty()) {
      return "/";
    }
    
    Stack<String> stack2 = new Stack<>();
    while(!stack.isEmpty()) {
      stack2.push(stack.pop());
    }
    StringBuilder builder = new StringBuilder();
    while(!stack2.isEmpty()) {
      builder.append("/");
      builder.append(stack2.pop());
    }
    
    return builder.toString();
  }
  
  public static void main(String[] args) {
    DirectoryPath main=new DirectoryPath();
    System.out.println(main.simplifyPath("/a/./b/../../c/"));
    System.out.println(main.simplifyPath("/../"));
  }
}
