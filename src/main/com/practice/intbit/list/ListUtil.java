package com.practice.intbit.list;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
  
  public static int length(ListNode start) {
    int length=0;
    while (start != null) {
      length++;
      start=start.next;
    }
    return length;
  }
  
  public static ListNode reverse(ListNode A) {
    ListNode prev=null, current=A, next;
    while(A!=null) {
      next=A.next;
      A.next=prev;
      prev=A;
      A=next;
    }
    return prev;
  }
  
  public static ListNode sort(ListNode A) {
    MergeSort sort=new MergeSort();
    return sort.mergeSort(A);
  }
  
  public static void print(ListNode head) {
    System.out.print("[");
    while(head != null && head.next != null) {
      System.out.print(head.val + " -> ");
      head=head.next;
    }
    if (head != null) {
      System.out.print(head.val);
    }
    System.out.println("]");
  }
  
  public static ListNode generateList(int start, int end) {
    List<Integer> list = new ArrayList<>();
    for (int i=start; i<=end; i++) {
      list.add(i);
    }
    return makeList(list);
  }
  
  public static ListNode makeList(List<Integer> list) {
    ListNode start = null, end = null;
    for(int i: list) {
      ListNode nnd = new ListNode(i);
      
      if (start == null) {
        start = end = nnd;
      } else {
        end.next=nnd;
        end=nnd;
      }
    }
    return start;
  }
}
