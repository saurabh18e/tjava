package com.practice.intbit.list;

import java.util.HashSet;

import static com.practice.intbit.list.ListUtil.length;

public class ListIntersect {
  
  public ListNode getIntersectionNode1(ListNode a, ListNode b) {
    int len1 = length(a);
    int len2 = length(b);
    ListNode big, small;
    if (len1 > len2) {
      big=a;
      small=b;
    } else {
      big=b;
      small=a;
    }
    int diff = Math.abs(len1-len2);
    ListNode intersect=big;
    while(diff>0) {
      big=big.next;
      diff--;
    }
    while(big!=null && small!=null) {
      if (big==small) {
        return big;
      }
      big=big.next;
      small=small.next;
    }
    return null;
  }
  
  public ListNode getIntersectionNode(ListNode a, ListNode b) {
    HashSet<ListNode> visited=new HashSet<>();
    while(a != null) {
      visited.add(a);
      a=a.next;
    }
    while (b!=null) {
      if (visited.contains(b)) {
        return b;
      }
      b=b.next;
    }
    return null;
  }
}
