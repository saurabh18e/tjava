package com.practice.intbit.tree;

import java.util.ArrayList;

public class SortedListToTree {
  
  private static class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    
    TreeNode(int x) {
      val = x;
    }
  }
  
  private static class ListNode {
    public int val;
    public ListNode next;
    
    ListNode(int x) {
      val = x;
    }
  }
  
  public TreeNode sortedListToBST(ListNode a) {
    if (a == null) {
      return null;
    }
    if (a.next == null) {
      return new TreeNode(a.val);
    }
    
    ListNode slow=a, fast=a, slowPrev=null;
    while(fast!=null && fast.next!=null) {
      slowPrev = slow;
      slow = slow.next;
      fast = fast.next.next;
    }
    
    ListNode leftList=a, rootListNode=slow, rightList=rootListNode.next;
    if (slowPrev != null) {
      slowPrev.next = null;
    } else {
      leftList = null;
    }
    rootListNode.next = null;
    
    TreeNode root = new TreeNode(rootListNode.val);
    root.left = sortedListToBST(leftList);
    root.right = sortedListToBST(rightList);
    return root;
  }
  
  public static void main(String[] args) {
    SortedListToTree main = new SortedListToTree();
    ListNode start = null;
    ListNode prev = null;
    for (int i=1; i<=10; i++) {
      ListNode node = new ListNode(i);
      if(start == null) {
        start = node;
      }
      if (prev != null) {
        prev.next = node;
      }
      prev = node;
    }
    TreeNode tree = main.sortedListToBST(start);
    System.out.println(string(tree));
  }
  private static String string(TreeNode tree) {
    if (tree == null) {
      return "";
    }
    return string(tree.left) + " " + tree.val + " " + string(tree.right);
  }
}
