package com.practice.intbit.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Tree {
  
  public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
  
    ArrayList<ArrayList<Integer>> levels = new ArrayList<>();
    if (root == null) {
      return levels;
    }
    
    Queue<TreeNode> queue = new LinkedList<TreeNode>();
    queue.add(root);
    
    while(!queue.isEmpty()) {
      queue.add(null);
      ArrayList<Integer> level = new ArrayList<>();
      while(queue.peek() != null) {
        TreeNode node = queue.remove();
        level.add(node.val);
        if (node.left != null) {
          queue.add(node.left);
        }
        if (node.right != null) {
          queue.add(node.right);
        }
      }
      queue.remove();
      levels.add(level);
    }
    
    return levels;
  }
  
}
