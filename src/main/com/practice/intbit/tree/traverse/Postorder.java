package com.practice.intbit.tree.traverse;

import com.practice.intbit.tree.Tree;
import com.practice.intbit.tree.TreeNode;

import java.util.ArrayList;
import java.util.Stack;

public class Postorder {
  
  public ArrayList<Integer> postorderRecUtil(TreeNode root, ArrayList<Integer> postorderList) {
    if (root != null) {
      postorderRecUtil(root.left, postorderList);
      postorderRecUtil(root.right, postorderList);
      postorderList.add(root.val);
    }
    return postorderList;
  }
  public ArrayList<Integer> postorderRec(TreeNode A) {
    return postorderRecUtil(A, new ArrayList<Integer>());
  }
  
  public ArrayList<Integer> postorderTraversal(TreeNode A) {
    ArrayList<Integer> output = new ArrayList<Integer>();
    Stack<TreeNode> stack = new Stack<>();
    
    TreeNode current=A;
    while (current!=null || !stack.isEmpty()) {
      if (current == null && !stack.isEmpty()) {
      
      }
    }
    
    return output;
  }
  
  // postorder=preorder and reverse
  public ArrayList<Integer> postorderTraversal1(TreeNode A) {
    ArrayList<Integer> output = new ArrayList<Integer>();
    Stack<TreeNode> stack = new Stack<>();
    stack.add(A);
    while (!stack.isEmpty()) {
      TreeNode current = stack.pop();
      if (current == null) {
        continue;
      }
      output.add(current.val);
      stack.add(current.left);
      stack.add(current.right);
    }
    
    for (int i=0, j=output.size()-1; i<j; i++, j--) {
      Integer tmp=output.get(i);
      output.set(i, output.get(j));
      output.set(j, tmp);
    }
    
    return output;
  }
  
  // using visited marker
  public ArrayList<Integer> postorderTraversal2(TreeNode A) {
    ArrayList<Integer> output=new ArrayList<Integer>();
    Stack<TreeNode> stack=new Stack<>();
    stack.add(A);
    
    while(!stack.isEmpty()) {
      TreeNode current=stack.pop();
      if (current==null) { continue;}
      if (stack.isEmpty() || stack.peek()!=null) {
        stack.push(null);
        stack.push(current);
        if (current.right!=null) {stack.push(current.right);}
        if (current.left!=null) {stack.push(current.left);}
      } else {
        stack.pop();
        output.add(current.val);
      }
    }
    return output;
  }

}
