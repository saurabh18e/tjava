package com.practice.intbit.tree.traverse;

import com.practice.ds.array.Array;
import com.practice.intbit.tree.TreeNode;

import java.util.ArrayList;
import java.util.Stack;

public class Preorder {
  
  private ArrayList<Integer> preorderRecUtil(TreeNode root, ArrayList<Integer> preorderList) {
    if (root==null) {
      return preorderList;
    }
    preorderList.add(root.val);
    preorderRecUtil(root.left, preorderList);
    preorderRecUtil(root.right, preorderList);
    return preorderList;
  }
  public ArrayList<Integer> preorderRec(TreeNode A) {
    return preorderRecUtil(A, new ArrayList<Integer>());
  }
  
  public ArrayList<Integer> preorderTraversal(TreeNode A) {
    ArrayList<Integer> output = new ArrayList<>();
    Stack<TreeNode> stack=new Stack<>();
  
    TreeNode current=A;
    while(current !=null || !stack.isEmpty()) {
      if (current==null && !stack.isEmpty()) {
        current = stack.pop();
      }
      
      if (current != null) {
        output.add(current.val);
        stack.push(current.right);
        current=current.left;
      }
    }
    
    return output;
  }
  
  public ArrayList<Integer> preorderTraversal2(TreeNode A) {
    ArrayList<Integer> output = new ArrayList<>();
    Stack<TreeNode> stack=new Stack<>();
  
    TreeNode current=A;
    while(current != null) {
      output.add(current.val);
      if (current.right != null) {
        stack.push(current.right);
      }
    
      current = current.left;
      if (current == null && !stack.isEmpty()) {
        current = stack.pop();
      }
    }
    
    return output;
  }
  
  public ArrayList<Integer> preorderTraversal3(TreeNode A) {
    ArrayList<Integer> output = new ArrayList<>();
    Stack<TreeNode> stack=new Stack<>();
      stack.push(A);
    
    while(!stack.isEmpty()) {
      TreeNode current=stack.pop();
      if (current==null) {
        continue;
      }
      output.add(current.val);
      stack.push(current.right);
      stack.push(current.left);
    }
    
    return output;
  }
  
}

