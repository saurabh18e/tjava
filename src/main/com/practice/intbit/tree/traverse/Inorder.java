package com.practice.intbit.tree.traverse;

import com.practice.ds.array.Array;
import com.practice.intbit.tree.Tree;
import com.practice.intbit.tree.TreeNode;

import java.util.ArrayList;
import java.util.Stack;

public class Inorder {
  
  private ArrayList<Integer> inorderRecUtil(TreeNode root, ArrayList<Integer> indorderList) {
    if (root != null) {
      inorderRecUtil(root.left, indorderList);
      indorderList.add(root.val);
      inorderRecUtil(root.right, indorderList);
    }
    return indorderList;
  }
  public ArrayList<Integer> inorderRec(TreeNode A) {
    return inorderRecUtil(A, new ArrayList<Integer>());
  }
  
  // no extra visited marker
  public ArrayList<Integer> inorderTraversal(TreeNode A) {
    ArrayList<Integer> output=new ArrayList<>();
    Stack<TreeNode> stack=new Stack<>();
    
    TreeNode current=A;
    while(current !=null || !stack.isEmpty()) {
      if (current==null && !stack.isEmpty()) {
        current=stack.pop();
        output.add(current.val);
        current=current.right;
      }
      
      if (current != null) {
        stack.push(current);
        current=current.left;
      }
    }
    
    return output;
  }
  
  // using visited marker
  public ArrayList<Integer> inorderTraversal2(TreeNode A) {
    ArrayList<Integer> output=new ArrayList<>();
    Stack<TreeNode> stack=new Stack<>();
    stack.push(A);
    
    while(!stack.isEmpty()) {
      TreeNode current=stack.pop();
      if (current==null) { continue;}
      
      if (stack.isEmpty() || stack.peek()!=null) {
        stack.push(null); // this null is like a visited flag
        stack.push(current);
        if (current.left!=null) {stack.push(current.left);}
      } else {
        stack.pop();
        output.add(current.val);
        if (current.right!=null) {stack.push(current.right);}
      }
    }
    
    return output;
  }

}
