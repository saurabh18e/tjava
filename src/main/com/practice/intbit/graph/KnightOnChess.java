package com.practice.intbit.graph;

import java.util.LinkedList;
import java.util.Queue;

public class KnightOnChess {
  
  private class Point {
    int x=-1;
    int y=-1;
    
    Point(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }
  private class Cell {
    int length=-1;
  }
  
  private int[][] neighbours = {
      {-1, -2},
      {-2, -1},
      {-2, 1},
      {-1, 2},
      {1, -2},
      {2, -1},
      {2, 1},
      {1, 2}
  };
  
  public int knight(int A, int B, int C, int D, int E, int F) {
    Cell[][] matrix = new Cell[A][B];
    for (int i=0; i<A; i++) {
      for (int j=0; j<B; j++) {
        matrix[i][j] = new Cell();
      }
    }
    Point src = new Point(C-1, D-1);
    Point dest = new Point(E-1, F-1);
    
    Queue<Point> queue = new LinkedList<>();
    queue.add(src);
    matrix[src.x][src.y].length=0;
    
    int minPathLength = -1;
    while(!queue.isEmpty()) {
      Point p = queue.remove();
      if (p.x == dest.x && p.y == dest.y) {
        minPathLength = matrix[p.x][p.y].length;
        break;
      }
      for (int[] neighbour: neighbours) {
        int x = p.x + neighbour[0];
        int y = p.y + neighbour[1];
        boolean isSafe = (x >=0 && y>=0 && x<A && y<B);
        if (isSafe && matrix[x][y].length == -1) {
          matrix[x][y].length = matrix[p.x][p.y].length + 1;
          queue.add(new Point(x, y));
        }
      }
    }
    
    return minPathLength;
  }
  
}
