package com.practice.intbit.graph;

public class Edge {
  
  public int id;
  
  public int sourceId;
  public int destinationId;
  
  public int weight;
  public boolean undirected = false;
  
  public Edge(int sourceId, int destinationId) {
    this(sourceId, destinationId, 1);
  }
  
  public Edge(int sourceId, int destinationId, int weight) {
    this.id = sourceId;
    this.sourceId = sourceId;
    this.destinationId = destinationId;
    this.weight = weight;
  }
}
