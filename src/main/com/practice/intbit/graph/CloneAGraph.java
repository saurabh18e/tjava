package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CloneAGraph {
  
  class UndirectedGraphNode {
    int label;
    List<UndirectedGraphNode> neighbors;
    
    UndirectedGraphNode(int x) {
      label = x;
      neighbors = new ArrayList<UndirectedGraphNode>();
    }
  }
  
  public UndirectedGraphNode cloneGraph(UndirectedGraphNode node, HashMap<UndirectedGraphNode, UndirectedGraphNode> map) {
    if (node == null) {
      return null;
    }
    UndirectedGraphNode clone = map.get(node);
    if (clone != null) {
      return clone;
    }
    
    clone = new UndirectedGraphNode(node.label);
    map.put(node, clone);
    
    for (UndirectedGraphNode neighbour : node.neighbors) {
      UndirectedGraphNode neighbourClone = cloneGraph(neighbour, map);
      clone.neighbors.add(neighbourClone);
    }
    
    return clone;
  }
  
  public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
    HashMap<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
    return cloneGraph(node, map);
  }
}
