package com.practice.intbit.graph;

import java.util.*;

public class WordLadder1 {
  
  private List<String> findNeighbours(HashSet<String> dict, String pattern) {
    List<String> dist1 = new ArrayList<>();
    for (String word : dict) {
      int distance = 0;
      for (int i = 0; i < pattern.length(); i++) {
        if (word.charAt(i) != pattern.charAt(i)) {
          distance++;
          if (distance > 1) {
            break;
          }
        }
      }
      if (distance == 1) {
        dist1.add(word);
      }
    }
    return dist1;
  }
  
  public int ladderLength(String start, String end, ArrayList<String> dictV) {
    HashSet<String> dict = new HashSet<>();
    dict.addAll(dictV);
    dict.add(end);
    int ladderLength = 0;
    Queue<String> queue = new LinkedList<>();
    queue.add(start);
    queue.add(null);
    while (!queue.isEmpty()) {
      String word = queue.remove();
      if (word == null) {
        if (queue.isEmpty()) {
          break;
        }
        ladderLength++;
        queue.add(null);
        continue;
      }
      if (word.equals(end)) {
        return ladderLength + 1;
      }
      List<String> dist1 = findNeighbours(dict, word);
      dict.removeAll(dist1);
      queue.addAll(dist1);
    }
    return 0;
  }
  
  public static void main(String[] args) {
    String start = "hit";
    String end = "cog";
    ArrayList<String> dict = new ArrayList(Arrays.asList("hot", "dot", "dog", "lot", "log"));
    WordLadder1 main = new WordLadder1();
    System.out.println(main.ladderLength(start, end, dict));
  }
}
