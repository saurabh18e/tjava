package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
  
  public static enum Color { UnVisited, BeingVisited, Visited}
  
  public int id;
  public ArrayList<Edge> edges;
  
  public int distance;
  public Color color = Color.UnVisited;
  public int parent;
  
  public Vertex(int id) {
    this.id = id;
    edges = new ArrayList<>();
    
    this.distance = 0;
  }
}
