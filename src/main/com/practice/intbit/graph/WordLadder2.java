package com.practice.intbit.graph;

import java.util.*;

public class WordLadder2 {
  
  
  private List<Integer> findNeighbours(HashSet<Integer> dict, ArrayList<String> dictV, Integer patternIndex) {
    List<Integer> dist1 = new ArrayList<>();
    for (Integer wordIndex : dict) {
      int distance = 0;
      String pattern = dictV.get(patternIndex);
      String word = dictV.get(wordIndex);
      for (int i = 0; i < pattern.length(); i++) {
        if (word.charAt(i) != pattern.charAt(i)) {
          distance++;
          if (distance > 1) {
            break;
          }
        }
      }
      if (distance == 1) {
        dist1.add(wordIndex);
      }
    }
    return dist1;
  }
  
  
  public ArrayList<ArrayList<String>> findLadders(String start, String end, ArrayList<String> dictV) {
    dictV.addAll(Arrays.asList(start, end));
    int n = dictV.size();
    int rootIndex = n-2;
    
    int[] parent = new int[n];
    Arrays.fill(parent, -1);
    parent[rootIndex] = rootIndex;
    
    HashSet<Integer> dict = new HashSet<>();
    for (int i=0; i<n; i++) {
      dict.add(i);
    }
    
    Queue<Integer> queue = new LinkedList<>();
    queue.add(rootIndex);
    
    
    while(!queue.isEmpty()) {
      int node = queue.remove();
      dict.remove(rootIndex);
      List<Integer> neighbours = findNeighbours(dict, dictV, node);
      
    }
    
    return null;
  }
  
  
  public static void main(String[] args) {
    String start = "hit";
    String end = "cog";
    ArrayList<String> dict = new ArrayList(Arrays.asList("hot", "dot", "dog", "lot", "log"));
    WordLadder2 main = new WordLadder2();
    System.out.println(main.findLadders(start, end, dict));
  }
}
