package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.List;

import static com.practice.util.Util.list;

public class Solution {
  
  class Graph {
    
    public Vertex[] vertices;
    
    public List<Edge> edges() {
      List<Edge> edges = new ArrayList<>();
      for (Vertex v : vertices) {
        edges.addAll(v.edges);
      }
      return edges;
    }
    
    public Graph() {
    }
    public Graph(int vertexCount, List<Edge> edges) {
      this.vertices = new Vertex[vertexCount + 1];
      for (int i = 1; i <= vertexCount; i++) {
        this.vertices[i] = new Vertex(i);
      }
      for (Edge edge : edges) {
        this.vertices[edge.sourceId].edges.add(edge);
      }
    }
    
    public void print() {
      for (Vertex v : vertices) {
        StringBuilder builder = new StringBuilder();
        builder.append(v.id + " -> ");
        for (Edge e : v.edges) {
          builder.append(" " + e.id + ",");
        }
        System.out.println(builder.toString());
      }
    }
  }
  
  public static enum Color {UnVisited, BeingVisited, Visited}
  
  class Vertex {
    
    public int id;
    public ArrayList<Edge> edges;
    
    public int distance;
    public Color color = Color.UnVisited;
    public int parent;
    
    public Vertex(int id) {
      this.id = id;
      edges = new ArrayList<>();
      this.distance = 0;
    }
  }
  
  class Edge {
    
    public int id;
    
    public int sourceId;
    public int destinationId;
    
    public int weight;
    public boolean undirected = false;
    
    public Edge(int sourceId, int destinationId) {
      this(sourceId, destinationId, 1);
    }
    
    public Edge(int sourceId, int destinationId, int weight) {
      this.id = sourceId;
      this.sourceId = sourceId;
      this.destinationId = destinationId;
      this.weight = weight;
    }
  }
  
  public boolean visit(Graph graph, Vertex v) {
    v.color = Color.BeingVisited;
    for (Edge e : v.edges) {
      Vertex destination = graph.vertices[e.destinationId];
      if (destination.color == Color.BeingVisited) {
        return false;
      }
      visit(graph, destination);
    }
    v.color = Color.Visited;
    return true;
  }
  
  public int solve(int A, ArrayList<Integer> B, ArrayList<Integer> C) {
    List<Edge> edges = new ArrayList<>();
    for (int i = 0; i < B.size(); i++) {
      edges.add(new Edge(B.get(i), C.get(i)));
    }
    Graph graph = new Graph(A, edges);
    for (Vertex v : graph.vertices) {
      if (v != null && v.color == Color.UnVisited) {
        if (!visit(graph, v)) {
          return 0;
        }
      }
    }
    return 1;
  }
  
  public static void main(String[] args) {
    Solution sol = new Solution();
    System.out.println(sol.solve(70,
        list(67, 8, 48, 42, 35, 25, 37, 69, 31, 36, 7, 33, 2, 47, 42, 52, 31, 70, 29, 38, 36, 60, 15, 37, 33, 27, 4, 32, 43, 55, 49, 35, 21, 28, 62, 17, 2, 61, 54, 22, 9, 56, 12, 3, 60, 52, 21, 15, 54, 63, 33, 64, 38, 16, 59, 69, 49, 52, 10, 10, 6, 56, 43, 32, 41, 66, 6),
        list(51, 43, 55, 27, 34, 8, 14, 5, 70, 64, 65, 57, 45, 19, 53, 50, 44, 51, 19, 41, 14, 68, 12, 58, 50, 66, 7, 47, 40, 62, 29, 5, 22, 39, 23, 34, 25, 4, 40, 26, 26, 45, 18, 28, 61, 59, 17, 46, 39, 46, 68, 24, 63, 59, 67, 53, 9, 11, 3, 44, 24, 37, 13, 1, 65, 18, 48)));
  }
}
