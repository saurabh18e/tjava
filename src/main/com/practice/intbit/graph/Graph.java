package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Graph {

  public Vertex[] vertices;
  
  public List<Edge> edges() {
    List<Edge> edges = new ArrayList<>();
    for(Vertex v: vertices) {
      edges.addAll(v.edges);
    }
    return edges;
  }
  
  public Graph() { }
  public Graph(int vertexCount, List<Edge> edges) {
    this.vertices = new Vertex[vertexCount+1];
    for (int i = 1; i <= vertexCount; i++) {
      this.vertices[i] = new Vertex(i);
    }
    for (Edge edge: edges) {
      this.vertices[edge.sourceId].edges.add(edge);
    }
  }
  
  public static Graph weightedGraph(int vertexCount, List<String> edges) {
    Graph graph = new Graph();
    graph.vertices = new Vertex[vertexCount+1];
    for (int i=1; i<=edges.size(); i++) {
      graph.vertices[i] = new Vertex(i);
    }
    for (String edge: edges) {
      graph.addEdge(edge);
    }
    return graph;
  }
  
  private void addEdge(String edgeStr) {
    String[] arr = edgeStr.split(" ");
    int source = Integer.parseInt(arr[0]);
    int destination = Integer.parseInt(arr[1]);
    int weight = Integer.parseInt(arr[2]);
    Edge edge = new Edge(source, destination, weight);
    vertices[source].edges.add(edge);
  }
  
  public void print() {
    for (Vertex v: vertices) {
      StringBuilder builder = new StringBuilder();
      builder.append(v.id + " -> ");
      for (Edge e: v.edges) {
        builder.append(" " + e.id + ",");
      }
      System.out.println(builder.toString());
    }
  }
  
}
