package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.Arrays;

public class IslandCount {
  
  private int[][] neighbours = {
      {-1, 0},
      {0, -1},
      {0, 1},
      {1, 0},
  };
  
  private class Cell {
    char ch;
    int islandNum = -1;
    
    public Cell(char ch) {
      this.ch = ch;
    }
  }
  
  public int black(ArrayList<String> A) {
    int rows = A.size();
    int columns = A.get(0).length();
    Cell[][] matrix = new Cell[rows][columns];
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        matrix[i][j] = new Cell(A.get(i).charAt(j));
      }
    }
    
    int islandCount=0;
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        Cell cell = matrix[i][j];
        if (cell.ch == 'X' && cell.islandNum == -1) {
          islandCount++;
          visit(matrix, i, j, rows, columns, islandCount);
        }
      }
    }
    return islandCount;
  }
  
  private void visit(Cell[][] matrix, int i, int j, int rows, int columns, int islandNum) {
    matrix[i][j].islandNum = islandNum;
    for (int[] neighbour : neighbours) {
      int x = i + neighbour[0];
      int y = j + neighbour[1];
      boolean isSafe = (x >= 0 && y >= 0 && x < rows && y < columns && matrix[x][y].ch == 'X' && matrix[x][y].islandNum == -1);
      if (isSafe) {
        visit(matrix, x, y, rows, columns, islandNum);
      }
    }
  }
  
  public static void main(String[] args) {
    IslandCount main = new IslandCount();
    System.out.println(main.black(new ArrayList<>(Arrays.asList(
        "OOOXOOO",
        "OOXXOXO",
        "OXOOOXO"))));
  }
  
}
