package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.Arrays;

public class IslandCapture {
  
  private int[][] neighbours = {
      {-1, 0},
      {0, -1},
      {0, 1},
      {1, 0},
  };
  
  private class Cell {
    char ch;
    boolean visited;
    
    public Cell(char ch) {
      this.ch = ch;
    }
  }
  
  public void solve(ArrayList<ArrayList<Character>> a) {
    int rows = a.size();
    int columns = a.get(0).size();
    Cell[][] matrix = new Cell[rows][columns];
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        matrix[i][j] = new Cell(a.get(i).get(j));
      }
    }
    
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        Cell cell = matrix[i][j];
        if (cell.ch == 'O' && !cell.visited) {
          boolean canCapture = visit(matrix, i, j, rows, columns, false);
          if (canCapture) {
            visit(matrix, i, j, rows, columns, true);
          }
        }
      }
    }
  
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        a.get(i).set(j, matrix[i][j].ch);
      }
    }
  }
  
  private boolean visit(Cell[][] matrix, int i, int j, int rows, int columns, boolean capture) {
    matrix[i][j].visited = true;
    if (capture) {
      matrix[i][j].ch = 'X';
    }
    boolean canCapture = true;
    for (int[] neighbour : neighbours) {
      int x = i + neighbour[0];
      int y = j + neighbour[1];
      boolean isSafe = (x >= 0 && y >= 0 && x < rows && y < columns);
      if (!isSafe) {
        canCapture = false;
      }
      if (isSafe && matrix[x][y].ch == 'O' && (!matrix[x][y].visited || capture)) {
        if (!visit(matrix, x, y, rows, columns, capture)) {
          canCapture = false;
        }
      }
    }
    return canCapture;
  }
  
  public static void main(String[] args) {
    IslandCapture main = new IslandCapture();
    ArrayList<ArrayList<Character>> input = new ArrayList<>(Arrays.asList(
        new ArrayList<>(Arrays.asList('X', 'X', 'X', 'X')),
        new ArrayList<>(Arrays.asList('X', 'O', 'O', 'X')),
        new ArrayList<>(Arrays.asList('X', 'X', 'O', 'X')),
        new ArrayList<>(Arrays.asList('X', 'O', 'X', 'X'))));
    System.out.println(input);
    main.solve(input);
    System.out.println(input);
  }
  
}
