package com.practice.intbit.graph;

import com.practice.intbit.graph.Vertex.Color;

import java.util.ArrayList;
import java.util.List;

public class CycleDetect {
  
  public static boolean visit(Graph graph, Vertex v) {
    v.color = Color.BeingVisited;
    
    for (Edge e: v.edges) {
      Vertex destination = graph.vertices[e.destinationId];
      if (destination.color != Color.UnVisited) {
        return false;
      }
      visit(graph, destination);
    }
    
    v.color = Color.Visited;
    return true;
  }
  
  public static int solve(int A, ArrayList<Integer> B, ArrayList<Integer> C) {
    List<Edge> edges = new ArrayList<>();
    for (int i=0; i<A; i++) {
      edges.add(new Edge(B.get(i), C.get(i)));
    }
    Graph graph = new Graph(A, edges);
    for (Vertex v : graph.vertices) {
      if (v.color == Color.UnVisited) {
        if (!visit(graph, v)) {
          return 0;
        }
      }
    }
    
    return 1;
  }

}
