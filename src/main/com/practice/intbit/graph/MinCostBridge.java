package com.practice.intbit.graph;

import java.util.*;

public class MinCostBridge {
  
  public static int solve(int A, ArrayList<ArrayList<Integer>> B) {
    HashMap<Integer, HashSet<Integer>> sets = new HashMap<>();
    for (int i=1; i<=A; i++) {
      sets.put(i, new HashSet<>(Collections.singleton(i)));
    }
    
    B.sort(new Comparator<ArrayList<Integer>>() {
      @Override
      public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
        return o1.get(2) - o2.get(2);
      }
    });
    
    int mspCost = 0;
    for (List<Integer> edge : B) {
      int sourceId = edge.get(0);
      int destinationId = edge.get(1);
      HashSet<Integer> sourceSet = sets.get(sourceId);
      HashSet<Integer> destinationSet = sets.get(destinationId);
      int cost = edge.get(2);
      if (sourceSet != destinationSet) {
        sourceSet.addAll(destinationSet);
        for (int vertex: destinationSet) {
          sets.put(vertex, sourceSet);
        }
        mspCost += cost;
      }
    }
    return mspCost;
  }
  
  public static void main(String[] args) {
    ArrayList<ArrayList<Integer>> edges = new ArrayList<ArrayList<Integer>>();
    edges.add(new ArrayList<Integer>(Arrays.asList(1, 2, 1)));
    edges.add(new ArrayList<Integer>(Arrays.asList(2, 3, 4)));
    edges.add(new ArrayList<Integer>(Arrays.asList(1, 4, 3)));
    edges.add(new ArrayList<Integer>(Arrays.asList(4, 3, 2)));
    edges.add(new ArrayList<Integer>(Arrays.asList(1, 3, 10)));
    System.out.println(solve(4, edges));
  }
  
}
