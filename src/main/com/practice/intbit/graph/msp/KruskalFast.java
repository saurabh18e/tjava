package com.practice.intbit.graph.msp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class KruskalFast {
  
  public int solve(int A, ArrayList<ArrayList<Integer>> B) {
    PriorityQueue<ArrayList<Integer>> heap = new PriorityQueue<>(10, new Comparator<ArrayList<Integer>>(){
      public int compare(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        return list1.get(2) - list2.get(2);
      }
    });
    heap.addAll(B);
  
    UnionFind uf = new UnionFind(A);
    int cost = 0;
    
    while (!heap.isEmpty()) {
      ArrayList<Integer> list = heap.poll();
      
      int id1 = list.get(0);
      int id2 = list.get(1);
      
      if (!uf.sameSet(id1, id2)) {
        uf.union(id1, id2);
        cost += list.get(2);
      }
    }
    
    return cost;
  }
  
}
