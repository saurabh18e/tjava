package com.practice.intbit.graph.msp;

public class UnionFind {
  
  private int[] parent = null;
  
  public UnionFind(int n) {
    parent = new int[n+1];
    for (int i = 1; i <= n; i++) {
      parent[i] = i;
    }
  }
  
  public boolean sameSet(int id1, int id2) {
    return find(id1) == find(id2);
  }
  
  public void union(int from, int to) {
    int id1 = find(from);
    int id2 = find(to);
    if (id1 != id2) {
      parent[id1] = id2;
    }
  }
  
  public int find(int id) {
    while (id != parent[id]) {
      parent[id] = parent[parent[id]]; // path compression
      id = parent[id];
    }
    return id;
  }
  
}
