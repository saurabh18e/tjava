package com.practice.intbit.graph.msp;

import com.practice.intbit.graph.Edge;
import com.practice.intbit.graph.Graph;
import com.practice.intbit.graph.Vertex;

import java.util.*;
import java.util.stream.Collectors;

public class KruskalBasic {

  public static List<Edge> kruskal(Graph g) {
    HashMap<Integer, HashSet<Vertex>> sets = new HashMap<>();
    for(Vertex v: g.vertices) {
      HashSet<Vertex> hs = new HashSet<>(Collections.singleton(v));
      sets.put(v.id, hs);
    }
    
    List<Edge> edges = g.edges();
    edges.sort(new Comparator<Edge>() {
      @Override
      public int compare(Edge e1, Edge e2) {
        return (e1.weight - e2.weight);
      }
    });
    
    List<Edge> msp = new ArrayList<>();
    for (Edge e: edges) {
      HashSet<Vertex> sourceSet = sets.get(e.destinationId);
      HashSet<Vertex> destinationSet = sets.get(e.destinationId);
      if (sourceSet != destinationSet) {
        msp.add(e);
        sourceSet.addAll(destinationSet);
        for (Vertex vertex: destinationSet) {
          sets.put(vertex.id, sourceSet);
        }
      }
    }
    return edges;
  }
  
}
