package com.practice.intbit.graph;

import java.util.ArrayList;
import java.util.Arrays;

public class WordInMatrix {
  
  private int[][] neighbours = {
      {-1, 0},
      {0, -1},
      {0, 1},
      {1, 0},
  };
  
  private class Cell {
    char ch;
    int islandNum = -1;
    
    public Cell(char ch) {
      this.ch = ch;
    }
  }
  
  public int exist(ArrayList<String> A, String B) {
    int rows = A.size();
    int columns = A.get(0).length();
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (visit(A, i, j, rows, columns, B, 0, B.length()-1)) {
          return 1;
        }
      }
    }
    return 0;
  }
  
  private boolean visit(ArrayList<String> matrix, int i, int j, int rows, int columns, String pattern, int pStart, int pEnd) {
    if (pStart > pEnd) {
      return true;
    }
    if (matrix.get(i).charAt(j) != pattern.charAt(pStart)) {
      return false;
    }
    if (pStart == pEnd) {
      return true;
    }
    
    for (int[] neighbour : neighbours) {
      int x = i + neighbour[0];
      int y = j + neighbour[1];
      boolean isSafe = (x >= 0 && y >= 0 && x < rows && y < columns);
      if (isSafe && visit(matrix, x, y, rows, columns, pattern, pStart+1, pEnd)) {
          return true;
      }
    }
    return false;
  }
  
  public static void main(String[] args) {
    WordInMatrix main = new WordInMatrix();
    ArrayList<String> dict = new ArrayList<>(Arrays.asList(
        "ABCE",
        "SFCS",
        "ADEE"));
    System.out.println(main.exist(dict, "ABCCED"));
    System.out.println(main.exist(dict, "ABCD"));
  }
  
}
