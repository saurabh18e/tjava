package com.practice.intbit.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class Flip01 {
  
  public static ArrayList<Integer> flip(String A) {
    int maxSoFar=0, maxStart=-1, maxEnd=-1;
    int currentSum=0, currentStart=0, currentEnd=-1;
    
    int n=A.length();
    for (int i=0; i<n; i++) {
      int indexVal = (A.charAt(i) == '0') ? 1 : -1;
      currentSum += indexVal;
      currentEnd = i;
      
      if (currentSum < indexVal) {
        currentSum = indexVal;
        currentStart = i;
      }
      
      if (currentSum > maxSoFar) {
        maxSoFar = currentSum;
        maxStart = currentStart;
        maxEnd = currentEnd;
      }
    }
    
    if (maxStart == -1) {
      return new ArrayList<>();
    } else {
      return new ArrayList<>(Arrays.asList(maxStart+1, maxEnd+1));
    }
  }
  
  public static void main(String[] args) {
    System.out.println(flip("011011001"));
  }
  
}
