package com.practice.intbit.arrays;

import java.util.List;

import static com.practice.util.Util.list;

public class MaxSumSubArray {
  
  public static int maxSubArray(final List<Integer> A) {
    if (A == null || A.size()==0) {
      return Integer.MIN_VALUE;
    }
    
    int maxSoFar=A.get(0), maxStart=0, maxEnd=0;
    int currentSum=A.get(0), currentStart=0, currentEnd=0;
    
    for (int i=1; i<A.size(); i++) {
      currentSum += A.get(i);
      currentEnd = i;
      
      if (currentSum < A.get(i)) {
        currentSum=A.get(i); currentStart=i; currentEnd=i;
      }
      
      if (currentSum > maxSoFar) {
        maxSoFar = currentSum;
        maxStart = currentStart;
        maxEnd = currentEnd;
      }
    }
    
    return maxSoFar;
  }
  
  public static void main(String[] args) {
    // System.out.println(maxSubArray(list(-2,1,-3,4,-1,2,1,-5,4)));
    System.out.println(maxSubArray(list(1, 2, 3)));
  }
}
