package com.practice.intbit.arrays;

import static com.practice.util.Util.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddOne {
  
  public static ArrayList<Integer> plusOne(ArrayList<Integer> A) {
    if (A == null) {
      A = new ArrayList<>();
    }
    if (A.isEmpty()) {
      A.add(1);
      return A;
    }
    
    int n=A.size();
    int carry=1, sum=0;
    
    for (int i=n-1; i>=0; i--) {
      sum = A.get(i) + carry;
      A.set(i, sum % 10);
      carry = sum/10;
    }
    
    if (carry!=0) {
      ArrayList<Integer> sumList = new ArrayList<Integer>();
      sumList.add(carry);
      sumList.addAll(A);
      A = sumList;
    }
    
    while(A.get(0) == 0) {
      A.remove(0);
    }
    return A;
  }
  
  public static void main(String[] args) {
    System.out.println(plusOne(list(9, 9, 9)));
    System.out.println(plusOne(list(0, 1, 2, 3)));
    System.out.println(plusOne(list(0)));
    System.out.println(plusOne(list(null)));
  }
}
