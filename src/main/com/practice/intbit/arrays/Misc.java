package com.practice.intbit.arrays;

import java.util.ArrayList;

public class Misc {
  
  public static int maxArr(ArrayList<Integer> A) {
    int maxDiff = Integer.MIN_VALUE;
    
    if (A == null || A.isEmpty()) {
      return maxDiff;
    }
    
    for (int i=0; i<A.size(); i++) {
      for (int j=0; j<A.size(); j++) {
        int currentDiff = Math.abs(A.get(i) - A.get(j)) + Math.abs(i - j);
        maxDiff = Math.max(maxDiff, currentDiff);
      }
    }
    return maxDiff;
  }
  
}
