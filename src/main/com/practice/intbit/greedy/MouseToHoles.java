package com.practice.intbit.greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MouseToHoles {
  
  public int mice(ArrayList<Integer> A, ArrayList<Integer> B) {
    Collections.sort(A);
    Collections.sort(B);
    
    int minTime = 0;
    for (int i=0; i<A.size(); i++) {
      minTime = Math.max(minTime, Math.abs(A.get(i) - B.get(i)));
    }
    return minTime;
  }
  
}
