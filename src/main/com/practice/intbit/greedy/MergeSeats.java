package com.practice.intbit.greedy;

public class MergeSeats {
  
  public static int seats(String A) {
    int n = A.length();
    int persons = 0;
    int firstPerson = -1;
    for (int i = 0; i < n; i++) {
      if (A.charAt(i) == 'x') {
        persons++;
        if (firstPerson == -1)
          firstPerson = i;
      }
    }
    
    int moves = 0;
    int personsOnLeft = 1;
    for (int i = firstPerson; i > 0 && i < n; ) {
      
      int nextPerson = -1;
      for (int j = i + 1; j < n; j++) {
        nextPerson = j;
        if (A.charAt(j) == 'x') {
          break;
        }
      }
      
      if (nextPerson > 0 && nextPerson < n) {
        int vacantSeats = (nextPerson - i - 1);
        int personsOnRight = (persons - personsOnLeft);
        moves += vacantSeats * (personsOnLeft < personsOnRight ? personsOnLeft : personsOnRight);
        moves = moves % 10000003;
        personsOnLeft++;
      }
      i = (nextPerson != -1) ? nextPerson : n;
    }
    return moves % 10000003;
  }
  
  public static void main(String[] args) {
    System.out.println(seats(".x.x..x.."));
  }
}
