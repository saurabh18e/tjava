package com.practice.intbit.greedy;

import java.util.ArrayList;

import static java.lang.Integer.*;

public class Max3P {
  
  public static int maxp3(ArrayList<Integer> A) {
    int max1 = MIN_VALUE, max2 = MIN_VALUE, max3 = MIN_VALUE;
    int min1 = MAX_VALUE, min2 = MAX_VALUE;
    for (int a : A) {
      
      if (a > max1) {
        max2 = max1;
        max3 = max2;
        max1 = a;
      } else if (a > max2) {
        max3 = max2;
        max2 = a;
      } else if (a > max3) {
        max3 = a;
      }
      
      if (a < min1) {
        min2 = min1;
        min1 = a;
      } else if (a < min2) {
        min2 = a;
      }
      
    }
  
    return Math.max(max1 * max2 * max3, max1 * min1 * min2);
  }
}
