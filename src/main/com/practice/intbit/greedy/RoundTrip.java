package com.practice.intbit.greedy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class RoundTrip {
  
  public static int canCompleteCircuit(final List<Integer> gas, final List<Integer> cost) {
    int currentFuel = 0;
    int stationFuel = 0;
    int totalFuel = 0;
    int start = 0;
    
    for(int i = 0; i < gas.size(); i++){
      stationFuel = gas.get(i) - cost.get(i);
      totalFuel += stationFuel;
      currentFuel += stationFuel;
      
      if(currentFuel < stationFuel) {
        currentFuel = stationFuel;
        start = i;
      }
    }
    
    return totalFuel >= 0 ? start : -1;
  }
  
  public static int canCompleteCircuit2(final List<Integer> A, final List<Integer> B) {
  
    List<Integer> result = new ArrayList<>();
    result.add(A.get(0) - B.get(0));
    for (int i=1; i<A.size(); i++) {
      result.add(A.get(i) - B.get(i));
    }
    
    for (int i=0; i<result.size(); i++) {
      if (checkCircle(result, i)) {
        return i;
      }
    }
    
    return -1;
  }
  
  private static boolean checkCircle(List<Integer> result, int i) {
    int n=result.size();
    int netsum=0;
    for (int j=0; j<n; j++) {
      netsum = netsum + result.get((i+j)%n);
      if (netsum < 0) {
        return false;
      }
    }
    return true;
  }
  
  public static void main(String[] args) {
    System.out.println(canCompleteCircuit(
        Arrays.asList(2, 3, 4, 4, 2, 1, 2, 8, 5),
        Arrays.asList(3, 5, 3, 6, 2, 2, 1, 6, 2)));
  }
  
}
