package com.practice.intbit.greedy;

import java.util.ArrayList;
import java.util.Arrays;

public class FibSum {
  
  /*
    - ref:
      - https://www.interviewbit.com/problems/sum-of-fibonacci-numbers/
      - https://www.careercup.com/question?id=5090409931866112
      
      - length of fib series till n is 0(lg n)
   */
  
  public static int fibsum(int n) {
    ArrayList<Integer> fibSeries = new ArrayList<>(Arrays.asList(1, 2));
    while (fibSeries.get(fibSeries.size()-1) < n) {
      fibSeries.add(fibSeries.get(fibSeries.size()-2) + fibSeries.get(fibSeries.size()-1));
    }
    int count=0;
    while (n>0) {
      if (n >= fibSeries.get(fibSeries.size()-1)) {
        count++;
        n -= fibSeries.get(fibSeries.size()-1);
        if (n==0) {
          break;
        }
      } else {
        fibSeries.remove(fibSeries.size()-1);
      }
    }
    return count;
  }
  
  public static void main(String[] args) {
    System.out.println(fibsum(0));
    System.out.println(fibsum(1));
    System.out.println(fibsum(2));
    System.out.println(fibsum(3));
    System.out.println(fibsum(10));
  }
}
