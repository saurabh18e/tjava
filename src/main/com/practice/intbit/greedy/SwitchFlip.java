package com.practice.intbit.greedy;

import java.util.ArrayList;
import java.util.Arrays;

public class SwitchFlip {
  
  public static int bulbs(ArrayList<Integer> A) {
    int flipCount = 0;
    for (int a : A) {
      if (a == 0) {
        flipCount += (flipCount%2 == 1) ? 0 : 1;
      } else {
        flipCount += (flipCount%2 == 0) ? 0 : 1;
      }
    }
    return flipCount;
  }
  
  public static void main(String[] args) {
    System.out.println(bulbs(new ArrayList<>(Arrays.asList(0, 1, 0, 1))));
  }
}
