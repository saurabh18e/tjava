package com.practice.intbit.greedy;

import java.util.Arrays;
import java.util.List;

public class Majority {
  
  public static int majorityElement(final List<Integer> A) {
    int majority = A.get(0);
    int majorityCount = 1;
    
    for (int i=1; i<A.size(); i++) {
      int a = A.get(i);
      if (a == majority) {
        majorityCount++;
      } else {
        majorityCount--;
      }
      
      if (majorityCount == 0) {
        majority=a;
        majorityCount=1;
      }
    }
    
    majorityCount= 0;
    for (int a: A) {
      if (majority == a) {
        majorityCount++;
      }
    }
    
    return (majorityCount >= A.size()/2) ? majority : -1;
  }
  
  public static void main(String[] args) {
    System.out.println(majorityElement(Arrays.asList(2, 2, 1)));
  }
}
