package com.practice.intbit.hash.string;

import java.util.*;
import java.util.Map.Entry;

public class StringConcatCheck {
  
  public ArrayList<Integer> findSubstring(String str, final List<String> dict) {
    int n=str.length();
    int m=dict.size();
    int k=dict.get(0).length();
    
    HashMap<String, Integer> map=new HashMap<>();
    for (int i=0; i<m; i++) {
      Integer count=map.get(dict.get(i));
      count = (count==null)?1:(count+1);
      map.put(dict.get(i), count);
    }
    
    ArrayList<Integer> output=new ArrayList<Integer>();
    for(int i=0; i<=n-m*k; i++) {
      HashMap<String, Integer> mapC=(HashMap<String, Integer>) map.clone();
      for(int j=0; j<m; j++) {
        String s = str.substring(i+j*k, i+(j+1)*k);
        Integer count=mapC.get(s);
        if (count == null) {
          break;
        }
        mapC.put(s, count-1);
      }
      boolean found=mapC.values().stream().allMatch(c -> c==0);
      if (found) {
        output.add(i);
      }
    }
    
    return output;
  }
  
  public static void main(String[] args) {
    StringConcatCheck main=new StringConcatCheck();
    System.out.println(main.findSubstring("aaaaaaaaaaaaaaaaaa", Arrays.asList("aa", "aa", "aa")));
  }
}
