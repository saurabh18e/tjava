package com.practice.intbit.hash.string;

import java.util.Arrays;

import static com.practice.util.Util.list;

public class StringWindow {
  
  public String minWindow(String A, String B) {
    if (A==null || B==null || A.length() < B.length()) {
      return "";
    }
  
    int[] charMap2=new int[256];
    Arrays.fill(charMap2, 0);
    for (char ch: B.toCharArray()) {
      charMap2[ch] += 1;
    }
    int[] charMap1=charMap2.clone();
    
    int minStart=-1;
    int minEnd=-1;
    for (int i=0, j=0; j<A.length(); ) {
      System.out.println("ghoom1: " + i + " " + j);
      System.out.println(list(charMap2));
      boolean found=false;
      while(!found && j<A.length()) {
        char ch=A.charAt(j);
        charMap2[ch] = Math.max(0, charMap2[ch]-1);
        found = checkCover(charMap2);
        if(!found){
          j++;
        }
      }
      if (found) {
        while(i<j && (charMap1[A.charAt(i)]==0)) {
          i++;
        }
        if (minStart==-1 || (j-i < minEnd-minStart)) {
          System.out.println("updating: " + i + " " + j);
          minStart = i;
          minEnd = j;
        }
        
        charMap2[A.charAt(i)]++;
        i++;
      }
    }
    System.out.println(minStart + " " + minEnd);
    return (minStart==-1)? "" : A.substring(minStart, minEnd+1);
  }
  
  private boolean checkCover(int[] charMap) {
    return Arrays.stream(charMap).allMatch(c -> c==0);
  }
  
  public static void main(String[] args) {
    StringWindow main=new StringWindow();
    System.out.println(main.minWindow("ADOBECODEBANC", "ABC"));
  }
}
