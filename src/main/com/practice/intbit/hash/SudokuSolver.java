package com.practice.intbit.hash;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class SudokuSolver {
  
  private class Cell {
    int val;
    HashSet<Integer> hs;
    public Cell(int val) {
      this.val = val;
      this.hs = new HashSet<>();
    }
  }
  
  int n=9;
  Cell[][] matrix;
  HashSet<Integer>[] rowHashes;
  HashSet<Integer>[] colHashes;
  HashSet<Integer>[] subSqHashes;
  
  public int isValidSudoku(final List<String> A) {
    initSudoku(A);
    solveSudoku();
    return 1;
  }
  
  private int solveSudoku() {
    // try to fill each cell
    int fillStatus = fillSudoku();
    if (fillStatus == -1) {
      return -1;
    }
    
    // fillSudokuWithBackTrack();
    return 1;
  }
  
  private int fillSudoku() {
    int updated=0;
    boolean repeat=false;
    do {
      for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
          if (matrix[i][j].val == -1) {
            if (matrix[i][j].hs.size()==0) {
              return -1; // invalid sudoku
            } else if (matrix[i][j].hs.size()==1) {
              int val = matrix[i][j].hs.stream().findAny().get();
              fill(i, j, val);
              updated=1;
              repeat=true;
            }
          }
        }
      }
    } while (repeat);
    return updated;
  }
  
  private void fill(int i, int j, int val) {
    for (int i1=0; i1<n; i1++) {
      matrix[i1][j].hs.remove(val);
    }
    for (int j1=0; j1<n; j1++) {
      matrix[i][j1].hs.remove(val);
    }
    matrix[i][j].val=val;
  }
  
  private void initSudoku(List<String> list) {
    List<Integer> nums = new ArrayList<>();
    for (int i=0; i<n; i++) {nums.add(i+1);}
    
    rowHashes = new HashSet[n];
    colHashes = new HashSet[n];
    subSqHashes = new HashSet[n];
    for (int i=0; i<n; i++) {
      rowHashes[i] = new HashSet<>(nums);
      colHashes[i] = new HashSet<>(nums);
      subSqHashes[i] = new HashSet<>(nums);
    }
    
    matrix = new Cell[n][n];
    for (int i=0; i<n; i++) {
      String row = list.get(i);
      for (int j=0; j<n; j++) {
        int ch = (row.charAt(j) == '.' ? 0 : (row.charAt(j)-'0'));
        matrix[i][j] = new Cell(ch);
        if (ch != 0) {
          rowHashes[i].remove(ch);
          colHashes[j].remove(ch);
          subSqHashes[subSqIndex(i, j)].remove(ch);
        }
      }
    }
    for (int i=0; i<n; i++) {
      for (int j=0; j<n; j++) {
        matrix[i][j].hs = findHashIntersection(rowHashes[i], colHashes[j], subSqHashes[subSqIndex(i, j)]);
      }
    }
  }
  
  private HashSet<Integer> findHashIntersection(HashSet<Integer> hs1, HashSet<Integer> hs2, HashSet<Integer> hs3) {
    HashSet<Integer> hs = new HashSet<>();
    for (Integer i : hs1) {
      if (hs2.contains(i) && hs3.contains(i)) {
        hs.add(i);
      }
    }
    return hs;
  }
  
  private int subSqIndex(int i, int j) {
    return 3*(i/3)+j/3;
  }
  
  public static void main(String[] args) {
    ArrayList<String> rows = new ArrayList<>(Arrays.asList(
        "53..7....", "6..195...", ".98....6.", "8...6...3", "4..8.3..1", "7...2...6", ".6....28.", "...419..5", "....8..79"));
    SudokuSolver main=new SudokuSolver();
    main.isValidSudoku(rows);
  }
}
