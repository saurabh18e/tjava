package com.practice.intbit.hash.sumdiff;

import java.util.*;

public class FourSum {
  
  public ArrayList<ArrayList<Integer>> fourSum(ArrayList<Integer> A, int B) {
    int n=A.size();
    A.sort(new Comparator<Integer>() {
      @Override public int compare(Integer o1, Integer o2) {
        return o1-o2;
      }
    });
  
    ArrayList<ArrayList<Integer>> output = new ArrayList<ArrayList<Integer>>();
    if (A.size() < 4) {
      return output;
    }
    for (int i=0; i<n-3;i++) {
      for (int j=i+1; j<n-2;j++) {
        for (int k=j+1, l=n-1; k<l;) {
          long sum = ((long)A.get(i) + (long)A.get(j) + (long)A.get(k) + (long)A.get(l));
          if (B==sum) {
            ArrayList<Integer> quad= new ArrayList<>();
            quad.add(A.get(i));quad.add(A.get(j));quad.add(A.get(k));quad.add(A.get(l));
            output.add(quad);
            while(k<l && A.get(l-1) == A.get(l)) { l--; }; l--;
            while(k<l && A.get(k+1).equals(A.get(k))) { k++; }; k++;
          } else if (sum>B) {
            while(k<l && A.get(l-1) == A.get(l)) { l--; }; l--;
          } else {
            while(k<l && A.get(k+1).equals(A.get(k))) { k++;}; k++;
          }
        }
        while(j<n-2 && A.get(j) == A.get(j+1)) { j++; }
      }
      while(i<n-3 && A.get(i) == A.get(i+1)) { i++; }
    }
    return output;
  }
  
  public ArrayList<ArrayList<Integer>> fourSum2(ArrayList<Integer> A, int target) {
    Collections.sort(A);
    Integer[] num = new Integer[A.size()];
    num = A.toArray(num);
    HashSet<ArrayList<Integer>> hashSet = new HashSet<ArrayList<Integer>>();
    ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
    
    for (int i = 0; i < num.length; i++) {
      for (int j = i + 1; j < num.length; j++) {
        int k = j + 1;
        int l = num.length - 1;
        
        while (k < l) {
          int sum = num[i] + num[j] + num[k] + num[l];
          
          if (sum > target) {
            l--;
          } else if (sum < target) {
            k++;
          } else if (sum == target) {
            ArrayList<Integer> temp = new ArrayList<Integer>();
            temp.add(num[i]);
            temp.add(num[j]);
            temp.add(num[k]);
            temp.add(num[l]);
            
            if (!hashSet.contains(temp)) {
              hashSet.add(temp);
              result.add(temp);
            }
            
            k++;
            l--;
          }
        }
      }
    }
    
    return result;
  }

  public static void main(String[] args) {
    FourSum main=new FourSum();
    main.fourSum(new ArrayList<>(Arrays.asList(23, 20, 0, 21, 3, 38, 35, -6, 2, 5, 4, 21)), 29)
        .forEach(list -> System.out.println(list));
  }
}
