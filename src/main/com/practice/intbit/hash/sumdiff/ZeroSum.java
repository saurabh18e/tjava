package com.practice.intbit.hash.sumdiff;

import java.util.ArrayList;
import java.util.HashMap;

import static com.practice.util.Util.list;

public class ZeroSum {
  
  public ArrayList<Integer> lszero(ArrayList<Integer> A) {
    HashMap<Integer, Integer> hs=new HashMap<>();
    int oStart=-1;
    int oEnd=-1;
    int sum=0;
    int n=A.size();
    
    for (int i=0;i<n;i++) {
      sum += A.get(i);
      if (sum == 0) {
        if (oStart == -1 || (oEnd-oStart+1 < i+1)) {
          oStart=0;
          oEnd=i;
        }
      } else {
        Integer i1 = hs.get(sum);
        if (i1==null) {
          hs.put(sum, i);
        } else {
          if (oStart == -1 || (oEnd-oStart+1 < i-i1)) {
            oStart=i1+1;
            oEnd=i;
          }
        }
      }
    }
    if (oStart==-1) {
      return new ArrayList<>();
    } else {
      return new ArrayList<>(A.subList(oStart, oEnd+1));
    }
  }
  
  public ArrayList<Integer> lszero1(ArrayList<Integer> A) {
    int n=A.size();
    int[] sums=new int[n];
    
    int sum=0;
    for (int i=0;i<n;i++) {
      sum+=A.get(i);
      sums[i] = sum;
    }
    
    for (int i=0;i<n;i++) {
      for (int j=i;j<n;j++) {
        if ((i==0 && sums[j]==0) || (i>0 && sums[j] == sums[i-1])) {
          return new ArrayList<>(A.subList(i, j+1));
        }
      }
    }
    return new ArrayList<>();
  }
  
  public static void main(String[] args) {
    ZeroSum main=new ZeroSum();
    System.out.println(main.lszero1(list(-19, 8, 2, -8, 19, 2, -5, 5, -2, -23)));
    System.out.println(main.lszero(list(-19, 8, 2, -8, 19, 2, -5, 5, -2, -23)));
  }
}
