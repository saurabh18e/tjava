package com.practice.intbit.hash.sumdiff;

import java.util.*;

import static com.practice.util.Util.list;

public class FourSum2 {
  
  public ArrayList<Integer> equal(ArrayList<Integer> A) {
    int n=A.size();
    ArrayList<Integer> output=new ArrayList<Integer>();
    HashMap<Integer, List<Integer>> map=new HashMap<>();
    for (int i=0; i<n; i++) {
      List<Integer> list = map.get(A.get(i));
      if (list==null) {
        list=new ArrayList<>();
        map.put(A.get(i), list);
      }
      list.add(i);
    }
    
    for (int i=0;i<n-3;i++) {
      for (int j=i+1;j<n-2;j++) {
        for (int k=0; k<n-1; k++) {
          if (k==i || k==j) {
            continue;
          }
          int diff=A.get(i) + A.get(j) - A.get(k);
          List<Integer> list = map.get(diff);
          if (list!=null) {
            for (int l: list) {
              if (l != i && l!=j && l!=k) {
                output.addAll(Arrays.asList(i, j, k, l));
                return output;
              }
            }
          }
        }
      }
    }
    
    return output;
  }
  
  public static void main(String[] args) {
    FourSum2 main=new FourSum2();
    System.out.println(main.equal(list(1, 1, 1, 1, 1, 1)));
    System.out.println(main.equal(list(3, 4, 7, 1, 2, 9, 8)));
    System.out.println(main.equal(list(1, 3, 3, 3, 3, 2, 2)));
  }
}
