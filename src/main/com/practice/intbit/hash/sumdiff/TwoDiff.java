package com.practice.intbit.hash.sumdiff;

import java.util.*;

public class TwoDiff {
  
  public int diffPossible(final List<Integer> A, int B) {
    HashSet<Integer> set = new HashSet<>();
    for (int i=0; i<A.size(); i++) {
      // ith number is left of the two numbers or right of the two numbers
      if (set.contains((A.get(i)-B)) || set.contains((B + A.get(i)))) {
        return 1;
      }
      set.add(A.get(i));
    }
    return 0;
  }
  
  public int diffPossible2(final List<Integer> A, int B) {
    int n=A.size();
    for (int i=0;i<n;i++) {
      for (int j=i;j<n;j++) {
        if (A.get(i)-A.get(j) == B || A.get(j)-A.get(i) == B) {
          System.out.println(A.get(i) + " " + A.get(j));
        }
      }
    }
    return 0;
  }
  
  public static void main(String[] args) {
    TwoDiff main=new TwoDiff();
    List<Integer> arr1=Arrays.asList(77, 28, 19, 21, 67, 15, 53, 25, 82, 52, 8, 94, 50, 30, 37, 39, 9, 43, 35, 48, 82, 53, 16, 20, 13, 95, 18, 67,
        77, 12, 93, 0);
    main.diffPossible2(arr1, 53);
    System.out.println(main.diffPossible(arr1, 53));
    
    System.out.println();
    
    List<Integer> arr2=Arrays.asList(34, 63, 64, 38, 65, 83, 50, 44, 18, 34, 71, 80, 22, 28, 20, 96, 33, 70, 0, 25, 64, 96, 18, 2, 53, 100, 24, 47, 98, 69, 60, 55, 8, 38, 72, 94, 18, 68, 0, 53, 18, 30, 86, 55, 13, 93, 15, 43, 73, 68, 29);
    main.diffPossible2(arr2, 97);
    System.out.println(main.diffPossible(arr2, 97));
  }
}
