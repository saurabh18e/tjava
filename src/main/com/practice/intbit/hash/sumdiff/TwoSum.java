package com.practice.intbit.hash.sumdiff;

import java.util.*;

public class TwoSum {
  
  /*
    - find first pair with given sum in an unsorted array
    - ref: https://www.interviewbit.com/problems/2-sum/
   */
  
  public ArrayList<Integer> twoSum(final List<Integer> A, int B) {
    HashMap<Integer, Integer> map = new HashMap<>();
    for (int i=0; i<A.size(); i++) {
      Integer j=map.get(B-A.get(i));
      if (j==null) {
        if (!map.containsKey(A.get(i))) {
          map.put(A.get(i), i);
        }
      } else {
        return new ArrayList<>(Arrays.asList(j+1, i+1));
      }
    }
    return new ArrayList<>();
  }
  
}
