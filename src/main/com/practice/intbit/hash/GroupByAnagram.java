package com.practice.intbit.hash;

import java.util.*;

public class GroupByAnagram {
  
  public ArrayList<ArrayList<Integer>> anagrams(final List<String> A) {
    HashMap<String, ArrayList<Integer>> map = new HashMap<>();
    for (int i=0; i <A.size(); i++) {
      String a=A.get(i);
      if (a==null || a.isEmpty()) {
        continue;
      }
      
      String sig=signature(a);
      ArrayList<Integer> matches = map.get(sig);
      if (matches == null) {
        matches = new ArrayList<>();
        map.put(sig, matches);
      }
      matches.add(i+1);
    }
    return new ArrayList<>(map.values());
  }
  
  private String signature(String a) {
    char[] chars = a.toCharArray();
    Arrays.sort(chars);
    return new String(chars);
  }
  
  public static void main(String[] args) {
    GroupByAnagram main=new GroupByAnagram();
    System.out.println(main.anagrams(Arrays.asList("cat", "dog", "god", "tca")));
  }
}
