package com.practice.intbit.hash;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class SudokuValidity {
  
  private int n=9;
  private int[][] matrix;
  private HashSet<Integer>[] rowHashes;
  private HashSet<Integer>[] colHashes;
  private HashSet<Integer>[] subSqHashes;
  
  public int isValidSudoku(final List<String> A) {
    return initSudoku(A) ? 1 : 0;
  }
  
  private boolean initSudoku(List<String> list) {
    rowHashes = new HashSet[n];
    colHashes = new HashSet[n];
    subSqHashes = new HashSet[n];
    for (int i=0; i<n; i++) {
      rowHashes[i] = new HashSet<>();
      colHashes[i] = new HashSet<>();
      subSqHashes[i] = new HashSet<>();
    }
    
    matrix = new int[n][n];
    for (int i=0; i<n; i++) {
      String row = list.get(i);
      for (int j=0; j<n; j++) {
        int ch = (row.charAt(j) == '.' ? 0 : (row.charAt(j)-'0'));
        matrix[i][j] = ch;
        if (ch != 0) {
          if (rowHashes[i].contains(ch) || colHashes[j].contains(ch) || subSqHashes[subSqIndex(i, j)].contains(ch)) {
            return false;
          }
          rowHashes[i].add(ch);
          colHashes[j].add(ch);
          subSqHashes[subSqIndex(i, j)].add(ch);
        }
      }
    }
    return true;
  }
  
  private int subSqIndex(int i, int j) {
    return 3*(i/3)+j/3;
  }
  
  public static void main(String[] args) {
    ArrayList<String> rows = new ArrayList<>(Arrays.asList(
        "53..7....", "6..195...", ".98....6.", "8...6...3", "4..8.3..1", "7...2...6", ".6....28.", "...419..5", "....8..79"));
    SudokuValidity main=new SudokuValidity();
    System.out.println(main.isValidSudoku(rows));
  }
}
