package com.practice.intbit.hash;

import java.util.ArrayList;
import java.util.HashSet;

public class ColorfulNum {
  
  public int colorful(int A) {
    HashSet<Integer> hs=new HashSet<>();
    ArrayList<Integer> arr=new ArrayList<>();
    while(A != 0) {
      int digit = A%10;
      arr.add(digit);
      A=A/10;
    }
    int n=arr.size();
    for (int i=0;i<n;i++) {
      int prod=1;
      for(int j=i;j<n;j++) {
        prod=prod*arr.get(j);
        if (hs.contains(prod)) {
          return 0;
        } else {
          hs.add(prod);
        }
      }
    }
    return 1;
  }
  
  public static void main(String[] args) {
    ColorfulNum main=new ColorfulNum();
  
    System.out.println(main.colorful(1234));
    System.out.println(main.colorful(3245));
    System.out.println(main.colorful(3235));
    System.out.println(main.colorful(12));
    
  }
}
