package com.practice.util;

public class JavaHacks {
    
    /*
        - array to list and vice-versa
        - https://stackoverflow.com/questions/30122439/converting-array-to-list
        
        - compare
            - Comparator -> compare
            - Camparable -> compareTo
     */
    
    /*
        hashcode implementation: Objects.hasCode(a, b, c, ...)
     */
    
    /*
        Arrays API
        ------------
        - Arrays.toList
     */
    
    /*
        Collections API
        ----------------
        - Collections.emptyList()
     */
    
    /*
        List API
        ----------
        - subList
        - toArray
        - list init: https://stackoverflow.com/questions/13395114/how-to-initialize-liststring-object-in-java
     */
    
    /*
        Stack and Queue
        ----------------
        LinkedList is internally Doubly linked list
        LinkedList implements Deque and hence is good to use for both Stack and Queue
        Deque<Integer> stack = new LinkedList<>();
        
        PriorityQueue: add, remove
        Deque as stack: push, pop, peek
        
     */
    
    /*
        String
        -------
        toCharArray
        
     */
}
