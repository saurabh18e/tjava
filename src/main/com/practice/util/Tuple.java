package com.practice.util;

public class Tuple<F, S> {
    public F first;
    public S second;

    public Tuple(F first, S second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "Tuple[first=" + first + ", second=" + second + "]";
    }
}
