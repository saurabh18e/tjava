package com.practice.util;

import java.util.ArrayList;
import java.util.List;

public class Util {
    
    public static Integer[] toIntegerArray(List<Integer> list) {
        int i = 0;
        Integer[] arr = new Integer[list.size()];
        for (Integer e : list) {
            arr[i++] = e;
        }
        return arr;
    }
    public static int[] toIntArray(List<Integer> list) {
        int i = 0;
        int[] arr = new int[list.size()];
        for (Integer e : list) {
            arr[i++] = e;
        }
        return arr;
    }
    public static int[] toIntArray(Integer[] list) {
        int i = 0;
        int[] arr = new int[list.length];
        for (Integer e : list) {
            arr[i++] = e;
        }
        return arr;
    }
    public static String[] toStringArray(Integer[] list) {
        int i = 0;
        String[] arr = new String[list.length];
        for (Integer e : list) {
            arr[i++] = e + "";
        }
        return arr;
    }
    
    public static void printArr(int[] arr) {
        System.out.println(list(arr));
    }
    
    public static ArrayList<Integer> list(int... arr) {
        if (arr == null) {
            return null;
        }
        
        int n = arr.length;
        ArrayList<Integer> arrList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            arrList.add(arr[i]);
        }
        return arrList;
    }
    
    public static void swap(int[] arr, int i, int j) {
        if (i != j) {
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }
    }
}
